include .make/base.mk

PROJECT = ska-low-jupyter

########################################
# PYTHON
########################################

include .make/python.mk

PYTHON_LINE_LENGTH = 88
NOTEBOOK_LINT_TARGET = notebooks

# python and notebook linting rules are in the pyproject.toml file


########################################
# DOCS
########################################

DOCS_SPHINXOPTS=-W -n --keep-going

docs-pre-build:
	poetry config virtualenvs.create false
	poetry install --with docs



########################################
# OCI
########################################

export DOCKER_BUILDKIT=1

include .make/oci.mk

# This should work, but I don't see the caching behaviour in practice.
OCI_BUILD_ADDITIONAL_ARGS = --cache-to type=inline --cache-from $(CI_REGISTRY_IMAGE)/$(OCI_IMAGE):$(VERSION)
OCI_IMAGE_BUILD_CONTEXT = $(PWD)

oci-pre-build-all:
	apt-get update --yes && apt-get install --yes docker-buildx-plugin
	docker buildx install

-include PrivateRules.mak


########################################################################
# K8S
########################################################################

include .make/k8s.mk
include .make/raw.mk

JUNITXML_REPORT_PATH ?= build/reports/functional-tests.xml
CUCUMBER_JSON_PATH ?= build/reports/cucumber.json
JSON_REPORT_PATH ?= build/reports/report.json
BDD_HTML_REPORT_PATH ?= build/reports/report.html

PYTHON_VARS_AFTER_PYTEST = -v $(PYTEST_MARKERS) \
    --junitxml=$(JUNITXML_REPORT_PATH) \
    --cucumberjson=$(CUCUMBER_JSON_PATH) \
	--json-report --json-report-file=$(JSON_REPORT_PATH) \
	--bdd-report=$(BDD_HTML_REPORT_PATH) \
	--log-disable=kubernetes.client.rest --log-disable papermill --log-disable blib2to3.pgen2.driver --log-disable traitlets --log-disable asyncio

K8S_CHART="ska-low-tests"

########################################################################
# Xray publish
########################################################################

include .make/xray.mk

xray-post-publish:
	echo "XRAY_POST_PUBLISH"
	poetry config virtualenvs.create false
	poetry install
	xray-enrich-test-execution \
	 --jira-url  "https://jira.skatelescope.org" \
	 --project-key  XTP \
	 --repo-name  "ska-low-tests" \
	 --html-report  "https://ska-telescope.gitlab.io/-/ska-low-tests/-/jobs/$(TEST_JOB_ID)/artifacts/build/reports/report.html" \
	 --test-docs  "tests/docs/index.md" \
	 --commit-sha "$(CI_COMMIT_SHA)"

