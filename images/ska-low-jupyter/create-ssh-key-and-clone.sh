#!/usr/bin/env bash

set -ex

# Create an SSH key if no $HOME/.ssh directory exists.
# Check for the directory, not the file in case the user added a differently-named key.
if [ ! -d "${HOME}/.ssh" ] ; then
  mkdir "${HOME}/.ssh"
  ssh-keygen -t ed25519 \
      -C "JupyterLab key for ${JUPYTERHUB_USER}" \
      -q \
      -N "" \
      -f "${HOME}/.ssh/id_ed25519"
  ssh-keyscan -t ed25519 -H gitlab.com >> "${HOME}/.ssh/known_hosts"
fi

# Kubernetes sets group-write permissions on mount, which ssh doesn't like
chmod -vR 700 "${HOME}/.ssh"

# Clone the repos in SKA_JH_CLONE_REPOS and install them in the image Python environment
for REPO_URL in ${SKA_JH_CLONE_REPOS}
do
  # Split REPO_URL on "@"
  IFS=@ read -r REPO_URL BRANCH_NAME <<< "${REPO_URL}"

  # Work out where we the repository should exist on the filesystem
  REPO_NAME_GIT=$(basename "${REPO_URL}")
  REPO_NAME=${REPO_NAME_GIT%%.*}
  REPO_ROOT=${HOME}/${REPO_NAME}

  # Clone the repo if it doesn't exist
  if [ ! -d "${REPO_ROOT}" ] ; then
      git clone ${BRANCH_NAME:+--branch ${BRANCH_NAME}} "${REPO_URL}" "${REPO_ROOT}"
  fi
  pushd "${REPO_ROOT}"

  # We need a key to push, but want to be able to pull without one, via https.
  # Derive the SSH-based repository name (git@gitlab.com:ska-telescope/...)
  # but use it as the push URL only.
  REPO_URL_SSH=$(sed -r 's#^https?://(.+)/ska-telescope/#git@\1:ska-telescope/#g' <<< "${REPO_URL}")
  if [ -f ".git/config.lock" ] ; then rm .git/config.lock; fi
  git remote set-url origin "${REPO_URL}" || true
  git remote set-url origin --push "${REPO_URL_SSH}" || true

  # If there are no conflicting local changes, pull the latest in this branch.
  # This is mostly handy on main, where things like uncommitted cell output are
  # both common and likely to be cleanly rebaseable.
  if git pull --rebase --autostash; then
    # the rebase part succeeded, but applying the stash might not have,
    # so if there are any unmerged files, roll back via reset/stash pop
    git diff --exit-code --name-only --diff-filter=U || (git reset --hard '@{1}' && git stash pop)
  else
    # this can fail if the checked-out branch had been deleted upstream
    git rebase --abort || true
  fi

  # install the local package! We don't install deps - they should already be in the image,
  # and installing them can cause user image spawn to time out if the delta is too large.
  (poetry config virtualenvs.create false && poetry install --only-root) || true

  popd
done

# The kernel won't work in Jupyter until this is run - presumably some setup happening
itango3 --debug -c "exit()"

echo "Completed SKA user pre-start script"
