============
Introduction
============

The following sections document the process developers should follow to improve and maintain the ska-low-tests repository.


.. _contributing:

===========================================
Contributing to the ska-low-test repository
===========================================

To contribute to the ska-low-tests repository, you should first create a Jira issue describing your work and the acceptance criteria.
This ticket is used in the name of your branch and your commit message as described in the `SKA developer docs <https://developer.skatelescope.org/en/latest/howto/create_branch.html#create-branch>`_.
Once you have completed your changes and the tests pass, you can create a `merge request <https://developer.skatelescope.org/en/latest/explanation/merge-request.html>`_ and assign it to a reviewer.
The Vulcan team will review your changes and merge them into the main branch if they are approved.

=======
Testing
=======

There are three major ways that we run tests:

1. *Manually*: Run the notebooks by going through the jupyter notebook cells, copy the result notebook to a separate directory and manually write the results to either Jira Xray or Jama.
2. *run_notebooks*: Run the notebooks using the ``run_notebooks`` script which wraps ``papermill``.
3. *pytest BDD*: Run the tests using Behavior Driven Development (BDD) test descriptions and ``pytest``. Currently only used for the operational notebooks tests.

.. _sft_notebooks:

SFT Notebooks
-------------

The Short Functional Tests (SFTs) are located in ``notebooks/SFT`` and are run per station when it is first being integrated or after maintenance to ensure it is working correctly.
The results of these tests are stored in ``/home/jovyan/shared/test_executions`` and uploaded to Jira Xray under the `Station SFTs Test Plan <https://jira.skatelescope.org/browse/XTP-31002>`_.

The SFT notebooks are currently run either manually, using the ``run_notebooks`` command or using a gitlab runner (which uses the ``run_notebooks`` command).
When running the notebooks in this way, notebook outputs can be read by subsequent notebooks that are run as part of the same ``run_notebooks`` command.
For example, the frequency sweep notebook will store the path to the captured data in an environment variable that is then used by the closure and calibration SFT notebooks.
Similarly, the calibration SFT stores the path to calibration solutions that can be used by the sun drift SFT.


You can use ``run_notebooks -h`` to explain the command arguments.
Some of the most useful ones are:

- **--station-name**: the name of the station being tested
- **-p**: Can be called multiple times to pass parameter key pairs to the notebook. For example: ``-p CALIBRATION_TYPE SUN_FAST -p CAPTURED_DATA_PATH /data/path``.
- **--early-stop**: don't run subsequent notebooks if one has an error

The following are some examples of how to run the notebooks:

Run frequency sweep and calibration SFT:

.. code-block:: bash

    run_notebooks --station-name s10-3 --early-stop -p CALIBRATION_TYPE SUN_FAST notebooks/SFT/03*/*.ipynb notebooks/SFT/06*/*.ipynb

For more information on running the calibration SFT see :ref:`calibration`.

If you have already run the frequency sweep SFT or a manual frequency sweep, you can use this existing data in the calibration SFT using the ``CAPTURED_DATA_PATH`` parameter like so:

.. code-block:: bash

    run_notebooks --station-name s10-3 -p CALIBRATION_TYPE SUN_FAST -p CAPTURED_DATA_PATH /home/jovyan/shared/test_executions/03.FrequencySweep/s10-3_2024-11-24_155157/captured_data notebooks/SFT/06*/*.ipynb

In a similar way you can use the existing data source and the ``CAPTURED_DATA_PATH`` parameter for the closure SFTs:

.. code-block:: bash

    run_notebooks --station-name s10-3 -p CAPTURED_DATA_PATH /home/jovyan/shared/test_executions/03.FrequencySweep/s10-3_2024-11-24_155157/captured_data notebooks/SFT/04*/*.ipynb

.. note::

    If the ``run_notebooks`` command should is run outside of the base ``ska-low-tests`` directory, then you will need to specify absolute paths to the notebooks that you want to run. For example ``notebooks/SFT/06*/*.ipynb`` would need to be changed to ``/home/jovyan/ska-low-tests/notebooks/SFT/06*/*.ipynb``.
    For this reason, it is recommended to run ``cd ~/ska-low-tests`` before using the ``run_notebooks`` script.

The SFTs can also be launched from the ska-low-tests gitlab pipeline runners if you have at least "Developer" level access to the repository.
You can launch the manual pipeline from any commit, but the best practice is normally to run it off the most recent main commit.
To do this go to the `ska-low-tests <https://gitlab.com/ska-telescope/aiv/ska-low-tests>`_ repository and click on the pipeline status button:

.. image:: ../figures/select_main_commit_pipeline.png
    :alt: select_main_commit_pipeline
    :width: 800px
    :align: center

Then click on the text "sft-notebooks-test" and NOT the play button (this will launch the pipeline without parameters):

.. image:: ../figures/click_on_manual_job.png
    :alt: click_on_manual_job
    :width: 800px
    :align: center

Then you can add the parameters in the "Variables" section.
You must include the following variables:

- **STATION**: the name of the station being tested
- **NOTEBOOKS**: the notebooks to run
- **PARAMETERS**: the parameters to pass to the notebooks

For example:

.. code-block:: bash

    STATION: s9-2
    NOTEBOOKS: notebooks/SFT/04*/*.ipynb notebooks/SFT/05*/*.ipynb notebooks/SFT/06*/*.ipynb
    PARAMETERS: --early-stop -p CAPTURED_DATA_PATH path/to/data -p CALIBRATION_TYPE SUN

Which looks like this:

.. image:: ../figures/run_manual_job.png
    :alt: run_manual_job
    :width: 800px
    :align: center

Then click run and you can watch the progress of the job and even relaunch it with updated parameters if needed.


Operational Notebooks
---------------------

The operational notebooks are located in ``notebooks/operations`` and are testing using ``pytest`` and Behavior Driven Design (BDD).
The pipeline automatically runs a 2 am if the ITF is free and uploads the results to Jira Xray under the `Station Operations Test Plan <https://jira.skatelescope.org/browse/XTP-62364>`_.

The tests are described in the ``tests/features/*feature`` files using the `Gerkin syntax <https://cucumber.io/docs/gherkin/reference>`_.
These feature files are compiled into `markdown descriptions <https://gitlab.com/ska-telescope/ska-low-tests/-/tree/main/tests/docs?ref_type=heads>`_ using the `ska-ser-xray <https://gitlab.com/ska-telescope/ska-ser-xray>`_ ``generate-steps-docs`` command.
The docs describe the `high level features <https://gitlab.com/ska-telescope/ska-low-tests/-/tree/main/tests/docs/features/tests/features?ref_type=heads>`_ and the `low level steps <https://gitlab.com/ska-telescope/ska-low-tests/-/tree/main/tests/docs/steps/tests/functional?ref_type=heads>`_ (which can help finding the code that executes each step).
The ``bdd-test-docs-check`` gitlab pipeline job checks if these feature files are up to date, if they are not the must run the command:

.. code-block:: bash

    generate-steps-docs tests/ tests/docs

and commit the changes.

The tests are written following `this guide <https://confluence.skatelescope.org/pages/viewpage.action?spaceKey=SE&title=How+to+implement+BDD+tests>`_.
The majority of the tests are run by using ``papermill`` to execute operational notebooks with different parameters and then checking the results.
The tests can be run manually in the JupyterHub terminal using the following command:

.. code-block:: bash

    poetry run pytest -m operations

This tests is run as part of the gitlab pipeline job "itf-notebooks-test" which use the ``pytest-bdd`` module to create a ``cucumber.json`` file, the ``pytest-bdd-report`` module to create a html report to describe the results of the test.
The results are uploaded to Jira xray using ``ska-ser-xray`` repository's ``xray-upload`` command to create a detailed execution report with links to the relevant artifacts.
See `XTP-71802 <https://jira.skatelescope.org/browse/XTP-71802>`_ for an example of the results.

If any changes to the notebooks are made, the tests should be run as part of the gitlab pipeline.
The tests only run manually so go to the pipelines and click start/play on the job named "itf-calendar-check".
This job will use the ``is_calendar_booked`` script to check if the ITF calendar is booked and if it is not it will run the operations tests in the "itf-notebooks-test" job.



Integration Notebooks
---------------------

The integration notebooks are located in ``notebooks/integration`` and ``notebooks/ITF/integration`` and are only run manually.
Their results are written to Jama and copies of the executed notebooks pushed to the `ska-low-test-artefacts <https://gitlab.com/ska-telescope/aiv/ska-low-test-artefacts>`_ repository.


Unit tests
----------

There are a limited number of unit tests for the `aiv_utils` module which can be run using:

.. code-block:: bash

    pytest -m unit



===========================
ska-low-software Deployment
===========================

Vulcan uses the ``ska-low-software`` repository to deploy the SKA-Low software to the production clusters (North MCCS and South Pawsey) and to the Low-ITF.
This repository contains the helmfile and helm charts that can use the telmodel data to deploy the required software to the kubernetes clusters.

The ``ska-low-software`` repository has a ``README.md`` which explains `how to deploy the software <https://gitlab.com/ska-telescope/ska-low-software#installation>`_.


.. _grafana_and_eda:

=======================================
Grafana Dashboards and the EDA database
=======================================

To monitor current and historical data of the SKA-Low telescope's state, we use Grafana dashboards.
Grafana simply queries a database and presents the data graphically in a dashboard so it can be easily understood.
This database is populated continuously by recording the attributes of Tango devices that we consider important.
The following sections will explain how to use and improve the database and the dashboards.


The Database (EDA)
------------------

The Engineering Data Archive (EDA) is a database that stores the historical data of the SKA-Low telescope.
The EDA is a PostgreSQL database that is deployed on the SKA-Low (north and south) and the ITF.
You can access the EDA database using python (in JupyterHub) or through database tools like `pgAdmin <https://www.pgadmin.org/download/>`_.


Accessing the EDA using JupyterHub
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To query the EDA database through JupyterHub, log into the the cluster of the EDA you wish to query (you can access the North MCCS EDA from the North MCCS JupyterHub).
Then, you can use the following code to query the database:

.. code-block:: python

    import os
    import pandas as pd

    # Check you have the right environment variables to give you access permissions
    print("\n".join(f"{k} = {v}" for k, v in os.environ.items() if k.startswith("PG")))

    # Use pandas to make the query and display the results
    df = pd.read_sql_query(
        """
        SELECT
            data_time,
            member,
            value_r
        FROM
            scalar_devstring
        where
            quality = 0
            and name = 'tileprogrammingstate'
            and member in ('s9-2-tpm01','s9-2-tpm02','s9-2-tpm03')
            and data_time BETWEEN '2025-01-20T02:50:55.764Z' AND '2025-01-20T02:55:55.766Z'
        order by 1;
        """,
        "postgresql+psycopg://",
        params=('a parameter', 42, ["A", "B", "C"])
    )
    display(df)
    display(df.dtypes)

To understand the schema used in the above query, see the :ref:`Database Schema <database-schema>` section.
This code and more examples can be found in the ``notebooks/examples/EDA_example.ipynb`` file in the ``ska-low-tests`` repository.
You can follow along in the examples in the notebook (which have similar explanations to this document) to learn how to query the database.


Accessing the EDA using pgAdmin
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Tools like pgAdmin can be used to query the database using SQL queries.
To connect to the EDA database using pgAdmin, you need to create a new server connection with the following details:

*Connection: Host name/address*:

- North MCCS: eda.ska-tango-archiver.svc.mccs.low.internal.skao.int
- South Pawsey: eda.ska-tango-archiver.svc.low.internal.skao.int
- ITF: eda.ska-tango-archiver.svc.lowitf.internal.skao.int

*Maintenance database*: eda

*Username*: eda_ro

*Password*: Copy the password from the ``PGPASSWORD`` environment variable in the JupyterHub terminal.

Once you have connected to the EDA database, you can run SQL queries to retrieve data from the database.
See the :ref:`Database Schema <database-schema>` section for more information on the database schema and the :ref:`Query Examples <query-examples>` section for examples of SQL queries you can run.


Archivers
^^^^^^^^^

The database must be populated with data from the Tango devices which is done by the `ska-tango-archivers <https://gitlab.com/ska-telescope/ska-tango-archiver>`_.
There are several archivers for common devices and one for each station.
The archivers are configured in the ``ska-low-software`` repository in the ``charts/ska-low/eda-config`` directories.

For example in ``charts/ska-low/eda-config/north/mccs-control.yaml`` the following snippet of code can be found:

.. code-block:: yaml

  - class: MccsController
    attributes:
      state:
        archive_period: 5000
        polling_period: 5000
      adminMode:
        archive_period: 5000
        polling_period: 5000
      healthState:
        archive_period: 5000
        polling_period: 5000
      healthReport:
        archive_period: 5000
        polling_period: 5000


This snippet of code configures the archiver for the ``MccsController`` Tango device to record the ``state``, ``adminMode``, ``healthState`` and ``healthReport`` attributes every 5000 ms (5 seconds).
If there are additional tango attributes that need to be recorded, they can be added to the ``attributes`` section with an appropriate ``archive_period`` and ``polling_period``.
Members of the Vulcan team can help to deploy changes to the archiver configuration or you can follow the "`how to deploy the software <https://gitlab.com/ska-telescope/ska-low-software#installation>`_" guide.


.. _database-schema:

Database Schema
^^^^^^^^^^^^^^^

The tables that the archivers use have the name format ``<major_data_type>_dev<minor_data_type>`` where ``<major_data_type>_`` is either scalar, array, image or conf and ``<minor_data_type>`` is either boolean, double, encoded, enum, float, long, string, etc.
So health reports for example are strings and are written to the ``array_devstring`` table and the array of 4 subrack fan speeds for the FNDH are in ``array_devdouble``.
The tables have a large number of columns but the most important ones are:

- ``data_time``: The time the data was recorded with timezone information.
- ``value_r``: The actual data recorded.
- ``member``: The name of the Tango device (e.g. ``"s9-2-sb16"``).
- ``family``: The family or type of the Tango device (e.g. ``smartbox``).
- ``domain``: The domain or namespace of the Tango device (e.g. ``low-mccs``).
- ``name``: The name or attribute of the Tango device that is recorded (e.g. ``adcpower``).

You can use these columns to filter down to only the devices and attributes you are interested in.

To work out which table is used for the attribute you are interested in, the following query can be used to list all attributes recorded for a single device and their table:

.. code-block:: sql

    SELECT
        distinct name,
        table_name
    FROM
        att_conf
    WHERE
        family = 'smartbox'
        AND member = 's9-1-sb01'

In this example we are looking for all the attributes of the ``smartbox`` family for the ``s9-1-sb01`` device, but change it to the device you are interested in.


.. _query-examples:

Query Examples
^^^^^^^^^^^^^^

You can find examples of the queries by looking at similar panels in the Grafana dashboards, clicking edit and inspecting the query.
For example the TPM tileProgrammingState query in the `SPS dashboard <https://k8s.mccs.low.internal.skao.int/grafana2/d/fc517871-e5cd-4e34-8993-7c6f417bad1c/sps>`_ is:

.. code-block::

    SELECT
        $__timeGroup(data_time, $__interval, NULL),
        member metric,
        (select val from tpm_states where name = (mode() within group (order by value_r)))
    FROM
        scalar_devstring
    where
        quality = 0
        and name = 'tileprogrammingstate'
        and member in ($tpm)
        and $__timeFilter(data_time)
    group by 1, 2
    order by 1;

Two common things to note in the queries are the ``$__timeFilter(data_time)`` and the ``$__timeGroup(data_time, $__interval, NULL)``.
The ``$__timeFilter(data_time)`` is a Grafana macro that filters the data to the time range selected in the Grafana dashboard.
The ``$__timeGroup(data_time, $__interval, NULL)`` is a Grafana macro that groups the data by the time interval selected in the Grafana dashboard, this can be found under "Query options" and is best set to the same value as the ``polling_period``.
If you run these queries in pgAdmin, you will receive an error as the tools won't know what these functions are.
To convert this into something you can use, click on "Query inspector" and click "Refresh" and it should display the query with the macros expanded, including Variables like ``$tpm``:

.. code-block:: sql

    SELECT
        time_bucket('15.000s',data_time) AS "time",
        member metric,
        (select val from tpm_states where name = (mode() within group (order by value_r)))
    FROM
        scalar_devstring
    where
        quality = 0
        and name = 'tileprogrammingstate'
        and member in ('s9-2-tpm01','s9-2-tpm02','s9-2-tpm03','s9-2-tpm04','s9-2-tpm05','s9-2-tpm06','s9-2-tpm07','s9-2-tpm08','s9-2-tpm09','s9-2-tpm10','s9-2-tpm11','s9-2-tpm12','s9-2-tpm13','s9-2-tpm14','s9-2-tpm15','s9-2-tpm16')
        and data_time BETWEEN '2025-01-20T02:50:55.764Z' AND '2025-01-20T03:50:55.766Z'
    group by 1, 2
    order by 1;

You can also use this as an example of how to replace datetime and other variables in the query.
Another common query is how we use ``unnest`` to unpack arrays, for example the TPM preADU attenuation query:

.. code-block::

    SELECT
        $__timeGroup(data_time, $__interval, NULL),
        to_char(adc_id, 'fm00') AS metric,
        mode() within group (order by adc)
    FROM
        array_devdouble,
        unnest(value_r) with ordinality as t(adc, adc_id)
    WHERE
        name = 'preadulevels'
        AND family = 'tile'
        AND member = '${tpm}'
        AND $__timeFilter(data_time)
    GROUP BY 1, 2
    ORDER BY 1

The ``unnest(value_r) with ordinality as t(adc, adc_id)`` line is used to unpack the array of values into separate rows with the ``adc_id`` column being the one based index of the array and ``adc`` the values.
The ``adc`` values can then be used to get the mode (or the average in other cases) of the values in the array with ``mode() within group (order by adc)``.
The ``adc_id`` is used in ``to_char(adc_id, 'fm00')`` to give it a leading zero and  ``AS metric`` to label the row as what Grafana will use by default to label panels.


Grafana
-------

The Grafana dashboards are used to display the data from the EDA database in a graphical format.
When logged in with admin privileges we can edit the dashboards and create new ones.
Some of the version controlled dashboards are "provisioned" which means they cannot be edited, see section :ref:`Uploading changes <uploading-changes>` on how to save these changes.
For this reason it is often best to make a copy and edit it in a separate dashboard to avoid losing changes.
If you would like the admin username and password, ask either Alex Hill or Nick Swainston from the Vulcan team for the credentials.


The following sections will explain how to create and improve the dashboards.


Creating Dashboards
^^^^^^^^^^^^^^^^^^^

When logged in with admin privileges, you can create a new dashboard by clicking on the "New" icon on the right side of the screen and selecting "New dashboard".
You can also duplicate an existing dashboard by clicking "Edit" to enter edit mode on the dashboard, then on the down arrow next to "Save dashboard" and selecting "Save as copy".

One of the first things that are useful are setting up `Variables <https://grafana.com/docs/grafana/latest/dashboards/variables/?pg=blog&plcmt=body-txt>`_ which allow you to filter the data displayed on the dashboard.
To create a new variable, click on the "Settings" icon on the right side of the screen and select "Variables".
Then click on the "Add variable" button and fill in the query which will find the variable.

A common variable is the station variable which can be created with the following query:

.. code-block::

    SELECT DISTINCT member FROM scalar_devstate WHERE family = 'spsstation' AND \$__timeFilter(data_time)

You can press "Run query" to confirm that the query is working and then "Back to list" when you are done.

Another important Grafana feature is the ability to `Repeat rows and panels <https://grafana.com/blog/2020/06/09/learn-grafana-how-to-automatically-repeat-rows-and-panels-in-dynamic-dashboards/>`_.
This allows us to repeat a row for each station (like in the `station overview dashboard <https://k8s.mccs.low.internal.skao.int/grafana2/d/07c6410a-70bd-484b-bcad-9309a770ed20/station-overview>`_) or a panel for each TPM (like in the `SPS dashboard <https://k8s.mccs.low.internal.skao.int/grafana2/d/fc517871-e5cd-4e34-8993-7c6f417bad1c/sps>`_).
This is also an important thing to understand when editing dashboard because you must edit the first row or panel to effect the rest.


Creating Panels
^^^^^^^^^^^^^^^

To create a `panel <https://grafana.com/docs/grafana/latest/panels-visualizations/panel-overview/>`_, click "Add" then "Visualization" and select the type of panel you want to create.
If you plan to make this a repeating panel, click "Repeat options" and add the variable you want to repeat on.
You can then use this variable with the ``${variable_name}`` syntax in the query to filter the data to the selected value.

At the bottom window you can edit you query but switching to "Code" mode and editing the query similar to the examples in the :ref:`Query Examples <query-examples>` section.
You can also use the "Query inspector" to see the expanded query with the variables replaced with their values and the output of your query.
You can use this to debug your query and to inform what "Value mappings" you should add to colour and edit the display of your panel.
For example, we use value mappings to convert health state indexes to the health state names and a colour to reflect the severity of the health state.


.. _uploading-changes:

Uploading changes
^^^^^^^^^^^^^^^^^

You won't be able to save any of your changes to the version controlled as Grafana marks them as "provisioned".
This is because we use a JSON that defines the dashboards in the ``ska-low-software`` repository which we deploy to the cluster so Grafana can pulls that definition.
Each of the dashboards have their own JSON in ``charts/ska-low/grafana-config`` for dashboards that only describe the North MCCS cluster devices, and ``charts/ska-low/grafana-config/south`` for dashboards that describe the South Pawsey cluster devices.

To save the dashboard, you must first copy the JSON source, which has two methods depending on if the dashboard is provisioned (already exists) or not (a new dashboard you created).
For a provisioned dashboard click "Save dashboard" then "Copy JSON to clipboard".
For a non-provisioned dashboard make sure it has been saved at least once by clicking "Save dashboard" then "Save". Then click "Share", "Export", "View JSON" and "Copy to Clipboard".


Once you have copied the JSON source, paste it to an appropriate file under ``charts/ska-low/grafana-config`` in the ``ska-low-software`` repository.
You can then follow the instructions in the `ska-low-software README <https://gitlab.com/ska-telescope/ska-low-software#installation>`_ to deploy the changes to the cluster and the :ref:`contributing <contributing>` section to create a merge request.