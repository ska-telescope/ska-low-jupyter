.. _kibana:

Kibana
------

Kibana is a web interface primarily for searching and visualizing logs.
It is useful for investigating the logs of the various Tango pods when you encounter an issue.
Currently, only the ITF cluster has Kibana enabled.

To access Kibana, you must be connected to the VPN, then navigate to the following links:
- `ITF Kibana <https://k8s.stfc.skao.int/kibana/app/logs/stream>`_
- `SKA-Low Kibana <https://k8s.low.internal.skao.int/kibana/app/logs/stream>`_

The stream will be overwhelming at first because it will display all logs from the cluster (not just pod logs).
You can add several filters to the be more specific about the logs you want to see.

You often will want to see the logs for pod of the Tango DeviceProxy that gave you an error.
You can use the device name as a filter to only see logs from that pod.
You can get the device name with the ``.dev_name()`` command:

.. code-block:: python

    from aiv_utils.low_utils import get_sps_devices
    STATION_NAME = "itf1"
    %env TANGO_HOST=tango-databaseds.sut-mccs:10000
    station, subracks, tpms, (daq, _) = get_sps_devices(STATION_NAME)

    station.dev_name()

which outputs

..  code-block:: output

    'low-mccs/spsstation/itf1'

You can add a filter to Kibana to only see logs from this pod by adding the following filter in the filter bar:
``ska_tags_field.tango-device  : low-mccs/spsstation/itf1``

You may also need to change the time range to see logs from the time you encountered the error.
You can do this by clicking the calendar icon in the top right and selecting the time range you are interested in.

You can use more than one filter at a time by separating them with an " and " in the filter bar.
The following filters may be useful:

- ``ska_severity : "ERROR"`` Only look for error messages (filter our warnings and info messages)
- ``ska.application : kubernetes`` Only look for logs from the kubernetes pods (ignore cluster logs)
- ``kubernetes.namespace : sut-mccs`` Only look for pods of a certain namespace if you're only interested in MCCS devices for example
- ``message : *Failed to connect*`` Find logs that contain the phrase "Failed to connect" or any other phrase you are interested in
- ``kubernetes.pod.name : spsstations-spsstation-itf1-0`` Find logs from a specific pod
- ``ska_tags_field.tango-device  : low-mccs/spsstation/itf1`` As mentioned above, find logs for a Tango device's pod
