=======================================
Using SKA-Low Tests Notebooks and Tools
=======================================

When the JupyterHub server is launched, the `SKA-Low Tests <https://gitlab.com/ska-telescope/ska-low-tests>`__
repository is cloned and fetched in the directory ``/home/jovyan/ska-low-tests``.
The most important two directories in this repository are:

- ``ska-low-tests/notebooks/operations``: This directory contains the notebooks that are used for common operations such as initialisation of stations and acquiring data.
- ``ska-low-tests/src/aiv_utils``: This directory contains the helper functions that are used in a lot of the notebooks.


.. _operation-notebooks:

Operation Notebooks
-------------------

The operation notebooks cover common station operations such as initialisation, data acquisition, and calibration.
Each notebook is internally documented and should explain each step they take.
All of these notebooks are intended to run on the North MCCS cluster with the exception of the ``TMCObservation.ipynb`` notebook which should be run on the South Pawsey cluster.
Because the ITF is not two distinct clusters like SKA-Low, all the operation notebooks can be run on the ITF.
We will now go through the purpose of each operation notebook in the ``ska-low-tests/notebooks/operations`` to make it clear how they should work together.


``TMCObservation.ipynb``: This notebook uses the high level TMC software to observe with one or more stations.
This is the only operation notebook that needs to be run on the South Pawsey cluster (or the ITF).
The output data is stored in the ``/home/jovyan/sdp-data`` directory. For imaging observations, the data will be visibilities in the measurement set format; for tied-array beam observations, they will be voltage data in dada format.
For more detail on how to use this notebook and how to calibrate before the observation, see the :ref:`tmc-observation` and :ref:`cal_tmc_observation` sections.

.. _FieldNodeOn:

``FieldNodeOn.ipynb``: This notebook will attempt to get a field node into the state where the FNDH and all smartboxes are initialised and all FEM ports on.
This notebook should be run after any maintenance that might required the field node to be turned off or if output data is not as expected (none or too small to look like a bandpass).

.. _Initialise:

``Initialise.ipynb``: This notebook will bring the station up to the point where TPMs have started acquisition.
If the station has just been powered on, this notebook will have to be run after ``FieldNodeOn.ipynb``.
If all the TPMs are in a synchronised programming state (can be confirmed on the
`station overview dashboard <https://k8s.low.internal.skao.int/grafana2/d/07c6410a-70bd-484b-bcad-9309a770ed20/station-overview?orgId=1&from=now-15m&to=now&timezone=Australia%2FPerth&var-station=s8-1&var-station=s8-6&var-station=s9-2&var-station=s10-3&refresh=30s>`__),
then this notebook does not need to be run.
If the station is behaving unexpectedly (such as data files not being written) then reinitialising the station is a good first debug step.

``Equalise.ipynb``: This notebook will attempt to equalise the station by adjusting the preadu levels so that each antenna emits similar amounts of power.
The ``Initialise.ipynb`` now applies a default preadu levels from the TelModel, so this notebook is only necessary if the bandpasses are starting to noticeably drift.

``ManageStationCalibrationLoop.ipynb``: This notebook is used for starting and stopping the station calibrator device.
The calibration loop constantly acquires correlation data and calculates calibration solutions from it.
If you want to run a daq based observation, you will need to stop the calibrator before starting your observation.
TMC-based observations are unaffected by the station calibrator loop running.

``FrequencySweep.ipynb`` and ``FrequencySweepMultiple.ipynb``: These notebooks acquire correlator data from one station (``FrequencySweep.ipynb``), or one or more station (``FrequencySweepMultiple.ipynb``).
``FrequencySweep.ipynb`` is used for a single station and creates some extra plots to help with debugging, internally it runs ``FrequencySweepMultiple.ipynb`` where the list of stations contains a single station name.
``FrequencySweepMultiple.ipynb`` is used for multiple stations which start simultaneously.
Such data are used to calculate calibration solutions.

``06.Calibration.ipynb`` \*: This notebooks is not technically an operation notebook, but it is used to generate calibration solutions for the stations.
The notebook can be found in the ``ska-low-tests/notebooks/SFT/06.Calibration`` directory and is further explained in the :ref:`calibration` section.

``AcquireBeamformed.ipynb``: This notebook acquires beamformed data for a station.
For details on how to calibrate the beamformed data, see the :ref:`cal_acquire_beamformed` section.

``AcquireRaw.ipynb``: This notebook acquires a burst of raw voltage data for a station.

``AcquireChannelised.ipynb``: This notebook acquires a burst of channelised data for a station.


.. _tmc-observation:

TMC Observation Notebook
------------------------

The ``TMCObservation.ipynb`` notebook uses higher level software than other operational notebooks and is what will be use most often for science observations.
The following will describe how to decide on the parameters for the observation and how to run the notebook.

The ``STATION_NAMES`` variable is a list of stations you want to use, e.g. ``STATION_NAMES = ['s8-6', 's9-2']``.
You should check that the stations are available and you have booked them on the `calendar <https://confluence.skatelescope.org/display/TDT/Coordination+of+Planned+Activities+on+Site>`__
and they are in a good state by checking the `station overview dashboard <https://k8s.low.internal.skao.int/grafana2/d/07c6410a-70bd-484b-bcad-9309a770ed20/station-overview>`__.


To choose what type of observation the scan will be, set ``OBSERVING_MODE`` to either:

- ``VIS``: Which will output imaging visibilities and is explained in the :ref:`vis_observations` section.
- ``PST``: Which will output high time resolution voltages and is explained in the :ref:`pst_observations` section.

The calibrations for each station can be set using the ``CALIBRATION`` parameter.
It should be set as a list of strings of either ``"FROM_STORE"``, ``"UNITY"`` or ``"ZERO"`` for each station.
Leaving this blank will default to using the golden calibration solutions from the store.
For more information on the calibration, see the :ref:`cal_tmc_observation` section.

The final paramater to set is ``OBSERVATIONS`` which is a list of dictionaries that define the observations.
Each dictionary should have the following:

- ``"Observation name"``: A string that describes the observation. There is no parsing/validation on the string but it is good practice to use the official source name as it will be recorded in the metadata.
- ``"RA (deg)"``: The Right Ascension of the source in degrees.
- ``"Declination (deg)"``: The Declination of the source in degrees.
- ``"Duration (min)"``: The duration of the observation in minutes. This should be a minimum of 0.5 minutes (30 seconds) for PST observations.
- ``"Start frequency channel ID"``: The lowest coarse frequency channel ID for the observation. This must be an even number and can be multiplied by 0.78125 to get the frequency in MHz (e.g. channel 64 is 50 MHz).
- ``"Bandwidth channels"``: The number of coarse frequency channels to use for the observation.
  This must be a multiple of 8 and is currently recommended to be 24 or less for ``VIS`` observations so only a single SDP receiver is used which gives more consistent results
  (see `SKB-701 <https://jira.skatelescope.org/browse/SKB-701>`__ and `SKB-695 <https://jira.skatelescope.org/browse/SKB-695>`__).
  It is recommended to set it to 8 for ``PST`` observations so the filesystem can keep up with disk writes, and packets are not dropped.

You can set multiple observations by listing multiple dictionaries in the ``OBSERVATIONS`` list.
The notebook will ``ReleaseResources()`` and ``AssignResources()`` between each observation (until `SKB-701 <https://jira.skatelescope.org/browse/SKB-701>`__ is resolved).
For example, two observations could look like this:

.. code-block:: python

    OBSERVATIONS = [
        {
            "Observation name": "CasA",
            "RA (deg)": 350.85,
            "Declination (deg)": 58.81,
            "Duration (min)": 5,
            "Start frequency channel ID": 64,
            "Bandwidth channels": 8,
        },
        {
            "Observation name": "CygnusA",
            "RA (deg)": 299.868,
            "Declination (deg)": 40.7339,
            "Duration (min)": 5,
            "Start frequency channel ID": 64,
            "Bandwidth channels": 8,
        },
    ]


Once the parameters are set you can either use ``Shift + Enter`` for each cell to run the whole notebook
or click \ |ff| in the toolbar to restart the kernel and run all cells.
You can then use the `subarrays dashboard <https://k8s.low.internal.skao.int/grafana2/d/de6swro62hbswc/subarray>`__ to monitor the progress of the subsystems from EMPTY to IDLE to READY to SCANNING.
Once the system has got to scanning it is normally smooth sailing but you can check the `CBF connector dashboard <https://k8s.low.internal.skao.int/grafana2/d/c3f8b663-5ae0-4781-ab7b-626305189013/low-cbf-connector>`_
to confirm data is flowing (mostly only used by Vulcan for debugging) or the `SDP signal display dashboard <https://k8s.low.internal.skao.int/ska-low/signal/display/>`__ to monitor the real time visibility data.

The data will be stored in the ``/home/jovyan/sdp-data`` directory and the exact location will be printed in the notebook.
See the following sections for descriptions of the observation mode dependent data formats.


.. _pst_observations:

PST Voltage Observations
^^^^^^^^^^^^^^^^^^^^^^^^

The "PST" observations are high time resolution voltage data which can be used for pulsar observations.
The data sizes are very large so please be careful about observation length until the PST real time folding is implemented (currently waiting on PST hardware before the software can be integrated).
The data will be output to the ``/home/jovyan/sdp-data/<eb_id>/pst-low/1`` directory where ``<eb_id>`` is the execution block ID which can be found in the notebook outputs or should be the most recent directory in ``/home/jovyan/sdp-data/`` if you just observed.
The files in this directory include:

- ``scan_configuration.json``: The configuration given to the subarray nodes for the observation.
- ``ska-data-product.yaml``: A summary of the metadata of the observation. This is calculated after the observation finishes so will accurately describe the files, for example if the observation did not record data then ``t_exptime`` will be 0.
- ``data``: A directory of the raw voltage files in dada format.
- ``weights``: A directory of the weights files in dada format. These are used to adjust the data files to account for the different gains used throughout the observation.
- ``stats``: This directory contains a HDF5 file with a summary and statistics of the observation.

To process the data you must "fold" it for a pulsar using ``DSPSR`` or similar software.
To make this process easier and ensure the weights are used correctly, a script called `process_dada <https://gitlab.com/ska-telescope/ska-low-tests/-/blob/main/src/aiv_utils/scripts/process_dada.sh?ref_type=heads>`__ is provided.
You can use ``process_dada -h`` to view the help or follow this example:

.. code-block:: bash

    process_dada -p <pulsar_jname> -e <eb_id> -l <label> -d <output_base_dir>

Where:

- ``<pulsar_jname>`` is the pulsar name in J2000 format (e.g. J1939+2134).
- ``<eb_id>`` is the execution block ID.
- ``<label>`` is a label for the observation (e.g. "first_observation").
- ``<output_base_dir>`` is the base directory within which a directory in the format ``<pulsar_jname>_<eb_id>_<label>`` will be created and to store the output.

This script will fold the data and output the results to the ``<output_base_dir>/<pulsar_jname>_<eb_id>_<label>`` directory.
The png images can be viewed to confirm the quality of the observation and the archive files (``.ar``) can be used for further analysis.


.. _vis_observations:

SDP Visibility Observations
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The "VIS" observations are imaging visibilities which can be used for imaging and calibration.
The data will be output to the ``/home/jovyan/sdp-data/sdp-data/<eb_id>`/ska-sdp/<pb_id>>`` directory where ``<eb_id>`` is the execution block ID and ``<pb_id>`` is the processing block ID which can be found in the notebook outputs.
This directory will include a ``ska-data-product.yaml`` which contains (currently very limited) metadata about the observation and an ``output.scan-<N>.ms`` directory full of the measurement set for each scan in the assign resources block, where ``<N>`` is the ordinal number of the observation.
The measurement set files can be opened in CASA for imaging and calibration.
The :py:meth:`get_uv_visibility_data function <aiv_utils.uvdata_tools.get_uv_visibility_data>` can be used to load and summarise the data for further analysis in python.
For some examples of how the ``get_uv_visibility_data`` is used to plot results, see the end of the `TMC integration test notebook <https://gitlab.com/ska-telescope/ska-low-tests/-/blob/main/notebooks/integration/TC.L.AA0.5.1.2.1.4.TMC-Interfaces/TC.L.AA05.1.2.1.4.TMC-Interfaces.ipynb?ref_type=heads>`__ as an example.


Simultaneous Observations
-------------------------

The SKA-Low telescope is wonderfully flexible so under certain conditions we can do simultaneous observations of different types to make the most of the telescope.
For example, we can do a frequency sweep will doing a TMC observations with the same station.
This is because the stations have multiple "spigots" that can output data streams to different places.
These output data streams are:

- **CSP ingest**: This is main data stream that goes to the correlator. This is the data used for TMC observations and in the ``AcquireBeamformed.ipynb`` notebook.
- **LMC download**: This is an extra data stream which will be used by the calibrator but can also be used for frequency sweeps and other MCCS level observations (``FrequencySweepMultiple.ipynb`` , ``FrequencySweep.ipynb``, ``AcquireChannelised.ipynb`` and ``AcquireRaw.ipynb``).
- **LMC integrated download**: This data stream integrates data and is used to send bandpasses to the EDA to be displayed on Grafana. Not normally used for observations.

Because these data streams are separate, one user can use the CSP ingest for TMC observations while another user uses the LMC download for a frequency sweep.
As long as we are clear about what type of observation we are doing, we can put overlapping station bookings on the `calendar <https://confluence.skatelescope.org/display/TDT/Coordination+of+Planned+Activities+on+Site>`__ that use different data streams.

One current limitation with TMC is that we can only use a single subarray at a time.
This means that even though we have multiple stations, we can not currently perform separate simultaneous TMC observations.
This will hopefully be fixed in PI26.

You must also ensure that you are not using two notebooks that output data to the DAQ at the same time.
For example, you can't run ``AcquireBeamformed.ipynb`` and ``FrequencySweep.ipynb`` at the same time, even though they use different data streams, as they both output data to the DAQ.

The following sections will outline what you *should not* do when using different data streams at the same time and which notebooks use which data streams.


What Not To Do When Using Different Data Streams
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You don't want to interrupt or affect the other user's observation so here are a list of things not to do:

- Don't run ``Initialise.ipynb`` or ``FieldNodeOn.ipynb``. This will reset the station and stop an observation.
- Don't run ``Equalise.ipynb``. This won't stop an observation but will change the preadu levels and give inconsistent data.
- Don't run an observations with the same data stream. So don't run ``FrequencySweep.ipynb`` while someone is running ``AcquireChannelised.ipynb``.

If you need to do one of the above just talk to the other observer and make sure they are okay with it.


Data Streams Used By Each Notebook
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following table explains where the data comes from (data stream) and where the data goes to (DAQ or CBF) for each notebook.
This will allow you to confirm that you are not interrupting another user's observation by confirming that you are using a different data stream and not sending more than one of them to the DAQ.

.. list-table::
   :widths: 25 25 10 30
   :header-rows: 1

   * - Notebook
     - Data Stream
     - DAQ
     - Notes
   * - ``FieldNodeOn.ipynb``
     - None
     - ❌
     - Can interrupt observations
   * - ``Initialise.ipynb``
     - None
     - ❌
     - Can interrupt observations
   * - ``Equalise.ipynb``
     - None
     - ❌
     - Can affect observations
   * - ``TMCObservation.ipynb``
     - CSP ingest
     - ❌
     -
   * - ``AcquireBeamformed.ipynb``
     - CSP ingest
     - ✔
     -
   * - ``FrequencySweep.ipynb``
     - LMC download
     - ✔
     -
   * - ``FrequencySweepMultiple.ipynb``
     - LMC download
     - ✔
     -
   * - ``AcquireRaw.ipynb``
     - LMC download
     - ✔
     -
   * - ``AcquireChannelised.ipynb``
     - LMC download
     - ✔
     -


aiv_utils package
-----------------

In ``ska-low-tests/src/aiv_utils`` is a python package full of helper functions to make running these notebooks easier.
These functions are documented in the :ref:`api` section.

The most important functions are :func:`aiv_utils.low_utils.get_sps_devices` which returns the tango devices for a station
and the functions in the :ref:`metadata-module` which get metadata such as antenna positions for the station.

This package is interactively installed in your JupyterHub environment so you can make changes to the package and see
the effects in the notebooks as soon as your restart the ``Python 3 (ipykernal)`` kernel.