============
Introduction
============

The following is documentation for users of the SKA-Low system.
This documentation is intended to help users understand the system and how to interact with it.

.. include:: access.inc


.. _useful-links:

===================
Useful Link Summary
===================

Many of the following links are mentioned and explained in other sections, but for your convenience, here is a summary of useful links:

- SKA-Low North MCCS
    - `JupyterHub <https://k8s.mccs.low.internal.skao.int/jupyterhub/hub>`__
    - `Calendar (same for SKA-Low) <https://confluence.skatelescope.org/display/TDT/Coordination+of+Planned+Activities+on+Site>`__
    - `Grafana <https://k8s.mccs.low.internal.skao.int/grafana2/dashboards>`__ (deprecated as all North Grafana dashboards are also in the South Grafana)
    - `Taranta <https://k8s.mccs.low.internal.skao.int/ska-low/taranta/devices>`__
    - `Kibana (same for SKA-Low) <https://k8s.low.internal.skao.int/kibana/app/logs/stream>`__

- SKA-Low South Pawsey
    - `JupyterHub <https://k8s.low.internal.skao.int/jupyterhub/hub>`__
    - `Calendar (same for SKA-Low) <https://confluence.skatelescope.org/display/TDT/Coordination+of+Planned+Activities+on+Site>`__
    - `Grafana <https://k8s.low.internal.skao.int/grafana2/dashboards>`__
    - `Taranta <https://k8s.low.internal.skao.int/ska-low/taranta/devices>`__
    - `Kibana (same for SKA-Low) <https://k8s.low.internal.skao.int/kibana/app/logs/stream>`__
    - `Data product dashboard <http://k8s.low.internal.skao.int/ska-low/dashboard/>`__
    - `SDP signal display dashboard <https://k8s.low.internal.skao.int/ska-low/signal/display/>`_

- ITF
    - `JupyterHub <https://k8s.lowitf.internal.skao.int/jupyterhub/hub>`__
    - `Calendar <https://confluence.skatelescope.org/display/TDT/calendar/41e5ba28-6e80-4d9f-9873-c9d2f80f0f01?calendarName=Low%20ITF%20-%20TE%20%26%20SUT%20booking%20system>`__
    - `Grafana <https://k8s.lowitf.internal.skao.int/grafana2/d/fc517871-e5cd-4e34-8993-7c6f417bad1c/sps?orgId=1>`__
    - `Taranta SUT <https://k8s.lowitf.internal.skao.int/sut/taranta/devices>`__
    - `Taranta SUT MCCS <https://k8s.lowitf.internal.skao.int/sut-mccs/taranta/devices>`__
    - `Kibana <https://k8s.stfc.skao.int/kibana/app/logs/stream>`__

- Repositories
    - `SKA-Low Tests <https://gitlab.com/ska-telescope/ska-low-tests>`__
    - `SKA-Low MCCS <https://gitlab.com/ska-telescope/mccs/ska-low-mccs>`__
    - `SKA-Low MCCS SPSHW <https://gitlab.com/ska-telescope/mccs/ska-low-mccs-spshw>`__
    - `SKA-Low MCCS Calibration <https://gitlab.com/ska-telescope/mccs/ska-low-mccs-calibration>`__

- Documentation
    - `SKA term glossary <https://confluence.skatelescope.org/terms/all>`__
    - `TMC User Documentation <https://confluence.skatelescope.org/display/UD/TMC+User+Documentation>`_
    - `SKA-Low MCCS <https://developer.skao.int/projects/ska-low-mccs/en/latest/?badge=latest>`__
    - `SKA-Low MCCS SPSHW <https://developer.skao.int/projects/ska-low-mccs-spshw/en/latest/?badge=latest>`__
    - `SKA-Low MCCS Calibration <https://developer.skao.int/projects/ska-low-mccs-calibration/en/latest/?badge=latest>`__


===================
High Level Overview
===================

.. include:: telescope-components.inc

.. include:: kubernetes.inc

.. include:: tango.inc

===============================
Using and Monitoring the System
===============================

.. include:: jupyterhub.inc

.. include:: grafana.inc

.. include:: kibana.inc

.. _k9s:

k9s
---

If you have the correct permissions (see :ref:`k9s-access`),
you can use the graphical tool `k9s <https://k9scli.io/>`__ to investigate and manage a cluster.
Make sure you `install k9s <https://k9scli.io/topics/install/>`_
(and for full functionality also `install kubectl <https://kubernetes.io/docs/tasks/tools/#kubectl>`_)
Then once you have logged in with ``infra login`` and connected to the VPN you should be able to use ``k9s`` to connect to the clusters.

Once in the k9s interface, make sure you are in the correct cluster (:ref:`cluster-context`) which can be chosen using the ``:context`` command.
You may then also have to press ``0`` to see all the namespaces in the cluster.
You may then want to use ``/`` to filter the pod names to find what you are interested in.
For example I could type ``/daq`` then click enter to see all the daq pods or ``/s8-6`` to see all the pods for the s8-6 station.

Once you have found the pod you are interested in, you can click ``l`` to see the logs, ``d`` to describe the pod or ``Ctrl + k`` to kill the pod (don't do this lightly!).
Investigating the pods in this way can help you to understand what is happening in the cluster and debug any issues.


.. _kubectl:

kubectl
-------

If you want a more advanced command line tool,
you can use `kubectl <https://kubernetes.io/docs/reference/kubectl/>`__ to view and modify the cluster.
Some common commands are shown below.

.. csv-table:: kubectl commands
    :header: "Action", "Example Command Line"

    "List contexts (clusters)",   "``kubectl config get-contexts``"
    "Use a context (cluster)", "``kubectl config use-context infra:au-itf-k8s-master01-k8s``"
    "List namespaces in a cluster", "``kubectl get namespaces``"
    "List all pods in namespace", "``kubectl -n sut get pods``"
    "Show logs of a given pod in a namespace", "``kubectl -n sut logs centralnode-01-0``"

.. include:: ska_low_tests_repo.inc

==========================
Writing your own notebooks
==========================

.. include:: writing_notebooks.inc

=============
Common Issues
=============

Bellow are some of the common issues that users may encounter when using the system.
If you encounter an issue with one of the operational scripts that is not covered here, please contact the Vulcan team via their slack channel.


The Operation Notebooks Are Not Up to Date
------------------------------------------

If you encounter a bug in any of the operations notebooks, the first thing you should do is make sure they are up to date with the latest version.
To do this, make sure you are on the main branch (``git checkout main``) and then run ``git pull``.
You may have to restore your notebooks to their initial state (``git restore file_i_changed.ipynb``) before pulling the latest changes.
You can also ``git stash`` your changes before pulling and then ``git stash pop`` to reapply them after pulling.


Telescope is Not On
-------------------

After maintenance that requires the field node to be turned off or if output data is not as expected (none or too small to look like a bandpass),
the field node may need to be turned on.
You can think of this as turning on the telescope.
To do this, run the ``FieldNodeOn.ipynb`` and then the ``Intialise.ipynb`` operational notebooks, making sure that ``STATION_NAME`` is set to the correct station.


Station DevError CommunicationFailed
-------------------------------------

This error is usually happens during the initialisation of the station.
It will output a message similar to the following:

.. code-block:: python

    PyTango.CommunicationFailed: DevFailed[
    DevError[
        desc = TRANSIENT CORBA system exception: TRANSIENT_CallTimedout
      origin = DeviceProxy:read_attribute
      reason = API_CorbaException
    severity = ERR]

    DevError[
        desc = Timeout (3000 mS) exceeded on device low-mccs/tile/itf1-tpm01
      origin = DeviceProxy:read_attribute
      reason = API_DeviceTimedOut
    severity = ERR]
    ]
    Stack (most recent call last):
      File "/usr/lib/python3.10/threading.py", line 973, in _bootstrap
        self._bootstrap_inner()
      File "/usr/lib/python3.10/threading.py", line 1016, in _bootstrap_inner
        self.run()
      File "/usr/lib/python3.10/threading.py", line 953, in run
        self._target(*self._args, **self._kwargs)
      File "/usr/lib/python3.10/concurrent/futures/thread.py", line 83, in _worker
        work_item.run()
      File "/usr/lib/python3.10/concurrent/futures/thread.py", line 58, in run
        result = self.fn(*self.args, **self.kwargs)
      File "/usr/local/lib/python3.10/dist-packages/ska_tango_base/executor/executor.py", line 211, in _run
        self._call_task_callback(
      File "/usr/local/lib/python3.10/dist-packages/ska_tango_base/executor/executor.py", line 228, in _call_task_callback
        task_callback(**kwargs)
      File "/usr/local/lib/python3.10/dist-packages/ska_tango_base/base/command_tracker.py", line 142, in update_command_info
        self._exception_callback(command_id, exception)
      File "/usr/local/lib/python3.10/dist-packages/ska_tango_base/base/base_device.py", line 691, in _log_command_exception
        self.logger.error(message, exc_info=exc_info, stack_info=True)

This error is usually caused by the station not being able to communicate with the device.
Sometimes the device is too busy and you just need to wait a few moments and try again.
If the error persists, it may be due to the Taranta server making too many requests which can be fixed by turning the Taranta server off.
If the Taranata servers is on you will be able to see the devices in the Taranata server (links for `ITF <https://k8s.lowitf.internal.skao.int/sut-mccs/taranta/devices>`_ and `SKA-Low <https://10.132.123.31/ska-low/taranta/devices>`_).
You can then ask one of the Vulcan team members to turn the server off (scale the ``ska-tango-tangogql-skaffold`` replicas to 0) for you.


TPM unconnected
---------------

This error is often encountered when trying to initialise a station and it fails to do so after repeated attempts.
If you check the logs for the TPMs you will see an error similar to the following:

.. code-block:: output

    1|2024-09-10T01:12:12.853Z|ERROR|Polling thread|check_communication|tile.py#2457|tango-device:low-mccs/tile/itf2-tpm02|Not able to communicate with CPLD: Cannot call function get_temperature on unconnected TPM
    1|2024-09-10T01:12:13.257Z|WARNING|Polling thread|wrapper|tile.py#66|tango-device:low-mccs/tile/itf2-tpm02|Cannot call function get_temperature on unconnected TPM

This is due to the TPM not being able to connect to its hardware.
The only currently known solution is to restart the TPM pod which you can ask one of the Vulcan team members to do for you.


========
Metadata
========

Science Operations has a need for metadata from single-station, MCCS-driven observations that is not currently included in the hdf5 files.
While there are plans to implement these metadata in coming PIs, observations continue in the meantime which require a stopgap solution.
Where possible, we have taken names directly from either `ADR-55 <https://confluence.skatelescope.org/display/SWSI/ADR-55+Definition+of+metadata+for+data+management+at+AA0.5>`_
or `ADR-49 <https://confluence.skatelescope.org/display/SWSI/ADR-49+Define+standard+units+and+some+standard+keynames+for+use+in+JSON>`_.

The metadata is explained in the following table.
The metadata is split into two categories: "Fixed/Required" and "Flexible/Optional".
The "Fixed/Required" metadata should is automatically generated as part of the Jupyter notebook and is strictly formatted.
The "Flexible/Optional" metadata is optional and the user will be prompted to input it to provide additional information about the observation.
This information will be useful for Science Operations staff for later data retrieval and analysis.


+-----------------+-------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| Metadata name   | Required?                     | Explanation                                                                                                                               |
+=================+===============================+===========================================================================================================================================+
| station_name    | ✔                             | Name of the station (e.g. s8-1)                                                                                                           |
+-----------------+-------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| observer        | ✔                             | Observer/User: as taken directly from the JupyterHub user. (e.g. riley.keel)                                                              |
+-----------------+-------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| reference_frame | ✔                             | Reference frame in which the scan's phase centre (pointing) coordinates are defined. One of "ICRS" (RA and dec), "AltAz"(alt and az and). |
+-----------------+-------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| ra              | ✔ if reference_frame == ICRS  | Right Ascension of the scan's phase centre, in degrees                                                                                    |
+-----------------+-------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| ra_str          | ✔ if reference_frame == ICRS  | Right Ascension of the scan's phase centre, in the format "hh:mm:ss.sss"                                                                  |
+-----------------+-------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| dec             | ✔ if reference_frame == ICRS  | Declination of the scan's phase centre, in degrees                                                                                        |
+-----------------+-------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| dec_str         | ✔ if reference_frame == ICRS  | Declination of the scan's phase centre, in the format "±dd:mm:ss.sss"                                                                     |
+-----------------+-------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| alt             | ✔ if reference_frame == ALTAZ | Altitude (angle above the horizon) of the scan's phase centre, in degrees                                                                 |
+-----------------+-------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| az              | ✔ if reference_frame == ALTAZ | Azimuth (clockwise angle from North) of the scan's phase centre, in degrees                                                               |
+-----------------+-------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| target_name     | ❌                            | Provide the complete object name. That is, use the pattern "catalog_name object_identifier", e.g. PSR J0835-4510.                         |
|                 |                               | Explicitly include the "J" in names based on J2000 coordinates.                                                                           |
+-----------------+-------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| intent          | ❌                            | A short description of the purpose of the observation (e.g. Pulsar timing test).                                                          |
+-----------------+-------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| notes           | ❌                            | Can be used as a longer description but due to the character limit, consider using "jira_ticket" for anything detailed.                   |
+-----------------+-------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------+
| jira_ticket     | ❌                            | An optional link to the Jira ticket which contains longer observation notes (e.g. LCO-40).                                                |
+-----------------+-------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------+

You can read the metadata with the following commands:

.. code-block:: python

    import h5py
    from pprint import pprint
    hdf5_file = h5py.File("path/to/hdf5/file.hdf5", "r")
    hdf5_metadata = hdf5_file["observation_info"].attrs
    pprint(dict(hdf5_metadata), width=1)

This would output some example metadata like the following if the :py:meth:`insert_hdf5_metadata function <aiv_utils.hdf5_tools.insert_hdf5_metadata>` was used:

.. code-block:: json

  {
    "station_name": "S8-1"
    "observer": "riley.keel"
    "reference_frame": "ICRS"
    "ra": 123.45
    "ra_str": "08:13:36.000"
    "dec": -45.00
    "dec_str": "-45:00:00.000"
    "target_name": "NGC254"
    "intent": "Checking tracking"
    "notes": "Attempt 985 to get a good track on this target"
    "jira_ticket": "LCO-40"
  }

.. include:: calibration.inc