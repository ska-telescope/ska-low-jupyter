.. _grafana_links:

Grafana
-------

`Grafana <https://grafana.com/>`_ is an open-source visualisation platform for the creation of near realtime charts and displays.
The primary source of data displayed in the SKAO Grafana is the Engineering Data Archive (EDA), for more information see the ref:`grafana_and_eda` section
To view the dashboards you must be connected to the VPN.

Low ITF
^^^^^^^

- `Low ITF Dashboards <https://k8s.lowitf.internal.skao.int/grafana2/dashboards>`_ provides links to all dashboards.

- `System Under Test (SUT) Dashboard <https://k8s.lowitf.internal.skao.int/grafana2/d/d88ccf71-f6de-4192-ace1-b997449ccd04/system-under-test>`_ high level overview of the state of TMC, CSP and CBF devices.

- `CBF Connector Display <https://k8s.lowitf.internal.skao.int/grafana2/d/c3f8b663-5ae0-4781-ab7b-626305189013/low-cbf-connector>`_ Correlator Beamformer (CBF) connector status. Useful for debugging SDP and TMC observations.

- `SPS Dashboard <https://k8s.lowitf.internal.skao.int/grafana2/d/fc517871-e5cd-4e34-8993-7c6f417bad1c/sps>`_ Signal Processing System (SPS) devices such as TPMs, subracks and the bandpass's of the antennas.

- `PASD Devices <https://k8s.lowitf.internal.skao.int/grafana2/d/c5ea2954-afd8-4435-9a0c-1918bb27c337/pasd>`_ the FNDH and Smartbox temperatures and currents.

- `Test Equipment Devices <https://k8s.lowitf.internal.skao.int/grafana2/d/c7d0c1ae-357e-4c7a-9da7-b71c2a46db0e/test-environment>`_ the noise source relay status.

SKA-Low (South Pawsey)
^^^^^^^^^^^^^^^^^^^^^^

There is an EDA for both the North MCCS and South Pawsey sites and the South Pawsey cluster can access both databases.
For this reason, all dashboards are available on the South Pawsey cluster.

- `SKA-Low South Pawsey Dashboards <https://k8s.low.internal.skao.int/grafana2/dashboards>`_ provides links to all dashboards.

- `SPS <https://k8s.low.internal.skao.int/grafana2/d/fc517871-e5cd-4e34-8993-7c6f417bad1c/sps>`_ Signal Processing System (SPS) devices such as TPMs, subracks and the bandpasses of the antennas.

- `PaSD <https://k8s.low.internal.skao.int/grafana2/d/c5ea2954-afd8-4435-9a0c-1918bb27c337/pasd>`_ the FNDH and Smartbox temperatures and currents.

- `Station Overview <https://k8s.low.internal.skao.int/grafana2/d/07c6410a-70bd-484b-bcad-9309a770ed20/station-overview>`_ High level overview of the station to see if it is ready to observe.

- `Subarrays <https://k8s.low.internal.skao.int/grafana2/d/de6swro62hbswc/subarray>`__ high level overview of the state of TMC and subarrays.

- `TMC and Subarrays <https://k8s.low.internal.skao.int/grafana2/d/d88ccf71-f6de-4192-ace1-b997449ccd04/tmc-and-subarrays>`_ and older and simpler version of `Subarrays <https://k8s.low.internal.skao.int/grafana2/d/de6swro62hbswc/subarray>`__.

- `Low CBF Connector <https://k8s.low.internal.skao.int/grafana2/d/c3f8b663-5ae0-4781-ab7b-626305189013/low-cbf-connector>`_ Correlator Beamformer (CBF) connector status. Useful for debugging SDP and TMC observations.

- `Tango Devices (North and South) <https://k8s.low.internal.skao.int/grafana2/d/f9cfe9e6-faff-42b3-8167-76a3374c6087/tango-devices>`_ The status, state and health of the tango devices. The filter window uses regex so you can use the syntax ``(onefilter|anotherfilter)`` to filter efficiently.


















