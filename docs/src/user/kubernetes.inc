Kubernetes
----------

Kubernetes is a container management system.
Functionality is divided into groups of containers ("Pods") which can be deployed
amongst many servers ("Nodes") to share the load.
The total set of nodes and pods form a "cluster".
See https://kubernetes.io for more information.

The kubernetes system is used by SKAO so that we can automatically role out new software versions of the different
components and roll back these versions if there are any issues with new releases.
It also makes the cluster more fault tolerant as if one part of the telescope fails,
then that pod can be killed and a new one will automatically be generated to take its place.


.. _cluster-context:

Clusters
^^^^^^^^

Currently, the SKAO has three Clusters:

.. csv-table::
   :header: "Cluster", "Infra Context", "Location"

   "Low ITF",   "infra:au-itf-k8s-master01-k8s", "Geraldton ITF"
   "SKA-Low North (MCCS)",  "infra:au-aa-mccs-cloud01-k8s", "AA0.5 Site tcpf"
   "SKA-Low South (Pawsey)",  "infra:au-aa-k8s-master01-k8s", "Pawsey Centre"

See the :ref:`access` section if you haven't already been given access.


Pods
^^^^

A pod is the smallest unit in the Kubernetes ecosystem.
Kubernetes uses the description of the pod to assign it resources such as storage and ram,
which container it shall use and other start up commands.
These pods are ephemeral which allows automatic restarts in the case of failure.

Pods can be grouped into `namespaces <https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/>`_ for easier management.
The logs for a given pod can be also be viewed using Kibana, k9s or kubectl (see :ref:`kibana`, :ref:`k9s` and :ref:`kubectl` sections).

For SKAO, you can think of each pod contains a docker container that contains a tango device which interacts with hardware or other tango devices.









