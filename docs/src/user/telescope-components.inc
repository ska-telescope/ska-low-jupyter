Telescope Components
--------------------

An interferometer takes signals from multiple receivers and compares them over time. By applying a `specific mathematical transformation <https://www.robots.ox.ac.uk/~az/lectures/ia/lect2.pdf>`_ to these comparisons, an image can be formed.

In SKA Low, the receivers are *stations*, which consist of a set of antennas whose signals are added together as a `phased array <https://en.wikipedia.org/wiki/Phased_array>`_ to form *station beams*.
The comparison between station beams is performed by the *Correlator and BeamFormer*, or CBF, which is a part of the *Central Signal Processor*, or CSP.
The number crunching to take the outputs from CSP and produce images and other scientific products is performed by the *Science Data Processor*, or SDP, and this whole process is orchestrated by *Telescope Monitoring and Control* software, or TMC.

..  note:: Why is it called the correlator *and beamformer* if beams are already formed in MCCS?

    Beamforming can happen at multiple levels - antenna signals are summed by the Tile Processing Modules (TPMs) to create station beams, and then those station beams *may* be summed by CBF to form extremely sensitive *tied-array beams*, which aren't useful for imaging but are great for analysing time-domain phenomena like pulsars.

So, the software subsystems in play are:

- MCCS, the Monitoring, Calibration and Control Subsystem - this is responsible for controlling the Low station hardware that captures, digitises and sums antenna signals. MCCS is also responsible for calibrating stations, and capturing engineering sample data from stations.
- CSP, the Central Signal Processor - receives station beams from site, and does further cross-station processing, either correlating or further beamforming.
- SDP, the Science Data Processor - takes products from CSP for further processing, e.g. creating images. Currently, SDP simply receives and stores visibilities from CSP.
- TMC, Telescope Management and Control - the high-level control system which coordinates all the other subsystems in order to perform a scan.

MCCS, CSP, and SDP are functional subsystems which each implement a portion of the telescope signal chain. Each of these has a **controller** and its own representation of a **subarray**, implemented as Tango devices.
The controller is responsible for turning things on and off, and for allocating resources within its subsystem to subarrays.

These functional subsystems are controlled by TMC, which also has its own representation of a subarray.

MCCS
^^^^

The Monitoring, Calibration and Control Subsystem control system is loosely hierarchical. The primary interface to a Low station is an ``MccsStation`` Tango device.
This device should represent the aggregate state of all station hardware, and provide commands to operate a station as an atomic unit. Unfortunately this abstraction is not watertight, so it's often necessary to interact with devices beneath ``MccsStation`` in the hierarchy.

The next tier of devices are ``SpsStation``, and ``FieldStation``. These correspond to the two main functional areas of the hardware under MCCS control - PaSD and SPS.

(MCCS should also theoretically monitor and control the synchronisation and timing system (SAT) and infrastructure like Power Distribution Units (PDUs) and chillers, but this functionality is immature as of September 2024.)


..  csv-table:: MCCS Tango devices for high-level control
    :header: "Tango device name", "comment"

    "MccsController", "allocates resources to subarrays, turns the array on and off"
    "MccsStation", "represents a station, both the field node and signal processing system"
    "MccsStationBeam", "a station may eventually have up to 48 beams, each of which is configured for a specific subarray beam"
    "MccsSubarray", "MCCS' internal representation of a subarray"
    "MccsSubarrayBeam", "a beam for a specific target and frequency that may be instantiated in multiple stations"


There are three devices dedicated to calibration:

..  csv-table:: MCCS Tango devices for calibration
    :header: "Tango device name", "comment"

    "StationCalibrationSolverDevice", "a service device for performing calibration calculations"
    "MccsStationCalibrator", "orchestrates calibration: acquiring sample data, calculating and applying calibration solutions"
    "MccsCalibrationStore", "stores calibrations solutions?"


PaSD
....

``FieldStation`` represents the *Power and Signal Distribution* (PaSD) system, which consists of the hardware that sits outside in the weather alongside the antennas - 24 *smartboxes* and one *field node distribution hub* (FNDH).
The science inputs to this system are the sky, and the outputs are analogue optical signals on fibres, one per antenna.

PaSD hardware and Tango devices are initialised by the :ref:`FieldNodeOn.ipynb <FieldNodeOn>`: operational notebook.

PaSD Tango devices are developed in the https://gitlab.com/ska-telescope/mccs/ska-low-mccs-pasd repository by the Wombat and MCCS teams.

.. csv-table:: MCCS Tango devices for hardware components
   :header: "Tango device name", "hardware component", "comment"

   "FieldStation", "", "An abstract device representing a field node"
   "MccsFNDH", "field node distribution hub", "sits on the edge of the mesh, and powers and controls smartboxes"
   "MccsPasdBus", "ethernet-modbus gateway", "The central point of communication with all PaSD hardware. This is the only PaSD Tango device that directly communicates with hardware."
   "MccsSmartbox", "smartbox", "contains up to 12 FEMs (front-end modules) which convert an electrical signals from antennas into optical signals on fibre"

SPS
...
``SpsStation`` represents the Signal Srocessing System (SPS) for a station.
This is high-performance dedicated computing hardware (*tile processing modules* or TPMs) that lives in climate controlled, EM-shielded structures: either remote processing facilities in the case of stations on the spiral arms, or the central processing facility in the case of stations in the core.
The TPMs take analogue optical input from the PaSD, digitise them, add them together and send the resulting station beams to CSP.

The SPS hardware and Tango devices are initialised by the :ref:`Initialise.ipynb <Initialise>`: operational notebook.

SPS Tango devices are developed in the https://gitlab.com/ska-telescope/mccs/ska-low-mccs-spshw repository by the MCCS team. Within MCCS is a specialist team who develop the signal processing firmware that runs on the TPMs.

.. csv-table:: MCCS Tango devices for hardware components
   :header: "Tango device name", "hardware component", "comment"

   "SpsStation",  "", "An aggregate device representing the set of 2 subracks and 8 TPMs in for a station."
   "MccsTile", "TPM", "Tile processing module - digitises and beamforms antenna signals"
   "MccsSubrack",   "subrack", "Each subrack distributes power and LMC network to 8 TPMs"
   "MccsDaqReceiver",   "", "A software device which receives data samples from TPMs for engineering and calibration purposes"

CSP
^^^
The Central Signal Processor is responsible for correlating or beamforming the signals from stations and performing further processing for pulsars. Unlike MCCS, there are no Tango devices at the top level - high-level control is provided by the CSP-LMC component.

The CBF is implemented on special-purpose Commercial Off the Shelf (COTS) hardware - FPGAs and programmable P4 switches, of which there are six and one respectively in AA0.5.

:ref:`PSS and PST <psspst>` are implemented in software and run on COTS server hardware.

CSP-LMC
.......

..  csv-table:: CSP-LMC Tango devices for high-level control
    :header: "Tango device name", "comment"

    "CspController", "allocates resources to subarrays, turns the array on and off"
    "CspSubarray", "CSP's internal representation of a subarray"


CBF
...

..  csv-table:: CBF Tango devices for control of the correlator and beamformer
    :header: "Tango device name", "hardware component", "comment"

    "CbfAllocator", "", "allocates CBF hardware components (i.e. FPGAs) for particular observations"
    "CbfConnector", "P4 switch", "controls the programmable P4 switch by dynamically configuring routing of station beams to FPGAs"
    "CbfProcessor", "Alveo FPGA", "programs and commands the FPGAs on which the actual correlation and beamforming take place"

.. _psspst:

PSS and PST
...........

The PulSar Search (PSS) and PulSar Timing (PST) systems are software components to process the high time resolution pulsar data.

TODO - these are so far undeployed in the ITF or in AA0.5.


SDP
^^^
Science Data Processor has no specialised hardware, and uses less Tango than other areas of the signal chain.
SDP relies Kubernetes as a part of its internal operation, using Helm to create on-demand deployments of Pods for receiving data and running scripts for observations.
Much of this is unexposed to the Tango world.

SDP manages its state in an internal etcd database.

It still provides Tango devices for its high-level control interface:

..  csv-table:: SDP Tango devices for high-level control
    :header: "Tango device name", "comment"

    "SdpController", "allocates resources to subarrays, turns the array on and off"
    "SdpSubarray", "SDP's internal representation of a subarray"


TMC
^^^
Telescope Monitoring and Control provides the highest-level Tango devices for controlling the telescope.
The main public interfaces are the ``TmcCentralNode`` and ``TmcSubarrayNode``.
Internally, TMC also has Tango devices coupled to each of the subsystems' subarray devices and to their controllers.

..  csv-table:: TMC Tango devices for top-level control
    :header: "Tango device name", "comment"

    "CentralNode", "allocates resources to subarrays, turns the array on and off"
    "SubarrayNode", "SDP's internal representation of a subarray"
    "CspMasterLeafNode", "TMC's proxy to CspController"
    "CspSubarrayLeafNode", "TMC's proxy to a CspSubarray device"
    "MccsMasterLeafNode", "TMC's proxy to MccsController"
    "MccsSubarrayLeafNode", "TMC's proxy to an MccsSubarray device"
    "SdpMasterLeafNode", "TMC's proxy to SdpController"
    "SdpSubarrayLeafNode", "TMC's proxy to an SdpSubarray device"
