JupyterHub
----------

JupyterHub is a web-based interface that allows you to run notebooks and terminal commands on the cluster.
In the following sections we will describe the directories available to you and the basics of how to use JupyterHub.


Directories
^^^^^^^^^^^

When you first open JupyterHub, you will see a list of directories in your home directory on the cluster (``/home/jovyan/``).
Some are mounted volumes that are shared with all users, such as ``daq-data``, ``eep-data``, ``sdp-data``, ``cnic-data`` and ``shared``.

The ``shared`` directory is where you can put data that you want to share with other users.
It is recommended that you make a directory with your username in the ``shared`` directory and put your data there.
The ``daq-data`` directory is where the data from the daq is stored.
The ``eep-data`` directory is where the EEP simulated data files are stored.
The ``sdp-data`` directory is where the data from SDP is stored which includes TMC observations.
The ``cnic-data`` directory is where the Customisable Network Interface Card (CNIC) simulated data files are stored.

All other directories currently in ``/home/jovyan/`` or that you create are only accessible by you.
These means that any changes you make to the operational notebooks or the aiv_utils package will not affect other users.
If you have made a change that will benefit others, see the :ref:`contributing` section for an explanation of how to contribute.


The Launcher
^^^^^^^^^^^^

If you press the blue ``+`` button in the top left of the JupyterHub interface, you will see a list of options for creating new files or terminal.
If you open a terminal, you can run shell commands as normal which is useful for running git commands
to ensure you are using the most up-to-date version of the operational notebooks and aiv_utils package.
You can also open a new notebook or python console from the launcher.
Make sure you select the ``Python 3 (ipykernal)`` option so that you have access to the aiv_utils package.


Jupyter Notebooks
^^^^^^^^^^^^^^^^^

Jupyter Notebooks are a way to run python code interactively.
They are made up of cells which can be run individually.
The default command to run a cell is ``Shift + Enter`` which you can press for each cell to run the whole notebook
or click \ |ff| in the toolbar to restart the kernel and run all cells.

If you want to keep the output of the cells, the easiest way is to copy the notebook and give it a new filename before rerunning it.
If you want to clear the outputs (before git committing changes for example) you can click "Edit" then "Clear All Outputs".

.. |ff|    unicode:: U+23E9 .. fast-forward button
