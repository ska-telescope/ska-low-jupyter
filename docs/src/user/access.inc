.. _access:

==============
Acquire Access
==============

There are two (and a half) main clusters that you can access:

- **SKA-Low**: This is the main cluster where you can use the actual stations (currently s8-1, s8-6, s9-2 and s10-3). This is actually split into two clusters:

  - **North MCCS**: The cluster located on the MRO site which controls the SPS hardware which is primarily MCCS.
    This cluster is used to launch station observations with the low level MCCS software and to turn stations on.
  - **South Pawsey**: The cluster located at Pawsey which controls the data processing and other software components (TMC, SPS and CBF).
    This cluster is used to launch observations with the high level TMC software.
- **ITF**: The integration test facility where you can use test software with generated signals

There are two ways to access the clusters:

- **JupyterHub**: This is a web-based interface that allows you to run notebooks on the cluster. This is the easiest way to get started.\
- **k9s/kubectl**: This is a terminal-based interface that allows you to look at individual pod's logs and kill them if necessary. This is more advanced and is useful for debugging.


ITF Cluster
-----------

To access the ITF cluster, submit a `system team ticket <https://jira.skatelescope.org/servicedesk/customer/portal/166>`_ to request VPN access.
You will need to install the `cisco VPN client <https://confluence.skatelescope.org/display/SIG/VPN+-+Cisco+Secure+Client>`_
on your machine and connect to the SKAO VPN before you can access the cluster.

You should then be able to access the ITF JupyterHub interface at `https://k8s.lowitf.internal.skao.int/jupyterhub/hub <https://k8s.lowitf.internal.skao.int/jupyterhub/hub>`_.
You will have to login with your SKA and GitLab credentials then click launch server.
When you launch the server for the first time, several things will be installed which may cause the launch to time out.
If this happens, try again a few times until you can see the JupyterLab interface.

If you plan to use the Tango devices, first make sure that no one else is using the ITF cluster,
then reserve time using the `calendar <https://confluence.skatelescope.org/display/TDT/calendar/41e5ba28-6e80-4d9f-9873-c9d2f80f0f01?calendarName=Low%20ITF%20-%20TE%20%26%20SUT%20booking%20system>`__.

SKA-Low MCCS Cluster
--------------------

To access the SKA-Low MCCS cluster, send a message (or email lucio.tirone@csiro.au) to Lucio Tirone to request VPN access
(by adding you to the correct `security group <https://confluence.skatelescope.org/display/SIG/Delegated+Security+Groups+-+Requesting+and+Administering+Groups>`_).

Make sure you are connected to the SKAO VPN and then you can access the SKA-Low JupyterHub interface at `https://k8s.mccs.low.internal.skao.int/jupyterhub/hub <https://k8s.mccs.low.internal.skao.int/jupyterhub/hub>`_.
You will once again have to login with your credentials and have to try a few times until you can see the JupyterLab interface.

If you plan to use the Tango devices, first make sure that no one else is using the station you plan to use by checking the
`calendar <https://confluence.skatelescope.org/display/TDT/Coordination+of+Planned+Activities+on+Site>`__.
To reserve a station follow the instruction at the bottom of the calendar page.

.. _k9s-access:

k9s/kubectl Interface
---------------------

`k9s <https://k9scli.io/>`__ is a terminal-based interface that allows you to look at individual pod's logs and kill them if necessary.
Because this has the ability to kill pods, you will only be granted access if you are a more advanced user (such as part of the Vulcan team).
To install k9s, follow the instructions on the `k9s website <https://k9scli.io/topics/install/>`_.

To access the k9s interface, you will need to be connected to the SKAO VPN and be granted access through `InfraHQ <https://confluence.skatelescope.org/display/SWSI/Compute+access+User+Guide+%3A+InfraHQ>`_.
After you have `downloaded and installed infra <https://infrahq.com/docs/download>`_, follow the `SKA instructions <https://confluence.skatelescope.org/display/SWSI/Compute+access+User+Guide+%3A+InfraHQ>`_ to login.
If you have the correct permissions, the command ``infra list`` will show you the available clusters,
which should include ``au-aa-mccs-cloud01-k8s`` for SKA-Low MCCS cluster and ``au-itf-k8s-master01-k8s`` for the ITF cluster.