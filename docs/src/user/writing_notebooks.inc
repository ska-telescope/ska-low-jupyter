Use Papermill to run operational notebooks
------------------------------------------

`Papermill <https://papermill.readthedocs.io/en/latest/index.html>`_ allows one Jupyter notebook to execute
another notebook and supply it with parameters.
This reduces replication of code.
The :ref:`operation-notebooks` have been written by Vulcan to be easily run using Papermill.

An example of calling a notebook via papermill is given here:

.. code-block:: python

   import papermill

   parameters = {"STATION_NAME": "itf-1"}

   papermill.execute_notebook("/home/jovyan/ska-low-tests/notebooks/operations/Initialise.ipynb",
                              "Initialise_local.ipynb",
                              cwd=".",
                              stdout_file=sys.stdout,
                              stderr_file=sys.stderr,
                              parameters=parameters)



When evaluated, this cell will create a copy of the notebook ``/home/jovyan/ska-low-tests/notebooks/operations/Initialise.ipynb``
and give it a new name ``Initialise_local.ipynb``.
It will be supplied a parameters dictionary which will overwrite any default parameters with the same name.
The results will be printed to the output cell in the calling notebook.
By creating local copies, test evidence can be accumulated as the local copy will contain the output cells when evaluated.

To know what parameters are exposed by a notebook click  the "Property Inspector" icon in the right sidebar
of an opened notebook and look in the "Common Tools" section.
For a cell that contains parameters the "parameters" cell tag will be checked.


Use aiv_libraries from ska-low-tests
------------------------------------

Vulcan has created python package called aiv_utils which includes helper functions which will make running notebooks easier.

Example Usage:


.. code-block:: python

   from aiv_utils.metadata import get_station_antenna_dataframe
   STATION_NAME = "s8-6"
   station_df = get_station_antenna_dataframe(STATION_NAME).sort_values(by="eep")


The aiv_utils package will already be in your python path in Jupyterhub.
See the :ref:`api` documentation for further details.

