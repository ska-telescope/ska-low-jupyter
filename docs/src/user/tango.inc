Tango
-----

`Tango <https://www.tango-controls.org/>`_ is a control system framework for physical hardware (as well
as software abstractions).
SKAO uses the python binding `PyTango <https://pytango.readthedocs.io/en/stable/>`_ to create devices representing
elements of the Telescope (such as TPMS and subarrays) as well as test equipment (e.g. an Arbritrary Waveform Generator (AWG) or a Noise Source).
After definition and deployment to a cluster, these devices can be used from scripts or Jupyter Notebooks remotely.



Tango Device Servers and Proxies
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The Tango devices are managed by a Tango Device Server which we can get ``DeviceProxy`` instances from.
This device proxy is a class that can communicate with the tango device that is being run
in a different kubernetes pod which in turn talks to hardware.

For example, on the ITF we can get a device proxy for an Arbitrary Waveform Generator (AWG) like this:

.. code-block:: python

   from tango import DeviceProxy
   trl = "tango-databaseds.test-environment:10000/low-itf/awg/1"
   awg = DeviceProxy(trl)


We used the Tango Resource Locator (`trl <https://tango-controls.readthedocs.io/projects/rfc/en/latest/16/TangoResourceLocator.html>`_)
for the AWG to get a device proxy that we interact with in Jupyter Notebooks.

The trl shows the tango database name and port, along with the device name.
In the case of SKAO, the tango database is in kubernetes which is partitioned into namespaces, so the namespace qualifier is used.
To see a list of tango device servers (pods) residing in a given namespace one can use kubectl, k9s or Taranta (see :ref:`useful-links`).
You can also directly get the list from the tango database using the following code:

.. code-block:: python

   from tango import Database
   db = Database()
   list(db.get_device_exported("*"))

The Vulcan group have made several helper functions to get some of the device proxies for you.
For example, to get the Signal Processing System (SPS) device proxies you can use:

.. code-block:: python

   from aiv_utils.low_utils import get_sps_devices
   STATION_NAME = "itf1"
   station, subracks, tpms, (daq, _) = get_sps_devices(STATION_NAME)

This an other helper functions are documented in the :ref:`api` section.

Each tango device consists of sets of :ref:`tango-attributes` and :ref:`tango-commands` that can be read and executed respectively.


.. _tango-attributes:

Attributes
^^^^^^^^^^

Attributes are `typed <https://tango-controls.readthedocs.io/projects/rfc/en/latest/9/DataTypes.html>`_
variables representing features of the device (e.g. frequency, temperature).
To see a list of attributes for a given device type: ``device.get_attribute_list()``


.. csv-table:: Attribute Operations
   :header: "Operation", "Example Code", "Explanation"

   "List available device attributes", "``awg.get_attribute_list()``", "This will work for all devices"
   "Read Attribute",   "``awg.frequency``", "Use dot notation: device.attribute"
   "Write Attribute",  "``awg.frequency = 250e6``", "Assignment of new value"
   "Put in Admin Mode", "``awg.adminMode = 1``", "When a device is in adminMode it is not communicating with the hardware"
   "Take Device out of admin mode", "``awg.adminMode = 0``", "Allows communication"


.. _tango-commands:

Commands
^^^^^^^^

Commands are operations a device can perform.

.. csv-table:: Commands
   :header: "Operation", "Example Code", "Explanation"

   "List available commands", "``awg.get_command_list()``", "This will work for all devices"
   "Execute a command", "``awg.Play()``", ""
   "Execute a command with arguments", "``car.Turn(90)``",""

Depending on the Tango Device, commands can be *synchronous* (blocking), where the user must wait for the command to finish
or *asynchronous* (long running commands) which return immediately.
The execution status of a long running command can be checked by issuing: ``device.longrunningcommandstatus``


Tango Events
^^^^^^^^^^^^

Tango devices emit `events <https://pytango.readthedocs.io/en/stable/client_api/device_proxy.html#tango.DeviceProxy.subscribe_event>`_
when attributes are changes or commands are run.
Clients can subscribe to these events and add custom event handlers.

For example, a subscription looks like:

.. code-block:: python

   subscription_id = deviceproxy.subscribe_event(attribute, tango.EventType.CHANGE_EVENT, event_handler, [])

Where ``event_handler`` is any python function

To unsubscribe you can then run:

.. code-block:: python

   deviceproxy.unsubscribe_event(subscription_id)

