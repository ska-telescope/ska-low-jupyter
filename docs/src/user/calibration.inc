.. _calibration:

===========
Calibration
===========

Using Calibration in Observations
---------------------------------

If the calibration store has been set up correctly, then each station should have a "golden" calibration solution ready for use.
For more information on MCCS calibration see the `general calibration documentation <https://developer.skao.int/projects/ska-low-mccs/en/latest/reference/calibration/index.html>`_ or the `calibration store documentation <https://developer.skao.int/projects/ska-low-mccs/en/latest/api/calibration_store/index.html>`_. The process for adding new calibration solutions to the store is described in :ref:`adding_new_cal`.

Alternatively, calibration solutions can be generated with the calibration SFT and saved as a numpy file.
There are several functions for applying calibration solutions in :py:mod:`aiv_utils.calibration_utils`.

.. _cal_tmc_observation:

Calibrate in TMCObservation.ipynb
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
If you are using the :code:`TMCObservation.ipynb` notebook, TMC will automatically apply this solution as part of the :code:`Configure` command.
The notebook also supports applying manual calibration and flagging of antennas.

If you want to apply manual calibration to at least on of the stations, set the :code:`CALIBRATIONS` parameter.
This parameter should be a list of strings that is the same length, and in the same order as the :code:`STATION_NAMES` parameter.
Each string can specify either a calibration method (e.g. :code:`"UNITY"` to apply unity calibration), or a path to saved calibration solution in the standard :code:`[antenna, channels, pol]` format.
You can have a mix of calibration types, for example if :code:`STATION_NAMES = ["s8-1", "s8-6", "s9-2"]` and :code:`CALIBRATIONS = ["FROM_STORE", "solutions/fitted_gain_solutions.npy", "UNITY"]`, then S8-1 will have the calibration from the calibration store applied, S8-6 will have the calibration solution located at :code:`solutions/fitted_gain_solutions.npy` applied, and S9-2 will have a unity calibration applied.
If the :code:`CALIBRATIONS` parameter is not set, or is an empty list, then all stations will have solutions applied from the store.

.. note::
    All stations will by default have solutions applied from the calibration store when :code:`Configure` is called. The manual calibration step takes place between :code:`Configure` and :code:`Scan` to optionally overwrite the solutions that were automatically applied. There is no way for TMC to not apply any solution. If a solution is not found in the store, then a unity calibration is applied.

If you want to flag antennas (set their complex gain to zero so that they do not contribute to the beam), set the :code:`FLAGGED_ANTENNAS` parameter to be a list of lists of antenna names (e.g. "sb11-02") to flag.
The list of lists should be the same length, and in the same order as the :code:`STATION_NAMES` parameter.
Specify an empty list to not flag any antennas in that station.
For example, if :code:`STATION_NAMES = ["s9-2", "s10-3"]` and :code:`FLAGGED_ANTENNAS = [[], ["sb11-01", "sb08-09"]]`, then no antennas will be flagged for S9-2, but the antennas in S10-3 with names sb11-01 and sb08-09 will have complex gain of 0 and any data coming from these antennas will not be included in the beam.

.. _cal_acquire_beamformed:

Calibrate in AcquireBeamformed.ipynb
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
When using the :code:`AcquireBeamformed.ipynb` notebook, :code:`APPLY_CALIBRATION_TYPE` can be set to one of the following values:

* :code:`"FROM_STORE"`: the station’s “golden” calibration solution will automatically be applied.
* :code:`"FROM_FILE"`: use a saved calibration solution (e.g. from the calibration SFT). You need to set :code:`GAIN_SOLNS_PATH` to the path to the file. You can also specify the starting channel of the solutions with :code:`GAIN_SOLNS_START_CHANNEL`.If the solution file has the shape :code:`[channels, antenna, pol]`, instead of the usual :code:`[antenna, channels, pol]`, then set the :code:`IS_CHANNELS_FIRST` parameter to :code:`True`.
* :code:`"UNITY"`: unity calibration solutions will be used, that is, the complex gain for xx and yy polarisation will be :code:`1+0i` and the gain for xy and yx polarisation will be :code:`0+0i`.
* :code:`"NONE"`: no calibration is applied and the previous calibration is used. This only works if the previous calibration solutions loaded were for the same observation channels. For example, if the station was previously calibrated for an observation starting at channel 204, you would not be able to do an observation starting at channel 140 without needing to first apply new calibration solutions.
* :code:`"ZERO"`: zero amplitude calibration is applied to all polarisations so that no data comes out of the beam. This is likely only useful for testing purposes.


Antennas to be flagged (set their complex gain to zero so that they do not contribute to the beam) can be specified with the :code:`FLAGGED_ANTENNAS` parameter. This parameter is a list of antenna names.

.. _adding_new_cal:

Adding a new calibration to the store
-------------------------------------
Adding a new "golden" calibration solution to the store is done as part of the calibration SFT, located at :code:`notebooks/SFT/06.Calibration/06.Calibration.ipynb`.
The calibration SFT is performed on a previously captured frequency sweep and provides several outputs in order to verify the quality of the calibration.
The second last cell in the calibration SFT loads the file :code:`"fitted_gain_solutions.npy"` located in the same directory as the notebook that was run, splits it into individual channels, saves these in the same folder as the frequency sweep, and then puts them into the calibration store.
The last cell is commented out, but if uncommented and run, will test that the new golden solution for a channel is the same as the solution that we just loaded. This only works if the solution put in was marked as preferred. 

Running the calibration SFT with the run_notebooks script
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Step 1: Open a terminal in JupyterHub and change directory to the ska-low-tests repository. This should be as simple as running the command :code:`cd ~/ska-low-tests`.

Step 2: Ensure that the repository it up to date. The following commands should do this\:

.. code-block:: bash

    git stash
    git checkout main
    git pull


Step 3: If you want to acquire a new frequency sweep to calibrate on, run the following command replacing :code:`STATION_NAME` with the station name and :code:`CAL_TYPE` with the sort of calibration type you want to run\:

.. code-block:: bash

    run_notebooks --station-name STATION_NAME --early-stop -p CALIBRATION_TYPE CAL_TYPE notebooks/SFT/03*/*.ipynb notebooks/SFT/06*/*.ipynb


For example, runnning a frequency sweep on s8-1 and then performing a fast sun calibration, run the following command\:

.. code-block:: bash

    run_notebooks --station-name s8-1 --early-stop -p CALIBRATION_TYPE SUN_FAST notebooks/SFT/03*/*.ipynb notebooks/SFT/06*/*.ipynb


For more information, including on how to set other parameters, see :ref:`sft_notebooks`. The path to the frequency sweep is passed through to the calibration SFT as an environment variable.

If you have an existing frequency sweep run the following command, replacing :code:`STATION_NAME` with the station name, :code:`PATH_TO_DATA` with the path to the frequency sweep and :code:`CAL_TYPE` with the sort of calibration type you want to run\:

.. code-block:: bash

    run_notebooks --station-name STATION_NAME -p CALIBRATION_TYPE CAL_TYPE -p CAPTURED_DATA_PATH PATH_TO_DATA notebooks/SFT/06*/*.ipynb


For example, performing a sky model calibration, run the following command\:

.. code-block:: bash

    run_notebooks --station-name s10-3 -p CALIBRATION_TYPE GSM -p CAPTURED_DATA_PATH /home/jovyan/shared/test_executions/03.FrequencySweep/s10-3_2024-11-24_155157/captured_data notebooks/SFT/06*/*.ipynb


The calibration SFT can also be run manually by copying :code:`06.Calibration.ipynb` to a new folder, replacing the relevant parameters in the cell under the heading "Constants/Parameters", and running each cell in order.

Step 4: Once the command finishes, the final notebook will be located in the shared folder in the folder :code:`shared/test_executions/06.Calibration/[STATION_NAME]_[Timestamp]`.
Sorting by date modified is a good way to find the notebook that was just run.
You can view the notebook as well as the artifacts generated by the notebook such as images of plots.

If you ran a new frequency sweep for this calibration, you will need to change the :code:`CAPTURED_DATA_PATH` parameter to the location of data gathered by the new frequency sweep.
It will be under :code:`shared/test_executions/03.FrequencySweep/[STATION_NAME]_[Timestamp]/captured_data` with a similar time stamp to the calibration directory.

From this point, you have two options: if the outputs of the calibration look good and you want this to become the new "golden" calibration solution, then:

* Run the cells from the top of the notebook until the "Single Channel Calibration" heading cell.
* Run the cells below the heading cell "Add solutions to calibration store".
* Provide a value of :code:`True` when prompted for the value of :code:`ADD_TO_CALIBRATION_STORE`. The value of :code:`ADD_TO_CALIBRATION_STORE` is treated as case insensitive, so :code:`True`, :code:`TRUE`, and :code:`true` are treated the same.

If you want to investigate the calibration solutions further, change the parameter :code:`ONLY_LOAD_DATA` to :code:`True` and run all the cells in the notebook, providing a value of :code:`False` when prompted for the value of :code:`ADD_TO_CALIBRATION_STORE`.
The raw calibration solutions are stored in the :code:`daq-data` directory. The exact directory is stored in the :code:`CAPTURED_DATA_DAQ_PATH` variable.
