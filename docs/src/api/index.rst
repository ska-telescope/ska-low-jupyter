.. _api:

===
API
===

See this confluence page to see the schemas and examples for all TANGO commands for MCCS
https://confluence.skatelescope.org/display/SE/MCCS+1703%3A+Updated+TM+-%3E+MCCS+schemas

.. toctree::
  :caption: Package Modules
  :maxdepth: 2
  
  calibration_utils<calibration_utils>
  config_capture<config_capture>
  config_display<config_display>
  event_monitoring<event_monitoring>
  fault_simulation<fault_simulation>
  hdf5_tools<hdf5_tools>
  load_data<load_data>
  low_utils<low_utils>
  metadata<metadata>
  sft_utils/sft_utils<sft_utils>
  sky_simulate<sky_simulate>
  station<station>
  tango_utils<tango_utils>
  test_frame<test_frame>
  test_utils<test_utils>
  uvdata_tools<uvdata_tools>
