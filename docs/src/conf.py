import os
import sys

sys.path.insert(0, os.path.abspath('../../src'))

project = 'SKA-Low tests'
copyright = '2024, SKA Vulcan Team'
author = 'SKA Vulcan Team'

# The full version, including alpha/beta/rc tags
release = '0.1.0'

extensions = [
    "ska_ser_sphinx_theme",
    "sphinx.ext.autodoc",
    "sphinx.ext.intersphinx",
    "sphinx.ext.autosectionlabel",
    "sphinx_autodoc_typehints",
]

autosectionlabel_prefix_document = True

nitpick_ignore = [
    # TODO why does the below use tango._tango.DeviceProxy instead of tango.DeviceProxy?
    ("py:class", "tango._tango.DeviceProxy"),
    ("py:class", "tango._tango.Database"),
]

html_theme = 'ska_ser_sphinx_theme'

# -- Extension configuration -------------------------------------------------
intersphinx_mapping = {
    "python": ("https://docs.python.org/3.10/", None),
    "numpy": ("https://numpy.org/doc/1.25/", None),
    "tango": ("https://pytango.readthedocs.io/en/v9.2.0/", None),
    "pandas": ("https://pandas.pydata.org/docs/", None),
    'astropy': ('https://docs.astropy.org/en/stable/', None),
}
