"""This module contains shared configurations, fixtures, and hooks for pytest."""

import os
import shutil
from pathlib import Path

import pytest

FIXTURE_DIR = os.path.join(os.path.dirname(__file__), "fixtures")


@pytest.fixture
def fixture_dir():
    """Fixture to provide the path to the fixtures directory."""
    return FIXTURE_DIR


@pytest.fixture
def correlation_burst_hdf5_path(tmp_path):
    """
    Fixture to create a copy of a file for testing and clean it up afterward.

    Args:
        tmp_path: Pytest's temporary directory fixture.

    Yields:
        Path to the copied file.
    """
    source_file = Path(FIXTURE_DIR) / "correlation_burst_204_20240701_65074_0.hdf5"
    if not source_file.exists():
        raise FileNotFoundError(f"Source file does not exist: {source_file}")

    # Create a copy in the temporary directory
    temp_file = tmp_path / source_file.name
    shutil.copy(source_file, temp_file)

    yield temp_file  # Provide the path to the test
