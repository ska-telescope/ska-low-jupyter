"""
Tests of station-level operations.

Currently covers initialisation, equalisation, data acquisition and calibration.
"""
import json
import os
from glob import glob

import h5py
import numpy as np
import papermill as pm
import pytest
from pytest_bdd import given, parsers, scenario, then, when
from tango import Database

from aiv_utils.calibration_utils import (
    apply_cal_from_array,
    apply_zeroed_cal,
    mask_antennas_by_name,
    parse_selection_policy_string,
)
from aiv_utils.low_utils import (
    get_mccs_device,
    get_pasd_devices,
    get_sps_devices,
    is_station_initialised,
)
from aiv_utils.metadata import get_station_antenna_dataframe
from aiv_utils.tango_utils import member, single_prop, wait_for
from aiv_utils.test_utils import make_test_dir

ADC_TARGET = 17.0
FIXTURE_DIR = os.path.join(os.path.dirname(__file__), "../fixtures")

pytestmark = pytest.mark.operations


# Turn field nodes on ------------------------------------------------


@scenario("station_operations_XTP-62364.feature", "Turn field nodes on")
def test_field_nodes_on():
    """Turn field nodes on"""


@when("I turn on a field node", target_fixture="powered_fieldnode")
def turn_field_nodes_on(output_dir, station_name):
    """Turn a station's field node on

    The step uses `papermill` to run the Vulcan maintained `FieldNodeOn.ipynb` notebook.
    This should be ideally accomplished with FieldStation.On().
    This notebook will attempt to get a field node into the state where the
    FNDH and all smartboxes are initialised and all FEM ports on, with a current trip
    threshold set to a given value.
    """
    pm.execute_notebook(
        "notebooks/operations/FieldNodeOn.ipynb",
        output_path=output_dir / "FieldNodeOn.ipynb",
        kernel_name="python3",
        parameters={
            "STATION_NAME": station_name,
        },
    )
    return get_pasd_devices(station_name)


@then("the FNDH ports are on")
def field_nodes_on(powered_fieldnode):
    """Check that the FNDH ports are on by asserting `pasdbus.fndhstatus == "OK"`."""
    # TODO: expand these assertions
    _, pasdbus, _, _ = powered_fieldnode
    assert pasdbus.fndhstatus == "OK"


# Initialise --------------------------------------------


@scenario("station_operations_XTP-62364.feature", "Initialise single station")
def test_initialise_single_station():
    """Initialise single station"""


@given("tango database is reachable")
def check_database():
    """Check if the Tango database is reachable by running `tango.Database()`."""
    Database()


@when("I initialise a station")
def initialise_station(output_dir, station_name):
    """
    Initialise the station using the Initialise notebook.

    The fixture uses `papermill` to run the Vulcan-maintained
    `Initialise.ipynb` notebook.
    """
    pm.execute_notebook(
        "notebooks/operations/Initialise.ipynb",
        output_path=output_dir / "Initialise.ipynb",
        kernel_name="python3",
        parameters={
            "STATION_NAME": station_name,
        },
    )


@then("the station is initialised")
@given("an initialised station")
def station_initialised(station):
    """Read-only method that asserts that a station is initialised."""
    return is_station_initialised(station)


# Equalise ------------------------------------------------


@scenario("station_operations_XTP-62364.feature", "Equalise the station")
def test_equalise():
    """Equalise the station"""


@when("apply equalisation to the station")
def equalise(output_dir, station_name):
    """Equalise the stations ADC power levels.

    The step uses `papermill` to run the Vulcan maintained `Equalise.ipynb` notebook.
    Measures the ADC power level over all TPMS then tries to equalise them by adjusting
    their preADU attenuators.
    """
    pm.execute_notebook(
        "notebooks/operations/Equalise.ipynb",
        output_path=output_dir / "Equalise.ipynb",
        kernel_name="python3",
        parameters={
            "STATION_NAME": station_name,
            "ADC_TARGET": ADC_TARGET,
            "N_SAMPLES": 30,
        },
    )


@then("the median station power for each tpm is within 1 dB")
def equalised(station):
    """
    Check that ADC power for each TPM input is either <1, or within 1 of target.
    """

    def check_equalised(adc_powers, tpm):
        # in the ITF, we only have one preADU installed in our TPMs,
        # so we should only look at half the channels.
        # TODO: can we detect if a preADU is installed dynamically?
        if member(tpm).startswith("itf"):
            adc_power = adc_powers[[*range(0, 8), *range(16, 24)]]
        else:
            adc_power = adc_powers

        # result of equalisation should be some signal chains
        return np.all(
            np.logical_or(
                adc_power < 1,
                np.abs(adc_power - ADC_TARGET) < 1,
            )
        )

    try:
        return wait_for(station.tpms, "adcPower", check_equalised, timeout=10)
    except ValueError as ex:
        if [member(dev) for dev in ex.failed_devices] == ["itf2-tpm02"]:
            # for some reason pylint thinks this is raising None
            raise pytest.xfail(  # pylint: disable=raising-bad-type
                "itf2-tpm02 ADC channel 0 is known to be faulty - SPRTS-335"
            )
        raise


# Raw ------------------------------------------------


@scenario("station_operations_XTP-62364.feature", "Acquire raw data")
def test_acquire_raw_data():
    """Acquire raw data"""


@when("I send raw data to the DAQ")
def send_raw_data(output_dir, station_name):
    """Acquire raw data the acquire raw notebook.

    The step uses `papermill` to run the Vulcan maintained `AcquireRaw.ipynb` notebook.
    This notebook dumps raw voltage samples using `SpSstation.SendDataSamples()`.
    """
    pm.execute_notebook(
        "notebooks/operations/AcquireRaw.ipynb",
        output_path=output_dir / "AcquireRaw.ipynb",
        kernel_name="python3",
        parameters={
            "STATION_NAME": station_name,
            "DATA_OUTPUT_PATH": str(output_dir),
        },
    )


@then("I capture raw voltage data from the station")
def raw_data_acquired(output_dir, n_tpms):
    """Checks a raw data file is created for each tile in the station."""
    assert len(glob(f"{output_dir}/raw_burst_*hdf5")) == n_tpms


# Channelised ------------------------------------------------


@scenario("station_operations_XTP-62364.feature", "Acquire channelised data")
def test_acquire_channelised_data():
    """Acquire channelised data"""


@when("I send channelised data to the DAQ")
def send_channelised_data(output_dir, station_name):
    """Acquire channelised data the acquire channelised notebook.

    The step uses `papermill` to run the Vulcan-maintained `AcquireChannelised.ipynb`
    notebook. This notebook dumps channelised voltage samples with the
    `SpSstation.SendDataSamples()` method.
    """
    pm.execute_notebook(
        "notebooks/operations/AcquireChannelised.ipynb",
        output_path=output_dir / "AcquireChannelised.ipynb",
        kernel_name="python3",
        parameters={
            "STATION_NAME": station_name,
            "DATA_OUTPUT_PATH": str(output_dir),
        },
    )


@then("I capture burst channelised data from the station")
def channelised_data_acquired(output_dir, n_tpms):
    """Checks a channelised data file is created for each tile in the station."""
    assert len(glob(f"{output_dir}/channel_burst_*hdf5")) == n_tpms


# Frequency Sweep ------------------------------------------------


@scenario(
    "station_operations_XTP-62364.feature",
    "Acquire correlator frequency sweep data",
)
def test_acquire_correlator_frequency_sweep_data():
    """Acquire correlator frequency sweep data"""


@when(
    parsers.parse(
        "I send correlator data to the DAQ from channel {start_chan:d} to {stop_chan:d}"
    ),
    target_fixture="expected_channels",
)
def send_frequency_sweep_data(output_dir, station_name, start_chan, stop_chan):
    """Acquire frequency sweep data the acquire frequency sweep notebook.

    The step uses `papermill` to run the Vulcan maintained `FrequencySweep.ipynb`
    notebook. This notebook uses the `SpSstation.SendDataSamples()` method to iterate
    over the input channel range and dump a specified number of channelised voltage
    samples for each coarse channel.
    """
    expected_channels = stop_chan - start_chan + 1
    pm.execute_notebook(
        "notebooks/operations/FrequencySweep.ipynb",
        output_path=output_dir / "FrequencySweep.ipynb",
        kernel_name="python3",
        parameters={
            "STATION_NAME": station_name,
            "CHANNEL_START": start_chan,
            "CHANNEL_STOP": stop_chan,
            "DATA_OUTPUT_PATH": str(output_dir),
            "OPERATIONS_NOTEBOOK_DIR": os.getcwd() + "/notebooks/operations",
            "DO_ANALYSIS": False,
        },
    )
    return expected_channels


@then("I capture correlator data from the station")
def frequency_sweep_data_acquired(output_dir, expected_channels):
    """
    Checks a channelised data file is created for each of the input frequency channels.
    """
    assert len(glob(f"{output_dir}/correlation_burst_*hdf5")) == expected_channels


# Beamformed ------------------------------------------------


@scenario("station_operations_XTP-62364.feature", "Acquire beamformed data")
def test_acquire_beamformed_data():
    """Acquire beamformed data"""


@when(
    parsers.parse("I send beamformed data to the DAQ with {cal_type} calibration"),
    target_fixture="beamform_subdir",
)
def send_beamformed_data(output_dir, station_name, n_tpms, cal_type):
    """Acquire beamformed data the acquire beamformed notebook.

    The step uses `papermill` to run the Vulcan maintained `AcquireBeamformed.ipynb`
    notebook. Uses the `Station.TrackObject()` and `Station.Scan()` methods to acquire
    beamformed data for a station.
    """
    # Make subdirectory to keep beamformed data seperate
    output_subdir = make_test_dir(
        dir_prefix="acquire_beamformed",
        base_dir=output_dir,
    )

    apply_calibration_type = None
    gain_solns_path = None

    if cal_type == "zeroed":
        apply_calibration_type = "ZERO"
    if cal_type == "stored":
        apply_calibration_type = "FROM_STORE"
    if cal_type == "file":
        apply_calibration_type = "FROM_FILE"
        np.save(
            f"{output_subdir}/test_cal_soln.npy",
            np.full((n_tpms * 16, 384, 4), np.array([1, 0, 0, 1]), dtype="complex128"),
            allow_pickle=False,
        )
        gain_solns_path = f"{output_subdir}/test_cal_soln.npy"

    pm.execute_notebook(
        "notebooks/operations/AcquireBeamformed.ipynb",
        output_path=f"{output_subdir}/AcquireBeamformed.ipynb",
        kernel_name="python3",
        parameters={
            "STATION_NAME": station_name,
            "DRIFT_IN_DURATION": 0.1,
            "TRACK_DURATION": 0,
            "DRIFT_OUT_DURATION": 0,
            "SOURCE": "ZENITH",
            "APPLY_CALIBRATION_TYPE": apply_calibration_type,
            "GAIN_SOLNS_PATH": gain_solns_path,
            # THe below is hardcoded until SKB-761 is resolved
            "MASK_KNOWN_BAD_ANTENNAS": False,
            "IS_CHANNELS_FIRST": False,
            "DATA_OUTPUT_PATH": str(output_subdir),
            "OPERATIONS_NOTEBOOK_DIR": os.getcwd() + "/notebooks/operations",
            "OBS_START_WAITTIME": 5,
        },
    )
    return output_subdir


@then("I capture beamformed data from the station")
def beamformed_data_acquired(beamform_subdir):
    """Checks there is a station beam data file."""
    assert len(glob(f"{beamform_subdir}/station*hdf5")) == 1


# Calibrate station ------------------------------------------------


@scenario("station_operations_XTP-62364.feature", "Test ability to calibrate station")
def test_calibrate_station():
    """Test ability to calibrate station"""


@given("the signal generator is on")
def is_signal_generator_on(station_with_signal_generator):
    """Assert the signal generator is on from the station_with_signal_generator fixture.

    Make sure the signal generator is on with the`SpsStation.ConfigureTestGenerator()`
    command` to ensure the even when there are no signal generators attached to the
    ITF tpms that this test can still be run.
    """
    for tpm in station_with_signal_generator.tpms:
        assert tpm.testGeneratorActive


@then(
    parsers.parse("I observe there is {amount} {pol} pol data in the beamformed data")
)
def check_calibrated_beamformed_data(beamform_subdir, amount, pol):
    """Checks that the beamformed data file has the right amount of data in it."""
    file_path = sorted(glob(f"{beamform_subdir}/station*hdf5"), key=os.path.getmtime)[
        -1
    ]
    data_file = h5py.File(file_path, "r")

    if pol.upper() == "X":
        data_sum = np.nansum(data_file["polarization_0"]["data"])
    else:
        data_sum = np.nansum(data_file["polarization_1"]["data"])

    if amount == "some":
        assert data_sum > 0
    else:
        assert data_sum == 0


# Test ability to mask antennas -----------------------------------


@scenario("station_operations_XTP-62364.feature", "Test ability to mask antennas")
def test_mask_antennas():
    """Test ability to mask antennas"""


@when("I calibrate with some antennas on")
def calibrate_with_some_antennas(station_name, n_tpms, antennas_to_test):
    """
    Calibrate the station with specific antennas having unity calibration, the rest
    having zero calibration.
    """

    cal_soln = np.zeros((384, n_tpms * 16, 4), dtype="complex128")

    antenna_df = get_station_antenna_dataframe(station_name)
    antenna_name_to_antnum_tpm = dict(
        zip(antenna_df["antenna_name"], antenna_df["antnum_tpm"])
    )
    antenna_numbers_tpm = [
        antenna_name_to_antnum_tpm[antenna_name] for antenna_name in antennas_to_test
    ]

    for ant in antenna_numbers_tpm:
        cal_soln[:, ant, 0] = 1
        cal_soln[:, ant, 3] = 1

    apply_cal_from_array(station_name, cal_soln)


@when("I mask those antennas")
def mask_antennas(station_name, antennas_to_test):
    """Mask specific antennas by setting them to zeroed calibration."""
    mask_antennas_by_name(station_name, antennas_to_test)


# Test ability to store and retrieve calibration -----------------------------------
@scenario(
    "station_operations_XTP-62364.feature",
    "Test ability to store and retrieve calibration",
)
def test_store_retrieve():
    """Test ability to store and retrieve calibration"""


@when(parsers.parse("I store a {cal_type} calibration solution"))
def store_cal(station_name, n_ant, cal_type):
    """Store a specific type of calibration in the store."""
    cal_store = get_mccs_device(f"low-mccs/calibrationstore/{station_name}")
    sps_station = get_sps_devices(station_name).station

    cal_soln = np.zeros((n_ant, 4), dtype="complex128")
    if cal_type == "unity":
        cal_soln = np.full(
            (n_ant, 4),
            np.array([1, 0, 0, 1]),
            dtype="complex128",
        )
    if cal_type == "x pol only":
        cal_soln = np.full(
            (n_ant, 4),
            np.array([1, 0, 0, 0]),
            dtype="complex128",
        )
    if cal_type == "y pol only":
        cal_soln = np.full(
            (n_ant, 4),
            np.array([0, 0, 0, 1]),
            dtype="complex128",
        )

    jones_matrices = cal_soln
    flattened_matrices = jones_matrices.flatten()
    cal_soln = np.array(
        (np.real(flattened_matrices), np.imag(flattened_matrices))
    ).T.ravel()

    np.save(
        f"{os.getenv('DAQ_DATA_PATH')}/calibration_store_testing/{cal_type}.npy",
        cal_soln,
        allow_pickle=False,
    )

    [[result_code], [message]] = cal_store.StoreSolution(
        json.dumps(
            {
                "frequency_channel": 0,
                "outside_temperature": 1000.0,
                "solution_path": f"/product/calibration_store_testing/{cal_type}.npy",
                "station_id": int(single_prop(sps_station, "StationId")),
            }
        )
    )

    if not result_code == 0:
        raise ValueError(
            f"StoreSolution failed. MccsStation returned message: {message}"
        )


@then(parsers.parse("I retrieve a {cal_type} calibration solution"))
def retrieve_cal(station_name, n_ant, cal_type):
    """Retrieve a solution from the store and check that it is the right type."""
    cal_store = get_mccs_device(f"low-mccs/calibrationstore/{station_name}")
    sps_station = get_sps_devices(station_name).station

    stored_soln = cal_store.GetSolution(
        json.dumps(
            {
                "frequency_channel": 0,
                "outside_temperature": 1000.0,
                "station_id": int(single_prop(sps_station, "StationId")),
            }
        )
    )
    assert stored_soln is not None

    cal_soln = np.zeros((n_ant, 4), dtype="complex128")
    if cal_type == "unity":
        cal_soln = np.full(
            (n_ant, 4),
            np.array([1, 0, 0, 1]),
            dtype="complex128",
        )
    if cal_type == "x pol only":
        cal_soln = np.full(
            (n_ant, 4),
            np.array([1, 0, 0, 0]),
            dtype="complex128",
        )
    if cal_type == "y pol only":
        cal_soln = np.full(
            (n_ant, 4),
            np.array([0, 0, 0, 1]),
            dtype="complex128",
        )

    jones_matrices = cal_soln
    flattened_matrices = jones_matrices.flatten()
    cal_soln = np.array(
        (np.real(flattened_matrices), np.imag(flattened_matrices))
    ).T.ravel()

    assert np.all(stored_soln == cal_soln)


# Test ability to calibrate from store ------------------------------------------
@scenario(
    "station_operations_XTP-62364.feature", "Test ability to calibrate from store"
)
def test_cal_from_store():
    """Test ability to calibrate from store"""


@when("I clear the calibration coefficients")
def clear_cal_coefficients(station_name):
    """
    Clear the calibration coefficients.
    We need to do this for calibration store testing because the calibration store can
    only see the real antennas, not the tpm simulated ones.
    """
    apply_zeroed_cal(station_name)
    apply_zeroed_cal(station_name)


@given("the selection policy is lenient")
def is_selection_policy_lenient(cal_store_with_lenient_selection_policy):
    """
    Assert that the selection policy is lenient.
    """
    selection_policy = parse_selection_policy_string(
        cal_store_with_lenient_selection_policy.selectionPolicy
    )
    if selection_policy["policy_name"] == "preferred":
        assert selection_policy["frequency_tolerance"] > 385
    if selection_policy["policy_name"] == "closest_in_range":
        assert selection_policy["frequency_tolerance"] > 385
        assert selection_policy["temperature_tolerance"] > 100
