"""
Tests of TMC-driven scans.
"""

import json
import os
import time
from glob import glob

import numpy as np
import papermill as pm
import pytest
import yaml
from casacore.tables import table
from pytest_bdd import given, parsers, scenario, then, when
from pyuvdata import UVData
from ska_control_model import AdminMode

from aiv_utils.calibration_utils import (
    parse_selection_policy_string,
    reset_selection_policy,
    set_impossible_selection_policy,
)
from aiv_utils.low_utils import (
    calculate_pst_cfreq_bandwidth,
    get_mccs_device,
    get_sps_devices,
    get_stations,
    is_station_initialised,
    split_host_port,
)
from aiv_utils.metadata import get_station_antenna_dataframe
from aiv_utils.tango_utils import member, single_prop
from aiv_utils.test_utils import grab_tagged_cell_json
from aiv_utils.uvdata_tools import get_uv_visibility_data

# pylint: disable=duplicate-code


COARSE_CHANNEL_WIDTH_HZ = 781_250
NUM_FINE_CHANNELS = 144

pytestmark = pytest.mark.tmc_scans


@pytest.fixture(name="subarray_id")
def fixture_subarray_id() -> int:
    """Pick a subarray, from env or by inspecting the system."""
    if "SUBARRAY_ID" in os.environ:
        return int(os.environ["SUBARRAY_ID"])
    # tmc = get_tmc_devices()
    # subarray = next(s for s in tmc.tmc_subarray_nodes if s.obsState == ObsState.EMPTY)
    # return int(member(subarray))
    return 1


@pytest.fixture(name="subarray_beam_id")
def fixture_subarray_beam_id() -> int:
    """Pick a subarray beam, from env or by inspecting the system."""
    if "SUBARRAY_BEAM_ID" in os.environ:
        return int(os.environ["SUBARRAY_BEAM_ID"])
    # See SKB-672. Currently only beam ID 1 is supported.
    return 1


@pytest.fixture(scope="module", name="two_stations")
def fixture_two_stations():  # pylint: disable=too-many-locals
    """Pick two stations and assert that they are ready to use for observing."""
    first_station = os.getenv("STATION_NAME")
    second_station = os.getenv("SECOND_STATION_NAME")
    if first_station is None or second_station is None:
        stations = get_stations()[:2]
    else:
        first_station_device = get_mccs_device(f"low-mccs/spsstation/{first_station}")
        second_station_device = get_mccs_device(f"low-mccs/spsstation/{second_station}")
        stations = [first_station_device, second_station_device]
    stations_devs = [get_sps_devices(member(stn)) for stn in stations]

    # We need CSP data going to the correlator, but it might have been sent
    # to the DAQ by a previous user (such as our station operations tests).
    # TODO make this redundant with a mechanism for getting a station into a
    # desired state without unnecessarily re-initialising
    for station, *_ in stations_devs:
        csp_ip, csp_port = split_host_port(single_prop(station, "CspIngestIp"))
        csp_config = {
            "destination_ip": csp_ip,
            "destination_port": csp_port,
        }
        print(f"Setting CSP destination to {':'.join(map(str, csp_config.values()))}")
        station.SetCspIngest(json.dumps(csp_config))

    # Set up test generators
    for sps_devices in stations_devs:
        for tpm in sps_devices.tpms:
            tpm.adminMode = AdminMode.ENGINEERING
        sps_devices.station.ConfigureTestGenerator(
            json.dumps({"noise_amplitude": 0.25})
        )

    # We need restrictive selection policies so that unity calibration is applied to
    # both stations, so that data will come out.
    cal_stores = [
        get_mccs_device(f"low-mccs/calibrationstore/{member(stn)}") for stn in stations
    ]

    current_selection_policies = []
    for cal_store, stn in zip(cal_stores, stations):
        current_selection_policies.append(
            parse_selection_policy_string(cal_store.selectionPolicy)
        )
        set_impossible_selection_policy(member(stn))

    yield stations_devs

    # Restore old selection policies
    for cal_store, selection_policy, stn in zip(
        cal_stores, current_selection_policies, stations
    ):
        # Account for the rare case where the selection policy has never been set.
        # TODO: MCCS 0.23 should set this to a default on deployment.
        if selection_policy:
            [[result_code], [message]] = cal_store.updateSelectionPolicy(
                json.dumps(selection_policy)
            )

            if not result_code == 0:
                raise ValueError(
                    f"UpdateSelectionPolicy failed for {member(stn)}."
                    f" MccsStation returned message: {message}"
                )
        else:
            reset_selection_policy(member(stn))

    # Tear down test generators
    for sps_devices in stations_devs:
        sps_devices.station.ConfigureTestGenerator(json.dumps({}))
        for tpm in sps_devices.tpms:
            tpm.adminMode = AdminMode.ONLINE


@scenario("tmc_scans_XTP-75814.feature", "two-station visibility capture")
def test_two_station_visibility_tmc():
    """TMC test visibility scans using two stations"""


@scenario("tmc_scans_XTP-75814.feature", "two-station voltage capture")
def test_two_station_voltage_tmc():
    """TMC test voltage scans using two stations"""


@given("two initialised stations with signal generators on")
def step_two_stations(two_stations):
    """Pick two stations and assert that they are ready to use for observing."""
    assert all(is_station_initialised(stn) for stn in two_stations)
    for stn in two_stations:
        for tpm in stn.tpms:
            assert tpm.testGeneratorActive


@when(
    parsers.parse(
        "I run a {obs_type} scan using TMC of coarse channels {start_chan:d} to {stop_chan:d}"  # pylint: disable=line-too-long
    ),
    target_fixture="successful_scan_info",
)
# pylint: disable=too-many-arguments
def scan_with_tmc(
    output_dir,
    subarray_id,
    subarray_beam_id,
    two_stations,
    obs_type,
    start_chan,
    stop_chan,
):
    """
    Scan with two stations using TMC given a start_chan and stop_chan.

    start_chan is inclusive, stop_chan is exclusive.
    """
    observation_mode = "VIS"
    duration_min = 0.25
    if obs_type == "voltage":
        observation_mode = "PST"
        # PST obs must be at least 30 seconds long
        duration_min = 0.5
    elif obs_type == "visibility":
        observation_mode = "VIS"

    # pylint: disable=too-many-arguments
    obs = {
        "Observation name": "default_observation",
        "RA (deg)": 180.0,
        "Declination (deg)": -30.0,
        "Duration (min)": duration_min,
        # The coarse channel ID of the lowest frequency
        # coarse_channel = MHz * 0.78125
        "Start frequency channel ID": start_chan,
        # The number of coarse channels in the frequency bandwidth
        "Bandwidth channels": stop_chan - start_chan + 1,
    }

    result = pm.execute_notebook(
        "notebooks/operations/TMCObservation.ipynb",
        output_path=f"{output_dir}/TMCObservation_{observation_mode}.ipynb",
        kernel_name="python3",
        parameters={
            "CONFIGURE_DELAY": 0,
            "STATION_NAMES": [member(stn.station) for stn in two_stations],
            "SUBARRAY_ID": subarray_id,
            "SUBARRAY_BEAM_ID": subarray_beam_id,
            "OBSERVATIONS": [obs],
            "OBSERVING_MODE": observation_mode,
            # THe below is hardcoded until SKB-761 is resolved
            "MASK_KNOWN_BAD_ANTENNAS": False,
        },
    )

    output_variables = grab_tagged_cell_json(result, "output_variables")
    return {
        "scan_params": obs,
        "data_path": output_variables["data_path"],
    }


@then("a measurement set is created")
def check_measurement_set(successful_scan_info):
    """Check that a measurement set exists at the expected path."""
    ms_path = successful_scan_info["data_path"]
    ms_files = glob(f"{ms_path}/*.ms")
    assert (
        len(ms_files) == 1
    ), f"{len(ms_files)} measurement sets found at {ms_path}, expecting 1"


@then("the measurement set has the expected dimensions and frequency content")
def ms_has_expected_metadata(successful_scan_info):
    """Check that the data in the measurement set has the expected shape."""
    [ms_path] = glob(f"{successful_scan_info['data_path']}/*.ms")

    uv_data = UVData.from_file(ms_path)

    ntime, nchan, npol = uv_data.data_array.shape

    obs = successful_scan_info["scan_params"]
    start_channel = obs["Start frequency channel ID"]
    band_channels = obs["Bandwidth channels"]

    # coarse channels in scan * fine channels per coarse channel
    assert nchan == band_channels * NUM_FINE_CHANNELS

    # xx, yy, xy, yx
    assert npol == 4

    # should have three baselines
    nbaseline = len(uv_data.get_baseline_nums())
    assert nbaseline == 3

    # timestamps should be repeated once for each baseline
    assert ntime % nbaseline == 0
    assert all(
        np.all(ts == t)
        for t, ts in zip(
            sorted(np.unique(uv_data.time_array)),
            uv_data.time_array.reshape((-1, nbaseline)),
        )
    )

    # Some metadata is not exposed by pyuvdata
    measurement_set = table(ms_path)
    assert len(measurement_set.SPECTRAL_WINDOW) == 1
    [freq] = measurement_set.SPECTRAL_WINDOW

    assert freq["REF_FREQUENCY"] == (start_channel - 0.5) * COARSE_CHANNEL_WIDTH_HZ
    assert freq["TOTAL_BANDWIDTH"] == band_channels * COARSE_CHANNEL_WIDTH_HZ

    expected_chan_width = COARSE_CHANNEL_WIDTH_HZ / NUM_FINE_CHANNELS
    assert np.all(freq["CHAN_WIDTH"] == expected_chan_width)
    assert np.all(freq["RESOLUTION"] == expected_chan_width)
    assert np.all(freq["EFFECTIVE_BW"] == expected_chan_width)


@then(parsers.parse("there is {amount} data in the measurement set"))
def check_measurement_set_data(successful_scan_info, amount):
    """Check that there is the measurement set contains data, or all zero data."""
    [ms_path] = glob(f"{successful_scan_info['data_path']}/*.ms")

    (
        pol_xx,
        pol_yy,
        _,
        _,
        _,
        _,
        _,
        _,
    ) = get_uv_visibility_data(ms_path)

    if amount == "zero":
        assert np.all(pol_xx == 0)
        assert np.all(pol_yy == 0)
    else:
        assert not np.all(pol_xx == 0)
        assert not np.all(pol_yy == 0)


@scenario(
    "tmc_scans_XTP-75814.feature", "manual calibration and masking on two stations"
)
def test_two_station_calibrated_tmc():
    """TMC test scan using two stations with calibration and antenna masking applied"""


@when(
    "I run a masked and zero calibrated scan using TMC",
    target_fixture="successful_scan_info",
)
def scan_with_tmc_calibrated_and_masked(
    output_dir,
    subarray_id,
    subarray_beam_id,
    two_stations,
):
    """
    Scan with two stations using TMC with one station zero calibrated and the
    """
    # pylint: disable=too-many-arguments
    obs = {
        "Observation name": "default_observation",
        "RA (deg)": 180.0,
        "Declination (deg)": -30.0,
        "Duration (min)": 0.25,
        # The coarse channel ID of the lowest frequency
        # coarse_channel = MHz * 0.78125
        "Start frequency channel ID": 96,
        # The number of coarse channels in the frequency bandwidth
        "Bandwidth channels": 143 - 96 + 1,
    }

    antenna_df = get_station_antenna_dataframe(member(two_stations[1].station))
    antennas_to_mask = list(antenna_df["antenna_name"])

    result = pm.execute_notebook(
        "notebooks/operations/TMCObservation.ipynb",
        output_path=output_dir / "TMCObservation.ipynb",
        kernel_name="python3",
        parameters={
            "CONFIGURE_DELAY": 0,
            "STATION_NAMES": [member(stn.station) for stn in two_stations],
            "SUBARRAY_ID": subarray_id,
            "SUBARRAY_BEAM_ID": subarray_beam_id,
            "OBSERVATIONS": [obs],
            "MASKED_ANTENNAS": [[], antennas_to_mask],
            "CALIBRATIONS": ["ZERO", "UNITY"],
            # THe below is hardcoded until SKB-761 is resolved
            "MASK_KNOWN_BAD_ANTENNAS": False,
        },
    )

    output_variables = grab_tagged_cell_json(result, "output_variables")
    return {
        "scan_params": obs,
        "data_path": output_variables["data_path"],
    }


@then("the voltage data files are created")
def check_voltage_data_files(successful_scan_info):
    """Check that there are dada files in the data and weights subdirectories."""
    weights_files = glob(f"{successful_scan_info['data_path']}/weights/*.dada")
    data_files = glob(f"{successful_scan_info['data_path']}/data/*.dada")

    assert len(weights_files) > 0
    assert len(data_files) > 0


@then(
    parsers.parse(
        "the data-product YAML has the expected time and frequency dimensions of coarse channels {start_chan:d} to {stop_chan:d}"  # pylint: disable=line-too-long
    )
)
def check_voltage_data_dimensions(successful_scan_info, start_chan, stop_chan):
    """Check the output ska-data-product.yaml header file to confirm the observation
    has the expected time and frequency dimensions."""
    # Wait for up to 5 seconds as the yaml is created as a summary of the output files
    data_product_yaml = f"{successful_scan_info['data_path']}/ska-data-product.yaml"
    timeout = 5  # seconds
    start_time = time.time()
    while time.time() - start_time < timeout:
        if os.path.exists(data_product_yaml):
            print(f"File '{data_product_yaml}' found!")
            break
        time.sleep(0.5)  # Check every 0.5 seconds
    else:
        print(f"Timeout: File '{data_product_yaml}' not found after {timeout} seconds.")

    with open(data_product_yaml, "r") as file:
        data_product_dict = yaml.safe_load(file)

    scan_duration_seconds = data_product_dict["obscore"]["t_exptime"]
    scan_bandwidth_hertz = (
        data_product_dict["obscore"]["em_max"] - data_product_dict["obscore"]["em_min"]
    )

    # Assert durations is within 5 seconds of the expected 30 seconds
    assert abs(30 - scan_duration_seconds) < 5.0
    # Assert the bandwidth
    _, expected_bandwidth_hertz = calculate_pst_cfreq_bandwidth(start_chan, stop_chan)
    assert expected_bandwidth_hertz == scan_bandwidth_hertz
