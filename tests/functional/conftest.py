"""
Common test fixtures.
"""

import json
import os
from pathlib import Path

import pytest
from ska_control_model import AdminMode

from aiv_utils.calibration_utils import (
    apply_unity_cal,
    parse_selection_policy_string,
    reset_selection_policy,
)
from aiv_utils.config_capture import dump_configuration
from aiv_utils.low_utils import SPSDevices, get_mccs_device, get_sps_devices
from aiv_utils.metadata import get_station_antenna_dataframe
from aiv_utils.test_utils import make_test_dir

# pylint: disable=duplicate-code


@pytest.fixture(name="station_name", scope="session")
def fixture_station_name() -> str:
    """Return the name of the station to use for station tests."""
    return os.getenv("STATION_NAME", "itf1")


@pytest.fixture(name="station", scope="module")
def fixture_station(station_name) -> SPSDevices:
    """Returns Tango devices for the given station."""
    return get_sps_devices(station_name)


@pytest.fixture(name="n_ant", scope="module")
def fixture_num_antennas(station_name) -> int:
    """
    Returns number of actual antennas in the given station.
    When TPMs have the signal generator on, they simulate 16 antennas per TPM,
    regardless of how many antennas are actually attached.
    """
    antennas_df_eep = get_station_antenna_dataframe(station_name)
    return len(antennas_df_eep)


@pytest.fixture(name="n_tpms", scope="module")
def fixture_num_tpms(station_name) -> int:
    """
    Returns number of tpms in the given station.
    """
    return len(get_sps_devices(station_name).tpms)


@pytest.fixture(name="output_dir", scope="module")
def fixture_output_dir(request):
    """Create an output directory for this test run."""
    mod_name = request.module.__name__.split(".")[-1]
    if mod_name == "conftest":
        raise pytest.skip()
    return Path(make_test_dir(dir_prefix=mod_name))


@pytest.fixture(scope="module", autouse=True)
def capture_configuration(output_dir):
    """Capture configuration and write to a JSON file in the test output directory."""
    dump_configuration(output_dir / "config_capture.json")


@pytest.fixture(scope="module")
def station_with_signal_generator(station, station_name):
    """Puts the tpms in engineering mode and turns on the test signal generator.
    Will return the station to online mode after the test finishes."""
    tpms = station.tpms
    # Put TPMs into engineering mode
    for tpm in tpms:
        tpm.adminMode = AdminMode.ENGINEERING
    # Turn on the test signal generator
    print(station.station.ConfigureTestGenerator(json.dumps({"noise_amplitude": 0.25})))
    # Yield station to be used in tests
    yield station
    # Turn off the signal generator
    print(station.station.ConfigureTestGenerator(json.dumps({})))
    # Tear down return back to online mode
    for tpm in tpms:
        tpm.adminMode = AdminMode.ONLINE

    apply_unity_cal(station_name)


@pytest.fixture(scope="module")
def cal_store_with_lenient_selection_policy(station_name):
    """
    Sets the calibration store selection policy to be extremely lenient.
    Returns the selection policy to an impossible standard once the test finishes.

    When the lenient selection policy is active, regardless of frequency and
    temperature, we choose the most recent calibration.

    When the lenient selection policy is active, regardless of frequency and
    temperature, a unity calibration solution is chosen.
    """
    cal_store = get_mccs_device(f"low-mccs/calibrationstore/{station_name}")

    selection_policy = parse_selection_policy_string(cal_store.selectionPolicy)

    [[result_code], [message]] = cal_store.updateSelectionPolicy(
        json.dumps(
            {
                "policy_name": "closest_in_range",
                "frequency_tolerance": 1000,
                "temperature_tolerance": 2000.0,
            }
        )
    )

    if not result_code == 0:
        raise ValueError(
            f"UpdateSelectionPolicy failed. MccsStation returned message: {message}"
        )

    yield cal_store

    # Account for the rare case where the selection policy has never been set.
    # TODO: MCCS 0.23 should set this to a default on deployment.
    if selection_policy:
        [[result_code], [message]] = cal_store.updateSelectionPolicy(
            json.dumps(selection_policy)
        )

        if not result_code == 0:
            raise ValueError(
                f"UpdateSelectionPolicy failed. MccsStation returned message: {message}"
            )
    else:
        reset_selection_policy(station_name)


@pytest.fixture(name="antennas_to_test", scope="module")
def fixture_antennas_to_test(station_name) -> list[int]:
    """
    Return a list of antennas to test masking.

    We choose every second antenna.
    """
    antenna_df = get_station_antenna_dataframe(station_name)
    return list(antenna_df["antenna_name"][::2])
