import pytest

from aiv_utils.low_utils import calculate_pst_cfreq_bandwidth

pytestmark = pytest.mark.unit

test_calculate_pst_cfreq_bandwidth_data = [
    # ( sps_a_channel, sps_b_channel, expected_freq, expected_bw )
    (224, 231, 177732567.0, 6250000.0),
    (64, 447, 199607567.0, 300000000.0),
    (383, 384, 299607567.0, 1562500.0),
    (160, 351, 199607567.0, 150000000.0),
]


@pytest.mark.parametrize(
    "sps_a_channel,sps_b_channel,expected_freq,expected_bw",
    test_calculate_pst_cfreq_bandwidth_data,
)
def test_calculate_pst_cfreq_bandwidth(
    sps_a_channel,
    sps_b_channel,
    expected_freq,
    expected_bw,
):
    (freq, bw) = calculate_pst_cfreq_bandwidth(sps_a_channel, sps_b_channel)
    print(f"{freq=} vs {expected_freq} {bw=} vs {expected_bw}")
    assert freq == expected_freq, f"expected {freq} == {expected_freq}"
    assert bw == expected_bw, f"expected {bw} == {expected_bw}"
