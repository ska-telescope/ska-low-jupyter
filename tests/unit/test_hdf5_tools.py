import os

import h5py
import pytest

from aiv_utils.hdf5_tools import insert_hdf5_metadata

pytestmark = pytest.mark.unit

test_insert_hdf5_metadata_data = [
    # Basic example with ICRS coordinates
    (
        "ICRS",
        "riley.keel",
        123.45,
        "08:13:36.000",
        -45.00,
        "-45:00:00.000",
        None,
        None,
        "NGC254",
        "Checking tracking",
        "Attempt 985 to get a good track on this target",
        "LCO-40",
    ),
    # Basic example with AltAz coordinates
    (
        "AltAz",
        "riley.keel",
        None,
        None,
        None,
        None,
        45.00,
        180.00,
        "NGC254",
        "Checking tracking",
        "Attempt 985 to get a good track on this target",
        "LCO-40",
    ),
    # Basic example with ICRS coordinates but only degrees
    (
        "ICRS",
        "riley.keel",
        123.45,
        None,
        -45.00,
        None,
        None,
        None,
        "NGC254",
        "Checking tracking",
        "Attempt 985 to get a good track on this target",
        "LCO-40",
    ),
    # Basic example with ICRS coordinates but only strings
    (
        "ICRS",
        "riley.keel",
        None,
        "08:13:36.000",
        None,
        "-45:00:00.000",
        None,
        None,
        "NGC254",
        "Checking tracking",
        "Attempt 985 to get a good track on this target",
        "LCO-40",
    ),
]


@pytest.mark.parametrize(
    "reference_frame,observer,ra_deg,ra_str,dec_deg,dec_str,alt_deg,az_deg,target_name,intent,notes,jira_ticket",
    test_insert_hdf5_metadata_data,
)
def test_insert_hdf5_metadata(
    correlation_burst_hdf5_path,
    reference_frame,
    observer,
    ra_deg,
    ra_str,
    dec_deg,
    dec_str,
    alt_deg,
    az_deg,
    target_name,
    intent,
    notes,
    jira_ticket,
):
    # Insert the metadata
    insert_hdf5_metadata(
        hdf5_path=correlation_burst_hdf5_path,
        reference_frame=reference_frame,
        observer=observer,
        ra_deg=ra_deg,
        ra_str=ra_str,
        dec_deg=dec_deg,
        dec_str=dec_str,
        alt_deg=alt_deg,
        az_deg=az_deg,
        target_name=target_name,
        intent=intent,
        notes=notes,
        jira_ticket=jira_ticket,
    )

    # Check that the metadata was inserted correctly
    with h5py.File(correlation_burst_hdf5_path, "r") as hdf5_file:
        hdf5_metadata = hdf5_file["observation_info"].attrs

        assert hdf5_metadata["reference_frame"] == reference_frame
        if observer is not None:
            assert hdf5_metadata["observer"] == observer
        if target_name is not None:
            assert hdf5_metadata["target_name"] == target_name
        if intent is not None:
            assert hdf5_metadata["intent"] == intent
        if notes is not None:
            assert hdf5_metadata["notes"] == notes
        if jira_ticket is not None:
            assert hdf5_metadata["jira_ticket"] == jira_ticket
        if reference_frame == "ICRS":
            # Only check their types because conversions can be made (get ra_str from ra_deg)
            assert isinstance(hdf5_metadata["ra"], float)
            assert isinstance(hdf5_metadata["ra_str"], str)
            assert isinstance(hdf5_metadata["dec"], float)
            assert isinstance(hdf5_metadata["dec_str"], str)
        elif reference_frame == "AltAz":
            assert hdf5_metadata["alt"] == alt_deg
            assert hdf5_metadata["az"] == az_deg


test_insert_hdf5_metadata_fails_data = [
    # No reference frame
    (
        None,
        "riley.keel",
        123.45,
        "08:13:36.000",
        -45.00,
        "-45:00:00.000",
        None,
        None,
    ),
    # Alt and Az for an ICRS reference frame
    (
        "ICRS",
        "riley.keel",
        None,
        None,
        None,
        None,
        45.00,
        180.00,
    ),
    # ra dec with with AltAz reference frame
    (
        "AltAz",
        "riley.keel",
        123.45,
        "08:13:36.000",
        -45.00,
        "-45:00:00.000",
        None,
        None,
    ),
    # Only an Alt value
    (
        "AltAz",
        "riley.keel",
        None,
        None,
        None,
        None,
        45.00,
        None,
    ),
    # No ra
    (
        "ICRS",
        "riley.keel",
        None,
        None,
        -45.00,
        "-45:00:00.000",
        None,
        None,
    ),
    # No dec
    (
        "ICRS",
        "riley.keel",
        123.45,
        "08:13:36.000",
        None,
        None,
        None,
        None,
    ),
]


@pytest.mark.parametrize(
    "reference_frame,observer,ra_deg,ra_str,dec_deg,dec_str,alt_deg,az_deg",
    test_insert_hdf5_metadata_fails_data,
)
def test_insert_hdf5_metadata_fails(
    correlation_burst_hdf5_path,
    reference_frame,
    observer,
    ra_deg,
    ra_str,
    dec_deg,
    dec_str,
    alt_deg,
    az_deg,
):
    with pytest.raises(ValueError) as e_info:  # noqa: F841
        # Insert the metadata and see if it raises a ValueError as expected
        insert_hdf5_metadata(
            hdf5_path=correlation_burst_hdf5_path,
            reference_frame=reference_frame,
            observer=observer,
            ra_deg=ra_deg,
            ra_str=ra_str,
            dec_deg=dec_deg,
            dec_str=dec_str,
            alt_deg=alt_deg,
            az_deg=az_deg,
        )


test_insert_hdf5_metadata_no_observer_data = [
    # No observer or other optional data
    (
        "ICRS",
        None,
        123.45,
        "08:13:36.000",
        -45.00,
        "-45:00:00.000",
        None,
        None,
        None,
        None,
        None,
        None,
    ),
]


@pytest.mark.parametrize(
    "reference_frame,observer,ra_deg,ra_str,dec_deg,dec_str,alt_deg,az_deg,target_name,intent,notes,jira_ticket",
    test_insert_hdf5_metadata_no_observer_data,
)
def test_insert_hdf5_metadata_no_observer(
    correlation_burst_hdf5_path,
    reference_frame,
    observer,
    ra_deg,
    ra_str,
    dec_deg,
    dec_str,
    alt_deg,
    az_deg,
    target_name,
    intent,
    notes,
    jira_ticket,
):
    jupyterhub_user = os.getenv("JUPYTERHUB_USER")
    if jupyterhub_user is None:
        # No user so no observer can be set by default so test should fail
        with pytest.raises(ValueError) as e_info:  # noqa: F841
            # Insert the metadata and see if it raises a ValueError as expected
            insert_hdf5_metadata(
                hdf5_path=correlation_burst_hdf5_path,
                reference_frame=reference_frame,
                observer=observer,
                ra_deg=ra_deg,
                ra_str=ra_str,
                dec_deg=dec_deg,
                dec_str=dec_str,
                alt_deg=alt_deg,
                az_deg=az_deg,
            )
    else:
        # There is a user so test should pass and do other checks

        # Insert the metadata
        insert_hdf5_metadata(
            hdf5_path=correlation_burst_hdf5_path,
            reference_frame=reference_frame,
            observer=observer,
            ra_deg=ra_deg,
            ra_str=ra_str,
            dec_deg=dec_deg,
            dec_str=dec_str,
            alt_deg=alt_deg,
            az_deg=az_deg,
            target_name=target_name,
            intent=intent,
            notes=notes,
            jira_ticket=jira_ticket,
        )

        # Check that the metadata was inserted correctly
        with h5py.File(correlation_burst_hdf5_path, "r") as hdf5_file:
            hdf5_metadata = hdf5_file["observation_info"].attrs

            assert hdf5_metadata["reference_frame"] == reference_frame
            # Only check their types because conversions can be made (get ra_str from ra_deg)
            assert isinstance(hdf5_metadata["ra"], float)
            assert isinstance(hdf5_metadata["ra_str"], str)
            assert isinstance(hdf5_metadata["dec"], float)
            assert isinstance(hdf5_metadata["dec_str"], str)
