import numpy as np
import pytest
from astropy import units as u
from astropy.coordinates import EarthLocation, SkyCoord
from astropy.time import Time

from aiv_utils.sky_simulate import predict_observation_spectrum, predict_on_off_sources

pytestmark = pytest.mark.unit


def test_predict_on_off_sources():
    # Run predict_on_off_sources for a single frequency at a time the Galactic centre is up
    scan_freqs = [[50.0, 125]]
    text_utc = "2024-11-22T06:20:00"
    df = predict_on_off_sources(
        "s8-6",
        scan_freqs,
        test_utc=text_utc,
    )
    on_coord = SkyCoord(
        ra=df["RA (deg)"][0] * u.deg, dec=df["Declination (deg)"][0] * u.deg
    )
    off_coord = SkyCoord(
        ra=df["RA (deg)"][1] * u.deg, dec=df["Declination (deg)"][1] * u.deg
    )
    gc_coord = SkyCoord(
        ra=266.404988286 * u.deg, dec=-29.007810222 * u.deg, frame="icrs"
    )

    # Check on source is the Galactic centre
    assert on_coord.separation(gc_coord) < 3 * u.deg
    # Check off source is not the Galactic centre
    assert off_coord.separation(gc_coord) > 3 * u.deg


def test_predict_observation_spectrum(fixture_dir):
    # Use a known observation to test predict_observation_spectrum
    freq_array = np.array([100.0])
    time_utc = Time("2024-11-22 05:29:52", format="iso")
    sky_elevation = 84.34897394351779
    sky_azimuth = 111.47400291164813
    station_rotation = 193.6
    antenna_x_locations_m = np.load(f"{fixture_dir}/s8-6_antenna_x_locations_m.npy")
    antenna_y_locations_m = np.load(f"{fixture_dir}/s8-6_antenna_y_locations_m.npy")
    station_site = EarthLocation(
        lat=-26.855106431 * u.deg, lon=116.730025424 * u.deg, height=330.297 * u.m
    )
    predicted_spectrum_x = 7087.352
    predicted_spectrum_y = 7082.500

    calculated_spectrum_x, calculated_spectrum_y = predict_observation_spectrum(
        freq_array,
        time_utc,
        station_site,
        sky_elevation,
        sky_azimuth,
        antenna_x_locations_m,
        antenna_y_locations_m,
        station_rotation,
        eep_path=fixture_dir,
        file_name_part_1="HARP_SKALA41_randvogel_avg_",
    )
    assert np.isclose(calculated_spectrum_x, predicted_spectrum_x, rtol=1e-3)
    assert np.isclose(calculated_spectrum_y, predicted_spectrum_y, rtol=1e-3)
