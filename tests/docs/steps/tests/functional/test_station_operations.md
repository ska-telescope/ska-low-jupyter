# Test Steps and Scenarios from tests/functional/test_station_operations.py

Last updated on: 17 February 2025 04:44:31

## Scenarios

- Turn field nodes on (`test_field_nodes_on`)
- Initialise single station (`test_initialise_single_station`)
- Equalise the station (`test_equalise`)
- Acquire raw data (`test_acquire_raw_data`)
- Acquire channelised data (`test_acquire_channelised_data`)
- Acquire correlator frequency sweep data (`test_acquire_correlator_frequency_sweep_data`)
- Acquire beamformed data (`test_acquire_beamformed_data`)
- Test ability to calibrate station (`test_calibrate_station`)
- Test ability to mask antennas (`test_mask_antennas`)
- Test ability to store and retrieve calibration (`test_store_retrieve`)
- Test ability to calibrate from store (`test_cal_from_store`)

## Steps

### When: I turn on a field node

**Function:** `turn_field_nodes_on`

**Signature:**
```python
def turn_field_nodes_on(output_dir, station_name):
```

**Description:**
Turn a station's field node on

The step uses `papermill` to run the Vulcan maintained `FieldNodeOn.ipynb` notebook.
This should be ideally accomplished with FieldStation.On().
This notebook will attempt to get a field node into the state where the
FNDH and all smartboxes are initialised and all FEM ports on, with a current trip
threshold set to a given value.

---

### Then: the FNDH ports are on

**Function:** `field_nodes_on`

**Signature:**
```python
def field_nodes_on(powered_fieldnode):
```

**Description:**
Check that the FNDH ports are on by asserting `pasdbus.fndhstatus == "OK"`.

---

### Given: tango database is reachable

**Function:** `check_database`

**Signature:**
```python
def check_database():
```

**Description:**
Check if the Tango database is reachable by running `tango.Database()`.

---

### When: I initialise a station

**Function:** `initialise_station`

**Signature:**
```python
def initialise_station(output_dir, station_name):
```

**Description:**
Initialise the station using the Initialise notebook.

The fixture uses `papermill` to run the Vulcan-maintained
`Initialise.ipynb` notebook.

---

### Then: the station is initialised

**Function:** `station_initialised`

**Signature:**
```python
def station_initialised(station):
```

**Description:**
Read-only method that asserts that a station is initialised.

---

### Given: an initialised station

**Function:** `station_initialised`

**Signature:**
```python
def station_initialised(station):
```

**Description:**
Read-only method that asserts that a station is initialised.

---

### When: apply equalisation to the station

**Function:** `equalise`

**Signature:**
```python
def equalise(output_dir, station_name):
```

**Description:**
Equalise the stations ADC power levels.

The step uses `papermill` to run the Vulcan maintained `Equalise.ipynb` notebook.
Measures the ADC power level over all TPMS then tries to equalise them by adjusting
their preADU attenuators.

---

### Then: the median station power for each tpm is within 1 dB

**Function:** `equalised`

**Signature:**
```python
def equalised(station):
```

**Description:**
Check that ADC power for each TPM input is either <1, or within 1 of target.

---

### When: I send raw data to the DAQ

**Function:** `send_raw_data`

**Signature:**
```python
def send_raw_data(output_dir, station_name):
```

**Description:**
Acquire raw data the acquire raw notebook.

The step uses `papermill` to run the Vulcan maintained `AcquireRaw.ipynb` notebook.
This notebook dumps raw voltage samples using `SpSstation.SendDataSamples()`.

---

### Then: I capture raw voltage data from the station

**Function:** `raw_data_acquired`

**Signature:**
```python
def raw_data_acquired(output_dir, n_tpms):
```

**Description:**
Checks a raw data file is created for each tile in the station.

---

### When: I send channelised data to the DAQ

**Function:** `send_channelised_data`

**Signature:**
```python
def send_channelised_data(output_dir, station_name):
```

**Description:**
Acquire channelised data the acquire channelised notebook.

The step uses `papermill` to run the Vulcan-maintained `AcquireChannelised.ipynb`
notebook. This notebook dumps channelised voltage samples with the
`SpSstation.SendDataSamples()` method.

---

### Then: I capture burst channelised data from the station

**Function:** `channelised_data_acquired`

**Signature:**
```python
def channelised_data_acquired(output_dir, n_tpms):
```

**Description:**
Checks a channelised data file is created for each tile in the station.

---

### When: I send correlator data to the DAQ from channel {start_chan:d} to {stop_chan:d}

**Function:** `send_frequency_sweep_data`

**Signature:**
```python
def send_frequency_sweep_data(output_dir, station_name, start_chan, stop_chan):
```

**Description:**
Acquire frequency sweep data the acquire frequency sweep notebook.

The step uses `papermill` to run the Vulcan maintained `FrequencySweep.ipynb`
notebook. This notebook uses the `SpSstation.SendDataSamples()` method to iterate
over the input channel range and dump a specified number of channelised voltage
samples for each coarse channel.

---

### Then: I capture correlator data from the station

**Function:** `frequency_sweep_data_acquired`

**Signature:**
```python
def frequency_sweep_data_acquired(output_dir, expected_channels):
```

**Description:**
Checks a channelised data file is created for each of the input frequency channels.

---

### When: I send beamformed data to the DAQ with {cal_type} calibration

**Function:** `send_beamformed_data`

**Signature:**
```python
def send_beamformed_data(output_dir, station_name, n_tpms, cal_type):
```

**Description:**
Acquire beamformed data the acquire beamformed notebook.

The step uses `papermill` to run the Vulcan maintained `AcquireBeamformed.ipynb`
notebook. Uses the `Station.TrackObject()` and `Station.Scan()` methods to acquire
beamformed data for a station.

---

### Then: I capture beamformed data from the station

**Function:** `beamformed_data_acquired`

**Signature:**
```python
def beamformed_data_acquired(beamform_subdir):
```

**Description:**
Checks there is a station beam data file.

---

### Given: the signal generator is on

**Function:** `is_signal_generator_on`

**Signature:**
```python
def is_signal_generator_on(station_with_signal_generator):
```

**Description:**
Assert the signal generator is on from the station_with_signal_generator fixture.

Make sure the signal generator is on with the`SpsStation.ConfigureTestGenerator()`
command` to ensure the even when there are no signal generators attached to the
ITF tpms that this test can still be run.

---

### Then: I observe there is {amount} {pol} pol data in the beamformed data

**Function:** `check_calibrated_beamformed_data`

**Signature:**
```python
def check_calibrated_beamformed_data(beamform_subdir, amount, pol):
```

**Description:**
Checks that the beamformed data file has the right amount of data in it.

---

### When: I calibrate with some antennas on

**Function:** `calibrate_with_some_antennas`

**Signature:**
```python
def calibrate_with_some_antennas(station_name, n_tpms, antennas_to_test):
```

**Description:**
Calibrate the station with specific antennas having unity calibration, the rest
having zero calibration.

---

### When: I mask those antennas

**Function:** `mask_antennas`

**Signature:**
```python
def mask_antennas(station_name, antennas_to_test):
```

**Description:**
Mask specific antennas by setting them to zeroed calibration.

---

### When: I store a {cal_type} calibration solution

**Function:** `store_cal`

**Signature:**
```python
def store_cal(station_name, n_ant, cal_type):
```

**Description:**
Store a specific type of calibration in the store.

---

### Then: I retrieve a {cal_type} calibration solution

**Function:** `retrieve_cal`

**Signature:**
```python
def retrieve_cal(station_name, n_ant, cal_type):
```

**Description:**
Retrieve a solution from the store and check that it is the right type.

---

### When: I clear the calibration coefficients

**Function:** `clear_cal_coefficients`

**Signature:**
```python
def clear_cal_coefficients(station_name):
```

**Description:**
Clear the calibration coefficients.
We need to do this for calibration store testing because the calibration store can
only see the real antennas, not the tpm simulated ones.

---

### Given: the selection policy is lenient

**Function:** `is_selection_policy_lenient`

**Signature:**
```python
def is_selection_policy_lenient(cal_store_with_lenient_selection_policy):
```

**Description:**
Assert that the selection policy is lenient.

---

