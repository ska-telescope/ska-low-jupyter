# Test Steps and Scenarios from tests/functional/test_tmc_scans.py

Last updated on: 17 February 2025 04:44:31

## Scenarios

- two-station visibility capture (`test_two_station_visibility_tmc`)
- two-station voltage capture (`test_two_station_voltage_tmc`)
- manual calibration and masking on two stations (`test_two_station_calibrated_tmc`)

## Steps

### Given: two initialised stations with signal generators on

**Function:** `step_two_stations`

**Signature:**
```python
def step_two_stations(two_stations):
```

**Description:**
Pick two stations and assert that they are ready to use for observing.

---

### When: I run a {obs_type} scan using TMC of coarse channels {start_chan:d} to {stop_chan:d}

**Function:** `scan_with_tmc`

**Signature:**
```python
def scan_with_tmc(output_dir, subarray_id, subarray_beam_id, two_stations, obs_type, start_chan, stop_chan):
```

**Description:**
Scan with two stations using TMC given a start_chan and stop_chan.

start_chan is inclusive, stop_chan is exclusive.

---

### Then: a measurement set is created

**Function:** `check_measurement_set`

**Signature:**
```python
def check_measurement_set(successful_scan_info):
```

**Description:**
Check that a measurement set exists at the expected path.

---

### Then: the measurement set has the expected dimensions and frequency content

**Function:** `ms_has_expected_metadata`

**Signature:**
```python
def ms_has_expected_metadata(successful_scan_info):
```

**Description:**
Check that the data in the measurement set has the expected shape.

---

### Then: there is {amount} data in the measurement set

**Function:** `check_measurement_set_data`

**Signature:**
```python
def check_measurement_set_data(successful_scan_info, amount):
```

**Description:**
Check that there is the measurement set contains data, or all zero data.

---

### When: I run a masked and zero calibrated scan using TMC

**Function:** `scan_with_tmc_calibrated_and_masked`

**Signature:**
```python
def scan_with_tmc_calibrated_and_masked(output_dir, subarray_id, subarray_beam_id, two_stations):
```

**Description:**
Scan with two stations using TMC with one station zero calibrated and the

---

### Then: the voltage data files are created

**Function:** `check_voltage_data_files`

**Signature:**
```python
def check_voltage_data_files(successful_scan_info):
```

**Description:**
Check that there are dada files in the data and weights subdirectories.

---

### Then: the data-product YAML has the expected time and frequency dimensions of coarse channels {start_chan:d} to {stop_chan:d}

**Function:** `check_voltage_data_dimensions`

**Signature:**
```python
def check_voltage_data_dimensions(successful_scan_info, start_chan, stop_chan):
```

**Description:**
Check the output ska-data-product.yaml header file to confirm the observation
has the expected time and frequency dimensions.

---

