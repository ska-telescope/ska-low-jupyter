# Feature
## Feature: scanning using TMC

Test end-to-end data acquisition using TMC for control.


@XTP-75814 @XTP-75809
### Scenario: two-station visibility capture

- Given two initialised stations with signal generators on
- When I run a visibility scan using TMC of coarse channels 96 to 143
- Then a measurement set is created
- And the measurement set has the expected dimensions and frequency content
- And there is nonzero data in the measurement set

@XTP-75814 @XTP-75810
### Scenario: manual calibration and masking on two stations

- Given two initialised stations with signal generators on
- When I run a masked and zero calibrated scan using TMC
- Then a measurement set is created
- And the measurement set has the expected dimensions and frequency content
- And there is zero data in the measurement set

@XTP-75814 @XTP-75811
### Scenario: two-station voltage capture

- Given two initialised stations with signal generators on
- When I run a voltage scan using TMC of coarse channels 96 to 103
- Then the voltage data files are created
- And the data-product YAML has the expected time and frequency dimensions of coarse channels 96 to 103