# Feature
## Feature: Station operations
Test the basic operations of a station using the operation notebooks

Each scenario covers a different operational notebook to confirm that
they run successfully and produce the expected output.

@XTP-62364 @XTP-62525
### Scenario: Turn field nodes on

- When I turn on a field node
- Then the FNDH ports are on

@XTP-62364 @XTP-62524
### Scenario: Initialise single station

- Given tango database is reachable
- When I initialise a station
- Then the station is initialised

@XTP-62364 @XTP-62528
### Scenario: Equalise the station

- Given an initialised station
- When apply equalisation to the station
- Then the median station power for each tpm is within 1 dB

@XTP-62364 @XTP-62367
### Scenario: Acquire raw data

- Given an initialised station
- When I send raw data to the DAQ
- Then I capture raw voltage data from the station

@XTP-62364 @XTP-62522
### Scenario: Acquire channelised data

- Given an initialised station
- When I send channelised data to the DAQ
- Then I capture burst channelised data from the station

@XTP-62364 @XTP-62523
### Scenario: Acquire correlator frequency sweep data

- Given an initialised station
- When I send correlator data to the DAQ from channel 64 to 68
- Then I capture correlator data from the station

@XTP-62364 @XTP-63557
### Scenario: Acquire beamformed data

- Given an initialised station
- When I send beamformed data to the DAQ with no calibration
- Then I capture beamformed data from the station

@XTP-62364 @XTP-75006
### Scenario: Test ability to calibrate station

- Given an initialised station
- And the signal generator is on
- When I send beamformed data to the DAQ with zeroed calibration
- Then I capture beamformed data from the station
- And I observe there is no x pol data in the beamformed data
- And I observe there is no y pol data in the beamformed data
- When I send beamformed data to the DAQ with file calibration
- Then I capture beamformed data from the station
- And I observe there is some x pol data in the beamformed data
- And I observe there is some y pol data in the beamformed data

@XTP-62364 @XTP-75007
### Scenario: Test ability to mask antennas

- Given an initialised station
- And the signal generator is on
- When I calibrate with some antennas on
- And I mask those antennas
- And I send beamformed data to the DAQ with no calibration
- Then I capture beamformed data from the station
- And I observe there is no x pol data in the beamformed data
- And I observe there is no y pol data in the beamformed data

@XTP-62364 @XTP-75008
### Scenario: Test ability to store and retrieve calibration

- Given the selection policy is lenient
- When I store a x pol only calibration solution
- Then I retrieve a x pol only calibration solution

@XTP-62364 @XTP-75009
### Scenario: Test ability to calibrate from store

- Given an initialised station
- And the signal generator is on
- And the selection policy is lenient
- When I store a y pol only calibration solution
- And I clear the calibration coefficients
- And I send beamformed data to the DAQ with stored calibration
- Then I capture beamformed data from the station
- And I observe there is no x pol data in the beamformed data
- And I observe there is some y pol data in the beamformed data