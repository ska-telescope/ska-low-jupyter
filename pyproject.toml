[tool.poetry]
name = "ska-low-tests"
version = "0.1.0"
description = "Dependencies for ska-low-tests"
authors = ["Alan Chen <alan.chen@csiro.au>"]
readme = "README.md"
packages = [
    { include = "aiv_utils", from = "src" },
]
include = [
    'src/aiv_utils/data/*',
]

[[tool.poetry.source]]
name = "skao"
url = "https://artefact.skao.int/repository/pypi-internal/simple"
priority = "primary"

[[tool.poetry.source]]
name = "PyPI"
priority = "supplemental"

[build-system]
requires = ["poetry-core>=1.2.0"]
build-backend = "poetry.core.masonry.api"

[tool.poetry.scripts]
run_notebooks = "aiv_utils.scripts.run_notebooks:main"
is_calendar_booked = "aiv_utils.scripts.is_calendar_booked:main"
process_dada = "aiv_utils.scripts.process_dada:main"

[tool.poetry.dependencies]
python = ">=3.11,<3.12"
ska-low-cbf-integration = {git = "https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-integration.git", rev = "1d426810cf2bf192798f4ff3fe4a3eea6d5bd0f3"}
ska-low-mccs-calibration = "^0.4.0"
ska-control-model = "^1.2.0"
ska-k8s-config-exporter = "^0.5.1"
ska-oso-scripting = "^10.4.0"
ska-sdp-config = "^0.6.0"
ska-ser-logging = "^0.4.1"
ska-telmodel = "^1.19.7"
pytango = "^9.5.1"
pytest-jira-xray = "^0.8.10"
icalendar = "^5.0.12"
toolz = "^0.12.1"
pyvisa = "^1.14.1"
aipy = "^3.0.5"
pyserial = "^3.5.0"
pyvisa-py = "^0.7.1"
dsconfig = "^1.7.1"
humanize = "^4.9.0"
backoff = "^2.2.1"
diptest = "^0.8.0"
recurring-ical-events = "^2.2.1"
pyuvdata = "^3.1.3"
python-casacore = {version = "^3.5.2", platform = "linux"}
joblib = "^1.4.0"
papermill = "^2.5.0"
xlsxwriter = "^3.2.0"
matplotlib = "^3.9.0"
ska-ost-low-uv = {git = "https://github.com/ska-sci-ops/aa_uv.git"}
netmiko = "^4.4.0"

# lockfile and future are unlisted dependencies of aavs-system/pyaavs
aavs-system = {git = "https://gitlab.com/ska-telescope/aavs-system.git", subdirectory = "python"}
lockfile = "^0.12.2"
future = "^1.0.0"
psycopg = {extras = ["binary"], version = "^3.2.2"}
sqlalchemy = "^2.0.35"
async-timeout = "^4.0.3"
geopandas = "^1.0.1"
alphashape = "^1.3.1"
fastapi = {extras = ["standard"], version = ">=0.111.0"}
more-itertools = "^10.6.0"
marimo = "^0.10.9"
jupyter-marimo-proxy = "^0.0.4"


[tool.poetry.group.docs]
optional = true

[tool.poetry.group.docs.dependencies]
Sphinx = "^6.2"
ska-ser-sphinx-theme = "^0.1.3"
sphinx-autodoc-typehints = "^1.19"
typing-extensions = "^4.7.1"

[tool.poetry.group.dev.dependencies]
black = "^23.3.0"
flake8 = "^6.0.0"
isort = "^5.12.0"
pylint = "^2.17.4"
pylint-junit = "^0.3.2"
pytest = "^7.3.2"
pytest-cov = "^4.1.0"
pytest-json-report = "^1.5.0"
pytest-timeout = "^2.3.1"
flake8-pyproject = "^1.2.3"
nbqa = "^1.7.0"
pytest-bdd = "^5.0.0"
ipykernel = "^6.29.5"
ska-ser-xray = "^0.6.0"
pytest-bdd-report = "1.0.3b0"


[tool.poetry.group.jupyterhub]
optional = true

[tool.poetry.group.jupyterhub.dependencies]
itango = "^0.2.0"
jupyterlab-execute-time = "^3.2.0"
jupyterlab-h5web = "^12.3.0"
jupyterhub = "^5.2.1"
jupyter-server = "^2.10.0"


# Linting configuration

[tool.isort]
float_to_top = true
split_on_trailing_comma = true

[tool.flake8]
extend-ignore = "E501"

[tool.pylint]
disable = [
    "logging-fstring-interpolation",
    "fixme",
    "unspecified-encoding",
]
ignored-modules = "astropy.units"

[tool.nbqa.addopts]
flake8 = [
    "--extend-ignore=E402,E501,E203,E731,E722"
]
# E402: Module import not at top of file - picked up by pylint and ignored per file
# E501: Line too long - picked up by pylint and ignored per file
# E203: whitespace before ':' - fights with autoformatter
# E731 do not assign a lambda expression, use a def
# E722 do not use bare 'except'
pylint = [
    """--disable=pointless-statement,
    too-many-lines,
    too-many-statements,
    too-many-locals,
    too-many-arguments,
    missing-module-docstring,
    missing-class-docstring,
    missing-function-docstring,
    broad-exception-caught,
    broad-exception-raised,
    expression-not-assigned,
    duplicate-code,
    invalid-name,
    import-error,
    no-name-in-module,
    redefined-outer-name"""
]




[tool.nbqa.exclude]
isort = "^notebooks/.*/executions/.*"
flake8 = "^notebooks/.*/executions/.*"
pylint = "^notebooks/.*/executions/.*"
black = "^notebooks/.*/executions/.*"


# Pytest configuration

[tool.pytest.ini_options]
bdd_features_base_dir = "tests/features"
testpaths = "tests"
addopts = ["--verbose", "--strict-markers"]
xfail_strict = true
markers = [
    "itf",
    "sft",
    "operations",
    "tmc_scans",
    "unit",
    "bug",
    "XTP-62364",
    "XTP-62525",
    "XTP-62524",
    "XTP-62528",
    "XTP-62367",
    "XTP-62522",
    "XTP-62523",
    "XTP-63557",
    "XTP-75006",
    "XTP-75007",
    "XTP-75008",
    "XTP-75009",
    "XTP-75814",
    "XTP-75809",
    "XTP-75810",
    "XTP-75811",
]

