# This file is mostly copied from https://gitlab.com/ska-telescope/mccs/ska-low-mccs/-/blob/main/cache_data.py
# The following code downloads data which needs to be cached before we deploy.
# If we don't, GitLab runners are likely to timeout on the PyGDSM import. 

from astropy.time.core import Time

# Import ska_low_mccs_calibration, which will import PyGDSM as a dependency
# which will do all necessary downloading. 
from ska_low_mccs_calibration import sky_model

# This will trigger download of the IERS earth rotation dataset
_ = Time.now().ut1
