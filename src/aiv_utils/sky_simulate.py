"""This module contains functions to simulate the sky and make predictions for
various tests.
"""

import os

# pylint: disable=too-many-lines
from datetime import UTC, datetime, timedelta

import astropy
import healpy as hp
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pygdsm
import scipy.constants
from astropy import units as u
from astropy.coordinates import AltAz, EarthLocation, SkyCoord, get_sun
from astropy.coordinates.name_resolve import NameResolveError
from astropy.time import Time
from numpy import ma
from pygdsm import GlobalSkyModel16
from scipy.interpolate import CubicSpline

from aiv_utils.load_data import get_antenna_s_param, get_bandpass_function
from aiv_utils.metadata import get_station_location

EEP_PATH = os.getenv("EEP_PATH", None)
EEP_FILE_NAME = os.getenv("EEP_FILE_NAME", None)

BRIGHT_SRCS = np.array(
    [
        "Cas A",
        "CygA",
        "TauA",
        "VirA",
        "HerA",
        "SagA",
        "CenA",
        "HydA",
        "Vela Pulsar",
        "ForA",
        "PicA",
        "Orion",
        "LMC",
    ]
)

PI = np.pi
CVEL = scipy.constants.c
KB = scipy.constants.k


def get_source_coordinates(src_list: np.ndarray) -> tuple[list, list]:
    """Get the galactic coordinates of a list of source names.

    :param src_list: List of source names
    """
    # to get coordinates of bright sources used for plotting
    gal_long = np.zeros(len(src_list))
    gal_lat = np.zeros(len(src_list))
    for i, src in enumerate(src_list):
        src_coord = SkyCoord.from_name(src).transform_to("galactic")
        gal_long[i] = src_coord.l.deg
        gal_lat[i] = src_coord.b.deg
    return gal_long, gal_lat


def get_map_parameters(map_nside):
    """Get the IDs and celestial coordinates of each pixel in a healpix map.

    :param map_nside: The nside of the healpix map
    """
    # get IDs and RA and dec of each map pixel
    ids = np.arange(0, hp.nside2npix(map_nside))
    glat, glon = hp.pix2ang(
        map_nside, ids, lonlat=True
    )  # galactic latitude and longitude of pixels
    gal_coord = SkyCoord(glat, glon, unit=u.degree, frame="galactic")
    coords = gal_coord.transform_to("icrs")
    return ids, coords


def plot_on_off_sources(  # pylint: disable=too-many-arguments,too-many-locals,
    nside_dg: int,
    freq: float,
    temp_map_dg: np.ndarray,
    nside: int,
    pixel_src: int,
    time: datetime,
    scan_type: str,
    sun_altaz: SkyCoord,
    bright_srcs: np.ndarray = None,
    bright_srcs_l: np.ndarray = None,
    bright_srcs_b: np.ndarray = None,
):
    """Plot the on and off sources on the sky map.

    :param nside_dg: The nside of the downgraded healpix map.
    :param freq: The frequency of the observation.
    :param temp_map_dg: The downgraded healpix map.
    :param nside: The nside of the healpix map.
    :param pixel_src: The pixel of the source.
    :param time: The time of the observation.
    :param scan_type: The type of the scan.
    :param sun_altaz: The altitude and azimuth of the Sun.
    :param bright_srcs: The list of bright sources to plot on the sky map.
    :param bright_srcs_l: The galactic longitudes of the bright sources.
    :param bright_srcs_b: The galactic latitudes of the bright sources.
    """

    # plot selected pixel and other bright sources
    hp.mollview(
        temp_map_dg,
        norm="log",
        title=rf"Low-resolution map (~{hp.nside2resol(nside_dg, arcmin=True) / 60.0:.1f}$^{{\circ}}$) at {freq:.1f} MHz of observable sky (el. > 45$^{{\circ}}$)",  # pylint: disable=line-too-long
    )

    hp.projplot(
        hp.pix2ang(nside, pixel_src, lonlat=True), "rx", lonlat=True, markersize=10
    )

    if bright_srcs_l is not None:
        # Only add named sources if they exist
        hp.projscatter(
            bright_srcs_l,
            bright_srcs_b,
            lonlat=True,
            s=15,
            facecolors="none",
            edgecolors="w",
        )
        for i, bright_src in enumerate(bright_srcs):
            hp.projtext(
                bright_srcs_l[i], bright_srcs_b[i], bright_src[0:5], lonlat=True
            )
    hp.projscatter(
        sun_altaz.galactic.l.deg,
        sun_altaz.galactic.b.deg,
        lonlat=True,
        s=15,
        facecolors="none",
        edgecolors="w",
    )
    hp.projtext(sun_altaz.galactic.l.deg, sun_altaz.galactic.b.deg, "Sun", lonlat=True)

    time_str = datetime.strftime(time, "%Y-%m-%dT%H:%M")
    plt.savefig(f"point_track_1_{scan_type}_{time_str}.svg")


def get_scan_parameters(  # pylint: disable=too-many-arguments,too-many-locals,too-many-statements
    time: datetime,
    scan_type: str,
    freq: float,
    station_coord: EarthLocation,
    ra_prev: float = None,
    dec_prev: float = None,
    max_duration: int = None,
    on_off_dist: float = 2.0,
    sun_dist: float = 4.0,
    bright_srcs: np.ndarray = BRIGHT_SRCS,
    bright_srcs_l=None,
    bright_srcs_b=None,
    plot_results: bool = False,
    t_rec: float = 40.0,
):
    """Get the scan parameters for a given time and frequency.

    :param time: The datetime of the scan start time in UTC.
    :param scan_type: 'on' or 'off'.
        For the 'on' scan type, the function will select the hottest observable
        sky position. For the 'off' scan type, the coldest sky position will be
        selected.
    :param freq: Mean frequency of the scan (in MHz).
    :param station_coord: The EarthLocation of the station.
    :param ra_prev: If this is an off scan, supply RA of previous on-scan to
        avoid choosing an off scan location within `on_off_dist` beam widths.
    :param dec_prev: If this is an off scan, supply dec of previous on-scan
        to avoid choosing an off scan location within `on_off_dist` beam widths.
    :param max_duration: The maximum duration of the scan in minutes.
    :param on_off_dist: The distance in beam widths to avoid selecting an off
        scan location close to the on scan location.
    :param sun_dist: The distance in beam widths to avoid selecting an on or off
        scan location close to the Sun.
    :param bright_srcs: The list of bright sources to plot on the sky map.
    :param bright_srcs_l: The galactic longitudes of the bright sources.
    :param bright_srcs_b: The galactic latitudes of the bright sources.
    :param plot_results: Whether to plot the results.
    :param t_rec: The receiver temperature in K.
    """

    print("Start time:", datetime.strftime(time, "%Y-%m-%d %H:%M"))

    # generate all-sky temperature map at mean frequency
    model = pygdsm.LowFrequencySkyModel()
    temp_map = model.generate(freq)
    nside = hp.npix2nside(len(temp_map))
    # IDs and celestial coordinates of map pixels
    pixel_ids, cel_coord = get_map_parameters(nside)
    # reduce map resolution to roughly the size of the station beam
    # half-power beam width in degrees at zenith, as defined in SKAO-CSP_ASS-316
    beam_width = 5.9 * (100 / freq)
    # assuming beam is a two-dimensional, elliptical Gaussian function
    station_beam_area = np.pi * beam_width**2 / (4 * np.log(2))
    # picking an nside parameter so that map resolution is not worse than station beam
    possible_nsides = 2 ** np.arange(1, 9)
    nside_dg = possible_nsides[
        hp.nside2pixarea(possible_nsides, degrees=True) < station_beam_area
    ][0]
    # print ("Map resolution = %.2f sq. deg."%hp.nside2pixarea(nside_dg, degrees=True))
    temp_map_dg = hp.ud_grade(temp_map, nside_dg)  # downgrade map in resolution
    pixel_ids_dg, cel_coord_dg = get_map_parameters(nside_dg)

    # get azimuth and altitude of map pixels at time of scan
    altaz = cel_coord_dg.transform_to(AltAz(obstime=time, location=station_coord))
    temp_map_dg[
        altaz.alt.deg < 45.0
    ] = np.nan  # setting pixels that are at lower altitude than 45 deg. to zero.

    # if this is an off-scan,
    # then avoid choosing location within x beam widths of on-scan.
    if ra_prev is not None:
        on_location = SkyCoord(
            ra_prev, dec_prev, unit=(u.degree, u.degree), frame="icrs"
        )
        sep_on_location = cel_coord_dg.separation(on_location).deg
        temp_map_dg[sep_on_location < on_off_dist * beam_width] = np.nan

    # on and off source both need to be away from sun
    time_astropy = Time(
        datetime.strftime(time, "%Y-%m-%dT%H:%M"), format="isot", scale="utc"
    )  # needed for correct astropy conversion
    sun_altaz = get_sun(time_astropy).transform_to(
        AltAz(obstime=time, location=station_coord)
    )
    # the next statement might seem redundant but is necessary
    # for correct astropy conversions
    sun_altaz = SkyCoord(
        alt=sun_altaz.alt,
        az=sun_altaz.az,
        obstime=time,
        frame="altaz",
        location=station_coord,
    )
    sep_sun = altaz.separation(sun_altaz).deg
    temp_map_dg[sep_sun < sun_dist * beam_width] = np.nan

    # Also make sure the sidelobes aren't near the sun
    circles = sun_side_lobe_exclusion(freq)
    if circles is not None:
        circle_radius_in, circle_radius_out = circles
        print(f"Removing between {circle_radius_in} & {circle_radius_out} deg")
        mask = (sep_sun > circle_radius_in) & (sep_sun < circle_radius_out)
        print(mask)
        temp_map_dg[mask] = np.nan

    # get coordinates in downgraded map with maximum temperature
    if scan_type == "on":
        tsky = np.nanmax(temp_map_dg)
        pixel_src_dg = np.nanargmax(temp_map_dg)
    elif scan_type == "off":
        tsky = np.nanmin(temp_map_dg)
        pixel_src_dg = np.nanargmin(temp_map_dg)

    # get coordinates in original resolution map with maximum temperature -
    # i.e. pick brightest pixel from the high resolution map in the area
    # covered by the low resolution superpixel
    npix_ratio = len(pixel_ids) // len(pixel_ids_dg)
    temp_map_nest = hp.reorder(temp_map, r2n=True)
    pixel_ids_nest = hp.reorder(pixel_ids, r2n=True)
    pixel_ids_dg_n2r = hp.reorder(pixel_ids_dg, n2r=True)
    if scan_type == "on":
        idx_src_tmp = np.nanargmax(
            temp_map_nest.reshape(len(temp_map_dg), npix_ratio)[
                pixel_ids_dg_n2r[pixel_src_dg]
            ]
        )
    elif scan_type == "off":
        idx_src_tmp = np.nanargmin(
            temp_map_nest.reshape(len(temp_map_dg), npix_ratio)[
                pixel_ids_dg_n2r[pixel_src_dg]
            ]
        )
    pixel_src = pixel_ids_nest.reshape(len(pixel_ids_dg), npix_ratio)[
        pixel_ids_dg_n2r[pixel_src_dg]
    ][idx_src_tmp]
    print(
        f"To observe: RA = {cel_coord[pixel_src].ra.deg:.4f} deg.,",
        f" Dec. = {cel_coord[pixel_src].dec.deg:.4f} deg.",
    )

    if scan_type == "on":
        # Duration of on-source scan = transit time across two beam widths
        # (at mean observing frequency)
        scan_duration = int(
            2
            * beam_width
            * 1440
            / (np.cos(np.deg2rad(cel_coord[pixel_src].dec.deg)) * 360.0)
        )
        if max_duration:
            scan_duration = min(max_duration, scan_duration)
    elif scan_type == "off":
        if not max_duration:
            scan_duration = 5
        else:
            scan_duration = min(max_duration, 5)
    print(f"Duration: = {scan_duration} min")

    if plot_results:
        plot_on_off_sources(
            nside_dg,
            freq,
            temp_map_dg,
            nside,
            pixel_src,
            time,
            scan_type,
            sun_altaz,
            bright_srcs,
            bright_srcs_l,
            bright_srcs_b,
        )

    return (
        cel_coord[pixel_src].ra.deg,
        cel_coord[pixel_src].dec.deg,
        scan_duration,
        tsky + t_rec,
        time,
    )


def predict_on_off_sources(  # pylint: disable=too-many-arguments,too-many-locals,too-many-statements
    station_name: str,
    scan_freqs: list[list[float]],
    bright_srcs: np.ndarray = BRIGHT_SRCS,
    test_utc: datetime = None,
    buffer_min: float = 2.0,
    max_duration: float = None,
    on_off_dist: float = 2.0,
    sun_dist: float = 4.0,
    plot_results: bool = False,
    t_rec: float = 40.0,
):
    """Predict the on and off source locations for a given station and frequency.

    :param station_name: The name of the station (e.g. 's8-1', 's8-6').
    :param scan_freqs: A list of frequency ranges to scan where each list is a
        list of the min and max frequency in MHz.
    :param bright_srcs: A list of bright sources to plot on the sky map.
    :param test_utc: The time of the scan in UTC.
    :param buffer_min: The buffer time between scans in minutes.
    :param max_duration: The maximum duration of the scan in minutes.
    :param on_off_dist: The distance in beam widths to avoid selecting an off
        scan location close to the on scan location.
    :param sun_dist: The distance in beam widths to avoid selecting an on or off
        scan location close to the Sun.
    :param t_rec: The receiver temperature in K.
    """

    if test_utc is None:
        # None supplied so do it in 5 mins
        test_utc = (datetime.now(UTC) + timedelta(minutes=5)).strftime(
            "%Y-%m-%dT%H:%M:%S"
        )

    # get bright source locations for plotting
    try:
        bright_srcs_l, bright_srcs_b = get_source_coordinates(bright_srcs)
    except NameResolveError:
        # Normally a connection error but only needed for the plot
        # so just leave as Nones
        bright_srcs_l = None
        bright_srcs_b = None

    station_lat, station_lon, station_elevation = get_station_location(station_name)
    # define station astropy object
    station_coord = EarthLocation(
        lat=station_lat * u.degree,
        lon=station_lon * u.degree,
        height=station_elevation * u.meter,
    )

    # create data frame to output the calculated observation metadata
    obs_df = pd.DataFrame(
        columns=[
            "RA (deg)",
            "Declination (deg)",
            "Duration (min)",
            "Type (on/off)",
            "Expected Tsys (K)",
        ]
    )

    duration_off = None  # overwritten later
    for i, scan_freq in enumerate(scan_freqs):
        start_freq, end_freq = scan_freq[0], scan_freq[1]
        mean_freq = np.mean([start_freq, end_freq])

        if i == 0:
            start_time = datetime.strptime(test_utc, "%Y-%m-%dT%H:%M:%S")
        else:
            # if this is not the first scan, increase time by duration
            # of previous scan and allow for a few minutes buffer
            start_time += timedelta(minutes=duration_off + buffer_min)

        # select on-source location and transit time
        ra_on, dec_on, duration_on, tsys_on, start_time_on = get_scan_parameters(
            start_time,
            scan_type="on",
            freq=mean_freq,
            station_coord=station_coord,
            max_duration=max_duration,
            on_off_dist=on_off_dist,
            sun_dist=sun_dist,
            bright_srcs=bright_srcs,
            bright_srcs_l=bright_srcs_l,
            bright_srcs_b=bright_srcs_b,
            plot_results=plot_results,
            t_rec=t_rec,
        )

        # increase time by duration of on scan
        start_time += timedelta(minutes=duration_on + buffer_min)

        # select off-source location (to be observed for 5 minutes)
        ra_off, dec_off, duration_off, tsys_off, start_time_off = get_scan_parameters(
            start_time,
            scan_type="off",
            freq=mean_freq,
            station_coord=station_coord,
            ra_prev=ra_on,
            dec_prev=dec_on,
            max_duration=max_duration,
            on_off_dist=on_off_dist,
            sun_dist=sun_dist,
            bright_srcs=bright_srcs,
            bright_srcs_l=bright_srcs_l,
            bright_srcs_b=bright_srcs_b,
            plot_results=plot_results,
            t_rec=t_rec,
        )

        obs_df = pd.concat(
            [
                # previous dataframe
                obs_df,
                # on observation
                pd.DataFrame(
                    {
                        "RA (deg)": [ra_on],
                        "Declination (deg)": [dec_on],
                        "Duration (min)": [duration_on],
                        "Type (on/off)": ["on"],
                        "Expected Tsys (K)": [tsys_on],
                        "Start freq (MHz)": [start_freq],
                        "End freq (MHz)": [end_freq],
                        "Start time (UTC)": [start_time_on],
                    }
                ),
                # off observation
                pd.DataFrame(
                    {
                        "RA (deg)": [ra_off],
                        "Declination (deg)": [dec_off],
                        "Duration (min)": [duration_off],
                        "Type (on/off)": ["off"],
                        "Expected Tsys (K)": [tsys_off],
                        "Start freq (MHz)": [start_freq],
                        "End freq (MHz)": [end_freq],
                        "Start time (UTC)": [start_time_off],
                    }
                ),
            ],
            ignore_index=True,
        )
    return obs_df


def gsmodel(frequency_megahertz: np.ndarray) -> np.ndarray:
    """Get the GSM 2016 Global Sky Model for a given frequency.

    :param frequency_megahertz: The frequency to get the sky model for in MHz.
    :return: The Global Sky Model.
    """
    nside_out = 64
    gsm_2016 = GlobalSkyModel16(freq_unit="MHz", interpolation="PCHIP")

    if np.ndim(frequency_megahertz) == 0:
        gsmap = gsm_2016.generate(frequency_megahertz)
        gsmap = hp.pixelfunc.ud_grade(
            gsmap,
            nside_out,
            pess=False,
            order_in="RING",
            order_out="RING",
            power=None,
            dtype=None,
        )

    else:
        gsmap = []
        for freq in frequency_megahertz:
            cgsmap = gsm_2016.generate(freq)
            cgsmap = hp.pixelfunc.ud_grade(
                cgsmap,
                nside_out,
                pess=False,
                order_in="RING",
                order_out="RING",
                power=None,
                dtype=None,
            )
            gsmap.append(cgsmap)

    return gsmap


def get_sel_gsmap(  # pylint: disable=too-many-arguments,too-many-locals
    freq_array: np.ndarray,
    time_utc: astropy.time.Time,
    station_site: astropy.coordinates.EarthLocation,
    station_rotation: float,
) -> tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray, int]:
    """Get expected sky model based on diffused sky and the sun

    :param freq_array: The frequency array to get the sky model for in MHz.
    :param time_utc: The time of the observation in UTC.
    :param station_site: The EarthLocation of the station.
    :param station_rotation: The rotation of the station in degrees.
    :return: The selected sky model, azimuth, elevation, indices, index array, and npix.
    """

    # Model for the Sun
    def cnst1(freq_megahertz):
        return 2.0 * KB * (freq_megahertz * 1e6) ** 2 / (CVEL * CVEL)

    def sun_flux(freq_megahertz):
        return 1.94e-4 * freq_megahertz**1.992 * 1e-22

    # Get a sky model as a healpix array in Galactic coordinate frame
    gsmap = gsmodel(freq_array)
    nside = hp.pixelfunc.get_nside(gsmap[0])
    npix = hp.pixelfunc.get_map_size(gsmap[0])

    # coordinates of GSM sky pixels
    ipix = np.linspace(0, npix - 1, num=npix, endpoint=True, dtype=np.uint32)
    c_coord = hp.pixelfunc.pix2ang(nside, ipix, nest=False, lonlat=True)
    skypos = SkyCoord(l=c_coord[0], b=c_coord[1], frame="galactic", unit="deg")
    skypos_altaz = skypos.transform_to(AltAz(obstime=time_utc, location=station_site))
    elevation_deg = np.array(skypos_altaz.alt.degree)
    azimuth_deg = np.array(skypos_altaz.az.degree)

    # Restrict to elevations above horizon
    sel_indices = np.where((elevation_deg > 0.0))[0]
    sel_elevation_deg = elevation_deg[sel_indices]
    sel_azimuth_deg = azimuth_deg[sel_indices]
    sel_gsmap = np.transpose(np.transpose(gsmap)[sel_indices])

    # Indices to select beam values closest to healpix centres
    rotation_radians = (-(sel_azimuth_deg - station_rotation) + 90.0) * np.pi / 180.0
    newaz_deg = np.mod(rotation_radians, 2 * np.pi) * (180.0 / np.pi)
    iaz = (np.rint(np.round(2.0 * (newaz_deg)))).astype(int)
    iza = (np.rint(np.round(2.0 * (90.0 - np.abs(sel_elevation_deg))))).astype(int)
    index_array_temp = np.array([iaz, iza])
    index_array = np.matrix.transpose(index_array_temp)

    # Sun temperature for the pixel size, frequency
    sun_temperature = (sun_flux(freq_array) / cnst1(freq_array)) / (
        4.0 * PI / float(npix)
    )

    # Sun position at observing time
    sun_skypos = get_sun(time_utc)
    sun_skypos_altaz = sun_skypos.transform_to(
        AltAz(obstime=time_utc, location=station_site)
    )
    sun_elevation_deg = sun_skypos_altaz.alt.degree
    sun_azimuth_deg = sun_skypos_altaz.az.degree

    if sun_elevation_deg > 0.0:
        dist_array = (sel_elevation_deg - sun_elevation_deg) ** 2 + (
            sel_azimuth_deg - sun_azimuth_deg
        ) ** 2
        sun_index = np.argmin(dist_array)
        sel_gsmap_transpose = np.transpose(sel_gsmap)
        sel_gsmap_transpose[sun_index] += sun_temperature
        sel_gsmap = np.transpose(sel_gsmap_transpose)

    return (
        sel_gsmap,
        sel_azimuth_deg,
        sel_elevation_deg,
        sel_indices,
        index_array,
        npix,
    )


def get_eep_patterns_map(  # pylint: disable=too-many-arguments,too-many-locals,too-many-statements
    freq_array: np.ndarray,
    index_array: np.ndarray,
    eep_path: str = EEP_PATH,
    file_name_part_1: str = EEP_FILE_NAME,
):
    """Get the EEP patterns for selected frequencies.

    :param freq_array: The frequency array to get the EEP patterns for in MHz.
    :param index_array: The index array to select beam values
        closest to healpix centres.
    :param eep_path: The path to the EEPs.
    :param file_name_part_1: The start of the EEP file name.
    :return: The X and Y beam patterns of the horizontal and vertical beam.
    """

    # List of available frequencies for PVogel EEPs
    eep_freq = np.arange(50, 351)
    # print("Assume EEPs available at frequencies ",\
    #       eep_freq[0]," to ",eep_freq[-1]," MHz")
    file_name_part_2_x = "MHz_Xpol.npz"
    file_name_part_2_y = "MHz_Ypol.npz"

    beam_pattern_healpix_xh = []
    beam_pattern_healpix_yh = []
    beam_pattern_healpix_xv = []
    beam_pattern_healpix_yv = []

    for frequency_megahertz in freq_array:
        # Get the eep_freq value closest to freq_MHz
        idx = (np.abs(eep_freq - frequency_megahertz)).argmin()
        eep_x_filename = os.path.join(
            eep_path, file_name_part_1 + str(int(eep_freq[idx])) + file_name_part_2_x
        )
        eep_y_filename = os.path.join(
            eep_path, file_name_part_1 + str(int(eep_freq[idx])) + file_name_part_2_y
        )

        eep_model = np.load(eep_x_filename)
        beam_pattern_x_h = np.conjugate(eep_model[eep_model.files[0]])
        beam_pattern_x_v = np.conjugate(eep_model[eep_model.files[1]])
        if "avg" not in file_name_part_1:
            beam_pattern_x_h = np.mean(beam_pattern_x_h, axis=2)
            beam_pattern_x_v = np.mean(beam_pattern_x_v, axis=2)

        eep_model = np.load(eep_y_filename)
        beam_pattern_y_h = np.conjugate(eep_model[eep_model.files[0]])
        beam_pattern_y_v = np.conjugate(eep_model[eep_model.files[1]])
        if "avg" not in file_name_part_1:
            beam_pattern_y_h = np.mean(beam_pattern_y_h, axis=2)
            beam_pattern_y_v = np.mean(beam_pattern_y_v, axis=2)

        # beam patterns towards healpix sky positions, for all antennas (49152)
        beam_pattern_healpix_xh.append(
            [(beam_pattern_x_h[i1, i2]) for (i1, i2) in index_array]
        )
        beam_pattern_healpix_xv.append(
            [(beam_pattern_x_v[i1, i2]) for (i1, i2) in index_array]
        )
        beam_pattern_healpix_yh.append(
            [(beam_pattern_y_h[i1, i2]) for (i1, i2) in index_array]
        )
        beam_pattern_healpix_yv.append(
            [(beam_pattern_y_v[i1, i2]) for (i1, i2) in index_array]
        )

    # Gives healpiz beam patterns xh, yh, xv, yv with shape (freq, pixel, antenna)
    print(
        np.shape(beam_pattern_healpix_xh),
        np.shape(beam_pattern_healpix_yh),
        np.shape(beam_pattern_healpix_xv),
        np.shape(beam_pattern_healpix_yv),
    )

    return (
        beam_pattern_healpix_xh,
        beam_pattern_healpix_yh,
        beam_pattern_healpix_xv,
        beam_pattern_healpix_yv,
    )


def get_beam_map(  # pylint: disable=too-many-arguments,too-many-locals,too-many-statements
    sky_elevation: float,
    sky_azimuth: float,
    sel_elevation_deg: np.ndarray,
    sel_azimuth_deg: np.ndarray,
    freq_array: np.ndarray,
    antenna_x_locations_m: np.ndarray,
    antenna_y_locations_m: np.ndarray,
    station_rotation: float,
    beam_pattern_healpix_xh: np.ndarray,
    beam_pattern_healpix_yh: np.ndarray,
    beam_pattern_healpix_xv: np.ndarray,
    beam_pattern_healpix_yv: np.ndarray,
) -> tuple[np.ndarray, np.ndarray]:
    """Compute station beams as healpix arrays for pixels above horizon

    :param sky_elevation: The elevation of the beam on the sky in degrees.
    :param sky_azimuth: The azimuth of the beam on the sky in degrees.
    :param sel_elevation_deg: The elevation of the healpix sky positions in degrees.
    :param sel_azimuth_deg: The azimuth of the healpix sky positions in degrees.
    :param freq_array: The frequency array to get the beam map for in MHz.
    :param antenna_x_locations_m: The x locations of the antennas in metres.
    :param antenna_y_locations_m: The y locations of the antennas in metres.
    :param station_rotation: The rotation of the station in degrees.
    :param beam_pattern_healpix_xh: The X horizontal beam pattern.
    :param beam_pattern_healpix_yh: The Y horizontal beam pattern.
    :param beam_pattern_healpix_xv: The X vertical beam pattern.
    :param beam_pattern_healpix_yv: The Y vertical beam pattern.
    :return: The X and Y beam maps.
    """
    antenna_number = len(antenna_x_locations_m)

    # Unit vector towards the beam centre
    lla = np.cos(sky_elevation * PI / 180.0) * np.sin(sky_azimuth * PI / 180.0)
    mma = np.cos(sky_elevation * PI / 180.0) * np.cos(sky_azimuth * PI / 180.0)
    nna = np.sin(sky_elevation * PI / 180.0)

    # Unit vectors towards healpix sky positions
    sky_unit_vector_x = np.cos(sel_elevation_deg * PI / 180.0) * np.sin(
        sel_azimuth_deg * PI / 180.0
    )
    sky_unit_vector_y = np.cos(sel_elevation_deg * PI / 180.0) * np.cos(
        sel_azimuth_deg * PI / 180.0
    )
    sky_unit_vector_z = np.sin(sel_elevation_deg * PI / 180.0)

    beam_x_array = []
    beam_y_array = []
    index = 0
    for frequency_megahertz in freq_array:
        frequency = float(frequency_megahertz)
        wavelength = CVEL / (frequency * 1e6)  # metres

        # Baseline vectors to the antenna locations
        antenna_x_baselines = antenna_x_locations_m / wavelength
        antenna_y_baselines = antenna_y_locations_m / wavelength
        antenna_h_baselines = np.zeros(len(antenna_x_baselines), dtype=float)
        # assume for now that all antennas are at same height

        # Phase with which signal from healpix sky position arrives at antennas
        xyz_array = np.array([sky_unit_vector_x, sky_unit_vector_y, sky_unit_vector_z])
        uvw_array = np.matrix.transpose(
            np.array([antenna_x_baselines, antenna_y_baselines, antenna_h_baselines])
        )
        ft_argc = 2.0 * PI * np.matmul(uvw_array, xyz_array)

        # Phase for signals from beam centre
        xyz0 = [lla, mma, nna]
        ft_arg0 = 2.0 * PI * np.matmul(uvw_array, xyz0)

        # Difference phase between healpix sky positions and beam centre,
        # for each antenna for all healpix sky positions (nant, npix)
        ft_diff = [(ft_argc[i] - ft_arg0[i]) for i in range(antenna_number)]

        # Sum the voltages (complex) over all antennas.
        # Arriving at X & Y feeds from H and V sky components separately,
        # correcting for EEPs and path phase
        beam_sum_x_h = (beam_pattern_healpix_xh[index]) * np.mean(
            (np.cos(ft_diff) + 1j * np.sin(ft_diff)), axis=0
        )
        beam_sum_x_v = (beam_pattern_healpix_xv[index]) * np.mean(
            (np.cos(ft_diff) + 1j * np.sin(ft_diff)), axis=0
        )

        beam_sum_y_h = (beam_pattern_healpix_yh[index]) * np.mean(
            (np.cos(ft_diff) + 1j * np.sin(ft_diff)), axis=0
        )
        beam_sum_y_v = (beam_pattern_healpix_yv[index]) * np.mean(
            (np.cos(ft_diff) + 1j * np.sin(ft_diff)), axis=0
        )

        beam_x_h = beam_sum_x_h * np.cos(
            station_rotation * PI / 180.0
        ) + beam_sum_y_h * np.sin(station_rotation * PI / 180.0)
        beam_y_h = -beam_sum_x_h * np.sin(
            station_rotation * PI / 180.0
        ) + beam_sum_y_h * np.cos(station_rotation * PI / 180.0)

        beam_x_v = beam_sum_x_v * np.cos(
            station_rotation * PI / 180.0
        ) + beam_sum_y_v * np.sin(station_rotation * PI / 180.0)
        beam_y_v = -beam_sum_x_v * np.sin(
            station_rotation * PI / 180.0
        ) + beam_sum_y_v * np.cos(station_rotation * PI / 180.0)

        beam_x_array.append(
            np.abs(beam_x_h) * np.abs(beam_x_h) + np.abs(beam_x_v) * np.abs(beam_x_v)
        )
        beam_y_array.append(
            np.abs(beam_y_h) * np.abs(beam_y_h) + np.abs(beam_y_v) * np.abs(beam_y_v)
        )

        index += 1

    return beam_x_array, beam_y_array


def predict_observation_spectrum(  # pylint: disable=too-many-arguments,too-many-locals,too-many-statements
    freq_array: np.ndarray,
    time_utc: astropy.time.Time,
    station_site: EarthLocation,
    sky_elevation: float,
    sky_azimuth: float,
    antenna_x_locations_m: np.ndarray,
    antenna_y_locations_m: np.ndarray,
    station_rotation: float,
    t_rec: float = 40.0,
    eep_path: str = EEP_PATH,
    file_name_part_1: str = EEP_FILE_NAME,
    plot_results: bool = False,
) -> tuple[np.ndarray, np.ndarray]:
    """Predict the observation spectrum for a given station and frequency.

    :param freq_array: The frequencies of the observation in MHz.
    :param time_utc: The time of the observation in UTC.
    :param station_site: The EarthLocation of the station.
    :param sky_elevation: The elevation of the beam on the sky in degrees.
    :param sky_azimuth: The azimuth of the beam on the sky in degrees.
    :param antenna_x_locations_m: The x locations of the antennas in metres.
    :param antenna_y_locations_m: The y locations of the antennas in metres.
    :param station_rotation: The rotation of the station in degrees.
    :param t_rec: The receiver temperature in Kelvin.
    :param eep_path: The path to the EEPs.
    :param file_name_part_1: The beginning of the filename of the EEPs.
    :param plot_results: Whether to plot the results.
    :return: The predicted X and Y spectra.
    """
    # Get expected bandpass function
    bandpass_func = get_bandpass_function()

    # Get the expected bandpass
    bandpass_analog2 = bandpass_func(freq_array)
    bandpass_analog_norm = bandpass_analog2 / np.mean(bandpass_analog2)

    # Start doing the calcs
    (
        sel_gsmap,
        sel_azimuth_deg,
        sel_elevation_deg,
        sel_indices,
        index_array,
        npix,
    ) = get_sel_gsmap(freq_array, time_utc, station_site, station_rotation)

    (
        beam_pattern_healpix_xh,
        beam_pattern_healpix_yh,
        beam_pattern_healpix_xv,
        beam_pattern_healpix_yv,
    ) = get_eep_patterns_map(freq_array, index_array, eep_path, file_name_part_1)

    beam_x_array, beam_y_array = get_beam_map(
        sky_elevation,
        sky_azimuth,
        sel_elevation_deg,
        sel_azimuth_deg,
        freq_array,
        antenna_x_locations_m,
        antenna_y_locations_m,
        station_rotation,
        beam_pattern_healpix_xh,
        beam_pattern_healpix_yh,
        beam_pattern_healpix_xv,
        beam_pattern_healpix_yv,
    )

    # Weighted sum of gsmap pixels using the station beams to get spectra
    spectrum_x = [
        (
            np.average(sel_gsmap[i], weights=beam_x_array[i], axis=0)
            * np.max(beam_x_array[i])
        )
        for i in range(len(sel_gsmap))
    ]
    spectrum_y = [
        (
            np.average(sel_gsmap[i], weights=beam_y_array[i], axis=0)
            * np.max(beam_y_array[i])
        )
        for i in range(len(sel_gsmap))
    ]

    # Get the antenna transfer function
    giorgios_frequency_megahertz, giorgios_power_transfer = get_antenna_s_param()
    # Multiply by antenna transfer function,
    # correction for S11 assuming 50-ohm impedance for LNA.
    giorgios_power_transfer_func = CubicSpline(
        giorgios_frequency_megahertz, giorgios_power_transfer
    )

    antenna_spectrum_x = spectrum_x * giorgios_power_transfer_func(freq_array)
    antenna_spectrum_y = spectrum_y * giorgios_power_transfer_func(freq_array)

    predicted_spectrum_x = (antenna_spectrum_x + t_rec) * bandpass_analog_norm
    predicted_spectrum_y = (antenna_spectrum_y + t_rec) * bandpass_analog_norm

    if plot_results:
        plot_observation_spectrum(
            freq_array,
            sky_elevation,
            sky_azimuth,
            spectrum_x,
            spectrum_y,
            beam_x_array,
            beam_y_array,
            sel_indices,
            np.array(sky_elevation),
            npix,
            antenna_spectrum_x,
            antenna_spectrum_y,
            predicted_spectrum_x,
            predicted_spectrum_y,
            t_rec,
            bandpass_analog_norm,
            time_utc,
            station_site,
        )

    return predicted_spectrum_x, predicted_spectrum_y


def plot_observation_spectrum(  # pylint: disable=too-many-arguments,too-many-locals,too-many-statements
    freq_array,
    sky_elevation: float,
    sky_azimuth: float,
    spectrum_x,
    spectrum_y,
    beam_x_array,
    beam_y_array,
    sel_indices,
    elevation_deg,
    npix,
    antenna_spectrum_x,
    antenna_spectrum_y,
    predicted_spectrum_x,
    predicted_spectrum_y,
    t_rec,
    bandpass_analog_norm,
    time_utc,
    station_site,
):
    """Plot the observation spectrum.

    :param freq_array: The frequencies of the observation in MHz.
    :param sky_elevation: The elevation of the beam on the sky in degrees.
    :param sky_azimuth: The azimuth of the beam on the sky in degrees.
    :param spectrum_x: The X spectrum.
    :param spectrum_y: The Y spectrum.
    :param beam_x_array: The X beam array.
    :param beam_y_array: The Y beam array.
    :param sel_indices: The selected indices.
    :param elevation_deg: The elevation in degrees.
    :param npix: The number of pixels.
    :param antenna_spectrum_x: The X spectrum at the antenna.
    :param antenna_spectrum_y: The Y spectrum at the antenna.
    :param predicted_spectrum_x: The predicted X spectrum.
    :param predicted_spectrum_y: The predicted Y spectrum.
    :param t_rec: The receiver temperature in Kelvin.
    :param bandpass_analog_norm: The normalised bandpass.
    :param time_utc: The time of the observation in UTC.
    :param station_site: The EarthLocation of the station.
    """
    # Display station beams, first and last

    beam_x_display = np.ones(npix, dtype=float)
    np.put(beam_x_display, sel_indices, beam_x_array[0])
    print(elevation_deg.shape, beam_x_display.shape)
    beam_x_display = np.where(
        (elevation_deg <= 0.0), (hp.pixelfunc.UNSEEN), beam_x_display
    )
    mask = np.where((beam_x_display == hp.pixelfunc.UNSEEN), True, False)
    beam_x_display = ma.masked_array(beam_x_display, mask)

    beam_y_display = np.ones(npix, dtype=float)
    np.put(beam_y_display, sel_indices, beam_y_array[0])
    beam_y_display = np.where(
        (elevation_deg <= 0.0), (hp.pixelfunc.UNSEEN), beam_y_display
    )
    mask = np.where((beam_y_display == hp.pixelfunc.UNSEEN), True, False)
    beam_y_display = ma.masked_array(beam_y_display, mask)

    print("Frequency ", freq_array[0], " MHz")
    display_min = -30
    display_max = 0.0
    plt.figure(figsize=[10, 6])
    plt.rcParams.update({"font.size": 8})
    sub = [1, 2, 1]
    hp.mollview(
        10.0 * ma.log10(beam_x_display),
        coord=["G", "C"],  # None,
        norm=None,
        title="Station beam power pattern for X pol (dB scale)",
        cmap="jet",
        min=display_min,
        max=display_max,
        sub=sub,
    )
    hp.graticule()
    sub = [1, 2, 2]
    hp.mollview(
        10.0 * ma.log10(beam_y_display),
        coord=["G", "C"],  # None,
        norm=None,
        title="Station beam power pattern for Y pol (dB scale)",
        cmap="jet",
        min=display_min,
        max=display_max,
        sub=sub,
    )
    hp.graticule()

    # Define the circle parameters
    altaz_frame = AltAz(obstime=time_utc, location=station_site)
    altaz_coord = SkyCoord(
        az=sky_azimuth * u.deg, alt=sky_elevation * u.deg, frame=altaz_frame
    )
    galactic_coord = altaz_coord.transform_to("galactic")
    circle_center = (
        galactic_coord.l.deg,
        galactic_coord.b.deg,
    )  # (longitude, latitude) in degrees
    lon, lat = np.radians(circle_center)

    circles = sun_side_lobe_exclusion(freq_array[0])
    if circles is not None:
        circle_radius_in, circle_radius_out = circles
        # Generate points for the circle
        radius = np.radians(circle_radius_in)
        theta = np.linspace(0, 2 * np.pi, 100)
        circle_lon = lon + radius * np.cos(theta)
        circle_lat = lat + radius * np.sin(theta)
        # Mollweide projection transforms longitude and latitude to x and y
        hp.projplot(
            np.degrees(circle_lon),
            np.degrees(circle_lat),
            lonlat=True,
            coord=["G"],
            color="red",
            lw=2,
        )

        radius = np.radians(circle_radius_out)
        theta = np.linspace(0, 2 * np.pi, 100)
        circle_lon = lon + radius * np.cos(theta)
        circle_lat = lat + radius * np.sin(theta)
        # Mollweide projection transforms longitude and latitude to x and y
        hp.projplot(
            np.degrees(circle_lon),
            np.degrees(circle_lat),
            lonlat=True,
            coord=["G"],
            color="red",
            lw=2,
        )

    plt.show()

    beam_x_display = np.ones(npix, dtype=float)
    np.put(beam_x_display, sel_indices, beam_x_array[-1])
    beam_x_display = np.where(
        (elevation_deg <= 0.0), (hp.pixelfunc.UNSEEN), beam_x_display
    )
    mask = np.where((beam_x_display == hp.pixelfunc.UNSEEN), True, False)
    beam_x_display = ma.masked_array(beam_x_display, mask)

    beam_y_display = np.ones(npix, dtype=float)
    np.put(beam_y_display, sel_indices, beam_y_array[-1])
    beam_y_display = np.where(
        (elevation_deg <= 0.0), (hp.pixelfunc.UNSEEN), beam_y_display
    )
    mask = np.where((beam_y_display == hp.pixelfunc.UNSEEN), True, False)
    beam_y_display = ma.masked_array(beam_y_display, mask)

    print("Frequency ", freq_array[-1], " MHz")
    plt.figure(figsize=[10, 6])
    plt.rcParams.update({"font.size": 8})
    sub = [1, 2, 1]
    hp.mollview(
        10.0 * ma.log10(beam_x_display),
        coord=["G", "C"],  # None,
        norm=None,
        title="Station beam power pattern for X pol (dB scale)",
        cmap="jet",
        min=display_min,
        max=display_max,
        sub=sub,
    )
    hp.graticule()
    sub = [1, 2, 2]
    hp.mollview(
        10.0 * ma.log10(beam_y_display),
        coord=["G", "C"],  # None,
        norm=None,
        title="Station beam power pattern for Y pol (dB scale)",
        cmap="jet",
        min=display_min,
        max=display_max,
        sub=sub,
    )
    hp.graticule()

    plt.savefig("point_track_3_station_beams.svg")
    plt.show()

    plt.figure(figsize=[10, 3])
    plt.rcParams.update({"font.size": 10})
    ax1 = plt.subplot(1, 1, 1)

    ax1.plot(freq_array, spectrum_x, "b-", label="X pol")
    ax1.plot(freq_array, spectrum_y, "r-", label="Y pol")

    plt.ylabel("Predicted spectrum (K)")
    plt.xlabel("Frequency (MHz)")
    plt.legend(loc="best")
    plt.grid()
    plt.show()

    plt.figure(figsize=[10, 3])
    plt.rcParams.update({"font.size": 10})
    ax1 = plt.subplot(1, 1, 1)

    ax1.plot(freq_array, antenna_spectrum_x, "b-", label="X pol")
    ax1.plot(freq_array, antenna_spectrum_y, "r-", label="Y pol")

    plt.ylabel("Predicted spectrum * AntTF (K)")
    plt.xlabel("Frequency (MHz)")
    plt.legend(loc="best")
    plt.grid()
    plt.show()

    # Multiply by analog receiver bandpass function, as measured at ITF

    plt.figure(figsize=[10, 3])
    plt.rcParams.update({"font.size": 10})
    ax1 = plt.subplot(1, 1, 1)

    ax1.plot(freq_array, predicted_spectrum_x, "b-", label="X pol")
    ax1.plot(freq_array, predicted_spectrum_y, "r-", label="Y pol")

    plt.ylabel("Predicted spectrum * AntTF * bandpass (K)")
    plt.xlabel("Frequency (MHz)")
    plt.legend(loc="best")
    plt.grid()
    plt.show()

    # in log-linear domain

    plt.figure(figsize=[10, 4])
    plt.rcParams.update({"font.size": 10})
    ax1 = plt.subplot(1, 1, 1)

    ax1.plot(
        freq_array,
        10.0 * np.log10((antenna_spectrum_x + t_rec) * bandpass_analog_norm),
        "b-",
        label="X pol",
    )
    ax1.plot(
        freq_array,
        10.0 * np.log10((antenna_spectrum_y + t_rec) * bandpass_analog_norm),
        "r-",
        label="Y pol",
    )

    plt.ylabel("10log10 Predicted spectrum * AntTF * bandpass (K)")
    plt.xlabel("Frequency (MHz)")
    plt.legend(loc="best")
    plt.grid()

    plt.savefig("point_track_3_spectrum.svg")
    plt.show()


def sun_side_lobe_exclusion(freq_megahertz):
    """Estimate the inner and outer radius of where the majority of the
    station side lobes will be.

    :param freq_megahertz: Frequency of the observation in megahertz.
    :return lower, upper: The inner and outer radius in degrees or None if the
        freq_megahertz is low enough to not have significant side lobes.
    """
    # Define the known frequency points and their corresponding values
    frequencies = np.array([170, 190, 210, 230, 250, 270, 290, 310, 330, 350])
    lower_bounds = np.array([50, 45, 40, 35, 30, 25, 25, 20, 20, 20])
    upper_bounds = np.array([65, 65, 65, 65, 60, 60, 60, 55, 50, 50])

    if freq_megahertz < 150 or freq_megahertz > 350:
        return None

    # Interpolate for lower and upper bounds
    lower = np.interp(freq_megahertz, frequencies, lower_bounds)
    upper = np.interp(freq_megahertz, frequencies, upper_bounds)

    return lower, upper
