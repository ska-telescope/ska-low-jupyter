"""
Code to simulate faults in the system for testing purposes.
"""

import json
import os
import time
from copy import deepcopy

from ska_sdp_config import Config

from aiv_utils.low_utils import get_cbf_devices


def _update_model(tpm, health_model):
    """
    Update the health model of a TPM

    :param tpm: the TPM device
    :param health_model: the new health model
    """
    tpm.healthmodelparams = json.dumps(health_model)


def fix_tpm(tpm):
    """
    Put the TPM back to normal health rules

    :param tpm: the TPM device
    """
    print(f"putting tpm {tpm.name()} health rules back to normal ..")
    tpm_hm = json.loads(tpm.healthmodelparams)
    good = deepcopy(tpm_hm)
    good["voltages"]["VM_ADA0"] = {"min": 0, "max": 3}
    good["voltages"]["VM_ADA1"] = {"min": 0, "max": 3}
    _update_model(tpm, good)


def trigger_fault(tpm, stay_failed=True, delay_failed=5):
    """
    Trigger a fault in a TPM by changing the health model

    :param tpm: the TPM device
    :param stay_failed: whether to stay in failed state or not
    :param delay_failed: how long in seconds to stay in failed state
    """
    tpm_hm = json.loads(tpm.healthmodelparams)

    bad = deepcopy(tpm_hm)
    bad["voltages"]["VM_ADA0"] = {"min": 0, "max": 0}
    bad["voltages"]["VM_ADA1"] = {"min": 0, "max": 0}
    print(f"Triggering fake fault in TPM {tpm.name()} by changing rules")

    _update_model(tpm, bad)

    if not stay_failed:
        time.sleep(delay_failed)
        fix_tpm(tpm)


def trigger_fault_sdp(subarray_name):
    """
    Manually set the SDP subarray to FAULT state

    :param subarray_name: the name of the subarray
    """
    os.environ["SDP_CONFIG_HOST"] = "ska-sdp-etcd-client.sut"
    for txn in Config().txn():
        subarray = txn.component(subarray_name).get()
        subarray["obs_state_commanded"] = "FAULT"
        txn.component(subarray_name).update(subarray)


def manipulate_sdp(fault=False):
    """
    Cause a real failure in the SDP subsystem.

    :param fault: whether to cause a fault or not
    """

    os.environ["SDP_CONFIG_HOST"] = "ska-sdp-etcd-client.sut"

    for txn in Config().txn():
        for array in ["01", "02", "03", "04"]:
            subarray_name = f"lmc-subarray-{array}"
            subarray = txn.component(subarray_name).get()

            if "health_state_commanded" in subarray:
                del subarray["health_state_commanded"]

            if "state_commanded" in subarray:
                del subarray["state_commanded"]

            if fault:
                subarray["obs_state_commanded"] = "XXX"  # causes the error
                subarray["state_commanded"] = "OFF"  # causes the error
            else:
                subarray["obs_state_commanded"] = "EMPTY"  # back to normal
                subarray["state_commanded"] = "ON"  # back to normal

            txn.component(subarray_name).update(subarray)


def trigger_fault_cbf():
    """
    Topic suggested this as a way of inducing
    a fault _during_ a scan.
    See corr__low_cbf_processor_offline_during_scan_test.py
    in ska-low-csp-test
    """

    cbf = get_cbf_devices()
    for processor in cbf.processors:
        processor.adminMode = 1


class FaultGenerated(Exception):
    """
    Fault generated exception
    """
