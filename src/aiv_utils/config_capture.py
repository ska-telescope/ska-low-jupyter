"""Helper module for configuration in notebooks."""

import json
import os
from typing import Iterable

import tango
import toolz
from dsconfig.dump import get_db_data
from ska_k8s_config_exporter.utils import get_helm_info, get_pod_list, initialise_k8s

__all__ = [
    "dump_configuration",
    "get_tango_config",
    "get_pods",
    "get_charts",
]


@toolz.memoize
def get_current_namespace() -> str:
    """Get the default namespace of the mounted service account, if available."""
    try:
        with open("/var/run/secrets/kubernetes.io/serviceaccount/namespace") as file:
            return file.read().strip()
    except FileNotFoundError:
        return ""


def get_namespace_from_tango_host(tango_host: str) -> str | None:
    """Return a local namespace from a tango_host string, or None."""
    host, _ = split_tango_host(tango_host)
    parts = host.split(".")
    if len(parts) == 2:
        return parts[1]
    return None


def get_namespaces() -> list[str]:
    """Return all namespaces we think we might have access to inspect."""
    return sorted(
        filter(
            None,
            {
                get_current_namespace(),
                get_namespace_from_tango_host(os.getenv("TANGO_HOST", "")),
                get_namespace_from_tango_host(os.getenv("TANGO_HOST_MCCS", "")),
            },
        )
    )


def split_tango_host(tango_host) -> tuple[str, int]:
    """Take a TANGO_HOST string and return the host and port.

    :param tango_host: a string of the form "<hostname>[:<port>]"
    :return: a tuple of hostname and integer port (default 10000)
    """
    host, port, *_ = tango_host.split(":") + ["10000"]
    return host, int(port)


def get_tango_config(
    tango_hosts: list[str] | None = None,
    attributes: Iterable[str] = tuple(),
) -> dict:
    # pylint: disable=too-many-nested-blocks
    """Get tango database device server configuration.

    :param tango_hosts: a list of "<hostname>[:<port>]" strings
    :return: a dictionary containing Tango configurations for each of tango_hosts
    """
    tango_hosts = [os.getenv("TANGO_HOST"), os.getenv("TANGO_HOST_MCCS")]
    config = {}
    for tango_host in filter(None, tango_hosts):
        database = tango.Database(*split_tango_host(tango_host))
        tango_data = get_db_data(database)
        for server in tango_data["servers"].values():
            for instance in server.values():
                for device_class in instance.values():
                    for trl, device in device_class.items():
                        try:
                            dev = tango.DeviceProxy(f"{tango_host}/{trl}")
                            device["attributes"] = {
                                attr: result.value
                                for attr, result in zip(
                                    attributes, dev.read_attributes(attributes)
                                )
                            }
                        except tango.DevFailed:
                            pass
        config[tango_host] = tango_data
    return config


def get_pods(namespaces: list[str]) -> dict:
    """Get the pod configuration for given namespaces.

    :param namespaces: the namespaces to inspect
    :return: a (json) dict
    """
    core_v1_api = initialise_k8s()
    pods_info = {}
    for namespace in namespaces:
        pods_info[namespace] = get_pod_list(core_v1_api, namespace, pip_inspect=False)
    return pods_info


def get_charts(namespaces: list[str]) -> dict:
    """Get the chart configuration for given namespaces.

    :param namespaces: the namespaces to inspect
    :return: a (json) dict
    """
    core_v1_api = initialise_k8s()
    chart_info = {}
    for namespace in namespaces:
        chart_info[namespace] = get_helm_info(core_v1_api, namespace)
    return chart_info


def dump_configuration(
    configuration_filepath: str | os.PathLike,
    namespaces: list[str] | None = None,
) -> None:
    """
    Captures the state of the K8s and Tango environment in a JSON file
    written to configuration_filepath.
    """
    namespaces = namespaces or get_namespaces()

    charts = get_charts(namespaces)
    pods = get_pods(namespaces)
    tango_config = get_tango_config(
        attributes=["state", "status", "healthState", "versionId", "adminMode"],
    )
    config = {"charts": charts, "pods": pods, "tango": tango_config}

    with open(configuration_filepath, "w") as config_file:
        json.dump(config, config_file, indent=2, sort_keys=True)
