"""
Simple script to see if a calander is booked
"""

import argparse
import sys

from aiv_utils.test_utils import is_calendar_booked

EXPECTED_RUNTIME_MINS = 30


def main():  # pylint: disable=R0914,C0116
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--calendar",
        type=str,
        choices=["ITF", "AA0.5"],
        default="ITF",
        help="Calendar to check (either ITF or AA0.5)",
    )
    parser.add_argument(
        "--expected-runtime-mins",
        type=float,
        default=EXPECTED_RUNTIME_MINS,
        help="Expected runtime in minutes, "
        "will check if calendar is booked for this duration",
    )
    args = parser.parse_args()

    if is_calendar_booked(args.calendar, args.expected_runtime_mins):
        print(
            "Calendar is booked with events that cannot be ignored - skipping execution"
        )
        sys.exit(1)
    else:
        print("Calendar is not booked")


if __name__ == "__main__":
    main()
