"""
A script that is used on station deployment to generate a GeoJSON file that
represents the layout of the smartboxes in the station.
This GeoJSON file is used in Grafana to display which antennas belong to which smartbox.
"""

import argparse
import json
import os

import geopandas as gpd
import matplotlib.pyplot as plt
import numpy as np
from alphashape import alphashape
from fastapi import FastAPI

from aiv_utils.metadata import get_all_stations, load_station_metadata

app = FastAPI()


@app.get("/smartbox_geojson")
def read_smartbox_layouts():
    """Fastapi function to get a GeoJSON dict for all stations."""
    telmodel_source = os.getenv("TELMODEL_SOURCE")
    output_geojson, _ = get_smartbox_layouts(telmodel_source)
    return output_geojson


def get_smartbox_layouts(telmodel_source):  # pylint: disable=R0914
    """Generate a GeoJSON dict for all stations using the telmodel data source."""
    # Get the list of stations
    stations = get_all_stations(telmodel_source)
    print(f"Found {len(stations)} stations: {stations}")

    # Grab the position data
    filtered_data = {}
    for station_name in stations:
        antennas_data = load_station_metadata(station_name, telmodel_source)["antennas"]
        for antenna_name in antennas_data.keys():
            antenna_data = antennas_data[antenna_name]
            if "smartbox" not in antenna_data:
                continue
            smartbox_label = f"{station_name}_{antenna_data['smartbox']}"
            if smartbox_label in filtered_data:
                filtered_data[smartbox_label][antenna_name] = {
                    "longitude": antenna_data["position"]["longitude"],
                    "latitude": antenna_data["position"]["latitude"],
                }
            else:
                filtered_data[smartbox_label] = {
                    antenna_name: {
                        "longitude": antenna_data["position"]["longitude"],
                        "latitude": antenna_data["position"]["latitude"],
                    }
                }

    # Convert to GeoJSON format and plot
    output_geojson = {
        "type": "FeatureCollection",
        "features": [],
    }
    alpha = 0.33
    plot_data = {}
    for smartbox in filtered_data:  # pylint: disable=C0206
        x_plot = []
        y_plot = []
        box_points = []
        for ant in filtered_data[smartbox]:
            x_plot.append(filtered_data[smartbox][ant]["longitude"])
            y_plot.append(filtered_data[smartbox][ant]["latitude"])
            box_points.append(
                [
                    filtered_data[smartbox][ant]["longitude"],
                    filtered_data[smartbox][ant]["latitude"],
                ]
            )
        if "ska-low-tmdata" in telmodel_source:
            alpha_shape = alphashape(
                np.array(box_points),
                alpha,
            )
        elif "ska-low-itf" in telmodel_source:
            # Because there are only two points,
            # adjust the points to make a box around them
            line_array = np.array(box_points)
            max_long = np.max(line_array[:, 0])
            min_long = np.min(line_array[:, 0])
            max_lat = np.max(line_array[:, 1])
            min_lat = np.min(line_array[:, 1])
            move_by = 0.000002
            box_points = [
                [max_long + move_by, max_lat + move_by],
                [max_long + move_by, min_lat - move_by],
                [min_long - move_by, min_lat - move_by],
                [min_long - move_by, max_lat + move_by],
            ]
            alpha_shape = alphashape(np.array(box_points), alpha)

        alpha_shape_x = []
        alpha_shape_y = []
        alpha_shape_long_lat = []
        for coord in alpha_shape.exterior.coords:
            alpha_shape_x.append(coord[0])
            alpha_shape_y.append(coord[1])
            alpha_shape_long_lat.append((coord[0], coord[1]))
        plot_data[smartbox] = {
            "alpha_x": alpha_shape_x,
            "alpha_y": alpha_shape_y,
            "x": x_plot,
            "y": y_plot,
        }

        output_geojson["features"].append(
            {
                "type": "Feature",
                "properties": {
                    "name": smartbox,
                },
                "geometry": {
                    "type": "Polygon",
                    "coordinates": [alpha_shape_long_lat],
                },
            }
        )
    return output_geojson, plot_data


def main():  # pylint: disable=R0914
    """Generate a GeoJSON file for all stations in the input telmodel data source."""
    # Parse the command line arguments
    parser = argparse.ArgumentParser(
        description="Generate a GeoJSON file for all stations in the input telmodel data source"  # pylint: disable=C0301
    )
    parser.add_argument(
        "-s",
        "--source",
        help="The telmodel data source, eg: gitlab://gitlab.com/ska-telescope/ska-low-tmdata?main#tmdata",  # pylint: disable=C0301
        default="gitlab://gitlab.com/ska-telescope/ska-low-tmdata?main#tmdata",
    )
    args = parser.parse_args()

    output_geojson, plot_data = get_smartbox_layouts(args.source)

    for smartbox, smartbox_data in plot_data.items():
        plt.scatter(smartbox_data["x"], smartbox_data["y"], label=smartbox)
        plt.plot(smartbox_data["alpha_x"], smartbox_data["alpha_y"])

    plt.gca().set_aspect("equal", adjustable="box")
    plt.legend(loc="upper left", bbox_to_anchor=(1.05, 1.15))
    plt.savefig("smartbox_layouts.png")

    with open("smartbox_layouts.json", "w") as file:
        json.dump(output_geojson, file, indent=4)

    # Plot the GeoDataFrame
    gdf = gpd.read_file("smartbox_layouts.json")
    gdf.plot()
    plt.show()


if __name__ == "__main__":
    main()
