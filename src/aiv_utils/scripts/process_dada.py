"""
Wrapper script to install a bash script on the PATH through poetry
"""

import os
import subprocess
import sys


def main():
    """Main function that calls a bash script using subprocess"""
    script_path = os.path.join(os.path.dirname(__file__), "process_dada.sh")
    subprocess.run(["bash", script_path, *sys.argv[1:]], check=True)
