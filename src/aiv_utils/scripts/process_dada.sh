#!/usr/bin/env bash

# Default values
pulsar="J1752-2806"
output_base_dir="/home/jovyan/shared/pst_processed"
eb_name="eb-notebook-20250213-89497"
label=""

# Help message function
usage() {
    echo "Usage: $0 [options]"
    echo ""
    echo "Options:"
    echo "  -p PULSAR    Pulsar J name (default: J1752-2806)"
    echo "  -d OUTPUT    The output base director which a sub dir for the obs will be made (default: /home/jovyan/shared/nick)"
    echo "  -e EB_NAME   The execution block name (default: eb-notebook-20250213-89497)"
    echo "  -l LABEL     Label to give the output dir to describe the obs (default: "")"
    echo "  -h           Show this help message and exit"
    echo ""
    exit 0
}

# Parse arguments
while getopts ":p:d:e:l:h" opt; do
    case ${opt} in
        p ) pulsar="$OPTARG" ;;
        d ) output_base_dir="$OPTARG" ;;
        e ) eb_name="$OPTARG" ;;
        l ) label="$OPTARG" ;;
        h ) usage ;;
        \? ) echo "Invalid option: -$OPTARG" >&2; usage ;; # Handle invalid options
    esac
done

# Shift arguments so that remaining arguments (if any) can be processed
shift $((OPTIND -1))

echo "Processing pulsar: $pulsar"

dada_dir="/home/jovyan/sdp-data/${eb_name}/pst-low/1/data"
weights_dir="/home/jovyan/sdp-data/${eb_name}/pst-low/1/weights"
dada_file_count=$(ls -1 ${dada_dir}/*.dada 2>/dev/null | wc -l)

echo "Found ${dada_file_count} dada files"

output_dir="${output_base_dir}/${pulsar}_${eb_name}"
if [[ -n "$label" ]]; then
    # Added label if given
    output_dir="${output_dir}_${label}"
fi
mkdir -p ${output_dir}

echo "Outputing data to ${output_dir}"

psrcat -e ${pulsar} > ${output_dir}/${pulsar}.par


# generate the dualfile.list input file list for DSPSR
ls -1 ${dada_dir}/*.dada | tail -n +2 | head -n -1 > ${output_dir}/data.list
ls -1 ${weights_dir}/*.dada | tail -n +2 | head -n -1 > ${output_dir}/weights.list
echo "DUAL FILE:" > ${output_dir}/dualfile.list
echo "${output_dir}/data.list" >> ${output_dir}/dualfile.list
echo "${output_dir}/weights.list" >> ${output_dir}/dualfile.list


archive_name=${output_dir}/${pulsar}_${eb_name}
if [[ -n "$label" ]]; then
    # Added label if given
    archive_name="${archive_name}_${label}"
fi
dspsr -E ${output_dir}/${pulsar}.par -O ${archive_name} -b 256 -U 2000 -k mwa -L 30  ${output_dir}/dualfile.list
# rm ${archive_name}_0*ar

# plot time vs phase
psrplot -p time -jFDp -D /png ${archive_name}.ar
mv pgplot.png ${archive_name}_time_vs_phase.png

# plot freq vs phase
psrplot -p freq+ -jDTp -D /png ${archive_name}.ar
mv pgplot.png ${archive_name}_freq_vs_phase.png