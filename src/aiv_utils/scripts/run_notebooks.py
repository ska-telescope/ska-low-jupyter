"""
Script for running SFT notebooks.

More details and documentation is available in the docs
https://developer.skao.int/projects/ska-low-tests/en/latest/developer/index.html#sft-notebooks
"""
import argparse
import glob
import json
import logging
import os
import sys
from base64 import b64encode
from datetime import datetime
from pathlib import Path

import papermill as pm
from pytest_xray.constant import TEST_EXECUTION_ENDPOINT
from pytest_xray.helper import Status, TestCase, TestExecution
from pytest_xray.xray_publisher import ApiKeyAuth, XrayPublisher
from pytz import timezone

from aiv_utils.config_capture import dump_configuration
from aiv_utils.test_utils import is_calendar_booked

EXPECTED_RUNTIME_MINS = 60

JIRA_TOKEN = os.getenv("JIRA_TOKEN")

publisher = XrayPublisher(
    base_url="https://jira.skatelescope.org",
    endpoint=TEST_EXECUTION_ENDPOINT,
    auth=ApiKeyAuth(JIRA_TOKEN),
)


def export_output_variables(notebook_name: str, results_path: os.PathLike) -> None:
    """Import output variables from previous tests into environment."""
    with open(results_path) as results_file:
        results = json.load(results_file)
        outputs_dict = results.get("outputs", {})
        for output_key, value in outputs_dict.items():
            key = f"output_{notebook_name.replace('.', '_')}_{output_key}"
            os.environ[key] = value
            print(f"set env var {key} = {value}")


def get_test_case(
    results_path: os.PathLike, configuration_path: os.PathLike
) -> TestCase:
    """
    Return a pytest_xray TestCase,
    populated from the result JSON file at results_path
    and the configuration capture file at configuration_path.
    """
    with open(results_path) as results_file:
        results = json.load(results_file)
    test_case = TestCase(
        test_key=results["test_key"],
        status=Status.PASS if results["status"] == "passed" else Status.FAIL,
        comment=results["comments"],
        evidences=results["evidences"],
    )

    with open(configuration_path, "rb") as config_file:
        config_data = config_file.read()
    config_evidence = {
        "filename": os.path.basename(configuration_path),
        "contentType": "application/json",
        "data": b64encode(config_data).decode("utf-8"),
    }

    test_case.evidences.append(config_evidence)
    return test_case


def create_test_execution(
    test_case: TestCase, results_path: os.PathLike
) -> TestExecution:
    """
    Create a test execution containing the given test_case,
    with metadata populated from the result JSON file at results_path.
    """
    with open(results_path) as results_file:
        results = json.load(results_file)
    return TestExecution(
        test_plan_key=results["test_plan_key"],
        tests=[test_case],
        test_environments=results["test_environments"],
        summary=results["summary"],
        description=results["description"],
    )


def check_is_test_ready_for_upload(results_path: os.PathLike):
    """
    Check if the test results JSON file is marked as ready for upload.
    """
    with open(results_path) as results_file:
        results = json.load(results_file)
    return results["is_ready_to_upload"]


def main():  # pylint: disable=too-many-statements, too-many-locals, too-many-branches # TODO
    """
    Defines a command-line interface for running notebook-based tests in SKA Low.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--output-folder",
        type=Path,
        default="/home/jovyan/shared/test_executions",
        help="The base directory to store test execution results. "
        "Within this folder subfolders will be created for each test execution.",
    )
    parser.add_argument(
        "--station-name",
        type=str,
        required=True,
        help="Name of the station (e.g. s10-3, s8-6 or itf1).",
    )
    parser.add_argument(  # pylint: disable=duplicate-code
        "--calendar",
        type=str,
        choices=["ITF", "AA0.5"],
        help="Calendar to check (either ITF or AA0.5).",
    )
    parser.add_argument(  # pylint: disable=duplicate-code
        "--expected-runtime-mins",
        type=float,
        default=EXPECTED_RUNTIME_MINS,
        help="Expected runtime in minutes, "
        "will check if calendar is booked for this duration.",
    )
    parser.add_argument(
        "--early-stop",
        action=argparse.BooleanOptionalAction,
        default=False,
        help="If set, will stop execution if any notebook fails.",
    )
    parser.add_argument(
        "--progress-bars-only",
        action=argparse.BooleanOptionalAction,
        default=False,
        help="If set, will only show progress bars and not stdout/stderr.",
    )
    parser.add_argument(
        "-p",
        "--parameters",
        nargs=2,
        action="append",
        default=[],
        help="Can be called multiple times to pass parameter key pairs to the notebook."
        " For example: -p CALIBRATION_TYPE SUN_FAST -p CAPTURED_DATA_PATH /data/path.",
    )
    parser.add_argument(
        "test_globs",
        nargs="*",
        help="Glob patterns to match test notebooks to run in order of file name."
        " For example: notebooks/SFT/04*/*.ipynb notebooks/SFT/05*/*.ipynb",
    )
    args = parser.parse_args()

    print(args)

    if args.progress_bars_only:
        stdout_file = None
        stderr_file = None
        progress_bar = True
    else:
        stdout_file = sys.stdout
        stderr_file = sys.stderr
        progress_bar = False

    if args.calendar:
        if is_calendar_booked(args.calendar, args.expected_runtime_mins):
            print(
                "Calendar is booked with events that cannot be ignored "
                "- skipping execution"
            )
            sys.exit(1)
        else:
            print("Calendar is not booked")

    print(args.test_globs)
    notebook_list = [
        path for _glob in args.test_globs for path in sorted(glob.glob(_glob))
    ]
    print(notebook_list)

    start_time = datetime.now(tz=timezone("Australia/Perth"))
    execution_id = f"{args.station_name}_{start_time.strftime('%Y-%m-%d_%H%M%S')}"

    for notebook_path in notebook_list:
        nb_name = os.path.basename(notebook_path)
        name, _ = os.path.splitext(nb_name)

        execution_folder = Path(args.output_folder) / name / execution_id
        os.makedirs(execution_folder)

        results_file = execution_folder / "results.json"
        configuration_file = execution_folder / "configuration.json"

        try:
            print(f"\n\n****** RUNNING NOTEBOOK {name} ******")
            print("dumping configuration ..")
            try:
                dump_configuration(configuration_file)
            except:  # noqa: E722  #pylint: disable=bare-except  # TODO
                print("could not dump config")

            print(f"Executing {nb_name} in {execution_folder} ...")
            error = False
            error_message = ""
            default_params = {
                "STATION_NAME": args.station_name,
                "station_name": args.station_name,
                "OPERATIONS_NOTEBOOK_DIR": os.getcwd() + "/notebooks/operations/",
            }

            def try_parse(parameter):
                try:
                    return int(parameter)
                except ValueError:
                    try:
                        return float(parameter)
                    except ValueError:
                        if str(parameter).upper() in ["TRUE", "FALSE"]:
                            return str(parameter).upper() == "TRUE"
                        return parameter

            params = dict(args.parameters)
            parsed_params = {k: try_parse(v) for k, v in params.items()}
            try:
                pm.execute_notebook(
                    input_path=notebook_path,
                    output_path=execution_folder / nb_name,
                    cwd=execution_folder,
                    parameters={**default_params, **parsed_params},
                    log_output=True,
                    stdout_file=stdout_file,
                    stderr_file=stderr_file,
                    progress_bar=progress_bar,
                )
                export_output_variables(name, results_file)
            except pm.PapermillExecutionError as nbex:
                logging.exception(f"notebook exception: {nbex}")
                if "EarlyTestFailure: " not in str(nbex):
                    error = True
                    error_message = f"Exception in notebook: {nbex}"

            print("creating test execution ...")
            test_case = get_test_case(results_file, configuration_file)
            if error:
                test_case.status = Status.ABORTED
                test_case.comment += (
                    f"Test aborted due to notebook exception: {error_message}"
                )

            test_execution = create_test_execution(test_case, results_file)
            test_execution.test_environments = [args.station_name.upper()]
            print(f"{name} test status = {test_case.status}")

            if JIRA_TOKEN:
                if check_is_test_ready_for_upload(results_file):
                    publisher.publish(test_execution.as_dict())
                    print(
                        "Successfully ran notebook and published test execution "
                        f"for {nb_name}"
                    )
                elif error:
                    publisher.publish(test_execution.as_dict())
                    print(
                        f"Published test execution for {nb_name},"
                        " which was aborted due to errors."
                    )
                else:
                    print(
                        f"Successfully ran notebook {nb_name}, "
                        "but test not yet passed or failed so not publishing to jira."
                    )
            else:
                print("no jira token - not publishing to jira")
                print(f"****** Ran notebook {name} ******\n")

            if error and args.early_stop:
                print("Stopping execution due to errors.")
                break
        except Exception as ex:  # pylint: disable=broad-exception-caught  # TODO
            logging.exception(f"Failure to execute notebook {nb_name}: {ex}")
            if args.early_stop:
                print("Stopping execution due to errors.")
                break


if __name__ == "__main__":
    main()
