"""Helper functions for working with SKA Low subarrays."""

import json
from warnings import warn

from ska_control_model import ObsState
from tango import DeviceProxy

from aiv_utils.low_utils import get_device, get_mccs_device
from aiv_utils.tango_utils import single_prop, wait_for


def back_to_empty(tm_subarray: DeviceProxy):
    """Take a TMC subarray and do our best to bring it back to EMPTY."""

    # get the MCCS subarray corresponding to the supplied TMC subarray

    mccs_subarray = get_device(single_prop(tm_subarray, "MccsSubarrayFQDN"))

    pst_beam = get_device("low-pst/beam/01")

    # If abortable, try Abort
    if tm_subarray.obsState not in {
        ObsState.EMPTY,
        ObsState.ABORTED,
        ObsState.FAULT,
        ObsState.RESTARTING,
    }:
        tm_subarray.Abort()
        wait_for(tm_subarray, "obsState", {ObsState.ABORTED, ObsState.FAULT})

    # Now try to Restart
    tm_subarray.Restart()
    try:
        wait_for(mccs_subarray, "obsState", ObsState.EMPTY, timeout=10)
    except ValueError:
        if mccs_subarray.obsState == ObsState.EMPTY:
            warn("Missed MCCS subarray obsState EMPTY event")
        else:
            if mccs_subarray.obsState not in {
                ObsState.EMPTY,
                ObsState.ABORTED,
                ObsState.FAULT,
            }:
                warn(f"Abort()ing MCCS subarray {mccs_subarray.dev_name()}")
                mccs_subarray.Abort()
                wait_for(mccs_subarray, "obsState", ObsState.ABORTED)

            warn(f"Restart()ing MCCS subarray {mccs_subarray.dev_name()}")
            mccs_subarray.Restart()
            wait_for(mccs_subarray, "obsState", ObsState.EMPTY, timeout=10)

    # pst beam doesn't get back to idle but can remove this after the
    # https://jira.skatelescope.org/browse/SKB-637
    # Fix is released
    if pst_beam.obsState not in {
        ObsState.IDLE,
        ObsState.ABORTED,
    }:
        warn(f"Abort()ing PST beam {pst_beam.dev_name()}")
        pst_beam.Abort()
        wait_for(pst_beam, "obsState", ObsState.ABORTED)
    if pst_beam.obsState == ObsState.ABORTED:
        warn(f"obsreset()ing PST beam {pst_beam.dev_name()}")
        pst_beam.obsreset()
        wait_for(pst_beam, "obsState", ObsState.IDLE)

    rcs = json.loads(mccs_subarray.assignedResources)
    mccs_subarray_beams = [
        get_mccs_device(f"low-mccs/subarraybeam/{int(sab):02}")
        for sab in rcs["subarray_beam_ids"]
    ]
    mccs_station_beams = [
        get_mccs_device(f"low-mccs/beam/{stb}") for stb in rcs["station_beam_ids"]
    ]
    bad_beams = [
        beam
        for beam in mccs_station_beams + mccs_subarray_beams
        if beam.obsState in {ObsState.ABORTED, ObsState.FAULT}
    ]
    for beam in bad_beams:
        warn(f"Restart()ing MCCS beam {beam.dev_name()}")
        beam.Restart()
    wait_for(bad_beams + [tm_subarray], "obsState", ObsState.EMPTY)
