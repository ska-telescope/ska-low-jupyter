"""
The test_frame module provides the TestFrame class.


Example:

    Show selected devices, attributes in a table ( data frame.)
    literal blocks::

        tf = TestFrame([device1, device2, ... ],["attr1", "attr2", ...])

    where device_i are Tango Devices and attribute_i are lists of attribute names
    literal blocks::

        state  = TestFrame([tile1,tile2],["state","adminMode","obstate"])

    Before Operation
    literal blocks::

        state.check()

    Shows a dataframe in the notebook: rows are devices columns are the attribute values
    (human readable.)

    If a given device does not have the attribute a blank cell is shown.

    After operation
    literal blocks::

        state.check()

    Allows easy viewing of sets of tango device attributes
    before and after an operation is performed.
"""

import time

import pandas as pd
from IPython.display import display as ipython_display
from tango import DevFailed


def state_difference(
    before_df: pd.DataFrame, after_df: pd.DataFrame, check_column="state"
) -> pd.DataFrame:
    """
    Show the difference between two .check() dataframes by joining before and after
    checks, joining on the device and showing only rows which have different
    check_column values (e.g. state.)

    :param before_df: A Pandas Dataframe. ( result of a TestFrame.check() before
        some operation.
    :param after_df: A Pandas Dataframe ( result of a TestFrame.check() after
        some operation
    :param check_column: str  The name of the column to check for differences.
        Defaults to 'state'.
    :return: A Pandas dataframe showing the merged frames but containing only
        the rows wherethe check_column is different in the before and after dataframes.
    """
    merged = pd.merge(
        before_df, after_df, on="device", how="inner", suffixes=("_before", "_after")
    )
    result = merged[merged[f"{check_column}_before"] != merged[f"{check_column}_after"]]
    return result


class TestFrame:
    """
    Show sets of attributes for multiple devices in a dataframe for ease
    of viewing.
    """

    def __init__(
        self,
        devices,
        attributes,
        display="name",
        check_column="state",
        context="jupyter",
    ):  # pylint: disable=too-many-arguments
        if context == "jupyter":
            self.cell_display = ipython_display
        else:
            self.cell_display = print

        self.check_column = check_column
        pd.options.display.max_rows = 1000
        self.devices = devices
        self.attributes = attributes
        self.display = display

        if self.display == "name":
            self.display_func = lambda value: (
                value.name if hasattr(value, "name") else value
            )
        else:
            self.display_func = lambda value: value

    def check(self) -> pd.DataFrame:
        "check the attributes"
        data = self.get_attribute_data()
        data_frame = pd.DataFrame(data)
        return data_frame

    def get_attribute_data(self):
        "return attributes"

        data = {}
        data["device"] = [device.name() for device in self.devices]
        other_atts = {
            attr: [self.get_attribute(device, attr) for device in self.devices]
            for attr in self.attributes
        }
        data.update(other_atts)
        return data

    def get_attribute(self, device, attr):
        "return single attribute"
        try:
            if hasattr(device, attr):
                value = getattr(device, attr)
                if callable(value):
                    value = value()
                return self.display_func(value)
            return ""
        except ValueError as ex:
            return str(ex)
        except DevFailed:
            return "DevFailed"

    def check_relationship(self, device):
        """
        Check what other devices refer to
        a given device via a trl attribute.

        :param device: device proxy
        :return: Nothing
        """
        device_name = device.name()
        for attr in device.get_attribute_list():
            try:
                value = getattr(device, attr)
                if isinstance(value, str):
                    for dev in self.devices:
                        trl = dev.name()
                        if value == trl:
                            print(f"{device_name}.{attr} -> {trl}")
            except DevFailed:
                pass

    def show_relationships(self):
        """
        For all devices loaded into this
        TestFrame, show which other devices
        refer to it.
        """
        for device in self.devices:
            self.check_relationship(device)

    def do_operation(self, operation, check_column="state", delay=30):
        """
        This method captures system state before an operation,
        performs an operation, waits for a delay and then
        captures the state again, showing what has changed.

        :param operation: a nilary function
        :param check_column: which column in the check dataframe to check
            for differences.
        :param: delay: A time to wait, in seconds, after the operation has been
            invoked, before checking for the system state again.
        :return: A Pandas Dataframe showing the differences between before and after.
        """
        print("running operation ...")
        before_df = self.check()
        operation()
        print("waiting for {delay} seconds ...")
        time.sleep(delay)
        after_df = self.check()
        return state_difference(before_df, after_df, check_column=check_column)

    def __enter__(self):
        self.before_df = self.check()  # pylint: disable=attribute-defined-outside-init
        return self

    def __exit__(self, exc_type, exc_value, exc_tb):
        if exc_type is not None:
            print("Error:")
            print(exc_type)
            print(exc_value)
            print(exc_tb)
        else:
            after_df = self.check()
            changed = state_difference(
                self.before_df, after_df, check_column=self.check_column
            )
            self.cell_display(changed)
