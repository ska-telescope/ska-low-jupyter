"""
General SKA-LOW utility functions.
"""

import json
import os
from collections import namedtuple
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime, timedelta
from enum import IntEnum
from typing import Any

import numpy as np
import pandas as pd
from humanize import naturaldelta, naturaltime
from ipykernel.kernelbase import StdinNotImplementedError
from tango import Database, DevFailed, DeviceProxy, DevSource, DevState

PASD_FIELDS = "station pasdbus fndh smartboxes"
PASDDevices = namedtuple("PASDDevices", PASD_FIELDS)

MCCS_FIELDS = "controller stations subarrays subarray_beams station_beams"
MCCSDevices = namedtuple("MCCSDevices", MCCS_FIELDS)

CSP_FIELDS = "controller subarrays"
CSPDevices = namedtuple("CSPDevices", CSP_FIELDS)

SPS_FIELDS = "station subracks tpms daqs"
SPSDevices = namedtuple("SPSDevices", SPS_FIELDS)

CBF_FIELDS = "controller allocator connector processors delaypoly cnics subarrays"
CBFDevices = namedtuple("CBFDevices", CBF_FIELDS)

SDP_FIELDS = "controller queue_connectors subarrays"
SDPDevices = namedtuple("SDPDevices", SDP_FIELDS)

TMC_FIELDS = (
    "central_node "
    + "csp_master_leafnode csp_subarray_leafnodes "
    + "mccs_master_leafnode mccs_subarray_leafnodes "
    + "sdp_master_leafnode sdp_subarray_leafnodes "
    + "tmc_subarray_nodes"
)
TMCDevices = namedtuple("TMCDevices", TMC_FIELDS)


def tpm_number_from_dev(tpm):
    """Get the TPM number from a device proxy."""
    return int(tpm.dev_name().split("-")[-1])


def split_host_port(endpoint, default_port=4660):
    """Split a host:port string into a tuple of (host, port)."""
    host, port, *_ = endpoint.split(":") + [default_port]
    return host, int(port)


def get_device(trl: str, tango_host=None, timeout_ms=10000) -> DeviceProxy:
    """
    Grab the Tango device from the Tango Resource Locator (TRL)
    :params trl: the fully qualified domain name
    :return device: The Tango device
    """
    device = DeviceProxy("/".join(filter(None, [tango_host, trl])))
    device.set_source(DevSource.DEV)
    if timeout_ms is not None:
        device.set_timeout_millis(timeout_ms)
    return device


def get_mccs_tangodb() -> Database:
    """
    Get the Database object for the Tango DB containing MCCS devices.

    :return: a tango.Database object representing the MCCS Tango database
    """
    mccs_host = os.getenv("TANGO_HOST_MCCS")
    return Database(*split_host_port(mccs_host, 10000) if mccs_host else [])


def get_mccs_device(trl: str, timeout_ms=10000) -> DeviceProxy:
    """
    Grab the Tango device using a TRL, using TANGO_HOST_MCCS.
    :params trl: the fully qualified domain name
    :return device: The Tango device
    """
    return get_device(trl, os.getenv("TANGO_HOST_MCCS"), timeout_ms=timeout_ms)


def get_stations():
    """
    Returns a list of MccsStation devices.
    :param host: the Tango database host
    :return: A list of MccsStation devices
    """
    database = get_mccs_tangodb()
    return [
        get_mccs_device(trl)
        for trl in database.get_device_exported_for_class("MccsStation")
    ]


def get_sps_devices(station_name: str, tango_timeout: int = 3500) -> SPSDevices:
    """
    Returns station, subracks, tpms and daqs Tango devices for a given station name.

    :param station_name: the name of the station
    :param tango_timeout: set the client timeout on the returned DeviceProxys
    :return: list of devices [station, subracks, tpms, daqs]
    """

    tango_host = os.getenv("TANGO_HOST_MCCS")

    station = get_device(f"low-mccs/spsstation/{station_name}", tango_host)
    station_props = station.get_property(station.get_property_list("*"))
    subracks = [get_device(trl, tango_host) for trl in station_props["SubrackFQDNs"]]
    tpms = [get_device(trl, tango_host) for trl in station_props["TileFQDNs"]]
    daqs = [get_device(trl, tango_host) for trl in station_props["DaqTRL"]]
    daqs += [
        get_device(f"{trl}-bandpass", tango_host) for trl in station_props["DaqTRL"]
    ]

    for dev in [station] + subracks + tpms + daqs:
        # Some devices have a tendency to time out, and setting greater than the default
        # 3000ms helps distinguish between timeouts waiting for the server-side Tango
        # monitor lock and timeouts waiting on Tango commands/writes/reads themselves.
        dev.set_timeout_millis(tango_timeout)
        dev.set_source(DevSource.DEV)

    # SpsStation does a lot of things in a loop with 16 MccsTiles,
    # so it needs a longer timeout for commands not to occasionally fail.
    # DAQ commands manipulating the daq library can also be slow.
    for dev in daqs + [station]:
        dev.set_timeout_millis(max(10000, tango_timeout))

    return SPSDevices(station, subracks, tpms, daqs)


def get_pasd_devices(
    station_name: str,
) -> PASDDevices:
    """
    Returns field station, pasdbus, FNDH and smartboxes for a given station name.

    :param station_name: the name of the station
    :return: list of devices [fs, pasdbus, fndh, smartboxes]
    """

    tango_host = os.getenv("TANGO_HOST_MCCS")
    station = get_device(f"low-mccs/fieldstation/{station_name}", tango_host)
    station_props = station.get_property(station.get_property_list("*"))
    fndh = get_device(station_props["FndhFQDN"][0], tango_host)
    fndh_props = fndh.get_property(fndh.get_property_list("*"))
    pasdbus = get_device(fndh_props["PasdFQDN"][0], tango_host)
    smartboxes = [get_device(d, tango_host) for d in station_props["SmartBoxFQDNs"]]

    for dev in [station, pasdbus, fndh] + smartboxes:
        dev.set_source(DevSource.DEV)

    return PASDDevices(station, pasdbus, fndh, smartboxes)


def get_mccs_devices() -> MCCSDevices:
    """
    Return stations, subarray beams, station beams, and subarrays.
    """

    database = get_mccs_tangodb()

    controller = get_mccs_device(
        database.get_device_exported_for_class("MccsController")[0]
    )

    props = controller.get_property(controller.get_property_list("*"))

    def get_subdevices(prop):
        return [get_mccs_device(trl) for trl in props[prop]]

    stations = get_subdevices("MccsStations")
    subarray_beams = get_subdevices("MccsSubarrayBeams")
    station_beams = get_subdevices("MccsStationBeams")
    subarrays = get_subdevices("MccsSubarrays")

    return MCCSDevices(controller, stations, subarrays, subarray_beams, station_beams)


def get_cbf_devices() -> CBFDevices:
    """
    CBF Devices.
    :return: CBFDevices named tuple
    """
    database: Database = Database()
    controller = get_device("low-cbf/control/0")
    allocator = get_device("low-cbf/allocator/0")
    connector = get_device("low-cbf/connector/0")

    delaypoly = get_device("low-cbf/delaypoly/0")

    cnics = [get_device(trl) for trl in database.get_device_exported_for_class("cnic*")]

    def get_cbf_subarrays(controller):
        props = controller.get_property(controller.get_property_list("*"))
        subarray_trls = [
            trl for trl in props["__SubDevices"] if "low-cbf/subarray" in trl
        ]

        return [get_device(trl) for trl in subarray_trls]

    subarrays = get_cbf_subarrays(controller)

    processors = [
        get_device(trl)
        for trl in database.get_device_exported_for_class("LowCbfProcessor")
    ]

    return CBFDevices(
        controller, allocator, connector, processors, delaypoly, cnics, subarrays
    )


def get_sdp_devices() -> SDPDevices:
    """
    SDP Devices.
    :return: A named tuple instance holding SDP devices.
    """
    database: Database = Database()
    controller = get_device("low-sdp/control/0")
    queue_connectors = [
        get_device(trl)
        for trl in database.get_device_exported_for_class("SDPQueueConnector")
    ]

    subarrays = [
        get_device(trl) for trl in database.get_device_exported_for_class("SDPSubarray")
    ]

    return SDPDevices(controller, queue_connectors, subarrays)


def get_csp_devices() -> CSPDevices:
    """
    CSP Devices.
    :return: A named tuple instance holding CSP devices.
    """
    database: Database = Database()
    controller = get_device("low-csp/control/0")

    subarrays = [
        get_device(trl)
        for trl in database.get_device_exported_for_class("LowCspSubarray")
    ]

    return CSPDevices(controller, subarrays)


def report_attr(dev, attr_name):  # pylint:disable=too-many-return-statements
    """
    Reads `attr_name` from `dev`, and returns if formatted with some Tango sugar.
    """

    def fromisoformat_compat(val: Any) -> Any:
        """Remove when Python 3.11"""
        if isinstance(val, str) and val.endswith("Z"):
            return datetime.fromisoformat(val[:-1] + "+00:00")
        return datetime.fromisoformat(val)

    try:
        attr = getattr(dev, attr_name)
        if callable(attr):
            attr = attr()

        try:
            attr = fromisoformat_compat(attr)
        except (ValueError, TypeError):
            pass

        match attr:
            case DevState():
                return str(attr)
            case IntEnum():
                return f"{attr.value} {attr.name}"
            case bool():
                return "✅" if attr else "❌"
            case datetime():
                return attr
            case str():
                return attr.split("/")[-1] if attr_name == "dev_name" else attr
            case _:
                return attr

    except DevFailed:
        return "🤯"
    except AttributeError:
        return "🫥"


def tpm_summary(tpms: list[DeviceProxy]):
    """
    Returns a DataFrame summarising critical TPM attributes.
    """
    attrs = [
        "dev_name",
        "logicalTileId",
        "stationId",
        "state",
        "tileProgrammingState",
        "fpgaReferenceTime",
        "fpgaTime",
        "fpgaFrameTime",
        "pllLocked",
        "ppsPresent",
        "ppsDelay",
        # these would be nice? but raise a not-yet-implemented error
        # "clockPresent",
        # "sysrefPresent",
        "isBeamformerRunning",
    ]

    # read attrs concurrently for less timing jank
    with ThreadPoolExecutor(max_workers=1) as executor:
        futs = {
            (tpm, k): executor.submit(report_attr, tpm, k)
            for k in attrs
            for tpm in tpms
        }
        report = pd.DataFrame(
            {k: futs[(tpm, k)].result() for k in attrs} for tpm in tpms
        )

    dt_idxs = [
        all(isinstance(t, datetime) for t in times)
        for i, times in enumerate(zip(report.fpgaFrameTime, report.fpgaTime))
    ]
    dt_rows = report[dt_idxs].infer_objects()

    if len(dt_rows):
        # Cast to object so pandas doesn't complain about incompatible types,
        # which we don't really care about in this case
        report = report.astype({"fpgaFrameTime": object})
        report.loc[dt_idxs, "fpgaFrameTime"] = (
            pd.to_datetime(dt_rows.fpgaFrameTime) - dt_rows.fpgaTime
        )

    def format_tpm_time(time_val):
        if time_val == datetime.fromisoformat("2106-02-07T06:28:15+00:00"):
            return "<max>"
        if time_val == datetime.fromisoformat("1970-01-01T00:00:00+00:00"):
            return "<min>"
        return naturaltime(time_val)

    def format_delta(val):
        if isinstance(val, timedelta):
            delta = naturaldelta(val, minimum_unit="milliseconds")
            return f"+{delta}".replace(" milliseconds", "ms")
        return val

    def format_datetime(val):
        if isinstance(val, datetime):
            return val.isoformat().replace("+00:00", "Z")
        return val

    return report.style.format(
        {
            "fpgaFrameTime": format_delta,
            "fpgaReferenceTime": format_datetime,
            "fpgaTime": format_tpm_time,
        }
    )


def interactive_variable_prompt(variable_name: str) -> str:
    """
    The user will be prompted to input a value.

    :params variable_name: The name of the variable to be printed on the prompt.
    :return variable: The variable which will no longer be empty.
    """
    variable = None
    try:
        while not variable:
            # Try to get input from the user
            variable = input(f"Please set the value for {variable_name}: ")
    except StdinNotImplementedError:
        # Handle the case where input is not supported (e.g., Jupyter or non-interactive environments) # pylint: disable=C0301
        print(
            "Input is not supported in this environment. Returning None and assuming it will be set elsewhere"  # pylint: disable=C0301
        )
        variable = None
    print(f"The value of {variable_name} is: {variable}")
    return variable


def get_tmc_devices() -> TMCDevices:
    """
    Returns TMC Tango devices.
    Of which the fields are:

    central_node
    csp_master_leafnode csp_subarray_leafnodes
    mccs_master_leafnode mccs_subarray_leafnodes
    sdp_master_leafnode sdp_subarray_leafnodes
    tmc_subarray_nodes

    The master leaf nodes have attribute links to the
    corresponding controllers ("masters") in the
    nominated subsystem.
    E.g. sdp_master_leafnode monitors the sdp controller.

    The subarray leafnodes are paired (have an attribute
    "link") to the corresponding subsystem subarray

    The central node has attribute links to each of the
    tmc master leafnodes as well has attribute links
    to the subsystem controllers as well.

    These attributes have names like sdpMasterDevName
    which holds the trl of the sdp controller in
    the sdp subsystem ( not part of TMC.)

    The TMC subarrays

    TMC itself only comprises the devices
    named in the TMC named tuple:
    the central node, master and subarray
    leafnodes for the mccs csp and sdp subsystems and
    the tmc subarrays.

    :return: TMC Named Tuple.
    """

    central_node = get_device("ska_low/tm_central/central_node")
    props = central_node.get_property(central_node.get_property_list("*"))

    csp_master_leafnode = get_device(props["CspMasterLeafNodeFQDN"][0])
    csp_subarray_leafnodes = [get_device(trl) for trl in props["CspSubarrayLeafNodes"]]

    mccs_master_leafnode = get_device(props["MccsMasterLeafNodeFQDN"][0])

    # There is a (Csp|Sdp)SubarrayLeafNodes property, but no MccsSubarrayLeafNodes.
    # TODO: raise an SKB and get this added.
    mccs_subarray_leafnodes = [
        get_device(trl.replace("/csp_", "/mccs_"))
        for trl in props["CspSubarrayLeafNodes"]
    ]

    sdp_master_leafnode = get_device(props["SdpMasterLeafNodeFQDN"][0])
    sdp_subarray_leafnodes = [get_device(trl) for trl in props["SdpSubarrayLeafNodes"]]

    tmc_subarray_nodes = [
        get_device(trl, timeout_ms=10000) for trl in props["TMCSubarrayNodes"]
    ]

    return TMCDevices(
        central_node,
        csp_master_leafnode,
        csp_subarray_leafnodes,
        mccs_master_leafnode,
        mccs_subarray_leafnodes,
        sdp_master_leafnode,
        sdp_subarray_leafnodes,
        tmc_subarray_nodes,
    )


def is_station_initialised(station: str | SPSDevices) -> bool:
    """
    Assert that the station is initialised.

    This function currently checks TPM tileProgrammingState state, FPGA time,
    FPGA reference time, SPEAD format, and that channeliser and CSP rounding
    are consistent between SpsStation and TPMs.

    :param station: The station to check.
    :return: True if the station is initialised to our satisfaction
    """

    if isinstance(station, str):
        station = get_sps_devices(station)

    # TODO: this function could be greatly expanded and brought into SpsStation
    # TODO: check daisy chaining and routing
    sps_station, _, tpms, _ = station
    for tpm in tpms:
        assert tpm.tileProgrammingState == "Synchronised"

    # FPGA times should all be the same,
    # but we might read over a second boundary so may be 1 off
    fpga_times = {datetime.fromisoformat(tpm.fpgaTime) for tpm in tpms}
    assert (max(fpga_times) - min(fpga_times)).total_seconds() <= 1

    # set at or before startacquisition, so should be identical
    assert len({tpm.fpgaReferenceTime for tpm in tpms}) == 1

    # should always be using the new SPEAD format
    assert {tpm.cspSpeadFormat for tpm in tpms} == {"SKA"}

    # station and TPM channeliser and CSP rounding should match
    # TODO: in the ITF, these SpsStation attributes are of length 16 with two TPMs. Bug?
    assert np.all(
        sps_station.channeliserRounding[: len(tpms)]
        == np.array([tpm.channeliserRounding for tpm in tpms])
    )
    assert {tuple(sps_station.cspRounding)} == {tuple(tpm.cspRounding) for tpm in tpms}

    return True


def print_port_status(connector):
    """Print P4 Packet Counters

    :param connector: The device proxy for the connector to the P4 device
        (low-cbf/connector/0)
    """
    ports = json.loads(connector.portStatus)["Ports_Status"]
    return pd.DataFrame(
        {
            k.replace("$", "").replace("_", " ").lower(): port[k]
            for k in [
                "$PORT_NAME",
                "$PORT_ENABLE",
                "$PORT_UP",
                "$SPEED",
                "packets_received",
                "packets_sent",
            ]
        }
        for port in ports
    )


def calculate_pst_cfreq_bandwidth(  # pylint:disable=too-many-locals
    sps_a_channel: int, sps_b_channel: int
) -> tuple[float, float]:
    """Calculate the centre frequency and bandwidth in hertz for the PST

    :params sps_a_channel: The lowest coarse frequency channel ID
    :params sps_b_channel: The highest coarse frequency channel ID
    """
    sps_channel_bw = 0.78125
    cbf_channel_bw = 0.003616898148
    cbf_nchan = 216
    sps_chan0_freq = 50

    sps_a_centre_freq = sps_channel_bw * sps_a_channel

    # the lower edge of the SPS channel will be the centre frequency of the lowest PST channel # pylint: disable=C0301
    sps_a_lower_freq = sps_a_centre_freq - sps_channel_bw / 2

    sps_nchan = (sps_b_channel - sps_a_channel) + 1
    cbf_nchan = sps_nchan * cbf_nchan
    cbf_bw = round(cbf_nchan * cbf_channel_bw, 6)

    cbf_a_lower_freq = sps_a_lower_freq - cbf_channel_bw / 2
    cbf_b_upper_freq = cbf_a_lower_freq + cbf_bw

    pst_bandwidth_hz = cbf_bw * 1e6
    pst_centre_freq_hz = ((cbf_a_lower_freq + cbf_b_upper_freq) / 2) * 1e6

    cbf_chan0_freq_hz = sps_chan0_freq - (sps_channel_bw / 2) - (cbf_channel_bw / 2)
    pst_schan = int(round((cbf_a_lower_freq - cbf_chan0_freq_hz) / cbf_channel_bw))
    pst_echan = int(round((cbf_b_upper_freq - cbf_chan0_freq_hz) / cbf_channel_bw))

    print(
        f"SPS channels=[{sps_a_channel} - {sps_b_channel}] PST channels=[{pst_schan} - {pst_echan}] {pst_centre_freq_hz=} {pst_bandwidth_hz=}"  # pylint: disable=C0301
    )

    return (float(round(pst_centre_freq_hz)), float(round(pst_bandwidth_hz)))
