"""
Module for loading data by return data file paths or parsing the files
"""

import csv
import os
from typing import Tuple

import numpy as np
from scipy.interpolate import CubicSpline

DATA_DIR = os.path.join(os.path.dirname(__file__), "data")

# Model for SKALA4.1 antenna S parameters# Model for SKALA4.1 antenna S parameters
ANTENNA_S_PARAM_PATH = os.path.join(
    DATA_DIR, "SKALA4.1_1antenna_SParameters_from Georgios.txt"
)

# Analog bandpass as measured at ITF
ANALOG_BANDPASS_PATH = os.path.join(DATA_DIR, "ITF_analog_bandpass.txt")


def get_antenna_s_param() -> Tuple[np.ndarray, np.ndarray]:
    """Get the antenna S parameters for a data text file.

    :returns: two numpy arrays of the frequency in MHz and the PT
    """

    giorgios_frequency_mhz = []
    giorgios_pt = []

    with open(ANTENNA_S_PARAM_PATH, encoding="latin-1") as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            if row != []:
                row = row[0].split()
                if row[0] != "#" and row[0] != "!":
                    giorgios_frequency_mhz.append((float(row[0]) / 1e6))
                    s11_mag = float(row[1])
                    s11_angle = float(row[2])
                    s11_real = s11_mag * np.cos(s11_angle * np.pi / 180.0)
                    s11_imag = s11_mag * np.sin(s11_angle * np.pi / 180.0)
                    s11 = s11_real + s11_imag * 1j
                    z_0 = 50.0 + 0.0 * 1j
                    zant = z_0 * (1 + s11) / (1 - s11)
                    pwrc = (z_0 - np.conjugate(zant)) / (z_0 + zant)
                    ptr = np.real(1 - pwrc * np.conjugate(pwrc))
                    giorgios_pt.append(ptr)

    return np.array(giorgios_frequency_mhz), np.array(giorgios_pt)


def get_bandpass_function():
    """Get the measured ITF bandpass and turn it into a function that can predict
    the bandpass for a given frequency

    :returns: CubicSpline function that inputs frequency and outputs predicted
        bandpass response
    """
    data_in = np.transpose(np.loadtxt(ANALOG_BANDPASS_PATH, dtype=float))
    freq_analog = data_in[0]
    bandpass_analog = data_in[1]
    return CubicSpline(freq_analog, bandpass_analog)
