"""
Generic utility functions for working with Tango devices.
"""

import queue
import time
from inspect import getfullargspec

import numpy as np
from ska_control_model import TaskStatus
from tango import DevFailed, DeviceProxy, EventType, GreenMode

__NO_ARG__ = object()


def _yield_attr_values(devices: list[DeviceProxy], attr: str, timeout: float = None):
    """
    Subscribes to attr on each of devices, and yields (device, attribute value) tuples.
    """

    def _evt_val(evt):
        if evt.err:
            return None
        if evt.attr_value is None:
            return evt.device, None
        return evt.device, evt.attr_value.value

    s_q = queue.SimpleQueue()
    subscriptions = [
        dev.subscribe_event(attr, EventType.CHANGE_EVENT, s_q.put, stateless=True)
        for dev in devices
    ]

    if timeout is not None:
        deadline = time.time() + timeout

        def get_fn():
            return s_q.get(timeout=max(0.0, deadline - time.time()))

    else:
        get_fn = s_q.get

    try:
        yield from filter(None, map(_evt_val, iter(get_fn, object())))
    finally:
        for dev, sub in zip(devices, subscriptions):
            dev.unsubscribe_event(sub)


def wait_for(
    devs: DeviceProxy | list[DeviceProxy],
    attr: str,
    desired_value,
    timeout: float = 60.0,
    quiet: bool = False,
) -> None:
    """
    Block until attr of each of devs has a certain value or matches a predicate.

    :param devs: a DeviceProxy or a list of DeviceProxy
    :param attr: the name of the attribute to wait for
    :param desired_value: an attribute value to wait for, or a custom predicate which
        may accept either one argument (the value) or two (the value, and the device)
    :param timeout: maximum time to wait for each device to meet the condition
    :param quiet: don't log anything
    :return: None
    """

    def default_predicate(value, _):
        match desired_value, value:
            case list(), np.ndarray():
                return np.array_equal(value, desired_value)
            case set(), _:
                return value in desired_value
            case _:
                return value == desired_value

    # allow predicate to accept only value, or value and device
    predicate_vararg = desired_value if callable(desired_value) else default_predicate
    if len(getfullargspec(predicate_vararg).args) == 1:

        def predicate(vararg, _):
            return predicate_vararg(vararg)

    else:
        predicate = predicate_vararg

    if isinstance(devs, DeviceProxy):
        devs = [devs]

    if len(devs) == 0:
        return

    devs_left = set(devs)
    try:
        for dev, value in _yield_attr_values(devs, attr, timeout=timeout):
            if dev not in devs_left:
                continue
            if not quiet:
                print(f"{dev.dev_name()}/{attr} = {value!r}")
            if predicate(value, dev):
                devs_left.remove(dev)
            if not devs_left:
                return
    except queue.Empty:
        error = f'{devs_left} did not reach desired value {desired_value!r} of attribute "{attr}"'  # pylint: disable=C0301
        exc = ValueError(error)
        exc.failed_devices = devs_left
        raise exc from None  # the queue.Empty is not useful information, drop it


def tango_attr_value(dev, attr, default=__NO_ARG__):
    """
    Like getattr(dev, attr, default), but using item lookup rather
    than attribute access.
    """
    try:
        return dev[attr].value
    except DevFailed as error:
        if error.args[0].reason == "API_AttrNotFound":
            if default != __NO_ARG__:
                return default
        raise


def single_prop(dev: DeviceProxy, prop_name: str) -> str:
    """
    Return the first value of a device property.

    Properties are always lists
    but many properties only expect a single value.
    This convenience method makes accessing those less verbose.
    """
    return dev.get_property(prop_name)[prop_name][0]


def member(dev: DeviceProxy) -> str:
    """
    Return the member part of the device TRL.

    For example, if the TRL is "low-mccs/tile/s8-1-tpm01",
    then the member part is "s8-1-tpm01".

    :return: the member part of the device TRL.
    """
    return dev.dev_name().split("/")[-1]


def read_all(devs: list[DeviceProxy], attr: str) -> list:
    """
    Uses GreenMode.Futures to read the same `attr` on all `devs`.
    """
    future_results = [
        dev.read_attribute(attr, green_mode=GreenMode.Futures, wait=False)
        for dev in devs
    ]
    return [f.result().value for f in future_results]


def wait_lrc(
    dev: DeviceProxy,
    cmd: str,
    *args,
    status=TaskStatus.COMPLETED,
    timeout=15,
    quiet=True,
):
    """
    Waits for a long-running command to complete.

    :param dev: a DeviceProxy
    :param cmd: the name of the long-running command to execute
    :param args: arguments to execute the command with
    :param status: the TaskStatus to wait for
    :param timeout: maximum time to wait for the command to reach `status`
    :param quiet: don't log anything
    :return: None
    """
    lrc_result = getattr(dev, cmd)(*args)
    wait_for(
        dev,
        "longRunningCommandStatus",
        lambda v: dict(zip(v[0::2], v[1::2]))[lrc_result[1][0]] == status.name,
        timeout=timeout,
        quiet=quiet,
    )
