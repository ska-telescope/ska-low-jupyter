"""
Wrappers for the station Tango devices.
"""

import json
import logging
import queue
from concurrent import futures
from contextlib import contextmanager
from pathlib import Path

import numpy as np
import toolz
from pandas import DataFrame
from tango import DeviceProxy, EventType, GreenMode

logger = logging.getLogger(__name__)


@toolz.memoize
def get_tpms(station: DeviceProxy):
    """Get the TPMs from the station."""
    return [DeviceProxy(trl) for trl in get_station_property(station, "TileFQDNs")]


def get_devices(station: DeviceProxy):
    """Returns station, subracks, tpms and daqs Tango devices for a given
    station name."""
    station_props = station.get_property(station.get_property_list("*"))
    subracks = [DeviceProxy(trl) for trl in station_props["SubrackFQDNs"]]
    tpms = get_tpms(station)
    daqs = [DeviceProxy(trl) for trl in station_props["DaqTRL"]]
    return subracks, tpms, daqs


def get_station_property(station: DeviceProxy, desired_property: str) -> list[str]:
    """Get a property from the station."""
    return station.get_property(desired_property)[desired_property]


def tpms_read_attr_concat(tpms: list[DeviceProxy], attr: str) -> np.ndarray:
    """Read a concatenated value from the TPMs in parallel."""
    future_results = np.full([512], np.nan)
    for tpm in tpms:
        tile_id: int = tpm.logicalTileId
        future_results[tile_id * 32 : (tile_id + 1) * 32] = (  # noqa E203
            tpm.read_attribute(attr, green_mode=GreenMode.Futures, wait=False)
            .result()
            .value
        )

    return future_results


def tpms_write_attr_packed(
    tpms: list[DeviceProxy], attr: str, value: np.ndarray
) -> None:
    """Write a packed value to the TPMs in parallel."""
    assert len(value) % len(tpms) == 0
    value_chunks = np.array(value).reshape((len(tpms), -1))
    future_results = [
        tpm.write_attribute(attr, chunk, green_mode=GreenMode.Futures, wait=False)
        for tpm, chunk in zip(tpms, value_chunks)
    ]
    futures.wait(future_results)


def station_summary(tpms):
    """Return a summary of the station state as a pandas DataFrame."""
    data_frame = DataFrame(
        {
            k: getattr(tpm, k)
            for k in [
                "stationId",
                "logicalTileId",
                "tileProgrammingState",
                "fpgaTime",
                "fpgaReferenceTime",
                "fpgaFrameTime",
                "clockPresent",
                "pllLocked",
                "ppsPresent",
                "ppsDelay",
            ]
        }
        for tpm in tpms
    )
    return data_frame


@contextmanager
def integrated_data_capture(station, daq, integration_time):
    """Integrate the channel data and yield the integrated data samples."""
    s_q = queue.SimpleQueue()
    sub = daq.subscribe_event("dataReceivedResult", EventType.CHANGE_EVENT, s_q.put)
    _ = s_q.get()
    logger.info("Starting to send integrated channel data")
    station.ConfigureIntegratedChannelData(
        # There is a bug where integrated samples are sent at half the rate requested
        # TODO: raise an SKB
        json.dumps({"integration_time": integration_time / 2.0})
    )
    get_except = toolz.excepts(
        queue.Empty,
        toolz.partial(s_q.get, timeout=integration_time * 1.5),
        lambda _: daq.unsubscribe_event(sub),
    )
    try:
        yield iter(get_except, None)
    finally:
        logger.info("Stopping integrated channel data transmission")
        station.StopIntegratedData()


def parse_capture_events(events, nof_tiles):
    """Parse the integrated data capture events and return the offset,
    count and filename of the integrated data samples.
    """
    events = list(events)
    events_per_tpm = len(events) / nof_tiles
    logger.info(
        f"{len(events)} integrated data samples captured, {events_per_tpm} per TPM"
    )
    if len(events) % nof_tiles != 0:
        events_not_received = nof_tiles - len(events) % nof_tiles
        logger.warning(f"Did not receive events for {events_not_received} data samples")
    first_event = json.loads(events[0].attr_value.value[1])
    last_event = json.loads(events[-1].attr_value.value[1])
    integ_sample_offset = first_event["metadata"]["n_blocks"]
    integ_sample_count = last_event["metadata"]["n_blocks"] - integ_sample_offset
    integ_filename = Path(first_event["file_name"]).name
    return integ_sample_offset, integ_sample_count, integ_filename
