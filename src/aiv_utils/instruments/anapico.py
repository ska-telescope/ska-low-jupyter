"""
Module for controlling Anapico signal generator
"""

import time

import pyvisa

INSTRUMENT_IP = "10.135.150.95"


class Anapico:
    """Interface for controlling the Anapico APSIN2010 instrument.

    This class provides methods to communicate with the Anapico APSIN2010
    via SCPI commands. It allows for setting frequency and power levels,
    turning the RF output on and off, and querying the instrument's state.

    Args:
        ip (str, optional): The IP address of the instrument. Defaults to INSTRUMENT_IP.
        verbose (bool, optional): If True, prints verbose output for commands sent.
            Defaults to False.
        delay (float, optional): Delay in seconds after sending commands. Defaults to 1.

    Methods:
        idn(): Prints the identification string of the instrument.
        cmd(scpi, query=False): Sends a SCPI command to the instrument
            and optionally queries a response.
        rf_on(): Turns the RF output on.
        rf_off(): Turns the RF output off.
        set_frequency_hz(freq_hz): Sets the output frequency in Hertz.
        set_power_dbm(power_dbm): Sets the output power in dBm.
        close(): Closes the connection to the instrument.

    Properties:
        power (float): Gets or sets the output power in dBm.
        frequency (float): Gets or sets the output frequency in Hertz.
        is_reference_locked (bool): Indicates if the reference is locked.
        reference_frequency (float): Gets the external reference frequency.
    """

    def __init__(self, insturment_ip=INSTRUMENT_IP, verbose=False, delay=1):
        resource_manager = pyvisa.ResourceManager()
        self.instr = resource_manager.open_resource(f"TCPIP::{insturment_ip}::INSTR")
        self.verbose = verbose
        self.delay = delay
        self.idn()

    def idn(self):
        """Prints the identification string of the Anapico instrument.

        This method sends the SCPI command to query the instrument's
        identification and prints the response. It is useful for verifying
        the connection and ensuring the correct instrument is being controlled.

        Args:
            None

        Returns:
            None
        """
        print(self.cmd("*idn?", query=True))

    def cmd(self, scpi, query=False):
        """
        Send raw scpi to device
        See programmers manual for
        Anapico APSIN2010
        """
        if self.verbose:
            print(f"Sending {scpi} ...")
        if query:
            value = self.instr.query(scpi)
            time.sleep(self.delay)
            return value
        self.instr.write(scpi)
        time.sleep(self.delay)
        return scpi

    def rf_on(self):
        """Turns the RF output on for the Anapico instrument."""
        self.cmd("OUTPut1:STATe 1")

    def rf_off(self):
        """Turns off the RF output of the Anapico instrument."""
        self.cmd("OUTPut1:STATe 0")

    def set_frequency_hz(self, freq_hz: float):
        """Sets the output frequency of the Anapico instrument in Hertz.

        Args:
            freq_hz (float): The desired output frequency in Hertz.

        Returns:
            None
        """
        self.cmd(f":SOUR1:FREQ:FIXED {freq_hz} Hz")

    def set_power_dbm(self, power_dbm: float):
        """Sets the output power level of the Anapico instrument in dBm.

        Args:
            power_dbm (float): The desired output power level in dBm.

        Returns:
            None
        """
        cmd = f":SOUR1:POW:LEV:IMM:AMPL {power_dbm}"
        self.cmd(cmd)

    @property
    def power(self) -> float:
        """Gets the current output power level of the Anapico instrument in dBm.

        Returns:
            float: The current output power level in dBm.
        """
        result = self.cmd(":SOUR1:POW:LEV:IMM:AMPL?", query=True).strip()
        return float(result)

    @power.setter
    def power(self, power_dbm: float):
        self.set_power_dbm(power_dbm)

    @property
    def frequency(self) -> float:
        """
        return frequency in Hz
        """
        return float(self.cmd(":SOUR1:FREQ:FIXED?", query=True).strip())

    @frequency.setter
    def frequency(self, freq_hz: float):
        print(f"setting freq to {freq_hz} Hz ...")
        self.set_frequency_hz(freq_hz)

    @property
    def is_reference_locked(self):
        """Checks if the reference oscillator is locked.

        Returns:
            bool: True if the reference oscillator is locked, False otherwise.
        """
        return int(self.cmd(":SOUR1:ROSCillator:LOCKed?", query=True).strip()) == 1

    @property
    def reference_frequency(self):
        """Gets the external reference frequency of the Anapico instrument.

        Returns:
            float: The current external reference frequency in Hz.
        """
        cmd = ":SOUR1:ROSCillator:EXTernal:FREQuency?"
        return float(self.cmd(cmd, query=True).strip())

    def close(self):
        """Closes the connection to the Anapico instrument."""
        self.instr.close()
