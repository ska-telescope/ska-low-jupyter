"""
Module for interfacing with the MSO64B oscilloscope.
"""

import time

import numpy as np
import pyvisa

INSTRUMENT_IP = "10.135.151.40"
GATEWAY = "10.135.151.1"
PORT = 5025
FLOAT_DATATYPE = (
    "f"  # See https://docs.python.org/3/library/struct.html#format-characters
)
BYTE_DATATYPE = "b"


class MSO64b:
    """
    Expose some scpi commands to collect data.
    NB.(!) Do NOT issue *rst as this clears the GUI ...
    """

    def __init__(self, instrument_ip=INSTRUMENT_IP, port=PORT):
        self.resource_manager = pyvisa.ResourceManager()
        self.instr = self.resource_manager.open_resource(
            f"TCPIP0::{instrument_ip}::{port}::SOCKET"
        )
        self.instr.timeout = 1000 * 60
        self.instr.encoding = "latin_1"
        self.instr.write_termination = "\n"
        self.instr.read_termination = "\n"
        self.verbose = True
        self.delay = 1
        self.idn()

    def idn(self):
        """Prints the identification string of the MSO64B instrument."""
        print(self.cmd("*idn?", query=True))
        time.sleep(self.delay)

    def save_waveform(self, waveform_name, filename):
        """Saves a specified waveform to a file on the MSO64B instrument.
        Args:
            waveform_name (str): The name of the waveform to be saved.
            filename (str): The name of the file where the waveform will be saved.

        Returns:
            None
        """
        cmd = f'SAVE:WAVEFORM {waveform_name},"{filename}"'
        self.cmd(cmd)

    def cmd(self, scpi, query=False):
        """Sends a SCPI command to the MSO64B instrument.

        Args:
            scpi (str): The SCPI command to be sent to the instrument.
            query (bool, optional): If True, the method will query the instrument
                                    and return the response. Defaults to False.

        Returns:
            str or None: The response from the instrument if querying, otherwise None.

        Raises:
            Exception: If a reset command is attempted, as it clears all defined
                waveforms.
        """
        print(scpi)
        value = None
        if "*rst" in scpi.lower():
            raise Exception(  # pylint: disable=W0719
                "don't RST! this clears all the defined waveforms!"
            )

        if self.verbose:
            print(f"Sending {scpi} ...")

        if query:
            print("reading scpi")
            value = self.instr.query(scpi)
            time.sleep(self.delay)
            return value
        print("writing scpi")
        self.instr.write(scpi)
        time.sleep(self.delay)
        return scpi

    def synch(self):
        """Synchronizes the MSO64B instrument."""

        _ = self.cmd("*opc?", query=True)  # sync

    def set_encoding(self, encoding="SFPBINARY"):
        """Sets the data encoding format for the MSO64B instrument.

        Args:
            encoding (str, optional): The encoding format to be used.

        Returns:
            None
        """

        # {ASCIi|RIBinary|RPBinary|FPBinary|SRIbinary|SRPbinary|SFPbinary}
        self.cmd(f"data:encdg {encoding}")

    def acquire_data(self, input_source: str) -> np.ndarray:
        """Acquires waveform data from the specified input source.

        This method retrieves data from the MSO64B instrument based on the
        provided input source, which can be a channel or a math function. It
        configures the necessary settings, initiates the acquisition, and
        returns the acquired data as a NumPy array.

        Args:
            input_source (str): The source from which to acquire data,
                such as a channel or math function.

        Returns:
            np.ndarray: The acquired waveform data as a NumPy array.

        Raises:
            Exception: If there is an error during the data acquisition process.
        """
        is_math = input_source.lower().startswith("math")
        # see programmer manual p 2-104

        if is_math:
            # MATH1 etc
            # these settings work
            bytes_per_datapoint = 4
            datatype = FLOAT_DATATYPE
            encoding = "SFPBINARY"
            is_big_endian = False
        else:
            # CH1 ..
            bytes_per_datapoint = 1
            datatype = BYTE_DATATYPE
            encoding = "SRIbinary"
            is_big_endian = False

        # self.cmd("autoset EXECUTE")
        self.cmd("HEADER 0")
        self.cmd(f"DATA:SOUR {input_source}")
        self.set_encoding(encoding)
        self.cmd(f"DAT:WIDTH {bytes_per_datapoint}")
        self.cmd("DAT:START 1")
        record_length = self.get_record_length()
        print(f"record length = {record_length}")
        self.cmd(f"DAT:STOP {record_length}")
        self.cmd("acquire:state 0")
        self.cmd("acquire:stopafter SEQUENCE")

        # Added by Ravi
        self.cmd("ACQuire:MODe AVErage")
        number_of_spectra_averaged = 100
        self.cmd(f"ACQuire:NUMAVg {number_of_spectra_averaged}")

        self.cmd("acquire:state 1")  # run
        _ = self.cmd("*opc?", query=True)  # sync
        print("running curve query ...")

        raw_data = self.instr.query_binary_values(
            "curve?", datatype=datatype, container=np.array, is_big_endian=is_big_endian
        )

        print("data acquired!")
        return raw_data

    def get_record_length(self):
        """
        Retrieves the record length from the MSO64B instrument.
        """
        max_attempts = 10
        cmd = "HORizontal:MODE:RECOrdlength?"

        attempts = 0
        while attempts < max_attempts:
            try:
                record_length = int(self.cmd(cmd, query=True))
                return record_length
            except Exception as ex:  # pylint: disable=W0718
                attempts += 1
                print(f"Error getting record length: {ex}")
                print(f"Number of attempts = {attempts}")
                print("Waiting before trying again ..")
                time.sleep(2)
        raise Exception(  # pylint: disable=W0719
            "Could not get record length after 5 attempts!?"
        )

    def close(self):
        """
        Closes the connection to the MSO64B instrument.
        """
        self.instr.close()
