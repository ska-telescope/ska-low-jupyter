"""
Utility functions for handling UV visibility data.
"""

import numpy as np
from astropy.time import Time
from pyuvdata import UVData


# pylint: disable=R0914(too-many-locals)
def get_uv_visibility_data(obs_path: str) -> tuple:
    """Grabs relevant and useful data out of visibility files

    :param obs_path: Path to the visibility directory
    :returns: a tuple of numpy arrays including:
        combined_data: all non-nan data
        frequency_samples_megahertz: the MHz values of the spectra
        mean_spectra: mean spectra (time scrunched)
        pol_xx_mean_spectra: mean spectra (time scrunched) of the XX polarisation
        pol_yy_mean_spectra: mean spectra (time scrunched) of the yy polarisation
        time_samples: astropy Time objects for each timestamp
        mean_time_series: mean time series (frequency scrunched)
    """
    # Grab data
    uv_data = UVData.from_file(obs_path, use_future_array_shapes=True)
    print(f"Getting pol {uv_data.get_pols()[0]}")
    pol_xx = uv_data.get_data((0, 0, uv_data.polarization_array[0]))
    print(f"Getting pol {uv_data.get_pols()[1]}")
    pol_yy = uv_data.get_data((0, 0, uv_data.polarization_array[1]))
    combined_data = np.array(pol_xx) + np.array(pol_yy)

    time_samples = Time(
        uv_data.get_times(0, 0, uv_data.polarization_array[0]),
        format="jd",
    ).iso

    # Should already be in MHz
    frequency_samples_megahertz = uv_data.freq_array / 1e6

    # Get mean spectra
    mean_spectra = np.nanmean(combined_data, axis=0)
    pol_xx_mean_spectra = np.nanmean(pol_xx, axis=0)
    pol_yy_mean_spectra = np.nanmean(pol_yy, axis=0)
    # Get mean time series
    mean_time_series = np.nanmean(combined_data, axis=1)

    return (
        pol_xx,
        pol_yy,
        frequency_samples_megahertz,
        mean_spectra,
        pol_xx_mean_spectra,
        pol_yy_mean_spectra,
        time_samples,
        mean_time_series,
    )
