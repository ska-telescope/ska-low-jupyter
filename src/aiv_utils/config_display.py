"""
Module containing notebook markup helpers.

I think this is unused and can be considered deprecated.
"""

import datetime

import pandas
import tango
from IPython.display import JSON, Markdown, display

__all__ = ["utc_now", "as_json_tree", "display_device", "log_link"]


KIBANA_URL = """https://k8s.stfc.skao.int/kibana/app/dashboards#/view/c59d04e0-4221-11ee-968a-b75fa630781d?_g=(time:(from:'{}',to:'{}'))"""  # pylint: disable=C0301


def utc_now() -> str:
    """Generate utc_now as a elastic compatible iso8601 string

    :return: the formatted string
    """
    return datetime.datetime.utcnow().isoformat(timespec="milliseconds") + "Z"


def as_json_tree(json_doc, expanded=False) -> None:
    """Display json as a notebook cell tree.

    :param json_doc: the json string/dict
    :param expanded: whether to expand the tree, defaults to False

    """
    display(JSON(json_doc, expanded=expanded))


def display_device(dev: tango.DeviceProxy) -> None:
    """Display a list of tango device attributes.

    :param dev: the tango device proxy
    """
    data_frame = pandas.DataFrame(dev.get_attribute_list(), columns=["All Attributes"])
    display(Markdown(data_frame.to_markdown(index=False)))

    data_frame = pandas.DataFrame(dev.get_command_list(), columns=["All Commands"])
    display(Markdown(data_frame.to_markdown(index=False)))


def log_link(start_time: str, end_time: str) -> None:
    """Display kibana log link in notebook.

    :param start_time: the start time of the logs to capture
    :param end_time: the end time of the logs to capture
    """
    query_url = KIBANA_URL.format(start_time, end_time)
    content = f"## Execution logs\n[Kibana: {start_time} to {end_time}]({query_url})"
    display(Markdown(content))
