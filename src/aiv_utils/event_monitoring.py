"""
Monitor events from devices
"""

import tango
from ska_control_model import ObsState


class EventMonitor:
    """Monitors events from a list of devices and manages subscriptions.

    This class allows for subscribing to and unsubscribing from event
    changes for specified attributes of devices. It handles the
    registration of event handlers and maintains a mapping of active
    subscriptions.

    Args:
        devices (list): A list of device objects to monitor for events.
    """

    def __init__(self, devices):
        self.devices = devices
        self.subscription_map = {}

    def change_handler(self, event):
        """
        Handles the event when a subscribed attribute changes.

        Args:
            event: The event object containing information about the change.
        """

        print(event)

    def subscribe(self, attribute):
        """Subscribes to change events for a specified attribute on all devices.

        Args:
            attribute (str):
                The name of the attribute to subscribe to for change events.
        """

        for device in self.devices:
            if hasattr(device, attribute):
                subscription_id = device.subscribe_event(
                    attribute, tango.EventType.CHANGE_EVENT, self.change_handler
                )
                print(f"subscribed {device.name()}.{attribute} change events ... ")
                if device in self.subscription_map:
                    self.subscription_map[device].append(subscription_id)
                else:
                    self.subscription_map[device] = [subscription_id]
            else:
                print(
                    f"{device.name()} does not have attribute {attribute} - not subscribing!"  # pylint: disable=C0301
                )

    def unsubscribe_all(self):
        """Unsubscribes from all events for all devices."""

        for device in self.subscription_map:  # pylint: disable=C0206
            for subscription_id in self.subscription_map[device]:
                print(f"unsubscribing {device.name()} {subscription_id} ... ")
                try:
                    device.unsubscribe_event(subscription_id)
                except KeyError:
                    pass


class ScanStarted(Exception):
    """Exception raised when a scanning process is initiated."""


class SDPSubarrayObsState(EventMonitor):
    """Monitors the observation state of SDP subarrays.

    This class extends the EventMonitor to specifically handle changes
    in the observation state of SDP subarrays. It allows for the
    subscription to these state events and can raise errors based on
    specific conditions.

    Args:
        devices (list): A list of device objects to monitor for observation state
            changes.
        raise_error_on_scan (bool, optional): A flag indicating whether to raise
            an error when scanning starts.

    Methods:
        change_handler(event): Handles change events for device attributes and
            processes errors.
        _get_human_value(value): Retrieves the human-readable name for a given
            observation state value.
    """

    def __init__(self, devices, raise_error_on_scan=False):
        self.raise_error_on_scan = raise_error_on_scan
        super().__init__(devices)
        self.subscribe("sdpSubarrayObsState")

    def change_handler(self, event):
        """Handles change events for device attributes and processes errors.

        Args:
            event: The event object containing details about the attribute change.
        Raises:
            ScanStarted:
                If the event indicates that scanning has started and the flag is set.
        """

        event_name = event.attr_name
        if event.err:
            error = (
                f"ERROR! error flag set for device={event.device} Error: {event.errors}"
            )
        else:
            error = ""

        if event.attr_value is not None:
            value = event.attr_value.value  # the enum value not the human readable form
            human_value = self._get_human_value(value)
            if human_value == "SCANNING":
                if self.raise_error_on_scan:
                    raise ScanStarted()

            print(f"event {event_name} {human_value} {error}")

    def _get_human_value(self, value):
        """Retrieves the human-readable name for a given observation state value.

        Args:
            value: The observation state value to be translated.

        Returns:
            str: The human-readable name associated with the given value, or
                None if not found.
        """
        for name, item in ObsState.__members__.items():
            if value == item.value:
                return name
        return None


class ObsStateMonitor(EventMonitor):
    """Monitors observation state changes for a set of devices.

    This class extends the EventMonitor to specifically handle changes
    in observation states. It allows for the subscription to observation
    state events and can raise errors based on specific conditions.

    Args:
        devices (list): A list of device objects to monitor for observation state
            changes.
        raise_error_on_scan (bool, optional): A flag indicating whether to raise an
            error when scanning starts.

    Methods:
        change_handler(event): Handles change events for device attributes
            and processes errors.
        _get_human_value(value): Retrieves the human-readable name for a given
            observation state value.
    """

    def __init__(self, devices, raise_error_on_scan=False):
        self.raise_error_on_scan = raise_error_on_scan
        super().__init__(devices)
        self.subscribe("obsState")

    def change_handler(self, event):
        """Handles change events for device attributes and processes errors.

        Args:
            event: The event object containing details about the attribute change.

        Raises:
            ScanStarted: If the event indicates that scanning has started and
            the flag is set.
        """

        event_name = event.attr_name
        if event.err:
            error = (
                f"ERROR! error flag set for device={event.device} Error: {event.errors}"
            )
        else:
            error = ""

        if event.attr_value is not None:
            value = event.attr_value.value  # the enum value not the human readable form
            human_value = self._get_human_value(value)
            if human_value == "SCANNING":
                if self.raise_error_on_scan:
                    raise ScanStarted()

            print(f"event {event_name} {human_value} {error}")

    def _get_human_value(self, value):
        """Retrieves the human-readable name for a given observation state value.

        Args:
            value: The observation state value to be translated.

        Returns:
            str: The human-readable name associated with the given value, or
                None if not found.
        """

        for name, item in ObsState.__members__.items():
            if value == item.value:
                return name
        return None


class HealthStateMonitor(EventMonitor):
    """
    Listen to devices
    """

    HEALTH_MAP = {0: "OK", 1: "DEGRADED", 2: "FAILED", 3: "UNKNOWN"}

    def __init__(self, devices):
        super().__init__(devices)
        self.central_node = None
        for device in devices:
            if "central_node" in device.name():
                self.central_node = device
        self.not_ok = []
        self.ok = []  # pylint: disable=invalid-name
        self.subscribe("healthState")
        if self.central_node is not None:
            self.subscribe("telescopehealthstate")
        # this will cause initial population of events
        # so we reset
        self.not_ok = []
        self.ok = []

    def change_handler(self, event):
        """Handles change events for device attributes.

        Args:
            event: The event object containing details about the attribute change.
        """

        event_name = event.attr_name
        # Guessing this is ok:
        reception_time = (
            event.reception_date.tv_sec
            + event.reception_date.tv_usec / 1.0e6
            + event.reception_date.tv_nsec / 1.0e9
        )
        if event.err:
            error = (
                f"ERROR! error flag set for device={event.device} Error: {event.errors}"
            )
        else:
            error = ""

        if event.attr_value is not None:
            value = event.attr_value.value  # the enum value not the human readable form
            try:
                human_value = self.HEALTH_MAP[value]
            except KeyError:
                human_value = ""

            if human_value != "OK":
                self.not_ok.append((event_name, human_value, reception_time))
            else:
                self.ok.append((event_name, human_value, reception_time))
            print(f"event {event_name} {human_value} {error}")

    def print_timeline(self, cutoff=None):
        """Prints a timeline of unhealthy events for devices.

        Args:
            cutoff (datetime, optional): The minimum timestamp for events to be printed.
        """

        time_ordered = sorted(self.not_ok, key=lambda triple: triple[2])
        if cutoff is None:
            for triple in time_ordered:
                time = triple[2]
                trl = triple[0]
                health_state = triple[1]
                print(f"{time}: {trl}.healthState = {health_state}")
        else:
            for triple in time_ordered:
                time = triple[2]
                if time >= cutoff:
                    trl = triple[0]
                    health_state = triple[1]
                    print(f"{time}: {trl}.healthState = {health_state}")

    @property
    def unhealthy_devices(self):
        """Retrieves a list of devices that are currently unhealthy.

        Returns:
            list: A list of device names that are considered unhealthy.
        """

        return [triple[0] for triple in self.not_ok]

    def raised_unhealthy_event(self, device_name):
        """Checks if an unhealthy event has been raised for a specific device.

        Args:
            device_name (str): The name of the device to check for unhealthy events.

        Returns:
            bool: True if an unhealthy event is raised for the device, False otherwise.
        """
        for triple in self.not_ok:
            trl = triple[0]
            if device_name in trl:
                return True
        return False
