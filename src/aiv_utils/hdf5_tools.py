"""
Utility functions for handling hdf5 files.
"""

import os

import h5py
import numpy as np
from astropy.coordinates import Angle, SkyCoord
from astropy.time import Time


# pylint: disable=R0914(too-many-locals)
def get_hdf5_beamformed_data(obs_file: str, start_channel: int = 64) -> tuple:
    """Grabs relevant and useful data out of hdf5 files

    :param obs_file: Path to the hdf5 file
    :param start_channel: The channel ID of the first channel for the file. Default: 64.
    :returns: a tuple of numpy arrays including:
        combined_data: all non nan data
        frequency_samples: the MHz values of the spectra
        mean_spectra: mean spectra (time scrunched)
        time_samples: astropy Time objects for each timestamp
        mean_time_series: mean time series (frequency scrunched)
    """
    # Grab data
    h5_data = h5py.File(obs_file)
    pol_0 = h5_data["polarization_0"]["data"]
    pol_1 = h5_data["polarization_1"]["data"]
    combined_data = np.array(pol_0) + np.array(pol_1)

    # Get last time sample
    for i in range(
        h5_data["sample_timestamps"]["data"].shape[0] - 1,  # pylint: disable=E1101
        -1,
        -1,
    ):
        last_time_stamp = h5_data["sample_timestamps"]["data"][i][0]
        last_time_id = i
        if last_time_stamp != 0:
            break

    # Grab last freq sample
    combined_data_freq_mean = np.nanmean(combined_data, axis=0)
    found_first_sample = False
    for freq_id, freq_val in enumerate(combined_data_freq_mean):
        if found_first_sample:
            if freq_val == 0.0:
                last_freq_id = freq_id - 1
                break
        else:
            if freq_val != 0.0:
                found_first_sample = True
    # Grab first freq sample
    first_freq_id = 0
    for freq_id in range(last_freq_id - 1, -1, -1):
        if combined_data_freq_mean[freq_id] == 0.0:
            first_freq_id = freq_id + 1
            break

    # Get actual valid data
    combined_data = combined_data[
        :last_time_id, first_freq_id : last_freq_id + 1  # noqa: E203
    ]
    # Get mean spectra
    mean_spectra = np.nanmean(combined_data, axis=0)
    pol_0_mean_spectra = np.nanmean(
        pol_0[:last_time_id, first_freq_id : last_freq_id + 1], axis=0  # noqa: E203
    )
    pol_1_mean_spectra = np.nanmean(
        pol_1[:last_time_id, first_freq_id : last_freq_id + 1], axis=0  # noqa: E203
    )
    # Get mean time series
    mean_time_series = np.nanmean(combined_data, axis=1)

    # Get frequency samples
    start_freq_mhz = (first_freq_id + start_channel) * (400 / 512)
    end_freq_mhz = (last_freq_id + start_channel) * (400 / 512)
    frequency_samples_megahertz = np.linspace(
        start_freq_mhz, end_freq_mhz, last_freq_id - first_freq_id + 1
    )

    # Get time samples
    time_samples = []
    for i in range(last_time_id):
        time_stamp = h5_data["sample_timestamps"]["data"][i][0]
        time_samples.append(Time(time_stamp, format="gps", scale="utc"))

    # Put together some metadata
    # metadata = {
    #     "start_chan_id": first_freq_id,
    #     "end_chan_id": last_freq_id,
    #     "start_freq_MHz": start_freq_mhz,
    #     "end_freq_MHz": end_freq_mhz,
    #     "duration_sec": h5_data["sample_timestamps"]["data"][last_time_id][0]
    #     - h5_data["sample_timestamps"]["data"][0][0],
    # }

    return (
        combined_data,
        frequency_samples_megahertz,
        mean_spectra,
        pol_0_mean_spectra,
        pol_1_mean_spectra,
        time_samples,
        mean_time_series,
    )


def insert_hdf5_metadata(  # pylint: disable=R0913,R0912
    hdf5_path: str,
    reference_frame: str,
    observer: str = None,
    ra_deg: float = None,
    ra_str: str = None,
    dec_deg: float = None,
    dec_str: str = None,
    alt_deg: float = None,
    az_deg: float = None,
    target_name: str = None,
    intent: str = None,
    notes: str = None,
    jira_ticket: str = None,
):
    """Insert metadata into an hdf5 file.

    The function can be used like so to get the same metadata as the
    :ref:`docs example <user/index:Metadata>`:

    .. code-block:: python

        insert_hdf5_metadata(
            hdf5_path="path/to/hdf5/file.hdf5",
            reference_frame="ICRS",
            observer="riley.keel",
            ra_deg=123.45,
            ra_str="08:13:36.000",
            dec_deg=-45.00,
            dec_str="-45:00:00.000",
            target_name="NGC254",
            intent="Checking tracking",
            notes="Attempt 985 to get a good track on this target,
            jira_ticket="LCO-40",
        )

    :param hdf5_path: Path to the hdf5 file.
    :param reference_frame: Reference frame in which the scan's phase centre (pointing)
        coordinates are defined. One of "ICRS" (RA and dec), "AltAz"(alt and az and).
    :param observer: The observer's name.
    :param ra_deg: Right Ascension of the scan's phase centre, in degrees.
    :param ra_str: Right Ascension of the scan's phase centre,
        in the format "hh:mm:ss.sss".
    :param dec_deg: Declination of the scan's phase centre, in degrees.
    :param dec_str: Declination of the scan's phase centre,
        in the format "±dd:mm:ss.sss".
    :param alt_deg: Altitude (angle above the horizon) of the scan's phase centre,
        in degrees.
    :param az_deg: Azimuth (clockwise angle from North) of the scan's phase centre,
        in degrees.
    :param target_name: Provide the complete object name.
        That is, use the pattern "catalog_name object_identifier", e.g. PSR J0835-4510.
        Explicitly include the "J" in names based on J2000 coordinates (optional).
    :param intent: A short description of the purpose of the observation
        (e.g. Pulsar timing test) (optional).
    :param notes: Can be used as a longer description but due to the character limit,
        consider using "jira_ticket" for anything detailed (optional).
    :param jira_ticket: A link to the Jira ticket which contains longer
        observation notes (e.g. LCO-40) (optional).
    """
    # Make some assertions to ensure we have all the required data

    # Check if the file exists
    if not os.path.isfile(hdf5_path):
        raise FileNotFoundError(f"The file at {hdf5_path} does not exist.")

    # Check reference frame and pointing coordinates
    if reference_frame == "ICRS":
        # Check if a degrees or string representation of RA and Dec is provided
        if ra_deg is None and ra_str is None:
            raise ValueError("Either ra_deg or ra_str must be provided")
        if dec_deg is None and dec_str is None:
            raise ValueError("Either dec_deg or dec_str must be provided")
        if ra_deg is not None and ra_str is None:
            coord = SkyCoord(ra=ra_deg, dec=0, unit="deg")
            ra_str = str(coord.ra.to_string(unit="hourangle", sep=":", precision=3))
        if dec_deg is not None and dec_str is None:
            coord = SkyCoord(ra=0, dec=dec_deg, unit="deg")
            dec_str = str(coord.dec.to_string(unit="deg", sep=":", precision=2))
        if ra_deg is None:
            ra_angle = Angle(ra_str, unit="hourangle")
            ra_deg = ra_angle.deg
        if dec_deg is None:
            dec_angle = Angle(dec_str, unit="deg")
            dec_deg = dec_angle.deg
    elif reference_frame == "AltAz":
        if alt_deg is None or az_deg is None:
            raise ValueError("Both alt_deg and az_deg must be provided")
    else:
        raise ValueError("reference_frame must be either 'ICRS' or 'AltAz'")

    # Check observer
    if observer is None:
        if "JUPYTERHUB_USER" in os.environ:
            observer = os.environ["JUPYTERHUB_USER"]
        else:
            raise ValueError("Observer must be provided")

    # Insert metadata
    with h5py.File(hdf5_path, "a") as hdf5_file:
        hdf5_metadata = hdf5_file["observation_info"].attrs

        hdf5_metadata["reference_frame"] = reference_frame
        hdf5_metadata["observer"] = observer
        if target_name is not None:
            hdf5_metadata["target_name"] = target_name
        if intent is not None:
            hdf5_metadata["intent"] = intent
        if notes is not None:
            hdf5_metadata["notes"] = notes
        if jira_ticket is not None:
            hdf5_metadata["jira_ticket"] = jira_ticket

        if reference_frame == "ICRS":
            hdf5_metadata["ra"] = ra_deg
            hdf5_metadata["ra_str"] = ra_str
            hdf5_metadata["dec"] = dec_deg
            hdf5_metadata["dec_str"] = dec_str
        elif reference_frame == "AltAz":
            hdf5_metadata["alt"] = alt_deg
            hdf5_metadata["az"] = az_deg
