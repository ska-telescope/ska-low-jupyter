"""
General SKA-LOW calibration functions.
"""

import itertools
import json
import os
import re
import time
from datetime import datetime, timedelta, timezone
from typing import Sequence

import numpy as np

from aiv_utils.low_utils import get_mccs_device, get_sps_devices
from aiv_utils.metadata import get_station_antenna_dataframe
from aiv_utils.tango_utils import wait_lrc


def apply_with_load_time(sps_station, load_time_seconds: float = 1) -> None:
    """
    Wrapper to SpsStation.ApplyCalibration with an increased load time to avoid SKB-702.

    :param sps_station: SpsStation tango device proxy.
    :param load_time_seconds: How many seconds in the future for the ApplyCalibration
        command to run on the tiles.

    :raises ValueError: If SpsStation.ApplyCalibration fails for any reason.
    """
    [[result], [message]] = sps_station.ApplyCalibration(
        (datetime.now(timezone.utc) + timedelta(seconds=load_time_seconds)).strftime(
            "%Y-%m-%dT%H:%M:%S.%fZ"
        )
    )
    if result != 0:
        raise ValueError(f"ApplyCalibration failed with message {message}.")
    time.sleep(load_time_seconds)


# TODO: This will be redundant in MCCS 0.23.
def parse_selection_policy_string(selection_policy: str) -> dict:
    """
    Convert the string representation of the selection policy into a dictionary.

    :param selection_policy: The string representation of a selection policy, as
        returned by the selectionPolicy attribute.

    :return: A dictionary containing the selection policy type and attributes. It is in
        the same format as required by UpdateSelectionPolicy.

    :raises ValueError: If we cannot match the string to a known selection policy.
    """
    # Number detection regex. Detects signed and unsigned floats and integers.
    pattern = r"[-+]? (?: (?: \d* \. \d+ ) | (?: \d+ \.? ) )(?: [Ee] [+-]? \d+ ) ?"
    numeric_regex = re.compile(pattern, re.VERBOSE)
    policy_attributes = numeric_regex.findall(selection_policy)

    # SelectPreferred doesn't specify a temperature tolerance
    if len(policy_attributes) == 1:
        return {
            "policy_name": "preferred",
            "frequency_tolerance": int(policy_attributes[0]),
        }
    # SelectClosestInRange specifies temperature and frequency tolerances.
    if len(policy_attributes) == 2:
        return {
            "policy_name": "closest_in_range",
            "temperature_tolerance": float(policy_attributes[0]),
            "frequency_tolerance": float(policy_attributes[1]),
        }
    if selection_policy == "No policy defined!":
        return {}
    raise ValueError("Unrecognised selection policy string format.")


def mask_antennas_by_number(
    station_name: str,
    antennas_tpm_numbering: Sequence[int],
    *,
    total_nof_channels: int = 384,
) -> None:
    """
    Zero out specific antennas in the calibration coefficients by tpm numbering.

    :param station_name: Name of the station to apply solutions to.
    :param antennas_tpm_numbering: Antennas to mask in zero-based tpm ordering.
    :param total_nof_channels: Total number of channels that the beamformer supports.

    :raises ValueError: If any tango device commands fail.
    """
    # Switch calibration banks to put the current coefficients into the inactive bank
    sps_station, _, _, _ = get_sps_devices(station_name)

    apply_with_load_time(sps_station)

    for ant in antennas_tpm_numbering:
        # Set calibration coefficients to be all 0. Each channel has 4 polarisations,
        # which is split into a real and imaginary part.
        calibrations = [0] * (total_nof_channels * 4 * 2 + 1)
        # The first element in the coefficients is the antenna to calibrate.
        calibrations[0] = float(ant)
        # Load coefficients for this antenna into the inactive bank.
        [[result], [message]] = sps_station.LoadCalibrationCoefficients(calibrations)
        if result != 0:
            raise ValueError(
                f"LoadCalibrationCoefficients failed with message {message}."
            )

    # Switch calibration banks to put the previous coefficients, with masked antennas
    # zeroed, into the active bank
    apply_with_load_time(sps_station)


def mask_antennas_by_name(station_name: str, antenna_names: list[str]) -> None:
    """
    Zero out specific antennas in the calibration coefficients by name (e.g. sb01-01).

    :param station_name: Name of the station to apply solutions to.
    :param antennas_tpm_numbering: Antennas to mask by name.
    :param total_nof_channels: Total number of channels that the beamformer supports.
    """
    antenna_df = get_station_antenna_dataframe(station_name)
    antenna_name_to_antnum_tpm = dict(
        zip(antenna_df["antenna_name"], antenna_df["antnum_tpm"])
    )
    antenna_numbers_tpm = [
        antenna_name_to_antnum_tpm[antenna_name] for antenna_name in antenna_names
    ]
    mask_antennas_by_number(station_name, antenna_numbers_tpm)


def mask_known_bad_antennas(station_name: str, *, num_ants_in_tpm=16) -> None:
    """
    Apply zero calibration to antennas masked or undefined in the telmodel.
    """
    antenna_df = get_station_antenna_dataframe(station_name)

    # get all antenna numbers for all TPMs defined in telmodel
    all_ants_in_tpms = np.concatenate(
        [
            np.arange(num_ants_in_tpm) + tpm_id * num_ants_in_tpm
            for tpm_id in antenna_df["tpm_id"].unique()
        ]
    )

    # subtract all unmasked antennas in telmodel
    unmasked_ants = antenna_df["antnum_tpm"][~antenna_df["masked"]]
    masked_antenna_numbers = np.setdiff1d(all_ants_in_tpms, unmasked_ants)
    mask_antennas_by_number(station_name, masked_antenna_numbers)


def gain_solns_to_cal_table(
    beamformer_table: np.ndarray,
    gain_solns_array: np.ndarray,
    *,
    n_ant: int = 256,
    gain_solns_start_channel: int = 64,
    total_nof_channels: int = 384,
) -> np.ndarray[complex]:
    """
    Create a table of gain solutions based on the channels we will beamform.

    :param beamformer_table: Beamformer table to use for applying calibration solutions.
        It should be in the same format as what sps_station.beamformertable returns.
    :param gain_solns_array: A numpy array containing the full gain solutions.
        The expected shape is [channels, antennas, polarisations].
    :param n_ant: The number of antennas in this station.
    :param gain_solns_start_channel: The coarse channel that the gain solutions start at
    :param total_nof_channels: Total number of channels that the beamformer supports.

    :return: A 2D complex valued numpy array containing the gain solutions for each
        beamformer channel that we will calibrate on with shape [channel, antenna].

    :raises ValueError: If beamformer table is illegal or inconsistent.
    """
    calibration_table = np.full(
        (total_nof_channels, n_ant, 4),
        np.array([1 + 0j, 0 + 0j, 0 + 0j, 1 + 0j]),
        dtype="complex128",
    )

    for block, table_entry in enumerate(beamformer_table):
        channel = table_entry[0]
        beamformer_channel = block * 8
        if channel == 0:
            # channel set to 0 marks unused (unconfigured) table entries.
            # These do not require calibration
            continue

        if channel < gain_solns_start_channel:
            raise ValueError(
                f"Channel {channel} is less than the specified starting channel"
                f" {gain_solns_start_channel} of the gain solutions."
            )
        for _ in range(8):  # 8 channels per block
            if beamformer_channel >= total_nof_channels:
                raise ValueError(
                    f"Beamformer channel {beamformer_channel} exceeds"
                    f" number of supported beamformer channels {total_nof_channels}. "
                )

            if channel - gain_solns_start_channel >= gain_solns_array.shape[0]:
                raise ValueError(
                    f"Channel {channel} in beamformer table is beyond the gain "
                    f"solutions provided in There are {gain_solns_array.shape[0]} "
                    f"channels in the gain solutions, and the gain solutions have "
                    f"been specified to start at {gain_solns_start_channel}."
                )

            calibration_table[beamformer_channel] = gain_solns_array[
                channel - gain_solns_start_channel, :, :
            ]
            beamformer_channel += 1
            channel += 1
    return calibration_table


def apply_cal_from_array(  # pylint: disable=too-many-locals
    station_name: str,
    gain_solns_array: np.ndarray,
    *,
    is_channels_first: bool = True,
    gain_solns_start_channel: int = 64,
    total_nof_channels: int = 384,
) -> None:
    """
    Use SpsStation to apply the provided calibration solution, given as a numpy array.

    :param station_name: Name of the station to apply solutions to.
    :param gain_solns_array: A numpy array containing the full gain solutions.
        The expected shape is [channels, antennas, polarisations].
    :param is_channels_first: Whether axis 0 is channels. If false, then it is assumed
        that axis 0 is antennas instead.
    :param gain_solns_start_channel: Coarse channel that the gain solutions start at
    :param total_nof_channels: Total number of channels that the beamformer supports.

    :raises ValueError: If any tango device commands fail.
    """
    sps_station, _, tpms, _ = get_sps_devices(station_name)
    n_ant = 16 * len(tpms)

    if not is_channels_first:
        gain_solns_array = gain_solns_array.swapaxes(0, 1)

    if not gain_solns_array.shape[1] == n_ant:
        raise ValueError(
            f"Invalid shape {gain_solns_array.shape} for gain_solns_array."
            f" Expected number of antennas is {n_ant}, "
            f"actually {gain_solns_array.shape[1]}"
        )

    beamformer_table = sps_station.beamformertable
    beamformer_table = np.array(beamformer_table).reshape((-1, 7))

    calibration_table = gain_solns_to_cal_table(
        beamformer_table,
        gain_solns_array,
        n_ant=n_ant,
        gain_solns_start_channel=gain_solns_start_channel,
        total_nof_channels=total_nof_channels,
    )
    for antenna in range(n_ant):
        calibrations = [float(antenna)]
        for channel in range(total_nof_channels):
            for jones in calibration_table[channel, antenna]:
                calibrations.append(jones.real)
                calibrations.append(jones.imag)
        [[result], [message]] = sps_station.LoadCalibrationCoefficients(calibrations)
        if result != 0:
            raise ValueError(
                f"LoadCalibrationCoefficients failed with message {message}."
            )
    apply_with_load_time(sps_station)


def apply_cal_from_file(
    station_name: str,
    gain_solns_path: str | os.PathLike,
    *,
    is_channels_first: bool = True,
    gain_solns_start_channel: int = 64,
    total_nof_channels: int = 384,
) -> None:
    """
    Use SpsStation to apply the provided calibration solution in .npy format.

    :param station_name: Name of the station to apply solutions to.
    :param gain_solns_path: Path to a .npy file containing the full gain solutions.
        The expected shape is [channels, antennas, polarisations] or
        [antennas, channels, polarisations] if is_channels_first is False.
    :param is_channels_first: Whether axis 0 is channels. If false, then it is assumed
        that axis 0 is antennas instead.
    :param gain_solns_start_channel: Coarse channel that the gain solutions start at
    :param total_nof_channels: Total number of channels that the beamformer supports.
    """
    gain_solns_array = np.load(gain_solns_path, allow_pickle=False)
    apply_cal_from_array(
        station_name,
        gain_solns_array,
        is_channels_first=is_channels_first,
        gain_solns_start_channel=gain_solns_start_channel,
        total_nof_channels=total_nof_channels,
    )


def apply_unity_cal(station_name: str, *, total_nof_channels: int = 384) -> None:
    """
    Apply unity calibration [1,0,0,1] to all antennas, ignoring the beamformer table.

    This function is primarily for testing as well as returning the calibration
    coefficients to a known state.

    :param station_name: Name of the station to apply solutions to.
    :param total_nof_channels: Total number of channels that the beamformer supports.
    """
    sps_station, _, tpms, _ = get_sps_devices(station_name)
    n_ant = 16 * len(tpms)

    for ant in range(n_ant):
        # Set calibration coefficients to be all 0. Each channel has 4 polarisations,
        # which is split into a real and imaginary part.
        calibrations = [0] * (total_nof_channels * 4 * 2 + 1)
        # Set xx and yy to have calibration 1+0j
        for i in range(1, len(calibrations), 4):
            calibrations[i] = 1
        # The first element in the coefficients is the antenna to calibrate.
        calibrations[0] = float(ant)

        [[result], [message]] = sps_station.LoadCalibrationCoefficients(calibrations)
        if result != 0:
            raise ValueError(
                f"LoadCalibrationCoefficients failed with message {message}."
            )

    apply_with_load_time(sps_station)


def apply_zeroed_cal(station_name: str, *, total_nof_channels: int = 384):
    """
    Apply zeroed calibration to all antennas, ignoring the beamformer table.

    This function is primarily for testing as well as returning the calibration
    coefficients to a known state.

    :param station_name: Name of the station to apply solutions to.
    :param total_nof_channels: Total number of channels that the beamformer supports.
    """
    sps_station, _, tpms, _ = get_sps_devices(station_name)
    n_ant = 16 * len(tpms)

    for ant in range(n_ant):
        # Set calibration coefficients to be all 0. Each channel has 4 polarisations,
        # which is split into a real and imaginary part.
        calibrations = [0] * (total_nof_channels * 4 * 2 + 1)
        # The first element in the coefficients is the antenna to calibrate.
        calibrations[0] = float(ant)

        [[result], [message]] = sps_station.LoadCalibrationCoefficients(calibrations)
        if result != 0:
            raise ValueError(
                f"LoadCalibrationCoefficients failed with message {message}."
            )

    apply_with_load_time(sps_station)


def create_mccs_beamformer_table(regions: list[int]) -> list[list[int]]:
    """
    Creates and returns the MCCS beamformer table based on the input regions, but
    does not apply it.

    This function is based on sps_station.SetBeamFormerRegions, with some
    modifications to generate a beamformer table in the MCCS station format instead.

    :param regions: A flattened 2D array specifying how the beamformer table should be
        set up. A maximum of 48 regions is supported and the total number of channels
        must be <= 384. Each region comprises:

        * start_channel - (int) region starting channel, must be even in range 0 to 510
        * num_channels - (int) size of the region, must be a multiple of 8
        * beam_index - (int) beam used for this region with range 0 to 47
        * subarray_id - (int) Subarray
        * subarray_logical_channel - (int) logical channel # in the subarray
        * subarray_beam_id - (int) ID of the subarray beam
        * substation_id - (int) Substation
        * aperture_id:  ID of the aperture (station*100+substation?)

    :return: The MCCS beamformer table as a 2D array of ints.

    :raises ValueError: If parameters are illegal or inconsistent.
    """
    if len(regions) < 8:
        raise ValueError("Insufficient parameters specified")
    if len(regions) > (48 * 8):
        raise ValueError("Too many regions specified")
    if len(regions) % 8 != 0:
        raise ValueError("Incomplete specification of region")
    beamformer_table: list[list[int]] = []
    total_chan = 0
    for i in range(0, len(regions), 8):
        region = list(regions[i : i + 8])  # noqa: E203
        start_channel = region[0]
        if start_channel % 2 != 0:
            raise ValueError("Start channel in region must be even")
        nchannels = region[1]
        if nchannels % 8 != 0:
            raise ValueError("Nos. of channels in region must be multiple of 8")
        beam_index = region[2]
        if beam_index < 0 or beam_index > 47:
            raise ValueError("Beam_index is out side of range 0-47")
        total_chan += nchannels
        if total_chan > 384:
            raise ValueError("Too many channels specified > 384")
        subarray_logical_channel = region[4]
        for channel_0 in range(start_channel, start_channel + nchannels, 8):
            entry = [channel_0] + region[2:8]
            entry[3] = subarray_logical_channel
            subarray_logical_channel = subarray_logical_channel + 8
            beamformer_table.append(entry)

    mccs_beamformer_table = [
        [i, *beamformer_table[i]] for i in range(0, len(beamformer_table))
    ]
    return mccs_beamformer_table


def set_up_single_beam(
    channel_start: int, channel_bandwidth: int, beam_index: int
) -> list[list[int]]:
    """
    Returns the MCCS beamformer table needed for a single beam observation.

    This function creates and returns a MCCS beamformer table for the single beam
    observation with the parameters specified. It does not apply them to the MCCS
    station device.

    :param channel_start: The starting channel, must be even in range 0 to 510.
    :param channel_bandwidth: The amount of channels to beamform, must be a multiple
        of 8.
    :param beam_index: The beam to be used, must be in the range 0 to 47.

    :return: The created MCCS beamformer table.
    """
    regions = [channel_start, channel_bandwidth, beam_index, 1, 0, 1, 1, 101]
    return create_mccs_beamformer_table(regions)


def apply_cal_from_store(station_name: str, mccs_beamformer_table: list[int]) -> None:
    """
    Use MccsStation to apply the best calibration solution from the store.

    :param station_name: Name of the station to apply solutions to.
    :param mccs_beamformer_table: Beamformer table to use for determining how to
        apply calibration solutions. Use `set_up_single_beam` or
        `create_mccs_beamformer_table` to create this table as needed.

    :raises ValueError: If any tango device commands fail.
    """
    mccs_station = get_mccs_device(f"low-mccs/station/{station_name}")
    [[result_code], [message]] = mccs_station.ConfigureChannels(
        list(itertools.chain.from_iterable(mccs_beamformer_table))
    )
    if not result_code == 0:
        raise ValueError(
            "ConfigureChannels failed. Check the format of the provided "
            f"beamformer table. MccsStation returned message: {message}"
        )
    wait_lrc(mccs_station, "ApplyConfiguration", "")


def reset_selection_policy(station_name: str) -> None:
    """
    Reset the selection policy so that the most recent stored solution will be used.

    :param station_name: Name of the station to apply solutions to.
    :raises ValueError: If any tango device commands fail.
    """
    cal_store = get_mccs_device(f"low-mccs/calibrationstore/{station_name}")
    [[result_code], [message]] = cal_store.updateSelectionPolicy(
        json.dumps(
            {
                "policy_name": "preferred",
                "frequency_tolerance": 0,
            }
        )
    )

    if not result_code == 0:
        raise ValueError(
            f"UpdateSelectionPolicy failed. MccsStation returned message: {message}"
        )


def set_impossible_selection_policy(station_name: str) -> None:
    """
    Set an impossible selection policy so that no calibrations solutions can be found.

    The calibration store returns a unity calibration when no solutions are found.

    :param station_name: Name of the station to apply solutions to.
    :raises ValueError: If any tango device commands fail.
    """
    cal_store = get_mccs_device(f"low-mccs/calibrationstore/{station_name}")
    [[result_code], [message]] = cal_store.updateSelectionPolicy(
        json.dumps(
            {
                "policy_name": "closest_in_range",
                "frequency_tolerance": -1,
                "temperature_tolerance": -1.0,
            }
        )
    )

    if not result_code == 0:
        raise ValueError(
            f"UpdateSelectionPolicy failed. MccsStation returned message: {message}"
        )
