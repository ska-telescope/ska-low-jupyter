"""
Testing helper functions to store common pytest and xray publishing functionality.
"""

import base64
import inspect
import json
import mimetypes
import os
import pathlib
from datetime import datetime, timedelta
from typing import Any

import nbformat
import recurring_ical_events
import requests
from icalendar import Calendar
from IPython.display import JSON, display
from pytz import timezone

RESERVATION_CALENDAR_URL = "https://confluence.skatelescope.org/rest/calendar-services/1.0/calendar/export/subcalendar/private/%s.ics"  # pylint: disable=C0301

CALENDAR_METADATA = {
    "AAVS3": {
        "UID": "f73ff28c60bfaccfb5e7233e7eba9f06c6ff56c6",
        "overridable_event_ids": [
            # Appears in right sidebar as 'Automated AAVS3 test runs'
            "a746e846-9800-4efb-86c0-16d34508edb2",
        ],
    },
    "ITF": {
        "UID": "b357b04180e81b8930b2d2c71f248c868b378908",
        "overridable_event_ids": [
            # AIV Verification Event
            # (used as week long booking mostly so ignore for now)
            "b02fcb59-bfa8-4a97-8022-8458723d0746",
            # SUT PaSD 2 (not connected to the SPS so ignore)
            "5b9c8d97-d9fc-494d-91a8-96591f7a7f60",
        ],
    },
    "AA0.5": {
        "UID": "7149a34cb565b249b59e775361f38e89c2cdf3d1",
        "overridable_event_ids": [],
    },
}


def make_test_dir(dir_prefix: str = "test", base_dir: str = "build/test_runs") -> str:
    """Create a directory for the test runs of the notebooks to be output to.

    :params dir_prefix: The prefix of the test directory, default is test
    :params base_dir: The base directory that the test directory will be
        made in, the default is build/test_runs
    """
    now = datetime.now()
    output_notebook_dir = f"{base_dir}/{dir_prefix}_{now.strftime('%Y-%m-%d_%H:%M:%S')}"
    os.makedirs(output_notebook_dir)
    return output_notebook_dir


def _grab_tagged_cell(
    notebook: nbformat.NotebookNode, tag: str
) -> nbformat.NotebookNode:
    """Grab the text of a papermill notebook cell with the input tag.

    :params nb: The output object of a papermill run
    :params tag: The tag the cell has been given
    :return cell_string: The output string of the tagged cell
    """
    tagged_cells = [
        cell for cell in notebook["cells"] if tag in cell.metadata.get("tags", [])
    ]
    if len(tagged_cells) == 0:
        raise ValueError(f"No cell with tag {tag} found.")
    if len(tagged_cells) > 1:
        raise ValueError(f"More than 1 cell with tag {tag} found.")
    return tagged_cells[0]


def grab_tagged_cell_text(
    notebook: nbformat.NotebookNode,
    tag: str,
) -> str:
    """Grab the text of a papermill notebook cell with the input tag.

    The output includes all display output of type text/plain and all
    stream output, e.g. from stdout and stderr.

    :params notebook: The output object of a papermill run
    :params tag: The tag the cell has been given
    :return cell_string: The output string of the tagged cell
    """
    cell = _grab_tagged_cell(notebook, tag)
    texts = []
    for out in cell["outputs"]:
        if out["output_type"] == "stream":
            texts.append(out["text"])
        elif out["output_type"] == "display_data":
            texts.append(out["data"].get("text/plain", ""))
    return "".join(texts).strip()


def grab_tagged_cell_json(notebook: nbformat.NotebookNode, tag: str) -> Any:
    """Grab the JSON content of the cell tagged with the input tag.

    There must be only one cell with the tag,
    and only one output with with application/json data in that cell.

    :params notebook: The output object of a papermill run
    :params tag: The tag the cell has been given
    :return: The tagged cell's text output parsed as JSON
    """
    cell = _grab_tagged_cell(notebook, tag)
    [data] = [
        out["data"]["application/json"]
        for out in cell["outputs"]
        if out["output_type"] == "display_data" and "application/json" in out["data"]
    ]
    return data


def dump_locals(*variables):
    """Display the named local variables as a JSON object.

    :params variables: names of local variables to include in JSON output
    """
    frame = inspect.currentframe().f_back

    def json_compat(obj):
        if isinstance(obj, pathlib.Path):
            return str(obj)
        if isinstance(obj, datetime):
            return obj.isoformat()
        return obj

    display(JSON({var: json_compat(frame.f_locals[var]) for var in variables}))


def is_calendar_booked(
    calendar: str,
    expected_runtime_mins: float,
):
    """
    Checks if calendar is booked within input time range.

    :params calendar: Calendar to check (either ITF or AA0.5).
    :params expected_runtime_mins: Expected runtime in minutes,
        will check if calendar is booked for this duration.
    """
    # Get the start and end time of calendar range to check
    start_time = datetime.now(tz=timezone("Australia/Perth"))
    end_time = start_time + timedelta(minutes=expected_runtime_mins)
    # Download the calendar ics file
    calendar_uid = CALENDAR_METADATA[calendar]["UID"]
    ics_url = RESERVATION_CALENDAR_URL % calendar_uid
    print(f"Getting ics file from: {ics_url}")
    response = requests.get(ics_url, timeout=30)
    if response.status_code != 200:
        print(f"Failed to download file. HTTP status code: {response.status_code}")
        return False
    downloaded_calendar = Calendar.from_ical(response.text)

    # Loop over all calendar events and return the ones that can not be ignored
    ignorable_event_type_ids = CALENDAR_METADATA[calendar]["overridable_event_ids"]
    events = list(
        recurring_ical_events.of(downloaded_calendar).between(start_time, end_time)
    )
    unignorable_events = []
    print("Events in alloted time:")
    for event in events:
        event_type_id = event.get("CUSTOM-EVENTTYPE-ID")
        event_description = event.get("DESCRIPTION").strip()
        event_summary = event.get("SUMMARY").strip()
        print(f"\nEvent summary: {event_summary}")
        print(f"Event description: {event_description}")
        print(f"Event type ID: {event_type_id}")
        if event_type_id in ignorable_event_type_ids:
            print(f'Event type {event_type_id} is ignored for event "{event_summary}"')
        else:
            unignorable_events.append(events)

    # If there are any unignorable events then can not run
    if len(unignorable_events) >= 1:
        print(f"{len(unignorable_events)} event(s) in alloted time so can not run")
        return True
    print("No events so can run")
    return False


class TestResults:  # pylint: disable=R0902,C0115,C0116
    def __init__(self, output_folder=None, path_to_previous_step_results=None):
        if output_folder is None:
            self.output_folder = os.getcwd()
        else:
            self.output_folder = output_folder
        if path_to_previous_step_results:
            with open(path_to_previous_step_results, "r") as results_file:
                results = json.load(results_file)
            self.evidences = results["evidences"]
            self.comments = results["comments"].split("\n")
            self.status = results["status"]
            self.test_plan_key = results["test_plan_key"]
            self.summary = results["summary"]
            self.description = results["description"]
            self.test_environments = results["test_environments"]
            self.test_key = results["test_key"]
            self.is_ready_to_upload = results["is_ready_to_upload"]

            self.json_file = os.path.join(self.output_folder, "results.json")
            self.outputs = {}
        else:
            self.evidences = []
            self.comments = []
            self.status = "failed"
            self.test_plan_key = ""
            self.summary = ""
            self.description = ""
            self.test_environments = ["AAVS3"]
            self.test_key = ""
            self.json_file = os.path.join(self.output_folder, "results.json")
            self.outputs = {}
            self.is_ready_to_upload = False

    def append_evidence(self, filename, content_type=None):
        _, ext = os.path.splitext(filename)
        if content_type is None:
            content_type = mimetypes.types_map.get(ext, "text/plain")
        with open(filename, "rb") as evidence_file:
            data = base64.b64encode(evidence_file.read()).decode()
        self.evidences.append(
            {
                "filename": os.path.basename(filename),
                "contentType": content_type,
                "data": data,
            }
        )
        self.write()

    def add_output(self, output_key: str, output_value: str):
        self.outputs[output_key] = output_value
        self.write()

    def add_comment(self, comment):
        self.comments.append(comment)
        self.write()

    def add_test_key(self, test_key):
        self.test_key = test_key
        self.write()

    def add_test_plan_key(self, test_plan_key):
        self.test_plan_key = test_plan_key
        self.write()

    def add_description(self, description):
        self.description = description
        self.write()

    def add_summary(self, summary):
        self.summary = summary
        self.write()

    def pass_test(self):
        self.is_ready_to_upload = True
        self.status = "passed"
        self.write()

    def fail_test(self):
        self.is_ready_to_upload = True
        self.status = "failed"
        self.write()

    def write(self):
        results = {
            "comments": "\n".join(self.comments),
            "evidences": self.evidences,
            "status": self.status,
            "summary": self.summary,
            "description": self.description,
            "test_environments": self.test_environments,
            "test_key": self.test_key,
            "test_plan_key": self.test_plan_key,
            "outputs": self.outputs,
            "is_ready_to_upload": self.is_ready_to_upload,
        }
        with open(self.json_file, "w") as results_file:
            json.dump(results, results_file)
