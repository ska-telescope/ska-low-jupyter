"""
Helper functions to parse sources of truth metadata from telmodel.
"""

import csv
import warnings
from dataclasses import dataclass
from ssl import SSLEOFError
from urllib.error import URLError

import backoff
import numpy as np
import pandas as pd
import toolz
import yaml
from pandas import DataFrame
from ska_telmodel.data import TMData
from tango import DevFailed, DeviceProxy

from aiv_utils.low_utils import get_mccs_device
from aiv_utils.tango_utils import member


@toolz.memoize
@backoff.on_exception(backoff.expo, (SSLEOFError, URLError), max_tries=5)
def load_tmdata(station: str | DeviceProxy, tmdata_source=None) -> TMData:
    """Return the TMData for a given station.

    :param station: The station name (eg. "s10-3", "s8-1")
        or an SpsStation DeviceProxy.
    :param tmdata_source: The telmodel data source, default: None
    :returns: A TMData object for the station.
    """
    if tmdata_source is None:
        try:
            if not isinstance(station, DeviceProxy):
                station = get_mccs_device(f"low-mccs/spsstation/{station}")

            tmdata_source, _ = station.get_property("AntennaConfigURI")[
                "AntennaConfigURI"
            ]

        except DevFailed:
            warnings.warn(
                "Could not connect to spsstation. TMData from the main branch will be used."  # pylint: disable=C0301
            )
            if station.startswith("itf"):
                tmdata_source = "gitlab://gitlab.com/ska-telescope/aiv/ska-low-itf?main#tmdata"  # pylint: disable=C0301
            else:  # it's an AA station
                tmdata_source = "gitlab://gitlab.com/ska-telescope/ska-low-tmdata?main#tmdata"  # pylint: disable=C0301

    tmdata = TMData([tmdata_source], update=True)
    return tmdata


@toolz.memoize
def load_rfi_mask(station: str | DeviceProxy, tmdata_source=None) -> list:
    """Return the RFI coarse channel mask for a given station.

    :param station: The station name (eg. "s10-3", "s8-1")
        or an SpsStation DeviceProxy.
    :param tmdata_source: The telmodel data source, default: None
    :returns: A list of every masked coarse channel.
    """
    if station.startswith("itf"):
        # No RFI mask for ITF
        return []

    tmdata = load_tmdata(station, tmdata_source)
    tmdata_file = "coarse_channel_rfi_mask.yaml"
    yaml_string = tmdata[tmdata_file].get().decode()

    data = yaml.safe_load(yaml_string)

    rfi_channels = []
    for channel, is_rfi_free in enumerate(data["coarse_channel_rfi_mask"]):
        if not is_rfi_free:
            rfi_channels.append(channel)
    return rfi_channels


@toolz.memoize
def load_station_metadata(station: str | DeviceProxy, tmdata_source=None) -> dict:
    """Return the metadata for a given station.

    :param station: The station name (eg. "s10-3", "s8-1")
        or an SpsStation DeviceProxy.
    :param tmdata_source: The telmodel data source, default: None
    :returns: A dictionary containing the metadata for the station.
    """
    tmdata = load_tmdata(station, tmdata_source)
    if isinstance(station, DeviceProxy):
        station = member(station)
    tmdata_file = f"stations/{station}.yaml"
    yaml_string = tmdata[tmdata_file].get().decode()

    # Parse the yaml string and return
    station_data = yaml.safe_load(yaml_string)["platform"]["stations"][station]
    for antenna in station_data["antennas"].values():
        antenna.setdefault("masked", False)
    return station_data


@toolz.memoize
def get_all_stations(tmdata_source=None) -> list[str]:
    """Return a list of stations that have yaml files to describe them in the telmodel.

    :param tmdata_source: The telmodel data source,
        default: gitlab://gitlab.com/ska-telescope/ska-low-tmdata?main#tmdata
    :returns: A list of the stations
    """
    # Use telmodel to grab the source of truth from github
    if tmdata_source is None:
        # Use source for aa0.5 by default
        tmdata_source = "gitlab://gitlab.com/ska-telescope/ska-low-tmdata?main#tmdata"
    tmdata = TMData([tmdata_source], update=True)
    return [
        station.replace(".yaml", "").replace("stations/", "")
        for station in tmdata
        if station.startswith("stations/")
    ]


def get_antenna_locations(station: str | DeviceProxy) -> np.ndarray:
    """Return the antenna locations for a given station.

    :param station: The station name (eg. "s10-3", "s8-1")
        or an SpsStation DeviceProxy.
    :returns: A numpy array of antenna locations in meters from the centre of the
        station where the columns are east, north and up.
    """
    antennas_data = load_station_metadata(station)["antennas"]

    antenna_locations = []
    for antenna_name in antennas_data.keys():
        antenna_data = antennas_data[antenna_name]
        east = antenna_data["location_offset"]["east"]
        north = antenna_data["location_offset"]["north"]
        up = antenna_data["location_offset"]["up"]  # pylint: disable=C0103
        eep_id = antenna_data["eep"]
        antenna_locations.append([east, north, up, eep_id])
    # Sort the antenna locations by EEP ID (this helps with using EEP models)
    sorted_antenna_locations = sorted(antenna_locations, key=lambda x: x[3])
    return np.array(sorted_antenna_locations)[:, :3]


def get_station_location(station: str | DeviceProxy) -> tuple:
    """Return the station location (lat, long and elevation) for a given station.

    :param station: The station name (eg. "s10-3", "s8-1")
        or an SpsStation DeviceProxy.
    :returns: a tuple of the station location
        (latitude, longitude and elevation in degrees, degrees and meters)
    """
    station_location = load_station_metadata(station)["reference"]
    return (
        station_location["latitude"],
        station_location["longitude"],
        station_location["ellipsoidal_height"],
    )


def get_station_rotation(station: str | DeviceProxy) -> float:
    """Return the station rotation for a given station.

    :param station: The station name (eg. "s10-3", "s8-1")
        or an SpsStation DeviceProxy.
    :returns: the station rotation in degrees
    """
    return load_station_metadata(station).get("rotation", 0.0)


def get_station_static_delays(station: str | DeviceProxy) -> list[list[int]]:
    """
    Fetch platform spec file for given station and return static delays per TPM.

    :param station: The station name (eg. "s10-3", "s8-1")
        or an SpsStation DeviceProxy.
    :return: a list of lists of sixteen integers representing the static delays
        for each of the antennas for a TPM, ordered by ADC pair index.
    """
    antennas_data = load_station_metadata(station)["antennas"]
    tpm_adc_delays = sorted(
        (
            int(a["tpm"].removeprefix("tpm")),
            a[f"tpm_{pol}_channel"],
            a.get(f"delay_{pol}", a["delay"]),
        )
        for a in antennas_data.values()
        for pol in ["y", "x"]
        if a.get("smartbox", None) is not None
    )
    delays_out = []
    for tpm in toolz.partitionby(lambda x: x[0], tpm_adc_delays):
        tpm_delays = np.zeros(32, dtype=float)
        for _, ant, delay in tpm:
            tpm_delays[ant] = delay
        delays_out.append(list(tpm_delays))

    return delays_out


def get_station_preadu_attenuations(
    station: str | DeviceProxy, default_atten=20.0
) -> list[float]:
    """
    Fetch platform spec station preadu attenuation values.

    :param station: The station name (eg. "s10-3", "s8-1")
        or an SpsStation DeviceProxy.
    :param default_atten: The default attenuation to use if not found in telmodel.
    :return: List of floats of preadu-attenuation values.
    """
    antennas_data = load_station_metadata(station)["antennas"]
    attenuation_data = sorted(
        (
            int(a["tpm"].removeprefix("tpm")),
            a[f"tpm_{pol}_channel"],
            a.get(f"attenuation_{pol}", default_atten),
        )
        for a in antennas_data.values()
        for pol in ["y", "x"]
    )

    attens_out = []
    for tpm in toolz.partitionby(lambda x: x[0], attenuation_data):
        tpm_attens = np.array([default_atten] * 32, dtype=float)
        for _, ant, atten in tpm:
            tpm_attens[ant] = atten
        attens_out.extend(list(tpm_attens))

    return attens_out


@toolz.memoize
def get_station_antenna_dataframe(station: str | DeviceProxy) -> DataFrame:
    """
    Fetch a DataFrame of all antenna information which can be useful for debugging.

    :param station: The station name (eg. "s10-3", "s8-1")
        or an SpsStation DeviceProxy.
    :return: a pandas DataFrame with all the antenna information
    """
    antennas_data = load_station_metadata(station)["antennas"]
    data_list = []
    antenna_names = antennas_data.keys()
    sb_numbering = dict(zip(sorted(antenna_names), range(len(antenna_names))))

    for antenna_name in antenna_names:
        antenna_data = antennas_data[antenna_name]

        data_list.append(
            {
                "antenna_name": antenna_name,
                "eep": antenna_data["eep"],
                "tpm": antenna_data["tpm"],
                # "tpm" is 3 letters long.
                "tpm_id": int(antenna_data["tpm"][3:]) - 1,
                "tpm_y_channel": antenna_data["tpm_y_channel"],
                "tpm_x_channel": antenna_data["tpm_x_channel"],
                "antnum_tpm": (int(antenna_data["tpm"][3:]) - 1) * 16
                + int(antenna_data["tpm_y_channel"]) // 2,
                "antnum_eep": antenna_data["eep"] - 1,
                "antnum_sb": sb_numbering[antenna_name],
                "delay": antenna_data["delay"],
                "delay_x": antenna_data.get("delay_x", antenna_data["delay"]),
                "delay_y": antenna_data.get("delay_y", antenna_data["delay"]),
                "east": antenna_data["location_offset"]["east"],
                "north": antenna_data["location_offset"]["north"],
                "up": antenna_data["location_offset"]["up"],
                "smartbox": antenna_data["smartbox"],
                # "sb" is 2 letters long.
                "smartbox_id": int(antenna_data["smartbox"][2:]) - 1,
                "smartbox_port": antenna_data["smartbox_port"],
                "masked": antenna_data.get("masked", False),
            }
        )

    return DataFrame(data_list)


# Determined after rigorous discussion with MCCS firmware experts.
# Taken from https://gitlab.com/ska-telescope/ska-low-aavs3/-/blob/58f4af2436818822fc399c6b26cefb6d29dff174/scripts/update_config.py#L27-27  # pylint: disable=C0301
fibre_adc_mapping = {
    16: (14, 15),
    15: (12, 13),
    14: (10, 11),
    13: (8, 9),
    12: (6, 7),
    11: (4, 5),
    10: (2, 3),
    9: (0, 1),
    8: (30, 31),
    7: (28, 29),
    6: (26, 27),
    5: (24, 25),
    4: (22, 23),
    3: (20, 21),
    2: (18, 19),
    1: (16, 17),
}


adc_fibre_mapping = {
    chan: fibre for fibre, chans in fibre_adc_mapping.items() for chan in chans
}


np_fibre_mapping = np.array(
    sorted(
        (chan, fibre) for fibre, chans in fibre_adc_mapping.items() for chan in chans
    )
)[:, 1]


@dataclass
class FibreMapping:  # pylint: disable=too-many-instance-attributes
    """A dataclass to represent a mapping of a fibre to a TPM."""

    sb: int  # pylint: disable=invalid-name
    fem: int
    has_antenna: bool
    fem_sn: str
    tail: int
    fibre: int
    tpm: int
    masked: bool


def parse_tsv(tsv_path):
    """Parse a TSV file and return a list of FibreMapping objects."""
    with open(tsv_path) as file:
        print(next(file))
        csv_iterator = csv.reader(file, delimiter="\t", quotechar='"')
        fms = []
        for row in csv_iterator:
            if not row[0]:
                continue
            try:
                f_m = FibreMapping(
                    sb=int(row[0]),
                    fem=int(row[1]),
                    has_antenna=bool(["NO", "YES"].index(row[2])),
                    fem_sn=row[3],
                    tail=int(row[4]) if row[4] else None,
                    fibre=int(row[-2]) if row[-2] else None,
                    tpm=int(row[-1]) if row[-1] else None,
                    masked=False,
                )
            except:  # noqa: E722
                print(row)
                raise
            fms.append(f_m)
    return fms


def parse_platform_spec(station_name):
    """Parse the platform spec file and return a list of FibreMapping objects."""
    station_spec = load_station_metadata(station_name)

    antennas_by_sb_port = {
        (antenna["smartbox"], antenna["smartbox_port"]): antenna
        for antenna in station_spec["antennas"].values()
        if "smartbox" in antenna and "smartbox_port" in antenna
    }

    fms = []
    for sbid in station_spec["pasd"]["smartboxes"]:
        for i in range(1, 13):
            antenna = antennas_by_sb_port.get((sbid, i))
            if antenna:
                fibres = [
                    adc_fibre_mapping[ch]
                    for ch in [antenna["tpm_y_channel"], antenna["tpm_x_channel"]]
                ]
                assert fibres[0] == fibres[1]
                fibre = fibres[0]
            else:
                fibre = None
            f_m = FibreMapping(
                sb=int(sbid.removeprefix("sb")),
                fem=i,
                has_antenna=bool(antenna),
                fem_sn=None,
                tail=None,
                fibre=fibre,
                tpm=int(antenna["tpm"].removeprefix("tpm")) if antenna else None,
                masked=antenna.get("masked", False) if antenna else None,
            )
            fms.append(f_m)
    return fms


def smartbox_ports_masked(station: str | DeviceProxy) -> dict[str, list[bool]]:
    """
    Returns a list of lists indicating which ports should be disabled for each smartbox.

    This takes into account both the "masked" flag in the station spec,
    and the fact that not all ports have an antenna connected to them.
    """
    station_spec = load_station_metadata(station)
    antennas: pd.DataFrame = pd.DataFrame(
        station_spec["antennas"].values()
    ).sort_values("smartbox")
    # pylint: disable-next=unsubscriptable-object
    antennas = antennas[antennas["smartbox"].notna()]
    antennas["smartbox"] = [int(sb.removeprefix("sb")) for sb in antennas["smartbox"]]
    return antennas.groupby("smartbox").apply(
        lambda ants: [
            mask_map.get(i, True)
            for mask_map in [dict(zip(ants.smartbox_port, ants.masked | False))]
            for i in range(1, 13)
        ],
        include_groups=False,
    )
