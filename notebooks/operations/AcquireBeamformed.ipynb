{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "b8b13147-faf5-4b84-b29c-9fd6f0670ae8",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "# Acquire Station Beamformed Data\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2db38f67-acd0-43d2-9ce5-80aecb25da8e",
   "metadata": {},
   "source": [
    "## Imports and constants"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dd16b3cd-08e0-44e0-b6f4-620ccd72a521",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "import itertools\n",
    "import json\n",
    "import os\n",
    "import warnings\n",
    "from datetime import datetime, timedelta\n",
    "from pathlib import Path\n",
    "from time import sleep\n",
    "\n",
    "import astropy.units as u\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from astropy.coordinates import AltAz, EarthLocation, SkyCoord, get_sun\n",
    "from astropy.time import Time\n",
    "\n",
    "from aiv_utils.calibration_utils import (\n",
    "    apply_cal_from_array,\n",
    "    apply_cal_from_file,\n",
    "    apply_cal_from_store,\n",
    "    mask_antennas_by_name,\n",
    "    mask_known_bad_antennas,\n",
    "    set_up_single_beam,\n",
    ")\n",
    "from aiv_utils.low_utils import (\n",
    "    get_mccs_device,\n",
    "    get_sps_devices,\n",
    "    interactive_variable_prompt,\n",
    ")\n",
    "from aiv_utils.metadata import get_station_antenna_dataframe, get_station_location\n",
    "from aiv_utils.tango_utils import wait_lrc"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4c2da701-7567-445a-b221-eff74ee4bf08",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": [
     "parameters"
    ]
   },
   "outputs": [],
   "source": [
    "STATION_NAME: str = interactive_variable_prompt(\"STATION_NAME\")\n",
    "\n",
    "# Where to point the telescope. Choices are \"SUN\", \"ZENITH\", a source name, or\n",
    "# [RA, DEC]/[ALT, AZ] in degrees\n",
    "# Note: RA is in degrees. To convert from hours to degrees, multiply by 360/24 or use\n",
    "# astropy. For example 6h=90 degrees. Or use astropy:\n",
    "# ra_deg=SkyCoord(ra=\"06h00m00s\",dec=\"00h00m00s\").ra.deg\n",
    "SOURCE = \"SUN\"\n",
    "# Type of source: NAMED, ALT_AZ, or RA_DEC\n",
    "SOURCE_TYPE = \"NAMED\"\n",
    "\n",
    "# 3 part observation, point with altaz as source drifts in, track, point with\n",
    "# altaz as source drifts out\n",
    "# Duration is in minutes.\n",
    "DRIFT_IN_DURATION = 0\n",
    "TRACK_DURATION = 0  # Must be 0 if SOURCE is ZENITH or SOURCE_TYPE is ALT_AZ\n",
    "DRIFT_OUT_DURATION = 0\n",
    "\n",
    "# The frequency channel ID to start at (lowest frequency of the band).\n",
    "# Channel ID * 0.78125 = frequency in MHz so 64 = 50 MHz and 160 = 125 MHz\n",
    "CHANNEL_START = 64\n",
    "\n",
    "# The number of frequency channels to use as a bandwidth (must be divisible by 8).\n",
    "# The system can handle up to 96 channels\n",
    "CHANNEL_BANDWIDTH = 96\n",
    "\n",
    "# The beam index to use.\n",
    "BEAM_INDEX = 0\n",
    "\n",
    "# Emit warnings if the altitude of an observation is below this\n",
    "WARN_ALT = 30\n",
    "\n",
    "# If you want the DAQ configured which creates a new directory.\n",
    "# Normally only turned off for some test runs\n",
    "CONFIGURE_DAQ = True\n",
    "\n",
    "# If you want to output station beams (integrated so lower resolution)\n",
    "# or raw beams (higher resolution)\n",
    "DAQ_MODE = \"STATION\"  # or RAW\n",
    "\n",
    "# When to start the observation in UTC in \"%Y-%m-%dT%H:%M:%S.%fZ\" format\n",
    "# For example, \"2024-12-31T00:00:0.0Z\"\n",
    "OBS_START_TIME = None\n",
    "\n",
    "# If no OBS_START_TIME given, how many seconds to wait until starting\n",
    "# This is used in gitlab runner testing which takes longer to do astropy calcs\n",
    "OBS_START_WAITTIME = 30\n",
    "\n",
    "# More detailed information about calibration can be found in the docs:\n",
    "# https://developer.skao.int/projects/ska-low-tests/en/latest/user/index.html#acquirebeamformed-ipynb\n",
    "\n",
    "# The calibration type you want applied. The options are:\n",
    "# \"FROM_STORE\": Apply the best calibration solution in the store.\n",
    "# \"FROM_FILE\": Apply a solution from a provided numpy file with shape\n",
    "#       [channels, antennas, polarisations].\n",
    "# \"UNITY\": Apply an amplitude 1 solution with no phase calibration.\n",
    "# \"ZERO\": Apply an amplitude 0 solution to all antennas so all data is zeroed out.\n",
    "#       Only useful for testing purposes.\n",
    "# \"NONE\": Don't apply any solutions. Whatever was last applied will be used.\n",
    "APPLY_CALIBRATION_TYPE = \"FROM_STORE\"\n",
    "\n",
    "# Path to gain solutions for calibrating. Only needed in FROM_FILE mode\n",
    "GAIN_SOLNS_PATH = None\n",
    "# Whether the gain solutions file is in [channels, antenna, pol] shape. The ones created\n",
    "# by the calibration SFT are in [antenna, channel, pol] shape so need this to be False.\n",
    "IS_CHANNELS_FIRST = False\n",
    "# The channel that the provided gain solutions starts at. Only needed in FROM_FILE mode.\n",
    "GAIN_SOLNS_START_CHANNEL = 64\n",
    "\n",
    "# Whether to automatically mask antennas listed as bad in the telmodel.\n",
    "MASK_KNOWN_BAD_ANTENNAS = True\n",
    "# A list of masked antennas by name (e.g. [\"sb01-01\", \"sb11-02\"]).\n",
    "MASKED_ANTENNAS = None\n",
    "\n",
    "# The output directory of the notebooks run with papermill\n",
    "OUTPUT_NOTEBOOK_DIR = \"./\"\n",
    "# The directory of the operation notebooks that papermill will use\n",
    "OPERATIONS_NOTEBOOK_DIR = \"/home/jovyan/ska-low-tests/notebooks/operations/\"\n",
    "\n",
    "# Copy output to here. If None then don't copy\n",
    "DATA_OUTPUT_PATH = None"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "304ffdbe",
   "metadata": {},
   "outputs": [],
   "source": [
    "TOTAL_DURATION = DRIFT_IN_DURATION + TRACK_DURATION + DRIFT_OUT_DURATION"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dae38013",
   "metadata": {},
   "outputs": [],
   "source": [
    "assert SOURCE_TYPE in [\n",
    "    \"NAMED\",\n",
    "    \"ALT_AZ\",\n",
    "    \"RA_DEC\",\n",
    "], \"SOURCE_TYPE must be NAMED, ALT_AZ, or RA_DEC\"\n",
    "\n",
    "if SOURCE_TYPE in [\"ALT_AZ\", \"RA_DEC\"]:\n",
    "    assert isinstance(SOURCE, list) and len(SOURCE) == 2\n",
    "    if SOURCE_TYPE == \"ALT_AZ\":\n",
    "        alt_deg, az_deg = SOURCE\n",
    "        assert 0 < alt_deg <= 90\n",
    "        if alt_deg < WARN_ALT:\n",
    "            warnings.warn(f\"Altitude {alt_deg} is lower than {WARN_ALT} degrees.\")\n",
    "\n",
    "\n",
    "assert CHANNEL_START > 0\n",
    "assert DAQ_MODE in [\"STATION\", \"RAW\"]\n",
    "\n",
    "\n",
    "assert DRIFT_IN_DURATION >= 0\n",
    "assert TRACK_DURATION >= 0\n",
    "assert DRIFT_OUT_DURATION >= 0\n",
    "\n",
    "assert TOTAL_DURATION > 0, f\"Total observation time {TOTAL_DURATION} must be positive.\"\n",
    "\n",
    "if SOURCE_TYPE == \"ALT_AZ\":\n",
    "    assert TRACK_DURATION == 0, \"Can't track in ALT_AZ pointing mode\"\n",
    "\n",
    "if APPLY_CALIBRATION_TYPE is None:\n",
    "    APPLY_CALIBRATION_TYPE = \"NONE\"\n",
    "\n",
    "assert APPLY_CALIBRATION_TYPE in [\"FROM_STORE\", \"FROM_FILE\", \"UNITY\", \"ZERO\", \"NONE\"]\n",
    "\n",
    "if APPLY_CALIBRATION_TYPE == \"FROM_FILE\":\n",
    "    assert GAIN_SOLNS_PATH\n",
    "    assert (\n",
    "        GAIN_SOLNS_START_CHANNEL <= CHANNEL_START\n",
    "    ), \"Channel to beamform from must be gain solution start channel or higher\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "07405b47-2adf-457d-82ba-99bb23f60d24",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Do the two metadata calls here to prevent time outs at later steps\n",
    "station_lat, station_lon, station_elevation = get_station_location(STATION_NAME)\n",
    "antennas_df_eep = get_station_antenna_dataframe(STATION_NAME).sort_values(by=\"eep\")\n",
    "n_ant = len(antennas_df_eep)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "71b2fd51-ab5a-4766-8bf8-87a15632fb0a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# define station astropy object\n",
    "station_coord = EarthLocation(\n",
    "    lat=station_lat * u.deg, lon=station_lon * u.deg, height=station_elevation * u.m\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6a07ea5f-749a-4837-bd8b-5454edca6b45",
   "metadata": {},
   "source": [
    "## Perform scans"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f0396369-7be2-4566-96d6-1397ac64d28b",
   "metadata": {},
   "source": [
    "### Setup telescope, assuming that we have an initialised, equalised, calibrated station"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d5ff3641-48a2-4b36-b6bb-8619273e2128",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "sps_station, subracks, tpms, (daq, _) = get_sps_devices(STATION_NAME)\n",
    "station = get_mccs_device(f\"low-mccs/station/{STATION_NAME}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ba2637ad-9401-496b-831a-774482ea7e7c",
   "metadata": {},
   "source": [
    "DAQ setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "55e3010c-a958-4351-9354-c638529a6e92",
   "metadata": {},
   "outputs": [],
   "source": [
    "description = f\"{STATION_NAME}, pointing at {SOURCE} ({SOURCE_TYPE}).\"\n",
    "\n",
    "if CONFIGURE_DAQ:\n",
    "    wait_lrc(daq, \"Stop\")\n",
    "    if DAQ_MODE == \"STATION\":\n",
    "        daq.Configure(\n",
    "            json.dumps({\"directory\": \"/\", \"nof_tiles\": 16, \"description\": description})\n",
    "        )\n",
    "        wait_lrc(daq, \"Start\", json.dumps({\"modes_to_start\": \"STATION_BEAM_DATA\"}))\n",
    "\n",
    "    elif DAQ_MODE == \"RAW\":\n",
    "        daq.Configure(\n",
    "            json.dumps(\n",
    "                {\n",
    "                    \"directory\": \"/\",\n",
    "                    \"nof_tiles\": 16,\n",
    "                    \"nof_beams\": 1,\n",
    "                    # \"nof_channels\": 32,\n",
    "                    \"station_beam_start_channel\": 64,\n",
    "                    # \"station_beam_source\": \"Zenith\",\n",
    "                    \"station_beam_dada\": True,\n",
    "                    \"station_beam_individual_channels\": True,\n",
    "                    \"description\": description,\n",
    "                }\n",
    "            )\n",
    "        )\n",
    "        wait_lrc(daq, \"Start\", json.dumps({\"modes_to_start\": \"RAW_STATION_BEAM\"}))\n",
    "    daq_status = json.loads(daq.DaqStatus())\n",
    "    print(daq_status)\n",
    "    daq_cfg = json.loads(daq.GetConfiguration())\n",
    "    print(daq_cfg)\n",
    "\n",
    "    tpm_config = {\n",
    "        \"destination_ip\": daq_status[\"Receiver IP\"][0],\n",
    "        \"destination_port\": daq_status[\"Receiver Ports\"][0],\n",
    "    }\n",
    "    sps_station.SetCspIngest(json.dumps(tpm_config))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aaeddb4a-da53-43d2-baf7-def43f123abe",
   "metadata": {},
   "outputs": [],
   "source": [
    "if not APPLY_CALIBRATION_TYPE == \"FROM_STORE\":\n",
    "    # Set band\n",
    "    regions = [\n",
    "        [CHANNEL_START, CHANNEL_BANDWIDTH, BEAM_INDEX, 1, 0 * 96, 1, 1, 101],\n",
    "    ]\n",
    "    print(regions)\n",
    "    beam_freq_regions = list(itertools.chain.from_iterable(regions))\n",
    "    sps_station.SetBeamFormerRegions(beam_freq_regions)\n",
    "    sps_beamformer_table = sps_station.beamformertable\n",
    "    print(f\"Beamformer freq table:\\n{sps_beamformer_table}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d863becd",
   "metadata": {},
   "source": [
    "## Apply Calibration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "02fa8a67",
   "metadata": {},
   "outputs": [],
   "source": [
    "if APPLY_CALIBRATION_TYPE == \"FROM_STORE\":\n",
    "    print(\"Applying the best calibration solutions in the store.\")\n",
    "    mccs_beamformer_table = set_up_single_beam(\n",
    "        CHANNEL_START, CHANNEL_BANDWIDTH, BEAM_INDEX\n",
    "    )\n",
    "    apply_cal_from_store(STATION_NAME, mccs_beamformer_table)\n",
    "\n",
    "if APPLY_CALIBRATION_TYPE == \"FROM_FILE\":\n",
    "    print(f\"Applying calibration solutions from {GAIN_SOLNS_PATH}.\")\n",
    "    apply_cal_from_file(\n",
    "        STATION_NAME,\n",
    "        GAIN_SOLNS_PATH,\n",
    "        gain_solns_start_channel=GAIN_SOLNS_START_CHANNEL,\n",
    "        is_channels_first=IS_CHANNELS_FIRST,\n",
    "    )\n",
    "\n",
    "if APPLY_CALIBRATION_TYPE == \"UNITY\":\n",
    "    print(\"Applying unity calibration solutions.\")\n",
    "    apply_cal_from_array(\n",
    "        STATION_NAME,\n",
    "        np.full(\n",
    "            (CHANNEL_BANDWIDTH, len(tpms) * 16, 4),\n",
    "            np.array([1, 0, 0, 1]),\n",
    "            dtype=\"complex128\",\n",
    "        ),\n",
    "        gain_solns_start_channel=CHANNEL_START,\n",
    "    )\n",
    "if APPLY_CALIBRATION_TYPE == \"ZERO\":\n",
    "    print(\"Applying zeroed calibration solutions.\")\n",
    "    apply_cal_from_array(\n",
    "        STATION_NAME,\n",
    "        np.zeros(\n",
    "            (CHANNEL_BANDWIDTH, len(tpms) * 16, 4),\n",
    "            dtype=\"complex128\",\n",
    "        ),\n",
    "        gain_solns_start_channel=CHANNEL_START,\n",
    "    )\n",
    "if APPLY_CALIBRATION_TYPE == \"NONE\":\n",
    "    print(\"Not applying calibration solutions. Whatever was applied last will be used.\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3d51a231",
   "metadata": {},
   "outputs": [],
   "source": [
    "if MASK_KNOWN_BAD_ANTENNAS:\n",
    "    mask_known_bad_antennas(STATION_NAME)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25c2806d",
   "metadata": {},
   "outputs": [],
   "source": [
    "if MASKED_ANTENNAS:\n",
    "    mask_antennas_by_name(STATION_NAME, MASKED_ANTENNAS)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "113ce4a5-5f2d-48ae-a7cc-0d458c8a04e5",
   "metadata": {},
   "outputs": [],
   "source": [
    "if OBS_START_TIME:\n",
    "    start_time = datetime.strptime(OBS_START_TIME, \"%Y-%m-%dT%H:%M:%S.%fZ\")\n",
    "    assert (\n",
    "        start_time > datetime.utcnow()\n",
    "    ), \"Observation Start Time must be in the future\"\n",
    "else:\n",
    "    start_time = datetime.utcnow() + timedelta(seconds=OBS_START_WAITTIME)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1bfd5200-8faf-4f0c-8e98-9f5a40097bba",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "if SOURCE_TYPE == \"ALT_AZ\":\n",
    "    alt_deg, az_deg = SOURCE\n",
    "    pointing_values_drift_in = {\n",
    "        \"altitude\": alt_deg,\n",
    "        \"azimuth\": az_deg,\n",
    "    }\n",
    "    pointing_values_drift_out = pointing_values_drift_in\n",
    "\n",
    "    if alt_deg < WARN_ALT:\n",
    "        warnings.warn(f\"Altitude {alt_deg} is lower than {WARN_ALT} degrees.\")\n",
    "\n",
    "elif SOURCE_TYPE == \"NAMED\" and SOURCE.upper() == \"ZENITH\":\n",
    "    pointing_values_drift_in = {\n",
    "        \"altitude\": 90,\n",
    "        \"azimuth\": 0,\n",
    "    }\n",
    "    pointing_values_drift_out = pointing_values_drift_in\n",
    "\n",
    "else:  # 3 part observation: drift in, track, drift out.\n",
    "    track_start_time = Time(start_time + timedelta(minutes=DRIFT_IN_DURATION))\n",
    "    track_end_time = Time(\n",
    "        start_time + timedelta(minutes=DRIFT_IN_DURATION + TRACK_DURATION)\n",
    "    )\n",
    "\n",
    "    if SOURCE_TYPE == \"RA_DEC\":\n",
    "        ra_deg, dec_deg = SOURCE\n",
    "    else:\n",
    "        if SOURCE.upper() == \"SUN\":\n",
    "            track_middle_time = Time(\n",
    "                start_time + timedelta(minutes=DRIFT_IN_DURATION + TRACK_DURATION / 2)\n",
    "            )\n",
    "            source_position = get_sun(track_middle_time)\n",
    "        else:\n",
    "            source_position = SkyCoord.from_name(SOURCE)\n",
    "        ra_deg = source_position.ra.deg\n",
    "        dec_deg = source_position.dec.deg\n",
    "\n",
    "    pointing_values_track = {\n",
    "        \"right_ascension\": ra_deg,\n",
    "        \"declination\": dec_deg,\n",
    "    }\n",
    "\n",
    "    sky_coordinates = SkyCoord(\n",
    "        ra=ra_deg,\n",
    "        dec=dec_deg,\n",
    "        unit=\"deg\",\n",
    "    )\n",
    "\n",
    "    source_altaz_start = sky_coordinates.transform_to(\n",
    "        AltAz(obstime=track_start_time, location=station_coord)\n",
    "    )\n",
    "    alt_deg_start = source_altaz_start.alt.deg\n",
    "    az_deg_start = source_altaz_start.az.deg\n",
    "\n",
    "    pointing_values_drift_in = {\n",
    "        \"altitude\": alt_deg_start,\n",
    "        \"azimuth\": az_deg_start,\n",
    "    }\n",
    "    assert alt_deg_start > 0\n",
    "    if alt_deg_start < WARN_ALT:\n",
    "        warnings.warn(\n",
    "            f\"Altitude at start {alt_deg_start} is lower than {WARN_ALT} degrees.\"\n",
    "        )\n",
    "\n",
    "    source_altaz_end = sky_coordinates.transform_to(\n",
    "        AltAz(obstime=track_end_time, location=station_coord)\n",
    "    )\n",
    "    alt_deg_end = source_altaz_end.alt.deg\n",
    "    az_deg_end = source_altaz_end.az.deg\n",
    "    pointing_values_drift_out = {\n",
    "        \"altitude\": alt_deg_end,\n",
    "        \"azimuth\": az_deg_end,\n",
    "    }\n",
    "    assert alt_deg_end > 0\n",
    "    if alt_deg_end < WARN_ALT:\n",
    "        warnings.warn(\n",
    "            f\"Altitude at end {alt_deg_end} is lower than {WARN_ALT} degrees.\"\n",
    "        )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "82b99b2a-e3c0-49f6-9136-930559ac4c37",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Observe\n",
    "print(f\"Obs starts at: {start_time} and current time is {datetime.utcnow()}\")\n",
    "seconds_until_start = (start_time - datetime.utcnow()).total_seconds()\n",
    "assert seconds_until_start > 0, \"Observation Start Time must be in the future\"\n",
    "\n",
    "command_result = station.Scan(\n",
    "    json.dumps(\n",
    "        {\n",
    "            \"scan_id\": 1,\n",
    "            \"subarray_id\": 1,\n",
    "            \"start_time\": start_time.strftime(\"%Y-%m-%dT%H:%M:%S.%fZ\"),\n",
    "            \"duration\": TOTAL_DURATION * 60,\n",
    "        }\n",
    "    )\n",
    ")\n",
    "print(command_result)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d3f3d408",
   "metadata": {},
   "outputs": [],
   "source": [
    "def show_delays(pointing_values):\n",
    "    # Check the delays\n",
    "    last_delays = station.lastPointingDelays  # In antenna eep ordered according to docs\n",
    "\n",
    "    ax = plt.figure(figsize=(9, 6)).subplots()\n",
    "\n",
    "    sc = ax.scatter(\n",
    "        antennas_df_eep[\"east\"],  # pylint: disable=unsubscriptable-object\n",
    "        antennas_df_eep[\"north\"],  # pylint: disable=unsubscriptable-object\n",
    "        c=last_delays[1::2][:n_ant] * 10**9,\n",
    "        cmap=\"viridis\",\n",
    "    )\n",
    "    plt.gca().set_aspect(\"equal\", adjustable=\"box\")\n",
    "\n",
    "    title = f\"Delays pointing at {SOURCE} at\"\n",
    "    for key, value in pointing_values.items():\n",
    "        title += f\" {key}: {value} deg\"\n",
    "    plt.title(title)\n",
    "    # Add a colorbar\n",
    "    cbar = plt.colorbar(sc)\n",
    "    cbar.set_label(\"Delays (ns)\")  # Label the colorbar"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4b678a8c",
   "metadata": {},
   "outputs": [],
   "source": [
    "if DRIFT_IN_DURATION > 0:\n",
    "    seconds_until_point = (start_time - datetime.utcnow()).total_seconds()\n",
    "    assert seconds_until_point > 0\n",
    "    print(f\"Waiting {seconds_until_point} seconds until we point for drift in.\")\n",
    "    sleep(seconds_until_point)\n",
    "\n",
    "    command_result = station.TrackObject(\n",
    "        json.dumps(\n",
    "            {\n",
    "                \"pointing_type\": \"alt_az\",\n",
    "                \"values\": pointing_values_drift_in,\n",
    "                \"scan_time\": DRIFT_IN_DURATION * 60,\n",
    "                # Required, but not used unless we specify RA and Dec rates\n",
    "                \"reference_time\": \"1970-01-01T00:00:00.00Z\",\n",
    "                \"station_beam_number\": 0,\n",
    "            }\n",
    "        )\n",
    "    )\n",
    "    print(\"Start drift in.\")\n",
    "    print(command_result)\n",
    "    show_delays(pointing_values_drift_in)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d6211ce2",
   "metadata": {},
   "outputs": [],
   "source": [
    "if TRACK_DURATION > 0:\n",
    "    seconds_until_point = (\n",
    "        start_time + timedelta(minutes=DRIFT_IN_DURATION) - datetime.utcnow()\n",
    "    ).total_seconds()\n",
    "    assert seconds_until_point > 0\n",
    "    print(f\"Waiting {seconds_until_point} seconds until we track.\")\n",
    "    sleep(seconds_until_point)\n",
    "\n",
    "    command_result = station.TrackObject(\n",
    "        json.dumps(\n",
    "            {\n",
    "                \"pointing_type\": \"ra_dec\",\n",
    "                \"values\": pointing_values_track,  # pylint: disable=used-before-assignment\n",
    "                \"scan_time\": TRACK_DURATION * 60,\n",
    "                # Required, but not used unless we specify RA and Dec rates\n",
    "                \"reference_time\": \"1970-01-01T00:00:00.00Z\",\n",
    "                \"station_beam_number\": 0,\n",
    "            }\n",
    "        )\n",
    "    )\n",
    "    print(\"Start track.\")\n",
    "    print(command_result)\n",
    "    show_delays(pointing_values_track)  # pylint: disable=used-before-assignment"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cdaf178e",
   "metadata": {},
   "outputs": [],
   "source": [
    "if DRIFT_OUT_DURATION > 0:\n",
    "    seconds_until_point = (\n",
    "        start_time\n",
    "        + timedelta(minutes=DRIFT_IN_DURATION + TRACK_DURATION)\n",
    "        - datetime.utcnow()\n",
    "    ).total_seconds()\n",
    "    assert seconds_until_point > 0\n",
    "    print(f\"Waiting {seconds_until_point} seconds until we point for drift out.\")\n",
    "    sleep(seconds_until_point)\n",
    "\n",
    "    command_result = station.TrackObject(\n",
    "        json.dumps(\n",
    "            {\n",
    "                \"pointing_type\": \"alt_az\",\n",
    "                \"values\": pointing_values_drift_out,\n",
    "                \"scan_time\": DRIFT_OUT_DURATION * 60,\n",
    "                # Required, but not used unless we specify RA and Dec rates\n",
    "                \"reference_time\": \"1970-01-01T00:00:00.00Z\",\n",
    "                \"station_beam_number\": 0,\n",
    "            }\n",
    "        )\n",
    "    )\n",
    "    print(\"Start drift out.\")\n",
    "    print(command_result)\n",
    "    show_delays(pointing_values_drift_out)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5a14ce0d",
   "metadata": {},
   "outputs": [],
   "source": [
    "seconds_until_done = (\n",
    "    start_time + timedelta(minutes=TOTAL_DURATION) - datetime.utcnow()\n",
    ").total_seconds()\n",
    "assert seconds_until_done > 0\n",
    "print(f\"Waiting {seconds_until_done} seconds until the scan ends.\")\n",
    "sleep(seconds_until_done)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cf206ee6-6e05-4f3a-bd34-d4cde437c079",
   "metadata": {},
   "outputs": [],
   "source": [
    "if DATA_OUTPUT_PATH and CONFIGURE_DAQ:\n",
    "    daq_path = \"/home/jovyan/daq-data\" / Path(daq_cfg[\"directory\"]).relative_to(\n",
    "        \"/product\"\n",
    "    )\n",
    "    print(daq_path)\n",
    "\n",
    "    sleep(2)  # Give daq time to write files\n",
    "    os.makedirs(DATA_OUTPUT_PATH, exist_ok=True)\n",
    "    !ln -s {daq_path}/*.hdf5 {DATA_OUTPUT_PATH}/"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
