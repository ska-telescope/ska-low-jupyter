{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "ea13a5bd-f604-4a90-97b3-6ba8de221d26",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "# Initialise an SKA Low station\n",
    "\n",
    "This notebook will bring the station up to the point where TPMs have started acquisition. The behaviour is parameterisable by options defined in cell 3.\n",
    "\n",
    "By default, at the end of this notebook, SpsStation.Initialise() has been called, TPMs have been initialised and configured with the DAQ's IP for LMC data samples, and all preADU attenuators have been set to 20dB. Data acquisition has started and integrated channelised data samples for bandpass monitoring are enabled and being sent to the DAQ every five seconds."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05afb029-9d19-4311-856d-3f16b6a63eba",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-06-14T15:45:08.240145Z",
     "iopub.status.busy": "2024-06-14T15:45:08.239138Z",
     "iopub.status.idle": "2024-06-14T15:45:08.242506Z",
     "shell.execute_reply": "2024-06-14T15:45:08.242017Z",
     "shell.execute_reply.started": "2024-06-14T15:45:08.240128Z"
    }
   },
   "source": [
    "## Parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0b20eff3-74e7-4944-a992-b7b87f48f78f",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": [
     "parameters"
    ]
   },
   "outputs": [],
   "source": [
    "import ipaddress\n",
    "import json\n",
    "import time\n",
    "from datetime import datetime, timedelta, timezone\n",
    "from pprint import pprint\n",
    "from warnings import warn\n",
    "\n",
    "import kubernetes\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import toolz\n",
    "from IPython.display import display\n",
    "from ska_control_model import AdminMode, HealthState\n",
    "from tango import (\n",
    "    DevFailed,\n",
    "    DeviceProxy,\n",
    "    DevState,\n",
    ")\n",
    "\n",
    "from aiv_utils.calibration_utils import (\n",
    "    reset_selection_policy,\n",
    ")\n",
    "from aiv_utils.low_utils import (\n",
    "    get_sps_devices,\n",
    "    interactive_variable_prompt,\n",
    "    report_attr,\n",
    "    split_host_port,\n",
    "    tpm_summary,\n",
    ")\n",
    "from aiv_utils.metadata import (\n",
    "    get_station_preadu_attenuations,\n",
    "    get_station_static_delays,\n",
    ")\n",
    "from aiv_utils.tango_utils import single_prop, wait_for, wait_lrc\n",
    "\n",
    "STATION_NAME: str = interactive_variable_prompt(\"STATION_NAME\")\n",
    "\n",
    "# Bits of channeliser rounding - an int 0-7.\n",
    "# Setting this to zero gives maximum bandpass fidelity.\n",
    "# Setting it less than two might cause problems - see SPRTS-195.\n",
    "# Sensible defaults should be set in Tango properties, but can be overridden here.\n",
    "CHANNELISER_ROUNDING: int | None = None\n",
    "\n",
    "# Bits of CSP rounding - an int 0-7.\n",
    "# CSP rounding is applied after beamforming, and scales the beamformed sum to a sensible\n",
    "# level to send to CSP. The number of antennas in the beam determines the best value.\n",
    "# Sensible defaults should be set in Tango properties, but can be overridden here.\n",
    "CSP_ROUNDING: int | None = None\n",
    "\n",
    "# Roughly what most antennas end up set to after equalisation\n",
    "PREADU_LEVEL: float | None = None\n",
    "\n",
    "# The default empty string will set the CSP destination to the station's CspIngestIp\n",
    "# property if it's set, otherwise to the DAQ. Use \"DAQ\" to send to the DAQ even if\n",
    "# CspIngestIp is set, or \"<ip>:<port>\" to send to an arbitrary destination.\n",
    "CSP_INGEST: str = \"\"\n",
    "\n",
    "# Enable bandpass monitoring and set the given integration time.\n",
    "# Anything evaluating boolean false will disable bandpass monitoring.\n",
    "BANDPASS_INTEGRATION_TIME: float = 5.0\n",
    "\n",
    "# Fetch static delays from external platform spec files and apply them\n",
    "APPLY_STATIC_DELAYS: bool = True\n",
    "\n",
    "# Start data acquisition in the TPMs.\n",
    "# If you are initialising two stations with a mind to correlating their beams,\n",
    "# you will need to set this to False and run StartAcquisition outside of this\n",
    "# notebook with the same start_time - until SP-3800 is resolved.\n",
    "# TODO this limitation should be fixed by new TPM firmware available June 2024\n",
    "START_ACQUISITION: bool = True\n",
    "\n",
    "# Only assert the station is already initialised\n",
    "# Only run through all the station initialise steps if a station initialised\n",
    "# check fails\n",
    "ONLY_ASSERT_INITIALISED: bool = False\n",
    "\n",
    "# Continue to execute notebook even if SpsStation.Initialise() fails\n",
    "ACCEPT_FAILURE: bool = False\n",
    "\n",
    "# AdminMode for MCCS devices. Defaults to ONLINE\n",
    "ADMIN_MODE: AdminMode = AdminMode.ONLINE"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b9ca549b-c5a6-4b54-972f-bc98688631a7",
   "metadata": {},
   "source": [
    "## Imports and get device proxies"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0215128e-a36f-4d89-9aba-2dd8554a0997",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-06-14T15:55:39.145812Z",
     "iopub.status.busy": "2024-06-14T15:55:39.145450Z",
     "iopub.status.idle": "2024-06-14T15:55:39.148119Z",
     "shell.execute_reply": "2024-06-14T15:55:39.147752Z",
     "shell.execute_reply.started": "2024-06-14T15:55:39.145792Z"
    }
   },
   "source": [
    "## Get device proxies"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6b2624cb-51e7-44d5-bcef-94f6b3468f05",
   "metadata": {},
   "outputs": [],
   "source": [
    "station, subracks, tpms, daqs = get_sps_devices(STATION_NAME)\n",
    "daq, bandpass_daq = daqs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7f03cf50-2945-46ff-9c4d-1fd2a6197100",
   "metadata": {},
   "outputs": [],
   "source": [
    "tpm_summary(tpms)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8464f198-655e-4024-9c82-1dba8b318be3",
   "metadata": {},
   "source": [
    "## Make sure all Tango devices are online and physical devices are on"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "db92924dc08a7685",
   "metadata": {},
   "source": [
    "If the DAQ is in this state, you can't even read adminMode - should be corrected by setting adminMode 1 and then 0."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "770777208675e34a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# After a fresh deploy or a reboot DAQ sometimes won't work. Why?\n",
    "# TODO make this hack better\n",
    "# TODO drill into this and raise an SKB\n",
    "for dev in daqs:\n",
    "    try:\n",
    "        if dev.adminMode != AdminMode.OFFLINE:\n",
    "            dev.daqstatus()\n",
    "    except DevFailed as e:\n",
    "        ok_errors = [\n",
    "            \"ska_control_model.faults.StateModelError: \"\n",
    "            \"Action component_fault is not allowed in op_state UNKNOWN.\",\n",
    "            \"grpc._channel._InactiveRpcError\",\n",
    "        ]\n",
    "        if not any(e.args[0].desc.startswith(err) for err in ok_errors):\n",
    "            raise\n",
    "        warn(f\"{dev.dev_name()} misbehaving - toggling adminMode\")\n",
    "        try:\n",
    "            dev.adminMode = AdminMode.OFFLINE\n",
    "        except DevFailed as e:\n",
    "            if not any(e.args[0].desc.startswith(err) for err in ok_errors):\n",
    "                raise\n",
    "        wait_for(dev, \"state\", DevState.DISABLE)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7633e90a10670611",
   "metadata": {},
   "source": [
    "Set all devices' adminMode to ONLINE."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4057307cda356e33",
   "metadata": {},
   "outputs": [],
   "source": [
    "def bring_online(devs):\n",
    "    disabled = [x for x in devs if x.State() == DevState.DISABLE]\n",
    "    for dev in disabled:\n",
    "        dev.adminMode = ADMIN_MODE\n",
    "    wait_for(disabled, \"state\", lambda v: v != DevState.DISABLE, timeout=10)\n",
    "\n",
    "\n",
    "bring_online(subracks + tpms + daqs + [station])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "549799fe9fb028b6",
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_subracks_out_of_unknown(subracks):\n",
    "    unknowns = [s for s in subracks if s.State() == DevState.UNKNOWN]\n",
    "    for s in unknowns:\n",
    "        print(\n",
    "            f\"subrack {s.dev_name()} is in UNKNOWN state, probably stuck due to \"\n",
    "            \"LOW-670/SPRTS-101. Restarting the device server.\"\n",
    "        )\n",
    "        s.Init()\n",
    "    wait_for(unknowns, \"state\", DevState.ON)\n",
    "\n",
    "\n",
    "get_subracks_out_of_unknown(subracks)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d5a66a52-a39f-4053-932a-f4044005dae2",
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: raise an SKB\n",
    "# The subracks' AUTO fan mode is not well-behaved when subracks aren't fully populated.\n",
    "# In the ITF we see TPMs turning themselves off due to overtemperature. Fans to max!\n",
    "\n",
    "\n",
    "def set_fans_to_max(subracks):\n",
    "    for subrack in subracks:\n",
    "        for n in range(1, 5):\n",
    "            subrack.setsubrackfanmode(json.dumps({\"fan_id\": n, \"mode\": 0}))\n",
    "            subrack.setsubrackfanspeed(\n",
    "                json.dumps({\"subrack_fan_id\": n, \"speed_percent\": 100})\n",
    "            )\n",
    "\n",
    "\n",
    "set_fans_to_max(subracks)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ee3784a6daf7d7d6",
   "metadata": {},
   "source": [
    "Ensure that TPMs are all powered on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bc1f03fb80b27419",
   "metadata": {},
   "outputs": [],
   "source": [
    "def ensure_tpms_on(tpms):\n",
    "    ON_STATES = {DevState.ON, DevState.ALARM}\n",
    "\n",
    "    restart_tpms = set()\n",
    "    for tpm in tpms:\n",
    "        if tpm.State() not in ON_STATES | {DevState.OFF}:\n",
    "            print(f\"TPM {tpm.dev_name()} is in state {tpm.State()}. Restarting it.\")\n",
    "        elif tpm.tileProgrammingState not in {\"Initialised\", \"Synchronised\"}:\n",
    "            print(\n",
    "                f\"TPM {tpm.dev_name()} tileProgrammingState is \"\n",
    "                f'{tpm.tileProgrammingState}\". Restarting it.'\n",
    "            )\n",
    "        else:\n",
    "            continue\n",
    "        restart_tpms.add(tpm)\n",
    "\n",
    "    if restart_tpms:\n",
    "        # TPMS off. Use the subrack in case MccsTile isn't working.\n",
    "        for tpm in restart_tpms:\n",
    "            subrack_trl = single_prop(tpm, \"SubrackFQDN\")\n",
    "            subrack_bay = single_prop(tpm, \"SubrackBay\")\n",
    "            subrack = next(sr for sr in subracks if sr.dev_name() == subrack_trl)\n",
    "            subrack.PowerOffTpm(int(subrack_bay))\n",
    "        wait_for(restart_tpms, \"state\", DevState.OFF)\n",
    "        # TPMs on. Used to use subrack here too, but that broke.\n",
    "        for tpm in restart_tpms:\n",
    "            tpm.On()\n",
    "        wait_for(restart_tpms, \"state\", ON_STATES)\n",
    "        # Wait for initialised\n",
    "        wait_for(restart_tpms, \"tileProgrammingState\", {\"Initialised\", \"Synchronised\"})\n",
    "\n",
    "    try:\n",
    "        wait_for(\n",
    "            tpms,\n",
    "            \"healthState\",\n",
    "            set(HealthState) - {HealthState.UNKNOWN},\n",
    "            timeout=10,\n",
    "            quiet=True,\n",
    "        )\n",
    "    except ValueError as e:\n",
    "        for dev in e.failed_devices:\n",
    "            ds = DeviceProxy(dev.adm_name())\n",
    "            ds.RestartServer()\n",
    "        wait_for(e.failed_devices, \"state\", ON_STATES)\n",
    "        wait_for(e.failed_devices, \"tileProgrammingState\", \"Initialised\")\n",
    "        wait_for(\n",
    "            e.failed_devices,\n",
    "            \"healthState\",\n",
    "            set(HealthState) - {HealthState.UNKNOWN},\n",
    "        )\n",
    "\n",
    "\n",
    "ensure_tpms_on(tpms)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c468b736-1f1f-4a14-a45a-c91856603633",
   "metadata": {},
   "outputs": [],
   "source": [
    "# see https://jira.skatelescope.org/browse/SKB-500\n",
    "assert station.antennasmapping\n",
    "# If this fails you need to apply the following then reset the sps station pod\n",
    "# station.put_property({\"AntennaConfigURI\": [\n",
    "#   \"gitlab://gitlab.com/ska-telescope/ska-low-aavs3?main#tmdata\",\n",
    "#   \"aavs3.yaml\"\n",
    "# ]})"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1fe6bd93d83bedfd",
   "metadata": {},
   "source": [
    "## Set the LMC and CSP endpoints for the station\n",
    "\n",
    "For TPMs to start acquiring, they need to have been configured with an endpoint that is responding to ARP requests. Therefore, we must fetch the IP from the DAQ which we'll use later to configure TPMs. The DAQ itself doesn't have to be running at this point, we just need the IP."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "101f5bc87cf02524",
   "metadata": {},
   "outputs": [],
   "source": [
    "daq_status = json.loads(daq.DaqStatus())\n",
    "pprint(daq_status)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d6d3c8f4a47cefa7",
   "metadata": {},
   "outputs": [],
   "source": [
    "daq_ip, daq_port = daq_status[\"Receiver IP\"][0], daq_status[\"Receiver Ports\"][0]\n",
    "lmc_config = {\n",
    "    \"mode\": \"10G\",\n",
    "    \"destination_ip\": daq_ip,\n",
    "    \"destination_port\": daq_port,\n",
    "}\n",
    "print(f\"Setting LMC destination to {daq_ip}:{daq_port}\")\n",
    "station.SetLmcIntegratedDownload(json.dumps(lmc_config))\n",
    "station.SetLmcDownload(json.dumps(lmc_config))\n",
    "\n",
    "if CSP_INGEST == \"DAQ\":\n",
    "    csp_ip, csp_port = daq_ip, daq_port\n",
    "elif CSP_INGEST:\n",
    "    csp_ip, csp_port = split_host_port(CSP_INGEST)\n",
    "elif station.get_property(\"CspIngestIp\")[\"CspIngestIp\"]:\n",
    "    csp_ip, csp_port = split_host_port(single_prop(station, \"CspIngestIp\"))\n",
    "else:\n",
    "    csp_ip, csp_port = daq_ip, daq_port\n",
    "csp_config = {\n",
    "    \"destination_ip\": csp_ip,\n",
    "    \"destination_port\": csp_port,\n",
    "}\n",
    "print(f\"Setting CSP destination to {':'.join(map(str, csp_config.values()))}\")\n",
    "station.SetCspIngest(json.dumps(csp_config))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3858dc06-0173-4fcf-a08b-d7e7ace9d2a4",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-06-14T16:21:37.579326Z",
     "iopub.status.busy": "2024-06-14T16:21:37.578632Z",
     "iopub.status.idle": "2024-06-14T16:21:37.582796Z",
     "shell.execute_reply": "2024-06-14T16:21:37.582103Z",
     "shell.execute_reply.started": "2024-06-14T16:21:37.579298Z"
    }
   },
   "source": [
    "## Apply static delays"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0777d51b-ce9f-4edf-aa45-e623ce1aff41",
   "metadata": {},
   "outputs": [],
   "source": [
    "station.staticTimeDelays = [0] * 512\n",
    "wait_for(tpms, \"staticTimeDelays\", [0] * 32, quiet=True)\n",
    "if APPLY_STATIC_DELAYS:\n",
    "    # Beware the ordering here. SpsStation uses EEP order for everything except this.\n",
    "    # TODO: fix this inconsistency in SpsStation\n",
    "    delays_by_tpm = get_station_static_delays(STATION_NAME)\n",
    "    station.staticTimeDelays = [x for xs in delays_by_tpm for x in xs]\n",
    "    print(f\"Loaded static delays for station {STATION_NAME}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "78b9da1c-0031-474e-96f4-55e5e765d90d",
   "metadata": {},
   "source": [
    "## Apply CSP rounding"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0cb7c1d3-f168-453f-839e-04c7e52a45fd",
   "metadata": {},
   "outputs": [],
   "source": [
    "DEFAULT_CSP_ROUNDING = 4\n",
    "if CSP_ROUNDING is None:\n",
    "    cr_prop = station.get_property(\"CspRounding\")[\"CspRounding\"]\n",
    "    station.cspRounding = [int(x) for x in cr_prop] or [DEFAULT_CSP_ROUNDING]\n",
    "    print(\"Set CSP rounding to value supplied in CspRounding property\")\n",
    "else:\n",
    "    # CSP rounding can be provided as a list of 384 values, one per beamformer channel.\n",
    "    # However for now the underlying implementation only looks at the first value,\n",
    "    # and applies it to all channels. So for now, we'll just provide a list of one.\n",
    "    station.cspRounding = [CSP_ROUNDING]\n",
    "    print(f\"Set CSP rounding to {CSP_ROUNDING} in all channels\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ec52c7547cb8ce60",
   "metadata": {},
   "source": [
    "## Apply channeliser rounding (Optional)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9839279d",
   "metadata": {},
   "outputs": [],
   "source": [
    "DEFAULT_CHANNELISER_ROUNDING = 2\n",
    "if CHANNELISER_ROUNDING is None:\n",
    "    cr_prop = station.get_property(\"ChanneliserRounding\")[\"ChanneliserRounding\"]\n",
    "    station.SetChanneliserRounding(\n",
    "        [int(x) for x in cr_prop] or [DEFAULT_CHANNELISER_ROUNDING]\n",
    "    )\n",
    "    print(\"Set channeliser rounding to value supplied in ChanneliserRounding property\")\n",
    "else:\n",
    "    station.SetChanneliserRounding([CHANNELISER_ROUNDING] * 512)\n",
    "    print(f\"Set channeliser rounding to {CHANNELISER_ROUNDING} for all coarse channels\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cdb2aeea-d032-4c4b-b546-090eb266eb17",
   "metadata": {},
   "source": [
    "## Set global reference time when using the new SPEAD format"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1a30497b-6b7f-4c98-b692-b0863da97236",
   "metadata": {},
   "outputs": [],
   "source": [
    "# When using the new SPEAD format, you specify a common global reference time\n",
    "# across all TPMs. This time needs to be sometime in the previous two weeks.\n",
    "# This code will set the reference time to 8am on the most recent Monday AWST,\n",
    "# chosen because AA0.5 is offline for generator maintenance at this time.\n",
    "if station.cspSpeadFormat != \"SKA\":\n",
    "    warn(f\"Resetting CSP Spead format from '{station.cspSpeadFormat}' back to 'SKA'\")\n",
    "    station.cspSpeadFormat = \"SKA\"\n",
    "today = datetime.now(timezone.utc).date()\n",
    "monday_morning = today - timedelta(days=today.weekday())\n",
    "station.globalReferenceTime = monday_morning.strftime(\"%Y-%m-%dT00:00:00.000000Z\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ff9f4b3c31adccda",
   "metadata": {},
   "source": [
    "## Initialise the station.\n",
    "\n",
    "This will erase, re-program, and initialise each TPM, then set some station-wide attributes on the TPMs and ensure they are synchronised."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "718268e9d92705f1",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "def lrc_status_dict(dev):\n",
    "    lrc_dict = dict(toolz.partition(2, dev.longRunningCommandStatus))\n",
    "    return lrc_dict\n",
    "\n",
    "\n",
    "# turning all the TPMs on when they're all off makes the subrack go into UNKNOWN\n",
    "# TODO report that SKB and remove this failure handling\n",
    "[init_result, [init_cmd_id]] = station.Initialise()\n",
    "\n",
    "print(\"Waiting for tiles to be unprogrammed...\")\n",
    "wait_for(tpms, \"tileProgrammingState\", \"NotProgrammed\")\n",
    "\n",
    "time.sleep(1)\n",
    "init_status = lrc_status_dict(station)[init_cmd_id]\n",
    "print(f\"Initialise() command status after 1 second: {init_status}\")\n",
    "\n",
    "if init_status == \"FAILED\":\n",
    "    print(\"station.Initialise() status is FAILED, restarting subracks\")\n",
    "    get_subracks_out_of_unknown(subracks)\n",
    "    [init_result, [init_cmd_id]] = station.Initialise()\n",
    "    time.sleep(1)\n",
    "    init_status = lrc_status_dict(station)[init_cmd_id]\n",
    "    print(f\"Initialise() command status after 1 second: {init_status}\")\n",
    "    if init_status == \"FAILED\":\n",
    "        raise RuntimeError(\n",
    "            \"station.Initialise() status is FAILED even after restarting subracks\"\n",
    "        )\n",
    "\n",
    "print(\"Waiting for tiles to be re-initialised...\")\n",
    "try:\n",
    "    wait_for(\n",
    "        tpms,\n",
    "        \"tileProgrammingState\",\n",
    "        \"Synchronised\",\n",
    "    )\n",
    "except ValueError as e:\n",
    "    print(e)\n",
    "    print(\"Restarting failed TPMs\")\n",
    "    ensure_tpms_on(tpms)\n",
    "\n",
    "print(\"Waiting for station initialisation to complete...\")\n",
    "while lrc_status_dict(station)[init_cmd_id] not in (\"COMPLETED\", \"FAILED\"):\n",
    "    time.sleep(5)\n",
    "if lrc_status_dict(station)[init_cmd_id] == \"COMPLETED\":\n",
    "    print(\"SpsStation.Initialise() succeeded.\")\n",
    "else:\n",
    "    err = \"SpsStation.Initialise() failed.\"\n",
    "    if ACCEPT_FAILURE:\n",
    "        print(err)\n",
    "    else:\n",
    "        raise RuntimeError(err)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "524f76abbbb54f41",
   "metadata": {
    "editable": true,
    "slideshow": {
     "slide_type": ""
    },
    "tags": []
   },
   "source": [
    "Check that all TPMs have a valid destination_ip."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fdab86510d152f33",
   "metadata": {},
   "outputs": [],
   "source": [
    "def int_to_mac(int_mac):\n",
    "    return \":\".join(\"\".join(x) for x in toolz.partition(2, f\"{int_mac:x}\"))\n",
    "\n",
    "\n",
    "with pd.option_context(\"display.max_rows\", None, \"display.width\", 999999):\n",
    "    df = pd.DataFrame(\n",
    "        {\n",
    "            \"tpm\": tpm.dev_name(),\n",
    "            **json.loads(\n",
    "                tpm.Get40GCoreConfiguration(\n",
    "                    json.dumps({\"core_id\": core_id, \"arp_table_entry\": e})\n",
    "                )\n",
    "            ),\n",
    "        }\n",
    "        for tpm in tpms\n",
    "        for core_id in [0]  # core 1 is not used for data transmission\n",
    "        for e in [0, 1]  # 2 and 3 should be the same as 0 and 1\n",
    "    )\n",
    "    # TODO: assert the assumptions in the above comments\n",
    "\n",
    "    df.netmask = [ipaddress.ip_address(x) for x in df.netmask]\n",
    "    df.gateway_ip = [ipaddress.ip_address(x) for x in df.gateway_ip]\n",
    "    df.source_mac = [int_to_mac(x) for x in df.source_mac]\n",
    "    display(df)\n",
    "\n",
    "assert all(df.iloc[1::2].destination_ip == daq_ip)\n",
    "assert df.iloc[-2].destination_ip == csp_ip"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9506c4429d8a6665",
   "metadata": {},
   "source": [
    "Set a flat attenuation across the station before starting acquisition."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "294e93fb9cd4a4cd",
   "metadata": {},
   "outputs": [],
   "source": [
    "if PREADU_LEVEL is None:\n",
    "    print(\"Setting preadulevels from telmodel ...\")\n",
    "    station.preaduLevels = get_station_preadu_attenuations(STATION_NAME, 20.0)\n",
    "else:\n",
    "    print(f\"Overriding predadu levels to {PREADU_LEVEL}\")\n",
    "    station.preaduLevels = [PREADU_LEVEL] * 512"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c698b95d-e847-4ce6-be93-925956aaab61",
   "metadata": {},
   "source": [
    "Assert that channeliser and CSP rounding have been set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "940385b0-2f99-4843-9d31-72d9b8a04119",
   "metadata": {},
   "outputs": [],
   "source": [
    "if CHANNELISER_ROUNDING is not None:\n",
    "    assert all(\n",
    "        all(tpm.channeliserRounding == [CHANNELISER_ROUNDING] * 512) for tpm in tpms\n",
    "    )\n",
    "if CSP_ROUNDING is not None:\n",
    "    assert all(tpms[-1].cspRounding == [CSP_ROUNDING] * 384)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f2f03a0e-9873-4a81-8014-690fa0bf3373",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Assert that MccsTile and SpsStation delays agree\n",
    "timeout = 30\n",
    "for i in range(timeout):\n",
    "    time.sleep(1)\n",
    "    diff = np.array([x for tpm in tpms for x in tpm.staticTimeDelays]) - np.array(\n",
    "        station.staticTimeDelays\n",
    "    )\n",
    "    num_remaining = len([x for x in diff if x])\n",
    "    if num_remaining == 0:\n",
    "        print(\"All delays equal between station and TPMs\")\n",
    "        break\n",
    "else:\n",
    "    raise RuntimeError(\n",
    "        f\"station and TPM static delays didn't agree after {timeout} seconds\"\n",
    "    )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "efc5c83e-5338-4596-9674-81ce199c95a6",
   "metadata": {},
   "source": [
    "### Start bandpass monitoring"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cccf535b-2015-4713-86b3-ed13b0683775",
   "metadata": {},
   "outputs": [],
   "source": [
    "if BANDPASS_INTEGRATION_TIME:\n",
    "    try:\n",
    "        assert bandpass_daq is not None\n",
    "    except DevFailed:\n",
    "        print(\n",
    "            \"No dedicated bandpass DAQ was found, \"\n",
    "            \"pointing integrated data at regular DAQ\"\n",
    "        )\n",
    "        bandpass_config = lmc_config\n",
    "    else:\n",
    "        # Start the bandpass monitor if not already.\n",
    "        # TODO: get rid of these sleeps\n",
    "        # TODO: get rid of this entirely\n",
    "        bp_status = json.loads(bandpass_daq.DaqStatus())\n",
    "        print(bp_status)\n",
    "        if not (\n",
    "            bp_status[\"Bandpass Monitor\"]\n",
    "            and bp_status[\"Running Consumers\"] == [[\"INTEGRATED_CHANNEL_DATA\", 5]]\n",
    "        ):\n",
    "            print(\"Reconfiguring bandpass DAQ\")\n",
    "            bandpass_daq.StopBandpassMonitor()\n",
    "            wait_lrc(bandpass_daq, \"Stop\")\n",
    "            bandpass_daq.Configure(\n",
    "                json.dumps(\n",
    "                    {\n",
    "                        \"directory\": \"/\",\n",
    "                        \"nof_tiles\": len(tpms),\n",
    "                        \"append_integrated\": False,\n",
    "                    }\n",
    "                )\n",
    "            )\n",
    "            wait_lrc(\n",
    "                bandpass_daq,\n",
    "                \"Start\",\n",
    "                json.dumps({\"modes_to_start\": \"INTEGRATED_CHANNEL_DATA\"}),\n",
    "            )\n",
    "            bandpass_daq.StartBandpassMonitor('{\"plot_directory\": \"/tmp\"}')\n",
    "\n",
    "        # This is pretty gnarly.\n",
    "        # It would be nicer if the bandpass DAQ could ascertain its\n",
    "        # own external IP and report it normally.\n",
    "        kubernetes.config.load_incluster_config()\n",
    "        core_v1_api = kubernetes.client.CoreV1Api(kubernetes.client.ApiClient())\n",
    "\n",
    "        svc = core_v1_api.read_namespaced_service(\n",
    "            name=f\"{single_prop(bandpass_daq, 'Host')}-data\",\n",
    "            namespace=bandpass_daq.info().server_host.split(\".\")[2],\n",
    "        )\n",
    "\n",
    "        bandpass_config = {\n",
    "            \"mode\": \"1G\",\n",
    "            \"destination_ip\": svc.status.load_balancer.ingress[0].ip,\n",
    "            \"destination_port\": 4660,  # get this from the service as well?\n",
    "        }\n",
    "\n",
    "    # Gotta do this to make it work - set both LMC destinations to the bandpass DAQ,\n",
    "    # then set the normal LMC destination back to the regular DAQ.\n",
    "    # TODO raise an SKB\n",
    "    station.SetLmcDownload(json.dumps(bandpass_config))\n",
    "    station.SetLmcIntegratedDownload(json.dumps(bandpass_config))\n",
    "    station.SetLmcDownload(json.dumps(lmc_config))\n",
    "\n",
    "    station.ConfigureIntegratedChannelData(\n",
    "        json.dumps({\"integration_time\": BANDPASS_INTEGRATION_TIME})\n",
    "    )\n",
    "    print(\n",
    "        f\"Sending bandpass samples every {BANDPASS_INTEGRATION_TIME}s \"\n",
    "        f\"to {bandpass_config['destination_ip']}\"\n",
    "    )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c7416900",
   "metadata": {},
   "source": [
    "## Set up calibration store to use \"golden\" solution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "74692df8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set the selection policy to use the most recent golden solution\n",
    "reset_selection_policy(STATION_NAME)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0dafe563-1326-42d1-9fa7-be0a9d90dd31",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-06-14T16:22:39.317041Z",
     "iopub.status.busy": "2024-06-14T16:22:39.316379Z",
     "iopub.status.idle": "2024-06-14T16:22:39.320814Z",
     "shell.execute_reply": "2024-06-14T16:22:39.319937Z",
     "shell.execute_reply.started": "2024-06-14T16:22:39.317013Z"
    }
   },
   "source": [
    "## Done - report on station status and health"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25dea7189b348380",
   "metadata": {},
   "source": [
    "Report on the station status."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5fb8d2c1b6d13aa",
   "metadata": {},
   "outputs": [],
   "source": [
    "tpm_summary(tpms)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "745aaf3935441303",
   "metadata": {},
   "outputs": [],
   "source": [
    "with pd.option_context(\"display.max_colwidth\", None):\n",
    "    display(\n",
    "        pd.DataFrame(\n",
    "            {\n",
    "                k: report_attr(tpm, k)\n",
    "                for k in [\n",
    "                    \"dev_name\",\n",
    "                    \"state\",\n",
    "                    \"status\",\n",
    "                    \"healthState\",\n",
    "                    \"alarmHealth\",\n",
    "                    \"adcHealth\",\n",
    "                    \"ioHealth\",\n",
    "                    \"dspHealth\",\n",
    "                    \"alarmHealth\",\n",
    "                    \"voltageHealth\",\n",
    "                    \"currentHealth\",\n",
    "                    \"temperatureHealth\",\n",
    "                    \"timingHealth\",\n",
    "                    \"healthReport\",\n",
    "                ]\n",
    "            }\n",
    "            for tpm in tpms + subracks\n",
    "        )\n",
    "    )"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
