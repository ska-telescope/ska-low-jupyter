import sys
import os
import glob
import papermill as pm

FOL = "/home/jovyan/shared/sft-executions/03.FrequencySweep/2024-05-14-09-32-05/captured_data"

sft = "/home/jovyan/shared/Lee/sensitivity/comparison/ravi_parametrised.ipynb"

top = "/home/jovyan/shared/Lee/sensitivity/comparison"

aavs3_yaml = "/home/jovyan/shared/Lee/sensitivity/comparison/old_aavs3.yaml"
antenna_layout_file = "/home/jovyan/shared/Lee/sensitivity/comparison/aavs3_antenna_layout.txt"

try:
    input_file = sys.argv[1]
except:
    input_file = None
    
    

def get_channel_id(filename):
    return filename.split("_")[3]
    
def timestamp(filename):
    a =  filename.split("_")[4]
    b =  filename.split("_")[5]
    ts = f"{a}_{b}"
    return ts
    

if input_file is not None:
    fp = input_file
    print(f"input = {fp}")
    f = os.path.basename(fp)
    print(f)
    channel_id = get_channel_id(f)
    ts = timestamp(f)
    
    print(f"channel id = {channel_id}")
    print(f"ts = {ts}")
    output_fol = os.path.join(top,f"ravi_{channel_id}_{ts}")
    print(f"output fol = {output_fol}")
    os.makedirs(output_fol, exist_ok=True)
    nb_name = os.path.basename(sft)
    name, _ = os.path.splitext(nb_name)
    new_name = f"{name}_{channel_id}.ipynb"
    out_nb = os.path.join(output_fol, new_name)
    print(f"output notebook = {out_nb}")
    print(f"Executing {out_nb} in {output_fol} ...")
    pm.execute_notebook(sft,
                        out_nb,
                        parameters={"param_input_file": fp,
                                    "param_captured_data_path": FOL,
                                    "param_channel_id": channel_id,
                                    "param_outputs_fol": output_fol},
                        cwd=output_fol)

else:
    for f in glob.glob(f"{FOL}/*.hdf5"):
        fp = os.path.join(FOL,f)
        print(f"input = {fp}")
        channel_id = get_channel_id(f)
        ts = timestamp(f)
        print(f"channel id = {channel_id}")
        print(f"ts = {ts}")
        output_fol = os.path.join(top,f"ravinb_{channel_id}_{ts}")
        print(f"output fol = {output_fol}")
        os.makedirs(output_fol, exist_ok=True)
        nb_name = os.path.basename(sft)
        name, _ = os.path.splitext(nb_name)
        new_name = f"{name}_{channel_id}.ipynb"
        out_nb = os.path.join(output_fol, new_name)
        print(f"output notebook = {out_nb}")
        print(f"Executing {out_nb} in {output_fol} ...")
        pm.execute_notebook(sft,
                            out_nb,
                            parameters={"param_input_file": fp,
                                        "param_captured_data_path": FOL,
                                        "param_station_config_file": aavs3_yaml,
                                        "param_antenna_layout_file": antenna_layout_file,
                                        "param_channel_id": channel_id,
                                        "param_outputs_fol": output_fol},
                            cwd=output_fol)
                        
                    
                





