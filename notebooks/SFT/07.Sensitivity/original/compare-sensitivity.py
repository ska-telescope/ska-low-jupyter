import os
import glob
import papermill as pm
import sys

def get_channel_id(filename):
    return filename.split("_")[3]
    
def timestamp(filename):
    a =  filename.split("_")[4]
    b =  filename.split("_")[5]
    ts = f"{a}_{b}"
    return ts

TESTING_FOL = "/home/jovyan/shared/sft-executions/03.FrequencySweep/2024-05-14-09-32-05/captured_data"

sft = "/home/jovyan/ska-low-jupyter/notebooks/SFT/07.Sensitivity/07.Sensitivity.ipynb"

top = "/home/jovyan/shared/Lee/sensitivity/comparison"

if __name__ == '__main__':
    run_name = sys.argv[1]
    if len(sys.argv) == 3:
        freq_sweep_fol = sys.argv[2]
    else:
        freq_sweep_fol = TESTING_FOL
    top = os.path.join(top,run_name)
    
    for f in glob.glob(f"{freq_sweep_fol}/*.hdf5"):
        fp = os.path.join(freq_sweep_fol,f)
        print(f"input = {fp}")
        channel_id = get_channel_id(f)
        ts = timestamp(f)
        print(f"channel id = {channel_id}")
        print(f"ts = {ts}")
        output_fol = os.path.join(top,f"vulcan_{channel_id}_{ts}")
        print(f"output fol = {output_fol}")
        os.makedirs(output_fol, exist_ok=True)
        nb_name = os.path.basename(sft)
        name, _ = os.path.splitext(nb_name)
        new_name = f"{name}_{channel_id}.ipynb"
        out_nb = os.path.join(output_fol, new_name)
        print(f"output notebook = {out_nb}")
        print(f"Executing {out_nb} in {output_fol} ...")
        pm.execute_notebook(sft,
                            out_nb,
                            parameters={"param_input_file": fp,
                                        "param_captured_data_path": freq_sweep_fol,
                                        "param_channel_id": channel_id,
                                        "param_outputs_fol": output_fol},
                            cwd=output_fol)
