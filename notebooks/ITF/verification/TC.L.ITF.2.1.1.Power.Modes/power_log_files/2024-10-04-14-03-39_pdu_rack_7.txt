2024-10-04-14-04-12, [36mSUCCESS
PDU ID 1 : OUTLET 3 power Feature
voltage       : 240.4V
current	      : 2.0A
activepower   : 260.9W
apparentpower : 485.2VA
powerfactor   : 0.54
energy        : 1460.103kWh [37m
EN2.0>
2024-10-04-14-04-13, [36mSUCCESS
PDU ID 1 : OUTLET 11 power Feature
voltage       : 240.2V
current	      : 1.3A
activepower   : 313.9W
apparentpower : 318.1VA
powerfactor   : 0.99
energy        : 1084.646kWh [37m
EN2.0>
2024-10-04-14-04-15, [36mSUCCESS
PDU ID 1 : OUTLET 12 power Feature
voltage       : 240.2V
current	      : 1.2A
activepower   : 272.7W
apparentpower : 285.6VA
powerfactor   : 0.96
energy        : 1226.601kWh [37m
EN2.0>
2024-10-04-14-04-17, [36mSUCCESS
PDU ID 1 : OUTLET 15 power Feature
voltage       : 238.9V
current	      : 1.2A
activepower   : 271.7W
apparentpower : 283.3VA
powerfactor   : 0.96
energy        : 1275.955kWh [37m
EN2.0>
2024-10-04-14-04-18, [36mSUCCESS
PDU ID 1 : OUTLET 16 power Feature
voltage       : 238.9V
current	      : 1.2A
activepower   : 276.5W
apparentpower : 288.6VA
powerfactor   : 0.96
energy        : 1279.810kWh [37m
EN2.0>
2024-10-04-14-04-20, [36mSUCCESS
PDU ID 1 : OUTLET 17 power Feature
voltage       : 238.9V
current	      : 1.2A
activepower   : 284.7W
apparentpower : 296.7VA
powerfactor   : 0.96
energy        : 1279.693kWh [37m
EN2.0>
2024-10-04-14-04-21, [36mSUCCESS
PDU ID 1 : OUTLET 18 power Feature
voltage       : 238.9V
current	      : 1.1A
activepower   : 262.7W
apparentpower : 274.7VA
powerfactor   : 0.96
energy        : 1289.603kWh [37m
EN2.0>
2024-10-04-14-04-23, [36mSUCCESS
PDU ID 1 : OUTLET 23 power Feature
voltage       : 240.1V
current	      : 1.2A
activepower   : 276.7W
apparentpower : 283.3VA
powerfactor   : 0.98
energy        : 1277.521kWh [37m
EN2.0>
2024-10-04-14-04-24, [36mSUCCESS
PDU ID 1 : OUTLET 24 power Feature
voltage       : 240.1V
current	      : 1.2A
activepower   : 280.7W
apparentpower : 285.2VA
powerfactor   : 0.98
energy        : 1214.496kWh [37m
EN2.0>
2024-10-04-14-04-26, [36mSUCCESS
PDU ID 1 : OUTLET 27 power Feature
voltage       : 240.1V
current	      : 1.3A
activepower   : 304.7W
apparentpower : 307.1VA
powerfactor   : 1.00
energy        : 1456.418kWh [37m
EN2.0>
2024-10-04-14-04-27, [36mSUCCESS
PDU ID 1 : OUTLET 28 power Feature
voltage       : 240.1V
current	      : 0.0A
activepower   : 0.0W
apparentpower : 0.0VA
powerfactor   : 1.00
energy        : 4.500kWh [37m
EN2.0>
2024-10-04-14-04-29, [36mSUCCESS
PDU ID 1 : OUTLET 33 power Feature
voltage       : 238.5V
current	      : 0.2A
activepower   : 32.1W
apparentpower : 41.5VA
powerfactor   : 0.77
energy        : 197.428kWh [37m
EN2.0>
2024-10-04-14-04-30, [36mSUCCESS
PDU ID 1 : OUTLET 34 power Feature
voltage       : 238.5V
current	      : 0.2A
activepower   : 35.5W
apparentpower : 41.3VA
powerfactor   : 0.86
energy        : 94.166kWh [37m
EN2.0>
2024-10-04-14-04-31, [36mSUCCESS
PDU ID 1 : OUTLET 35 power Feature
voltage       : 238.5V
current	      : 0.2A
activepower   : 44.6W
apparentpower : 50.3VA
powerfactor   : 0.89
energy        : 214.476kWh [37m
EN2.0>
2024-10-04-14-04-33, [36mSUCCESS
PDU ID 1 : OUTLET 36 power Feature
voltage       : 238.5V
current	      : 0.2A
activepower   : 49.7W
apparentpower : 54.4VA
powerfactor   : 0.91
energy        : 243.528kWh [37m
EN2.0>
