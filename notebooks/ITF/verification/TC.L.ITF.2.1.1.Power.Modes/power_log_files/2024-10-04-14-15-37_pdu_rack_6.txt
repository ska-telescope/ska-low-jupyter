2024-10-04-14-15-39, [36mSUCCESS
PDU ID 1 : OUTLET 1 power Feature
voltage       : 240.3V
current	      : 0.5A
activepower   : 115.8W
apparentpower : 123.0VA
powerfactor   : 0.94
energy        : 606.100kWh [37m
EN2.0>
2024-10-04-14-15-40, [36mSUCCESS
PDU ID 1 : OUTLET 2 power Feature
voltage       : 240.3V
current	      : 0.5A
activepower   : 111.3W
apparentpower : 120.6VA
powerfactor   : 0.92
energy        : 570.685kWh [37m
EN2.0>
2024-10-04-14-15-42, [36mSUCCESS
PDU ID 1 : OUTLET 3 power Feature
voltage       : 240.3V
current	      : 2.0A
activepower   : 261.1W
apparentpower : 491.1VA
powerfactor   : 0.53
energy        : 1345.954kWh [37m
EN2.0>
2024-10-04-14-15-44, [36mSUCCESS
PDU ID 1 : OUTLET 7 power Feature
voltage       : 239.9V
current	      : 0.7A
activepower   : 160.9W
apparentpower : 165.0VA
powerfactor   : 0.97
energy        : 803.202kWh [37m
EN2.0>
2024-10-04-14-15-45, [36mSUCCESS
PDU ID 1 : OUTLET 8 power Feature
voltage       : 239.9V
current	      : 0.5A
activepower   : 108.5W
apparentpower : 121.8VA
powerfactor   : 0.89
energy        : 563.472kWh [37m
EN2.0>
2024-10-04-14-15-46, [36mSUCCESS
PDU ID 1 : OUTLET 15 power Feature
voltage       : 239.2V
current	      : 0.7A
activepower   : 151.2W
apparentpower : 166.9VA
powerfactor   : 0.91
energy        : 750.941kWh [37m
EN2.0>
2024-10-04-14-15-48, [36mSUCCESS
PDU ID 1 : OUTLET 16 power Feature
voltage       : 239.2V
current	      : 0.8A
activepower   : 176.8W
apparentpower : 188.9VA
powerfactor   : 0.94
energy        : 876.229kWh [37m
EN2.0>
2024-10-04-14-15-49, [36mSUCCESS
PDU ID 1 : OUTLET 17 power Feature
voltage       : 239.2V
current	      : 0.3A
activepower   : 27.5W
apparentpower : 62.4VA
powerfactor   : 0.44
energy        : 136.588kWh [37m
EN2.0>
2024-10-04-14-15-51, [36mSUCCESS
PDU ID 1 : OUTLET 18 power Feature
voltage       : 239.2V
current	      : 0.0A
activepower   : 0.0W
apparentpower : 0.0VA
powerfactor   : 1.00
energy        : 0.000kWh [37m
EN2.0>
2024-10-04-14-15-53, [36mSUCCESS
PDU ID 1 : OUTLET 21 power Feature
voltage       : 239.9V
current	      : 0.2A
activepower   : 34.6W
apparentpower : 40.3VA
powerfactor   : 0.86
energy        : 237.366kWh [37m
EN2.0>
2024-10-04-14-15-55, [36mSUCCESS
PDU ID 1 : OUTLET 22 power Feature
voltage       : 239.9V
current	      : 0.3A
activepower   : 32.2W
apparentpower : 64.3VA
powerfactor   : 0.50
energy        : 67.137kWh [37m
EN2.0>
2024-10-04-14-15-56, [36mSUCCESS
PDU ID 1 : OUTLET 23 power Feature
voltage       : 239.9V
current	      : 0.8A
activepower   : 179.2W
apparentpower : 196.2VA
powerfactor   : 0.91
energy        : 620.300kWh [37m
EN2.0>
2024-10-04-14-15-58, [36mSUCCESS
PDU ID 1 : OUTLET 24 power Feature
voltage       : 239.9V
current	      : 0.9A
activepower   : 205.6W
apparentpower : 221.4VA
powerfactor   : 0.93
energy        : 261.950kWh [37m
EN2.0>
2024-10-04-14-15-59, [36mSUCCESS
PDU ID 1 : OUTLET 27 power Feature
voltage       : 239.8V
current	      : 0.4A
activepower   : 79.2W
apparentpower : 97.9VA
powerfactor   : 0.81
energy        : 210.075kWh [37m
EN2.0>
2024-10-04-14-16-01, [36mSUCCESS
PDU ID 1 : OUTLET 28 power Feature
voltage       : 239.8V
current	      : 0.5A
activepower   : 96.2W
apparentpower : 110.8VA
powerfactor   : 0.87
energy        : 254.462kWh [37m
EN2.0>
2024-10-04-14-16-02, [36mSUCCESS
PDU ID 1 : OUTLET 29 power Feature
voltage       : 239.8V
current	      : 0.8A
activepower   : 183.2W
apparentpower : 200.8VA
powerfactor   : 0.91
energy        : 16.867kWh [37m
EN2.0>
2024-10-04-14-16-03, [36mSUCCESS
PDU ID 1 : OUTLET 30 power Feature
voltage       : 239.8V
current	      : 0.9A
activepower   : 205.8W
apparentpower : 220.9VA
powerfactor   : 0.93
energy        : 639.321kWh [37m
EN2.0>
2024-10-04-14-16-05, [36mSUCCESS
PDU ID 1 : OUTLET 31 power Feature
voltage       : 238.9V
current	      : 0.0A
activepower   : 0.0W
apparentpower : 0.0VA
powerfactor   : 1.00
energy        : 0.000kWh [37m
EN2.0>
2024-10-04-14-16-06, [36mSUCCESS
PDU ID 1 : OUTLET 32 power Feature
voltage       : 238.9V
current	      : 0.0A
activepower   : 0.0W
apparentpower : 0.0VA
powerfactor   : 1.00
energy        : 0.000kWh [37m
EN2.0>
2024-10-04-14-16-08, [36mSUCCESS
PDU ID 1 : OUTLET 33 power Feature
voltage       : 238.9V
current	      : 0.0A
activepower   : 0.0W
apparentpower : 0.0VA
powerfactor   : 1.00
energy        : 0.000kWh [37m
EN2.0>
