2024-10-04-14-35-13, [36mSUCCESS
PDU ID 1 : OUTLET 1 power Feature
voltage       : 240.2V
current	      : 0.5A
activepower   : 114.3W
apparentpower : 121.8VA
powerfactor   : 0.94
energy        : 606.100kWh [37m
EN2.0>
2024-10-04-14-35-15, [36mSUCCESS
PDU ID 1 : OUTLET 2 power Feature
voltage       : 240.2V
current	      : 0.5A
activepower   : 109.7W
apparentpower : 119.2VA
powerfactor   : 0.92
energy        : 570.685kWh [37m
EN2.0>
2024-10-04-14-35-17, [36mSUCCESS
PDU ID 1 : OUTLET 3 power Feature
voltage       : 240.2V
current	      : 2.0A
activepower   : 261.1W
apparentpower : 491.3VA
powerfactor   : 0.53
energy        : 1346.055kWh [37m
EN2.0>
2024-10-04-14-35-18, [36mSUCCESS
PDU ID 1 : OUTLET 7 power Feature
voltage       : 240.0V
current	      : 0.7A
activepower   : 158.2W
apparentpower : 162.7VA
powerfactor   : 0.97
energy        : 803.202kWh [37m
EN2.0>
2024-10-04-14-35-20, [36mSUCCESS
PDU ID 1 : OUTLET 8 power Feature
voltage       : 240.0V
current	      : 0.5A
activepower   : 110.1W
apparentpower : 123.6VA
powerfactor   : 0.89
energy        : 563.472kWh [37m
EN2.0>
2024-10-04-14-35-22, [36mSUCCESS
PDU ID 1 : OUTLET 15 power Feature
voltage       : 239.3V
current	      : 0.7A
activepower   : 151.1W
apparentpower : 166.8VA
powerfactor   : 0.91
energy        : 751.042kWh [37m
EN2.0>
2024-10-04-14-35-24, [36mSUCCESS
PDU ID 1 : OUTLET 16 power Feature
voltage       : 239.3V
current	      : 0.8A
activepower   : 175.3W
apparentpower : 187.6VA
powerfactor   : 0.93
energy        : 876.330kWh [37m
EN2.0>
2024-10-04-14-35-25, [36mSUCCESS
PDU ID 1 : OUTLET 17 power Feature
voltage       : 239.3V
current	      : 0.3A
activepower   : 27.9W
apparentpower : 62.7VA
powerfactor   : 0.45
energy        : 136.588kWh [37m
EN2.0>
2024-10-04-14-35-26, [36mSUCCESS
PDU ID 1 : OUTLET 18 power Feature
voltage       : 239.3V
current	      : 0.0A
activepower   : 0.0W
apparentpower : 0.0VA
powerfactor   : 1.00
energy        : 0.000kWh [37m
EN2.0>
2024-10-04-14-35-28, [36mSUCCESS
PDU ID 1 : OUTLET 21 power Feature
voltage       : 239.9V
current	      : 0.2A
activepower   : 34.3W
apparentpower : 39.8VA
powerfactor   : 0.86
energy        : 237.366kWh [37m
EN2.0>
2024-10-04-14-35-29, [36mSUCCESS
PDU ID 1 : OUTLET 22 power Feature
voltage       : 239.9V
current	      : 0.3A
activepower   : 32.3W
apparentpower : 65.3VA
powerfactor   : 0.49
energy        : 67.137kWh [37m
EN2.0>
2024-10-04-14-35-31, [36mSUCCESS
PDU ID 1 : OUTLET 23 power Feature
voltage       : 239.9V
current	      : 0.6A
activepower   : 69.9W
apparentpower : 132.2VA
powerfactor   : 0.53
energy        : 620.401kWh [37m
EN2.0>
2024-10-04-14-35-32, [36mSUCCESS
PDU ID 1 : OUTLET 24 power Feature
voltage       : 239.9V
current	      : 0.7A
activepower   : 124.3W
apparentpower : 163.9VA
powerfactor   : 0.76
energy        : 261.950kWh [37m
EN2.0>
2024-10-04-14-35-34, [36mSUCCESS
PDU ID 1 : OUTLET 27 power Feature
voltage       : 239.8V
current	      : 0.4A
activepower   : 80.0W
apparentpower : 98.6VA
powerfactor   : 0.81
energy        : 210.176kWh [37m
EN2.0>
2024-10-04-14-35-35, [36mSUCCESS
PDU ID 1 : OUTLET 28 power Feature
voltage       : 239.8V
current	      : 0.5A
activepower   : 96.6W
apparentpower : 111.5VA
powerfactor   : 0.87
energy        : 254.462kWh [37m
EN2.0>
2024-10-04-14-35-36, [36mSUCCESS
PDU ID 1 : OUTLET 29 power Feature
voltage       : 239.8V
current	      : 0.5A
activepower   : 81.5W
apparentpower : 125.9VA
powerfactor   : 0.65
energy        : 16.968kWh [37m
EN2.0>
2024-10-04-14-35-38, [36mSUCCESS
PDU ID 1 : OUTLET 30 power Feature
voltage       : 239.8V
current	      : 0.6A
activepower   : 103.9W
apparentpower : 138.6VA
powerfactor   : 0.75
energy        : 639.422kWh [37m
EN2.0>
2024-10-04-14-35-39, [36mSUCCESS
PDU ID 1 : OUTLET 31 power Feature
voltage       : 239.2V
current	      : 0.0A
activepower   : 0.0W
apparentpower : 0.0VA
powerfactor   : 1.00
energy        : 0.000kWh [37m
EN2.0>
2024-10-04-14-35-41, [36mSUCCESS
PDU ID 1 : OUTLET 32 power Feature
voltage       : 239.2V
current	      : 0.0A
activepower   : 0.0W
apparentpower : 0.0VA
powerfactor   : 1.00
energy        : 0.000kWh [37m
EN2.0>
2024-10-04-14-35-43, [36mSUCCESS
PDU ID 1 : OUTLET 33 power Feature
voltage       : 239.2V
current	      : 0.0A
activepower   : 0.0W
apparentpower : 0.0VA
powerfactor   : 1.00
energy        : 0.000kWh [37m
EN2.0>
