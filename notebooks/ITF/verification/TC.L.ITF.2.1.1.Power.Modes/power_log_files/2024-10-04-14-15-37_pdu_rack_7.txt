2024-10-04-14-16-09, [36mSUCCESS
PDU ID 1 : OUTLET 3 power Feature
voltage       : 240.5V
current	      : 2.0A
activepower   : 260.9W
apparentpower : 485.1VA
powerfactor   : 0.54
energy        : 1460.103kWh [37m
EN2.0>
2024-10-04-14-16-11, [36mSUCCESS
PDU ID 1 : OUTLET 11 power Feature
voltage       : 240.2V
current	      : 1.3A
activepower   : 314.1W
apparentpower : 318.1VA
powerfactor   : 0.99
energy        : 1084.747kWh [37m
EN2.0>
2024-10-04-14-16-12, [36mSUCCESS
PDU ID 1 : OUTLET 12 power Feature
voltage       : 240.2V
current	      : 1.2A
activepower   : 267.5W
apparentpower : 280.8VA
powerfactor   : 0.95
energy        : 1226.601kWh [37m
EN2.0>
2024-10-04-14-16-14, [36mSUCCESS
PDU ID 1 : OUTLET 15 power Feature
voltage       : 238.9V
current	      : 1.2A
activepower   : 271.2W
apparentpower : 282.8VA
powerfactor   : 0.96
energy        : 1276.056kWh [37m
EN2.0>
2024-10-04-14-16-16, [36mSUCCESS
PDU ID 1 : OUTLET 16 power Feature
voltage       : 238.9V
current	      : 1.3A
activepower   : 302.0W
apparentpower : 312.2VA
powerfactor   : 0.97
energy        : 1279.810kWh [37m
EN2.0>
2024-10-04-14-16-17, [36mSUCCESS
PDU ID 1 : OUTLET 17 power Feature
voltage       : 238.9V
current	      : 1.3A
activepower   : 306.0W
apparentpower : 316.5VA
powerfactor   : 0.97
energy        : 1279.794kWh [37m
EN2.0>
2024-10-04-14-16-19, [36mSUCCESS
PDU ID 1 : OUTLET 18 power Feature
voltage       : 238.9V
current	      : 1.2A
activepower   : 267.9W
apparentpower : 280.0VA
powerfactor   : 0.96
energy        : 1289.603kWh [37m
EN2.0>
2024-10-04-14-16-20, [36mSUCCESS
PDU ID 1 : OUTLET 23 power Feature
voltage       : 240.0V
current	      : 1.1A
activepower   : 264.6W
apparentpower : 271.4VA
powerfactor   : 0.97
energy        : 1277.622kWh [37m
EN2.0>
2024-10-04-14-16-22, [36mSUCCESS
PDU ID 1 : OUTLET 24 power Feature
voltage       : 240.0V
current	      : 1.0A
activepower   : 243.4W
apparentpower : 248.6VA
powerfactor   : 0.98
energy        : 1214.597kWh [37m
EN2.0>
2024-10-04-14-16-23, [36mSUCCESS
PDU ID 1 : OUTLET 27 power Feature
voltage       : 240.2V
current	      : 1.2A
activepower   : 281.9W
apparentpower : 283.7VA
powerfactor   : 1.00
energy        : 1456.519kWh [37m
EN2.0>
2024-10-04-14-16-25, [36mSUCCESS
PDU ID 1 : OUTLET 28 power Feature
voltage       : 240.2V
current	      : 0.0A
activepower   : 0.0W
apparentpower : 0.0VA
powerfactor   : 1.00
energy        : 4.500kWh [37m
EN2.0>
2024-10-04-14-16-27, [36mSUCCESS
PDU ID 1 : OUTLET 33 power Feature
voltage       : 238.6V
current	      : 0.2A
activepower   : 31.7W
apparentpower : 41.5VA
powerfactor   : 0.76
energy        : 197.428kWh [37m
EN2.0>
2024-10-04-14-16-28, [36mSUCCESS
PDU ID 1 : OUTLET 34 power Feature
voltage       : 238.6V
current	      : 0.2A
activepower   : 35.4W
apparentpower : 41.3VA
powerfactor   : 0.86
energy        : 94.166kWh [37m
EN2.0>
2024-10-04-14-16-29, [36mSUCCESS
PDU ID 1 : OUTLET 35 power Feature
voltage       : 238.6V
current	      : 0.2A
activepower   : 45.1W
apparentpower : 50.6VA
powerfactor   : 0.89
energy        : 214.476kWh [37m
EN2.0>
2024-10-04-14-16-31, [36mSUCCESS
PDU ID 1 : OUTLET 36 power Feature
voltage       : 238.6V
current	      : 0.2A
activepower   : 49.5W
apparentpower : 54.2VA
powerfactor   : 0.91
energy        : 243.629kWh [37m
EN2.0>
