# -*- coding: utf-8 -*-
"""
Created on Wed Jul  5 09:45:52 2023

@author: Cameron.Mcdonald
"""

import os
import time
import sys

##from varname import nameof
from netmiko import ConnectHandler

## EN6808 PDU in SUT3 rack 1.
pdu_rack_1 = {
    'device_type': 'cisco_ios',
    'ip': '10.135.253.111',
    'username': 'admin',
    'password': 'WKKL0204',
    'port': 22,
}

## EN6808 PDU in SUT3 rack 2.
pdu_rack_2 = {
    'device_type': 'cisco_ios',
    'ip': '10.135.253.112',
    'username': 'admin',
    'password': 'WKKL0064',
    'port': 22,
}

## EN6325 PDU in SUT3 rack 5.
pdu_rack_5 = {
    'device_type': 'cisco_ios',
    'ip': '10.135.253.115',
    'username': 'admin',
    'password': 'WUCM0037',
    'port': 22,
}

## EN6810 PDU in SUT3 rack 6.
pdu_rack_6 = {
    'device_type': 'cisco_ios',
    'ip': '10.135.253.116',
    'username': 'admin',
    'password': 'W7AN0417',
    'port': 22,
}

## EN6810 PDU in SUT3 rack 7.
pdu_rack_7 = {
    'device_type': 'cisco_ios',
    'ip': '10.135.253.117',
    'username': 'admin',
    'password': 'W7AN0426',
    'port': 22,
}

## EN2317 PDU in the test equipment rack.
pdu_test_equip = {
    'device_type': 'cisco_ios',
    'ip': '10.135.151.50',
    'username': 'admin',
    'password': 'WQCN0596',
    'port': 22,
}

## SSH details for the WR Switch.
wr_z16 = {
    'device_type': 'cisco_ios',
    'ip': '10.135.253.167',
    'username': 'root',
    'password': 'z1P6gaqZ7G',
    'port': 22,
}

## SSH details for the WR Node.
wr_zen = {
    'device_type': 'cisco_ios',
    'ip': '10.135.253.168',
    'username': 'root',
    'password': 'p4rZ3bUgjT',
    'port': 22,
}

pdu_test_equip_sensors = ["AWXEM0125"]

log_folder = "./power_log_files"

time_zone_offset = 8 * 60 * 60
measurement_period = 300

initial_time_stamp = time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime(time.time() + time_zone_offset))

def write_to_log(device_name, log_text, print_log):
    
    log_file_name = initial_time_stamp + "_" + device_name + ".txt"
    
    if os.path.isdir(log_folder) != True:
        os.makedirs(log_folder)
    
    with open(os.path.join(log_folder, log_file_name), "a") as file:
        time_stamp = time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime(time.time() + time_zone_offset))
        
        file.write(time_stamp + ", " + log_text + "\n")

    if print_log is True:
        print(time_stamp + ": " + log_text)
        
def send_command_pdu(device_name, device, command):

    response = ""
    error_flag = False

    try:
        ## Connect to the device
        with ConnectHandler(**device) as net_connect:
            ## Send the command to the device.
            ##net_connect.enable()
            response = net_connect.send_command(command)
    except Exception as error:
        response = str(error)
        error_flag = True
        
    return response

def parse_pdu_response(response):
    
    dev_real_power = 0
    dev_app_power = 0
    
    ## Check the command returned successfully.
    if response.find("SUCCESS") != -1:
        response_lines = response.split("\n")
        
        for line in response_lines:
            if line.find("activepower") != -1:
                dev_real_power = float((line.split(":")[1]).split("W")[0])
            if line.find("apparentpower") != -1:
                dev_app_power = float((line.split(":")[1]).split("VA")[0])
        
    return dev_real_power, dev_app_power
        
def replace_spaces(text):

    ##previous_char = None
    ##comma_string = ""
    
    text = text.strip()
    comma_string = text.replace(chr(0x09),",")

    # for i in range(0, len(text)):
        # Add commas to separate.
        # if (text[i] == " ") and (previous_char != " "):
            # comma_string ++ (",")
        
        # Skip over repeated spaces.
        # if (text[i] == " ") and (previous_char == " "):
            # continue
        
        # Add any non-space characters to the string.
        # if text[i] != " ":
            # comma_string += (text[i])

    return comma_string

################################################################################
##################################### MAIN #####################################
################################################################################

real_power = 0
apparent_power = 0

#################### PDU 1 Ports ####################

##command = "pwr outlet 1 17"
##print(command)
##response = send_command_pdu("pdu_rack_1", pdu_rack_1, command)
##write_to_log("pdu_rack_1", response, True)

##command = "pwr outlet 1 18"
##print(command)
##response = send_command_pdu("pdu_rack_1", pdu_rack_1, command)
##write_to_log("pdu_rack_1", response, True)

command = "pwr outlet 1 24"
print(command)
response = send_command_pdu("pdu_rack_1", pdu_rack_1, command)
write_to_log("pdu_rack_1", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

#################### PDU 6 Ports ####################

command = "pwr outlet 1 1"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 2"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 3"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 7"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 8"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 15"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 16"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 17"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 18"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

##command = "pwr outlet 1 19"
##print(command)
##response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
##write_to_log("pdu_rack_6", response, True)

##command = "pwr outlet 1 20"
##print(command)
##response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
##write_to_log("pdu_rack_6", response, True)

command = "pwr outlet 1 21"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 22"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 23"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 24"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 27"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 28"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 29"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 30"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 31"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 32"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 33"
print(command)
response = send_command_pdu("pdu_rack_6", pdu_rack_6, command)
write_to_log("pdu_rack_6", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

#################### Rack 7 PDU ####################

command = "pwr outlet 1 3"
print(command)
response = send_command_pdu("pdu_rack_7", pdu_rack_7, command)
write_to_log("pdu_rack_7", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 11"
print(command)
response = send_command_pdu("pdu_rack_7", pdu_rack_7, command)
write_to_log("pdu_rack_7", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 12"
print(command)
response = send_command_pdu("pdu_rack_7", pdu_rack_7, command)
write_to_log("pdu_rack_7", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 15"
print(command)
response = send_command_pdu("pdu_rack_7", pdu_rack_7, command)
write_to_log("pdu_rack_7", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 16"
print(command)
response = send_command_pdu("pdu_rack_7", pdu_rack_7, command)
write_to_log("pdu_rack_7", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 17"
print(command)
response = send_command_pdu("pdu_rack_7", pdu_rack_7, command)
write_to_log("pdu_rack_7", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 18"
print(command)
response = send_command_pdu("pdu_rack_7", pdu_rack_7, command)
write_to_log("pdu_rack_7", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 23"
print(command)
response = send_command_pdu("pdu_rack_7", pdu_rack_7, command)
write_to_log("pdu_rack_7", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 24"
print(command)
response = send_command_pdu("pdu_rack_7", pdu_rack_7, command)
write_to_log("pdu_rack_7", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 27"
print(command)
response = send_command_pdu("pdu_rack_7", pdu_rack_7, command)
write_to_log("pdu_rack_7", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 28"
print(command)
response = send_command_pdu("pdu_rack_7", pdu_rack_7, command)
write_to_log("pdu_rack_7", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 33"
print(command)
response = send_command_pdu("pdu_rack_7", pdu_rack_7, command)
write_to_log("pdu_rack_7", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 34"
print(command)
response = send_command_pdu("pdu_rack_7", pdu_rack_7, command)
write_to_log("pdu_rack_7", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 35"
print(command)
response = send_command_pdu("pdu_rack_7", pdu_rack_7, command)
write_to_log("pdu_rack_7", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

command = "pwr outlet 1 36"
print(command)
response = send_command_pdu("pdu_rack_7", pdu_rack_7, command)
write_to_log("pdu_rack_7", response, True)

dev_real_power, dev_app_power = parse_pdu_response(response)
real_power += dev_real_power
apparent_power += dev_app_power

## White Rabbit Calls

## WR Z16

command = "gpa_ctrl hald pws/pwsl/power_in"
print(command)
response = send_command_pdu("wr_z16", wr_z16, command)
write_to_log("wr_z16", response, True)

## White 
real_power += float(response)
apparent_power += float(response)

command = "gpa_ctrl hald pws/pwsr/power_in"
print(command)
response = send_command_pdu("wr_z16", wr_z16, command)
write_to_log("wr_z16", response, True)

real_power += float(response)
apparent_power += float(response)

## WR ZEN TP-FL

command = "gpa_ctrl hald pws/pwsl/power_in"
print(command)
response = send_command_pdu("wr_zen", wr_zen, command)
write_to_log("wr_zen", response, True)

real_power += float(response)
apparent_power += float(response)

command = "gpa_ctrl hald pws/pwsr/power_in"
print(command)
response = send_command_pdu("wr_zen", wr_zen, command)
write_to_log("wr_zen", response, True)

real_power += float(response)
apparent_power += float(response)

print("Real Power: %s W" % real_power)
print("Apparent Power: %s VA" % apparent_power)

#################### PaSD System ####################

real_pasd_power = float(input("Enter manual power meter read: "))
pasd_power_factor = float(input("Enter manual power factor read: "))
app_pasd_power = real_pasd_power / pasd_power_factor

write_to_log("pasd", "%s, %s" % (real_pasd_power, app_pasd_power), True)

real_power += real_pasd_power
apparent_power += app_pasd_power

print("Real Power: %s W" % real_power)
print("Apparent Power: %s VA" % apparent_power)

write_to_log("total_power", "%s, %s" % (real_power, apparent_power), True)
sys.exit(0)