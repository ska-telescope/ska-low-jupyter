# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=attribute-defined-outside-init,too-many-instance-attributes,too-many-nested-blocks,too-many-locals,too-many-arguments
"""Class for analyasing visibility data."""

import json
import logging
import math
import traceback
from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np
import spead2
import spead2.recv
from matplotlib import cm

logging.basicConfig(level=logging.INFO)

COLOURS = "rgbcmykrgb"


class VisibilityAnalyser:
    """
    Implement a visibility analyser.

    This is used to check the results of the correlator
    """

    def __init__(self, configuration: str):
        """
        Initialiase the visibility analyser.

        :param configuration: the json configuration string, it contains
            the following for 2 stations and 2 channels:
            '{"coase_channels": [124, 125], "stations": [345, 350] }

        """
        self.configuration = json.loads(configuration)
        self.coarse_channels = self.configuration["coarse_channels"]
        self.stations = self.configuration["stations"]
        self.nb_station = len(self.stations)
        self.visu_type = np.dtype([("VIS", "<c8", (4,)), ("TCI", "|i1"), ("FD", "|u1")])
        self.visibilities = defaultdict(lambda: defaultdict(lambda: []))
        self.display_graph = False
        self.time_sequence = set()

    def set_display_graph(self, display: bool = True):
        """
        Display figure instead of saving if display = True.

        :param display: change the display_graph variable

        """
        self.display_graph = display

    def extract_spead_data(self, pcap_file: str, verbose: bool = False):
        """
        Extract visibility from the spead packets.

        :param pcap_file: the file containing packet following pcap format
        :paran verbose: boolean to have more information about extraction

        """
        self.n_baselines = -999
        spead_items_seen = set()
        first_heap_with_data = None
        heap_number = 0
        # Initialise spead2 library

        stream = spead2.recv.Stream(spead2.ThreadPool())

        # Reading from all ports so we don't miss anything
        stream.add_udp_pcap_file_reader(pcap_file, filter="")
        item_group = spead2.ItemGroup()
        for heap_number, heap in enumerate(stream):
            items = item_group.update(heap)
            if verbose:
                # General information about heap
                print(
                    f"Heap {heap.cnt:#X} ("
                    f"SPEAD-{heap.flavour.item_pointer_bits}"
                    f"-{heap.flavour.heap_address_bits} "
                    f"v{heap.flavour.version})",
                    end="",
                )
            # don't process start/end packets any further
            if heap.is_start_of_stream():
                for key, item in items.items():
                    if key == "Basel":
                        self.n_baselines = item.value
                continue

            if first_heap_with_data is None:
                first_heap_with_data = (
                    heap_number,
                    heap.cnt,
                )  # our enumeration count, heap ID

            # Extract correlation data
            for key, item in items.items():
                if heap_number == first_heap_with_data[0] and key == "Basel":
                    self.n_baselines = item.value
                if heap_number == first_heap_with_data[0]:
                    spead_items_seen.add(key)

                # this loop is silly, but this doesn't work:
                # x = items["Corre"]  # why doesn't this work?!
                if key == "Corre":
                    # print(f"Found a correlation for heap {heap.cnt:#x}")
                    visibility_baseline_data = item.value
                    # print(f"CORRELATION VALUE: {item.value}")
                elif key == "tOffs":
                    time = item.value
            # heap_number_byte = heap.cnt.to_bytes(6, byteorder="big")
            # byte_array_heap_number_byte = bytearray(heap_number_byte)

            # chan = int.from_bytes(byte_array_heap_number_byte[2:4], "big")
            chan = int.from_bytes(
                bytearray(heap.cnt.to_bytes(6, byteorder="big"))[2:4], "big"
            )

            try:
                self.visibilities[chan][time] = visibility_baseline_data
                # print(f"channel {chan} time {t} {visibility_baseline_data}")
                self.time_sequence.add(time)
                # dirty way to stop old data carrying over (FIXME)
                del chan
                del time
                del visibility_baseline_data
            except NameError:
                traceback.print_exc()
                print("Something went wrong in heap number {heap_number}")

        stream.stop()
        print()
        print(f"Received {heap_number + 1} heaps")
        print(f"\t{self.n_baselines} baselines")
        print("\tSPEAD item names:", " ".join(spead_items_seen))

    def populate_correlation_matrix(self):
        """
        Populate the correlation matrix.

        This matrix is in fact a dictionary on the form
        corr_abs_matrixes_time = {
        time1: {
        channel1: [2 * self.nb_station, 2 * self.nb_station]
        correlation matrix of sum of norm of visibilities in channel1
        channel2: [2 * self.nb_station, 2 * self.nb_station]
        correlation matrix of sum of norm of visibilities in channel2
        },
        time2: {
        channel1: [2 * self.nb_station, 2 * self.nb_station]
        correlation matrix of sum of norm of visibilities in channel1
        channel2: [2 * self.nb_station, 2 * self.nb_station]
        correlation matrix of sum of norm of visibilities in channel2
        }
        }

        """
        self.corr_abs_matrixes_time = {}
        self.corr_abs_matrixes_time_fine_channels = {}
        self.corr_phase_matrixes_time = {}
        for time in self.time_sequence:
            corr_matrixes_coarse = {}
            corr_matrixes_fine = {}
            corr_matrixes_angle = {}
            for coarse_channel in self.coarse_channels:
                corr_matrix = np.zeros([2 * self.nb_station, 2 * self.nb_station])

                for vis_channel in range(144):
                    index = 0
                    corr_matrix_fine = np.zeros(
                        [2 * self.nb_station, 2 * self.nb_station]
                    )
                    corr_matrix_phase = np.zeros(
                        [2 * self.nb_station, 2 * self.nb_station]
                    )
                    for i in range(0, self.nb_station):
                        for j in range(0, i + 1):
                            polarisation = 0
                            for polar_x in range(2):
                                for polar_y in range(2):
                                    corr_matrix[2 * i + polar_x][
                                        2 * j + polar_y
                                    ] = corr_matrix[2 * i + polar_x][
                                        2 * j + polar_y
                                    ] + np.abs(
                                        self.visibilities[
                                            coarse_channel * 144 + vis_channel
                                        ][time]["VIS"][index][polarisation]
                                    )
                                    corr_matrix_fine[2 * i + polar_x][
                                        2 * j + polar_y
                                    ] = corr_matrix[2 * i + polar_x][
                                        2 * j + polar_y
                                    ] + np.abs(
                                        self.visibilities[
                                            coarse_channel * 144 + vis_channel
                                        ][time]["VIS"][index][polarisation]
                                    )
                                    corr_matrix_phase[2 * i + polar_x][
                                        2 * j + polar_y
                                    ] = corr_matrix_phase[2 * i + polar_x][
                                        2 * j + polar_y
                                    ] + np.angle(
                                        self.visibilities[
                                            coarse_channel * 144 + vis_channel
                                        ][time]["VIS"][index][polarisation],
                                        deg=True,
                                    )
                                    polarisation = polarisation + 1
                            index = index + 1
                    corr_matrixes_fine[
                        coarse_channel * 144 + vis_channel
                    ] = corr_matrix_fine
                    corr_matrixes_angle[
                        coarse_channel * 144 + vis_channel
                    ] = corr_matrix_phase
                corr_matrixes_coarse[coarse_channel] = corr_matrix
            self.corr_abs_matrixes_time[time] = corr_matrixes_coarse
            self.corr_abs_matrixes_time_fine_channels[time] = corr_matrixes_fine
            self.corr_phase_matrixes_time[time] = corr_matrixes_angle

    def save_phase_heatmap_to_disk(self, mode, case=""):
        """Save the phase heatmap to disk."""
        if mode == "average_all":
            corr_matrix = np.zeros([2 * self.nb_station, 2 * self.nb_station])
            number_of_phase = 0
            for _, vis in self.corr_phase_matrixes_time.items():
                for matrix in vis.values():
                    corr_matrix = corr_matrix + matrix
                    number_of_phase = number_of_phase + 1
            corr_matrix = corr_matrix / number_of_phase
            plt.clf()
            plt.imshow(
                corr_matrix,
                extent=[
                    0.5,
                    self.nb_station + 0.5,
                    self.nb_station + 0.5,
                    0.5,
                ],
            )
            plt.colorbar()
            plt.title("Phase HeapMap matrix")
            if self.display_graph:
                plt.show()
            else:
                plt.savefig(f"{case}phase_heat_map.png")

    def save_heatmap_to_disk(self, mode, frequency_to_check=None, case=""):
        """
        Save in PNG the heat maps of correlation.

        :param mode: 3 different mode so far:
            average_all: heat map of the sum of correlation across time
            for all coarse channels
            average_frequencies: produces 1 figure per integration time
            across all frequencies
            average_times: produces 1 figure per channel across all
            integration times
            average_times_fine: produces 1 figure per visibility channel
            for frequency_to_check coarse channel
        :param frequency_to_check: the coarse channel to further analyse
            when using the average_times_fine mode
        :param case: case study for the various figures

        """
        # pylint: disable=R0912
        if mode == "average_all":
            corr_matrix = np.zeros([2 * self.nb_station, 2 * self.nb_station])
            for time, vis in self.corr_abs_matrixes_time.items():
                for matrix in vis.values():
                    corr_matrix = corr_matrix + matrix
            plt.clf()
            plt.imshow(
                corr_matrix,
                extent=[
                    0.5,
                    self.nb_station + 0.5,
                    self.nb_station + 0.5,
                    0.5,
                ],
            )
            plt.colorbar()
            plt.title("Correlation matrix (no delays applied)")
            if self.display_graph:
                plt.show()
            else:
                plt.savefig(f"{case}heat_map.png")
        elif mode == "average_frequencies":
            for time, vis in self.corr_abs_matrixes_time.items():
                corr_matrix = np.zeros([2 * self.nb_station, 2 * self.nb_station])
                for matrix in vis.values():
                    corr_matrix = corr_matrix + matrix
                plt.clf()
                plt.imshow(
                    corr_matrix,
                    extent=[
                        0.5,
                        self.nb_station + 0.5,
                        self.nb_station + 0.5,
                        0.5,
                    ],
                )
                plt.colorbar()
                plt.title(f"Correlation matrix at time {time}")
                if self.display_graph:
                    plt.show()
                else:
                    plt.savefig(f"{case}heat_map_time_{time}.png")
        elif mode == "average_times":
            for freq in self.coarse_channels:
                corr_matrix = np.zeros([2 * self.nb_station, 2 * self.nb_station])
                for time, vis in self.corr_abs_matrixes_time.items():
                    corr_matrix = corr_matrix + vis[freq]
                plt.clf()
                plt.imshow(
                    corr_matrix,
                    extent=[
                        0.5,
                        self.nb_station + 0.5,
                        self.nb_station + 0.5,
                        0.5,
                    ],
                )
                plt.colorbar()
                plt.title(f"Correlation matrix for frequency {freq}")
                if self.display_graph:
                    plt.show()
                else:
                    plt.savefig(f"{case}heat_map_freq{freq}.png")
        elif mode == "average_times_fine":
            for freq in range(frequency_to_check * 144, frequency_to_check * 144 + 144):
                corr_matrix = np.zeros([2 * self.nb_station, 2 * self.nb_station])
                for (
                    time,
                    vis,
                ) in self.corr_abs_matrixes_time_fine_channels.items():
                    corr_matrix = corr_matrix + vis[freq]
                plt.clf()
                plt.imshow(
                    corr_matrix,
                    extent=[
                        0.5,
                        self.nb_station + 0.5,
                        self.nb_station + 0.5,
                        0.5,
                    ],
                )
                plt.colorbar()
                plt.title(f"Correlation matrix for fine frequency {freq - 72}")
                if self.display_graph:
                    plt.show()
                else:
                    plt.savefig(f"{case}heat_map_fine_freq{freq - 72}.png")

    def angle_analysis(
        self,
        pair_of_station: (int, int),
        polarisation: str,
        nb_time_period: int,
        coarse_channels,
        starting_period: int = 0,
    ):
        """
        Generate the angle analysis figure.

        :param pair_of_station: the pair of stations to generate
            angle against
        :param polarisation: the polarisation of interest, 4 are
            possible XX, XY, YX, YY
        :param nb_time_period: the number of integration we want
            to analyse
        :param starting_period: the staring period for analysis
        :param coarse_channels: the list of coarse channels to plot

        """

        if polarisation not in ["XX", "XY", "YX", "YY"]:
            print("Polarisations should be on the form XX, XY, YX, YY")
            return
        if (
            pair_of_station[0] not in self.stations
            or pair_of_station[1] not in self.stations
        ):
            print("Stations unknown")
            return
        index_to_find = 0
        index = 0
        for i in range(self.nb_station):
            for j in range(0, i + 1):
                if self.stations.index(pair_of_station[0]) > self.stations.index(
                    pair_of_station[1]
                ):
                    if (
                        self.stations.index(pair_of_station[0]) == i
                        and self.stations.index(pair_of_station[1]) == j
                    ):
                        index_to_find = index_to_find + index
                else:
                    if (
                        self.stations.index(pair_of_station[1]) == i
                        and self.stations.index(pair_of_station[0]) == j
                    ):
                        index_to_find = index_to_find + index
                index = index + 1
        for time in range(starting_period, starting_period + nb_time_period):
            plt.clf()
            for coarse_channel in coarse_channels:
                for vis_channel in range(144):
                    plt.plot(
                        (coarse_channel * 144 + vis_channel - 72) / 144,
                        np.angle(
                            self.visibilities[coarse_channel * 144 + vis_channel][
                                int(list(self.time_sequence)[time])
                            ]["VIS"][index_to_find][
                                ["XX", "XY", "YX", "YY"].index(polarisation)
                            ],
                            deg=True,
                        ),
                        COLOURS[time % 10] + ".-",
                    )
            plt.xlabel("station channel")
            plt.ylabel("phase in degrees")
            plt.title(f"{pair_of_station[0]}x{pair_of_station[1]}_{polarisation}")
            plt.legend()
            if self.display_graph:
                plt.show()
            else:
                plt.savefig(
                    f"{pair_of_station[0]}x{pair_of_station[1]}_"
                    + f"{polarisation}_angle_time_{time}.png"
                )

    def amplitude_time_analysis(
        self,
        polarisation: str,
        nb_time_period: int,
        coarse_channels,
        sps_correction: bool,
    ):
        """
        Generate the amplitude vs time analysis figure.

        :param polarisation: the polarisation of interest, 4 are
            possible XX, XY, YX, YY
        :param nb_time_period: the number of integration we want
            to analyse
        :param coarse_channels: the list of coarse channels to plot

        """

        with open("fbscale_linear.txt", "r", encoding="utf8") as coefficient_file:
            correction_factor = coefficient_file.readlines()
        if not sps_correction:
            correction_factor = [1] * 144

        if polarisation not in ["XX", "XY", "YX", "YY"]:
            print("Polarisations should be on the form XX, XY, YX, YY")
            return
        list_of_correlation_name = []
        list_of_correlation = []
        for i in range(self.nb_station):
            for j in range(0, i + 1):
                list_of_correlation_name.append(f"{i + 1}x{j + 1}")
                list_of_correlation.append([])
        plt.clf()
        for time in range(nb_time_period):
            for i in range(int((self.nb_station * (self.nb_station + 1)) / 2)):
                absolute_value = 0
                for coarse_channel in coarse_channels:
                    for vis_channel in range(144):
                        absolute_value = absolute_value + float(
                            correction_factor[vis_channel]
                        ) * np.abs(
                            self.visibilities[coarse_channel * 144 + vis_channel][
                                int(sorted(list(self.time_sequence))[time])
                            ]["VIS"][i][["XX", "XY", "YX", "YY"].index(polarisation)]
                        )

                list_of_correlation[i].append(
                    absolute_value / (len(coarse_channels) * 144)
                )
        for i in range(int((self.nb_station * (self.nb_station + 1)) / 2)):
            plt.plot(
                range(nb_time_period),
                list_of_correlation[i],
                COLOURS[i % 10] + "-",
                label=list_of_correlation_name[i],
            )
        plt.xlabel("integration number")
        plt.ylabel("absolute value")
        plt.title(f"amplitude_vs_time_polarisation_{polarisation}")
        plt.legend(loc=2, prop={"size": 6})
        if self.display_graph:
            plt.show()
        else:
            plt.savefig(
                f"amplitude_vs_time_polarisation_{polarisation}.svg",
                format="svg",
                dpi=300,
            )

    def amplitude_freq_analysis(
        self,
        polarisation: str,
        nb_time_period: int,
        coarse_channels,
        correction_sps: bool,
    ):
        """
        Generate the amplitude vs freq analysis figure.

        :param polarisation: the polarisation of interest, 4 are
            possible XX, XY, YX, YY
        :param nb_time_period: the number of integration we want
            to analyse
        :param coarse_channels: the list of coarse channels to plot
        :param correction_sps: a boolean if we apply correction
        :return: the raw data of the figures

        """

        with open("fbscale_linear.txt", "r", encoding="utf8") as coefficient_file:
            correction_factor = coefficient_file.readlines()
        if not correction_sps:
            correction_factor = [1] * 144

        print(correction_factor)
        if polarisation not in ["XX", "XY", "YX", "YY"]:
            print("Polarisations should be on the form XX, XY, YX, YY")
            return correction_factor
        list_of_correlation_name = []
        list_of_correlation = []
        for i in range(self.nb_station):
            for j in range(0, i + 1):
                list_of_correlation_name.append(f"{i + 1}x{j + 1}")
                list_of_correlation.append([])
        plt.clf()
        for coarse_channel in coarse_channels:
            for vis_channel in range(144):
                for i in range(int((self.nb_station * (self.nb_station + 1)) / 2)):
                    absolute_value = 0
                    for time in range(nb_time_period):
                        absolute_value = absolute_value + float(
                            correction_factor[vis_channel]
                        ) * np.abs(
                            self.visibilities[coarse_channel * 144 + vis_channel][
                                int(sorted(list(self.time_sequence))[time])
                            ]["VIS"][i][["XX", "XY", "YX", "YY"].index(polarisation)]
                        )

                    list_of_correlation[i].append(absolute_value / nb_time_period)
        for i in range(int((self.nb_station * (self.nb_station + 1)) / 2)):
            plt.plot(
                range(
                    coarse_channels[0] * 144 - 72,
                    coarse_channels[0] * 144 + len(coarse_channels) * 144 - 72,
                ),
                list_of_correlation[i],
                COLOURS[i % 10] + "-",
                label=list_of_correlation_name[i],
            )
        plt.xlabel("visibility channel number")
        plt.ylabel("absolute value")
        plt.title(f"amplitude_vs_freq_polarisation_{polarisation}")
        plt.legend(loc=2, prop={"size": 6})
        if self.display_graph:
            plt.show()
        else:
            plt.savefig(
                f"amplitude_vs_freq_polarisation_{polarisation}.svg",
                format="svg",
                dpi=300,
            )
        return list_of_correlation

    def auto_station_analysis(
        self,
        station: int,
        nb_time_period: int,
        polarisation: str,
        coarse_channels,
    ):
        """
        Generate the 3D auto amplitude-freq-time analysis figure for a station.

        :param station: the station number.
        :param polarisation: the polarisation of interest, 4 are
            possible XX, XY, YX, YY
        :param nb_time_period: the number of integration we want
            to analyse
        :param coarse_channels: the list of coarse channels to plot

        """

        if polarisation not in ["XX", "XY", "YX", "YY"]:
            print("Polarisations should be on the form XX, XY, YX, YY")
            return

        real_index = self.get_index_in_low_triangle((station - 1, station - 1))
        amplitudes = np.zeros(
            (nb_time_period, len(coarse_channels) * 144), dtype=np.int32
        )

        time_scale = np.linspace(
            coarse_channels[0] * 144 - 72,
            coarse_channels[0] * 144 + 144 * len(coarse_channels) - 72,
            len(coarse_channels) * 144,
        )
        freq_scale = np.linspace(1, nb_time_period, nb_time_period)
        plt.clf()
        for coarse_channel in coarse_channels:
            for vis_channel in range(144):
                for time in range(nb_time_period):
                    amplitudes[time][
                        coarse_channels.index(coarse_channel) * 144 + vis_channel
                    ] = np.abs(
                        self.visibilities[coarse_channel * 144 + vis_channel][
                            int(sorted(list(self.time_sequence))[time])
                        ]["VIS"][real_index][
                            ["XX", "XY", "YX", "YY"].index(polarisation)
                        ]
                    )

        time_scale, freq_scale = np.meshgrid(time_scale, freq_scale)

        # Set up plot
        fig, axis = plt.subplots(subplot_kw={"projection": "3d"})

        surface = axis.plot_surface(
            time_scale,
            freq_scale,
            amplitudes,
            rstride=1,
            cstride=1,
            cmap=cm.get_cmap("Spectral"),
            linewidth=0,
            antialiased=False,
            shade=False,
        )
        plt.xlabel("visibility channels")
        plt.ylabel("integration period")
        plt.title(f"3D_mesh_station_{station}_{polarisation}")
        fig.colorbar(surface)
        if self.display_graph:
            plt.show()
        else:
            plt.savefig(
                f"3D_mesh_station_{station}_{polarisation}.svg", format="svg", dpi=300
            )

    def rms_total(self):
        """
        Calculate RMS Total.

        For the entire dataset calculate its RMS - square, sum, average
        and take square root. ONE RMS number comes out.
        Effectively the average cross correlation power over all outputs
        :return: a single RMS value for each of auto/cross (auto, cross)

        """
        rms_total_values = [0, 0]
        complex_cross_total = np.complex256()
        number_of_samples = [0, 0]
        complex_auto_total = np.complex256()
        for time in self.time_sequence:
            for coarse_channel in self.coarse_channels:
                for vis_channel in range(144):
                    index = 0
                    for i in range(0, self.nb_station):
                        for j in range(0, i + 1):
                            if i != j:
                                for polarisation in range(4):
                                    rms_total_values[0] = (
                                        rms_total_values[0]
                                        + np.abs(
                                            self.visibilities[
                                                coarse_channel * 144 + vis_channel
                                            ][time]["VIS"][index][polarisation]
                                        )
                                        ** 2
                                    )
                                    complex_cross_total = (
                                        complex_cross_total
                                        + self.visibilities[
                                            coarse_channel * 144 + vis_channel
                                        ][time]["VIS"][index][polarisation]
                                    )
                                    number_of_samples[0] = number_of_samples[0] + 1
                            else:
                                polarisation = 0
                                for polar_x in range(2):
                                    for polar_y in range(2):
                                        if polar_x == polar_y:
                                            rms_total_values[1] = (
                                                rms_total_values[1]
                                                + np.abs(
                                                    self.visibilities[
                                                        coarse_channel * 144
                                                        + vis_channel
                                                    ][time]["VIS"][index][polarisation]
                                                )
                                                ** 2
                                            )
                                            complex_auto_total = (
                                                complex_auto_total
                                                + self.visibilities[
                                                    coarse_channel * 144 + vis_channel
                                                ][time]["VIS"][index][polarisation]
                                            )
                                            number_of_samples[1] = number_of_samples[1]
                                        polarisation = polarisation + 1
                            index = index + 1

        print(
            f"RMS_Tot Cross {rms_total_values[0]}, Nb_samples Cross {number_of_samples}"
        )
        rms_totals = (
            math.sqrt(rms_total_values[0] / number_of_samples[0]),
            math.sqrt(rms_total_values[1] / number_of_samples[1]),
        )
        print(f"RMS_Tot Auto {rms_total_values[1]}, Nb_samples {number_of_samples[1]}")

        print(f"Ratio Auto/Cross {rms_total_values[1] / rms_total_values[0]}")
        print(f"Complex Cross Total {complex_cross_total / number_of_samples[0]}")
        print(f"Complex Auto Total {complex_auto_total / number_of_samples[1]}")
        return (rms_totals[0], rms_totals[1])

    # flake8: noqa: C901
    def rms_slice_total_preparation(self, slice_size: int):
        """
        Calculater Slice of RMS.

        For the entire dataset calculate two giant vectors sum_1 and rms1.
        From these two vectors we can generate various figures including
        the main figure of PTC 14.

        :param slice_size: the number of cross correlation baseline to
            group together slice = nb_baseline/sum_1 vector (power of 2)
        :return: (rms, sum) the two vectors of RMS and SUM

        """
        number_of_samples = 0
        sum_1 = np.zeros(64 * 1024, dtype=np.complex256)
        rms1 = np.zeros(64 * 1024, dtype=np.int64)
        for time in self.time_sequence:
            for coarse_channel in self.coarse_channels:
                for vis_channel in range(144):
                    index = 0
                    for i in range(0, self.nb_station):
                        for j in range(0, i + 1):
                            if i != j:
                                for polarisation in range(4):
                                    rms1[math.floor(number_of_samples / slice_size)] = (
                                        rms1[math.floor(number_of_samples / slice_size)]
                                        + np.abs(
                                            self.visibilities[
                                                coarse_channel * 144 + vis_channel
                                            ][time]["VIS"][index][polarisation]
                                        )
                                        ** 2
                                    )
                                    sum_1[
                                        math.floor(number_of_samples / slice_size)
                                    ] = (
                                        sum_1[
                                            math.floor(number_of_samples / slice_size)
                                        ]
                                        + self.visibilities[
                                            coarse_channel * 144 + vis_channel
                                        ][time]["VIS"][index][polarisation]
                                    )
                                    polarisation = polarisation + 1
                                    number_of_samples = number_of_samples + 1
                            index = index + 1
        return (rms1, sum_1)

    def phase_and_amplitude_closure(
        self, stations_closure, coarse_channels, polarisation, integration_period
    ):
        """
        Calculate the phase and amplitude closure.

        This is following documentation from
        https://www.atnf.csiro.au/computing/software/miriad/doc/closure.html
        We then need a list of at least 4 stations to perform as follows:
        * Closure (triple) phase:    arg( V12*V23*conjg(V13) )
        * Closure (quad) amplitude:  abs( (V12*V34)/(V14*conjg(V34)) )
        * Triple amplitude:          abs( V12*V23*conjg(V13) )**0.3333
        * Quad phase:                arg( (V12*V34)/(V14*conjg(V34)) )
        The closure phase, quad phase and closure amplitude should be
        independent of antenna-based errors, and for a point source
        should have values of zero phase or unit amplitude.  The triple
        amplitude is independent of antenna-based phase errors (but not
        amplitude errors), and for a point source is a measure of the
        flux density.

        The task works by averaging the quantites that are the argument
        of the abs or arg function.  These are always averaged over the
        selected frequency channels and over the parallel-hand
        polarizations. Optionally the averaging can also be done over
        time and over the different closure paths.

        CLOSURE also prints (and optionally plots) the theoretical error
        (as a result of thermal noise) of the quantities.  Note this
        assumes a point source model, and that the signal-to-noise ratio
        is appreciable (at least 10) in each averaged product.

        :param stations: the list of station to goes through in circle
        :param coarse_channels: the list of coarse channels to plot
        :return: (Closure (triple) phase, Closure (quad) amplitude)

        """
        # pair_index = (index_12, index_23, index_13, index_34, index_14)
        pair_index = [
            self.get_index_in_low_triangle((stations_closure[0], stations_closure[1])),
            self.get_index_in_low_triangle((stations_closure[1], stations_closure[2])),
            self.get_index_in_low_triangle((stations_closure[0], stations_closure[2])),
            self.get_index_in_low_triangle((stations_closure[2], stations_closure[3])),
            self.get_index_in_low_triangle((stations_closure[0], stations_closure[3])),
        ]
        # print(pair_index)
        closure_phase = []
        closure_amplitude = []
        # triple_amplitude = []
        # quad_phase = []
        for coarse_channel in coarse_channels:
            for vis_channel in range(144):
                closure_phase.append(
                    self.calculate_angle_closure(
                        [pair_index[0], pair_index[1], pair_index[2]],
                        coarse_channel * 144 + vis_channel,
                        polarisation,
                        integration_period,
                    )
                )
                closure_amplitude.append(
                    self.calculate_amplitude_closure(
                        [pair_index[0], pair_index[3], pair_index[4]],
                        coarse_channel * 144 + vis_channel,
                        polarisation,
                        integration_period,
                    )
                )
                # triple_amplitude.append()
                # quad_phase.append()

        # re_ordered_stations = sorted(stations_closure)
        # matched_stations = [
        #     self.stations.index(stn) for stn in re_ordered_stations
        # ]
        # indexes = {}
        return (closure_phase, closure_amplitude)

    def get_index_in_low_triangle(self, station_pair):
        """
        Return the index from the list of visibilities.

        :param station_pair: (station_i, station_j)
        :return: the correct index

        """
        (station_j, station_i) = tuple(sorted(station_pair))

        index = 0
        for i in range(0, self.nb_station):
            for j in range(0, i + 1):
                if i == station_i and j == station_j:
                    return index
                index = index + 1
        return index

    def calculate_angle_closure(
        self, indexes, vis_freq, polarisation, integration_period
    ):
        """
        Return the index from the list of visibilities.

        :param station_pair: (station_i, station_j)
        :return: the correct index

        """
        voltage_12 = self.visibilities[vis_freq][
            int(sorted(list(self.time_sequence))[integration_period])
        ]["VIS"][indexes[0]][["XX", "XY", "YX", "YY"].index(polarisation)]
        voltage_13 = self.visibilities[vis_freq][
            int(sorted(list(self.time_sequence))[integration_period])
        ]["VIS"][indexes[1]][["XX", "XY", "YX", "YY"].index(polarisation)]
        voltage_23 = self.visibilities[vis_freq][
            int(sorted(list(self.time_sequence))[integration_period])
        ]["VIS"][indexes[2]][["XX", "XY", "YX", "YY"].index(polarisation)]
        return (
            np.angle(
                voltage_12,
                deg=True,
            )
            + np.angle(
                voltage_23,
                deg=True,
            )
            - np.angle(
                voltage_13,
                deg=True,
            )
        )

    def calculate_amplitude_closure(
        self, indexes, vis_freq, polarisation, integration_period
    ):
        """
        Return the index from the list of visibilities.

        Closure (quad) amplitude:  abs( (V12*V34)/(V14*conjg(V34)) )
        :param station_pair: (station_i, station_j)
        :return: the correct index

        """
        voltage_12 = self.visibilities[vis_freq][
            int(sorted(list(self.time_sequence))[integration_period])
        ]["VIS"][indexes[0]][["XX", "XY", "YX", "YY"].index(polarisation)]
        voltage_34 = self.visibilities[vis_freq][
            int(sorted(list(self.time_sequence))[integration_period])
        ]["VIS"][indexes[1]][["XX", "XY", "YX", "YY"].index(polarisation)]
        voltage_14 = self.visibilities[vis_freq][
            int(sorted(list(self.time_sequence))[integration_period])
        ]["VIS"][indexes[2]][["XX", "XY", "YX", "YY"].index(polarisation)]

        return np.abs((voltage_12 * voltage_34) / (voltage_14 * np.conj(voltage_34)))

    def save_object(self, filename):
        """
        Save the Visibility Analyser.

        :param filename: name of the filename to save
        :return:

        """
